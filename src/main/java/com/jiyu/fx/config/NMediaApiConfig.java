package com.jiyu.fx.config;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/18 18:24
 */
@Accessors(chain = true)
@Data
@Configuration
@ConfigurationProperties(value = "xmt.api")
public class NMediaApiConfig {
    /**
     * 接口地址
     */
    private String domain;
    /**
     * 授权KEY
     */
    private String accessKey;
    /**
     * 授权私钥
     */
    private String accessSecret;
}
