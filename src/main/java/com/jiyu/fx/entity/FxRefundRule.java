package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_refund_rule")
@Data
@Accessors(chain = true)
public class FxRefundRule implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID(供应商)
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 名称
     */
    private String name;

    /**
     * 是否无损退
     */
    private Boolean free;

    /**
     * 是否可退
     */
    private Boolean refund;

    /**
     * 产品类型
     */
    @Column(name = "product_type")
    private Integer productType;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 规则详情
     */
    private String rule;

    private static final long serialVersionUID = 1L;
}