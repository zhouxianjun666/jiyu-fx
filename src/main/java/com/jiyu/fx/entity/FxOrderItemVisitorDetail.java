package com.jiyu.fx.entity;

import com.jiyu.common.dto.TwoGroup;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Table(name = "t_fx_order_item_visitor_detail")
@Data
@Accessors(chain = true)
public class FxOrderItemVisitorDetail implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 子订单ID
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 游客ID
     */
    @Column(name = "visitor_id")
    private Integer visitorId;

    /**
     * 订单天详情ID
     */
    @Column(name = "detail_id")
    private Integer detailId;

    /**
     * 天
     */
    private Date day;

    /**
     * 票数/天
     */
    private Integer quantity;

    /**
     * 已用/天
     */
    @Column(name = "used_quantity")
    private Integer usedQuantity;

    /**
     * 已退/天
     */
    @Column(name = "refund_quantity")
    private Integer refundQuantity;

    private static final long serialVersionUID = 1L;

    public TwoGroup<Integer, Integer> unique() {
        return new TwoGroup<>(visitorId, detailId);
    }

    public TwoGroup<Integer, Date> uniqueDate() {
        return new TwoGroup<>(visitorId, day);
    }

    public int getRemainQuantity() {
        return Optional.ofNullable(quantity).orElse(0)
                - Optional.ofNullable(usedQuantity).orElse(0)
                - Optional.ofNullable(refundQuantity).orElse(0);
    }
}
