package com.jiyu.fx.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_fx_order_item_consume")
@Data
@Accessors(chain = true)
public class FxOrderItemConsume implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 子订单ID
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 凭证&票务配置ID
     */
    @Column(name = "ma_config_id")
    private Integer maConfigId;

    /**
     * 流水号
     */
    @Column(name = "serial_no")
    private String serialNo;

    /**
     * 核销时间
     */
    @Column(name = "consume_time")
    private Date consumeTime;

    private static final long serialVersionUID = 1L;
}
