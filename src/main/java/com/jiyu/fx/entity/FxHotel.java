package com.jiyu.fx.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_fx_hotel")
@Data
@Accessors(chain = true)
public class FxHotel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 主产品id
     */
    @Column(name = "product_id")
    private Integer productId;

    /**
     * 酒店级别
     */
    @Column(name = "level_type")
    private Integer levelType;

    /**
     * 团队起订数
     */
    @Column(name = "team_num")
    private Integer teamNum;

    /**
     * 入住时间
     */
    @Column(name = "in_time")
    private String inTime;

    /**
     * 离店时间
     */
    @Column(name = "out_time")
    private String outTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 装修时间
     */
    @Column(name = "decorate_time")
    private Date decorateTime;

    /**
     * 开业时间
     */
    @Column(name = "open_time")
    private Date openTime;

    /**
     * '支付方式5960微信5961支付宝5962境内银联5963现金5964境外银行卡',
     */
    @Column(name = "canpay_type")
    private String canpayType;

    /**
     * 是否可带宠物
     */
    @Column(name = "pet_type")
    private Byte petType;

    /**
     * '5950Master5951Visa5952国内银联卡5953信用卡'
     */
    @Column(name = "card_type")
    private String cardType;

    /**
     * 网络状况5933免费WIF5934免费宽带5935收费宽带5936无网络
     */
    @Column(name = "net_type")
    private String netType;

    /**
     * 酒店标签 5900	顶级奢华酒店5901	商务型酒店5902	连锁品牌5903	假日酒店5904	公寓酒店5905	快捷酒店5906	经济型酒店5907	假日酒店5908	公寓酒店5909	快捷酒店5910	经济型酒店
     */
    @Column(name = "label_type")
    private String labelType;

    /**
     * 停车场5940免费停车场5941收费停车场5942无停车场
     */
    @Column(name = "park_type")
    private Integer parkType;

    /**
     * 酒店设施5920	西式餐厅5921	中式餐厅5922	室外游泳池5923	室内游泳池5924	残疾人设施5925	温泉5926	会议室5927	健身房5928	SPA5929	棋牌室5930	酒吧5931	商务中心
     */
    @Column(name = "facility_type")
    private String facilityType;

    /**
     * 服务5911	接机服务5912	接站服务5913	接待外宾5914	行李寄存5915	洗衣5916	看护小孩5917	租车5918	叫醒服务
     */
    @Column(name = "server_type")
    private String serverType;

    /**
     * 排序号？
     */
    @Column(name = "sort_num")
    private Integer sortNum;

    private static final long serialVersionUID = 1L;
}