package com.jiyu.fx.entity.shop;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_shop")
@Data
@Accessors(chain = true)
public class Shop implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 名称
     */
    private String name;

    /**
     * 微信应用ID
     */
    @Column(name = "app_id")
    private String appId;

    /**
     * 微信应用私钥
     */
    @Column(name = "app_secret")
    private String appSecret;

    /**
     * 微信公众号二维码地址
     */
    @Column(name = "wx_qr")
    private String wxQr;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}