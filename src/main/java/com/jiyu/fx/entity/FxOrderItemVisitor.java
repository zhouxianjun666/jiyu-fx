package com.jiyu.fx.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_fx_order_item_visitor")
@Data
@Accessors(chain = true)
public class FxOrderItemVisitor implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 子订单ID
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 真实姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 证件类型: 1-身份证;2-其它;
     */
    @Column(name = "card_type")
    private Integer cardType;

    /**
     * 证件号
     */
    @Column(name = "card_no")
    private String cardNo;

    /**
     * 性别:0-未知;1-男;2-女;
     */
    private Integer gender;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    @Transient
    private Integer tempQuantity;

    private static final long serialVersionUID = 1L;
}
