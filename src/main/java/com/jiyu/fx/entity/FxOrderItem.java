package com.jiyu.fx.entity;

import cn.hutool.core.date.DateTime;
import com.jiyu.common.entity.base.Member;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Table(name = "t_fx_order_item")
@Data
@Accessors(chain = true)
public class FxOrderItem implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 子订单编号
     */
    private Long number;

    /**
     * 主订单号
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 主订单编号
     */
    @Column(name = "order_number")
    private Long orderNumber;

    /**
     * 主产品编号
     */
    @Column(name = "product_number")
    private Long productNumber;

    /**
     * 主产品名称
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 子产品编号
     */
    @Column(name = "product_sub_number")
    private Long productSubNumber;

    /**
     * 子产品名称
     */
    @Column(name = "product_sub_name")
    private String productSubName;

    /**
     * 产品类型
     */
    @Column(name = "product_type")
    private Integer productType;

    /**
     * 团散
     */
    @Column(name = "ticket_flag")
    private Integer ticketFlag;

    /**
     * 发码产品ID
     */
    @Column(name = "ma_product_id")
    private String maProductId;

    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 天数
     */
    private Integer days;

    /**
     * 张数
     */
    private Integer quantity;

    /**
     * 已使用总数
     */
    @Column(name = "used_quantity")
    private Integer usedQuantity;

    /**
     * 已退总数
     */
    @Column(name = "refund_quantity")
    private Integer refundQuantity;

    /**
     * 最低使用数量限制(团队票,0不限制)
     */
    @Column(name = "low_ues_quantity")
    private Integer lowUesQuantity;

    /**
     * 供应商ID
     */
    @Column(name = "supplier_merchant_id")
    private Integer supplierMerchantId;

    /**
     * 供应商名称
     */
    @Column(name = "supplier_merchant_name")
    private String supplierMerchantName;

    /**
     * 供应商短信通知手机号
     */
    @Column(name = "supplier_phone")
    private String supplierPhone;

    /**
     * 最低售价(总价)
     */
    @Column(name = "low_price")
    private Integer lowPrice;

    /**
     * 结算价(总价)
     */
    @Column(name = "settle_price")
    private Integer settlePrice;

    /**
     * 抵扣总金额
     */
    @Column(name = "discount_money")
    private Long discountMoney;

    /**
     * 返佣总金额
     */
    private Integer rebate;

    /**
     * 最小生效时间
     */
    @Column(name = "min_effective_date")
    private Date minEffectiveDate;

    /**
     * 最大失效时间
     */
    @Column(name = "max_expiry_date")
    private Date maxExpiryDate;

    /**
     * 审核用户ID
     */
    @Column(name = "audit_user_id")
    private Integer auditUserId;

    /**
     * 审核用户名称
     */
    @Column(name = "audit_user_name")
    private String auditUserName;

    /**
     * 是否审核通过
     */
    @Column(name = "audit_success")
    private Boolean auditSuccess;

    /**
     * 审核时间
     */
    @Column(name = "audit_time")
    private Date auditTime;

    /**
     * 出票时间
     */
    @Column(name = "send_ma_time")
    private Date sendMaTime;

    /**
     * 票务配置ID
     */
    @Column(name = "ma_config_id")
    private Integer maConfigId;

    /**
     * 退订是否需要审核
     */
    @Column(name = "refund_audit")
    private Boolean refundAudit;

    /**
     * 最后退订订单ID
     */
    @Column(name = "last_refund_id")
    private Integer lastRefundId;

    /**
     * 当前预退订数量
     */
    @Column(name = "current_pre_refund_quantity")
    private Integer currentPreRefundQuantity;

    /**
     * 小票内容
     */
    @Column(name = "ticket_msg")
    private String ticketMsg;

    /**
     * 凭证短信模板ID
     */
    @Column(name = "sms_template_id")
    private Integer smsTemplateId;

    /**
     * 当前重新出票次数
     */
    @Column(name = "resend_times")
    private Integer resendTimes;

    /**
     * 最后一次重新出票时间
     */
    @Column(name = "last_resend_time")
    private Date lastResendTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 状态:1-待审核;2-已审核;3-未出票;4-出票失败;5-出票中;6-已出票;7-已完成;8-已取消
     */
    private Integer status;

    /**
     * 第三方子订单号
     */
    @Column(name = "out_item_id")
    private String outItemId;

    @Transient
    private FxRefundOrder refundOrder;

    @Transient
    private FxOrder order;

    @Transient
    private Member shipping;

    @Transient
    public boolean isComplete() {
        return Optional.ofNullable(usedQuantity).orElse(0) + Optional.ofNullable(refundQuantity).orElse(0) == quantity * days;
    }

    @Transient
    public int getInvalidQuantity() {
        if (maxExpiryDate != null && DateTime.now().isAfter(maxExpiryDate)) {
            return quantity * days - Optional.ofNullable(usedQuantity).orElse(0) - Optional.ofNullable(refundQuantity).orElse(0);
        }
        return 0;
    }

    private static final long serialVersionUID = 1L;
}
