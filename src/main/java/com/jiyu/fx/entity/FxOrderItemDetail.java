package com.jiyu.fx.entity;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Table(name = "t_fx_order_item_detail")
@Data
@Accessors(chain = true)
public class FxOrderItemDetail implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 子订单ID
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 日期时间(酒店为日)
     */
    private Date day;

    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 使用数量
     */
    @Column(name = "used_quantity")
    private Integer usedQuantity;

    /**
     * 已退数量
     */
    @Column(name = "refund_quantity")
    private Integer refundQuantity;

    /**
     * 使用时间限制(多少秒后才能使用)
     */
    @Column(name = "use_limit_second")
    private Integer useLimitSecond;

    /**
     * 生效时间
     */
    @Column(name = "effective_date")
    private Date effectiveDate;

    /**
     * 过期时间
     */
    @Column(name = "expiry_date")
    private Date expiryDate;

    /**
     * 禁用星期0000000-1111111的二进制
     */
    @Column(name = "disable_week")
    private Byte disableWeek;

    /**
     * 禁用日期逗号分隔
     */
    @Column(name = "disable_day")
    private String disableDay;

    /**
     * 最低使用数量(团队，0不限)
     */
    @Column(name = "low_use_quantity")
    private Integer lowUseQuantity;

    /**
     * 是否超卖
     */
    private Boolean oversell;

    /**
     * 结算价(单价)
     */
    @Column(name = "settle_price")
    private Integer settlePrice;

    /**
     * 零售价(单价)
     */
    @Column(name = "low_price")
    private Integer lowPrice;

    /**
     * 返佣(单个)
     */
    private Integer rebate;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 退货规则
     */
    @Column(name = "refund_rule")
    private String refundRule;

    private static final long serialVersionUID = 1L;

    @Transient
    private Long productSubNumber;

    public Set<String> getDisableDays() {
        return StrUtil.isBlank(disableDay) ? Collections.emptySet() : Arrays.stream(disableDay.split(",")).collect(Collectors.toSet());
    }
}
