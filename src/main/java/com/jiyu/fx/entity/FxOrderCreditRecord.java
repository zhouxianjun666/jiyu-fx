package com.jiyu.fx.entity;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_order_credit_record")
@Data
@Accessors(chain = true)
public class FxOrderCreditRecord implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 子订单编号
     */
    @Column(name = "order_item_number")
    private Long orderItemNumber;

    /**
     * 子产品code
     */
    @Column(name = "product_sub_code")
    private Long productSubCode;

    /**
     * 本单使用积分
     */
    @Column(name = "credit")
    private Integer credit;

    /**
     * 转换现金券
     */
    @Column(name = "cash_coupon")
    private Integer cashCoupon;

    /**
     * 返回结果
     */
    private String result;

    /**
     * 操作类型：1.积分兑换 2.确定积分生效 3.确定积分无效
     */
    private Integer type;

    /**
     * 异常信息
     */
    private String exception;

    private static final long serialVersionUID = 1L;
}