package com.jiyu.fx.entity;

import com.jiyu.common.annotation.UpdateIncrement;
import lombok.Data;
import lombok.experimental.Accessors;
import tk.mybatis.mapper.annotation.Version;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_fx_merchant_account")
@Data
@Accessors(chain = true)
public class FxMerchantAccount implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 老板商户ID
     */
    @Column(name = "boss_merchant_id")
    private Integer bossMerchantId;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 余额(分)
     */
    @UpdateIncrement
    private Long balance;

    /**
     * 现金(分)
     */
    @UpdateIncrement
    private Long cash;

    /**
     * 授信额度:0-没有授信;正数-授信值;负数-无限制;
     */
    @UpdateIncrement
    private Integer credit;

    /**
     * 配置的支付方式
     */
    @Column(name = "pay_config")
    private String payConfig;

    /**
     * 版本号
     */
    @Version
    private Long version;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}
