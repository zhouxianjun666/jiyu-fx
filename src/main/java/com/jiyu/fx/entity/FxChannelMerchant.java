package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_channel_merchant")
@Data
@Accessors(chain = true)
public class FxChannelMerchant implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 渠道id
     */
    @Column(name = "channel_id")
    private Integer channelId;

    /**
     * 商户id
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}