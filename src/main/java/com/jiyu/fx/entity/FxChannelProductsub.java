package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_channel_productsub")
@Data
@Accessors(chain = true)
public class FxChannelProductsub implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 渠道id
     */
    @Column(name = "channel_id")
    private Integer channelId;

    /**
     * 子产品id
     */
    @Column(name = "productsub_id")
    private Integer productsubId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 利润类型
     */
    @Column(name = "profit_type")
    private Integer profitType;

    /**
     * 利润值
     */
    private Integer profit;

    private static final long serialVersionUID = 1L;
}