package com.jiyu.fx.entity;

import com.jiyu.common.annotation.UpdateIncrement;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_fx_group_product")
@Data
@Accessors(chain = true)
public class FxGroupProduct implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 团队id
     */
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * 产品code
     */
    private Long code;

    /**
     * 产品名称
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 子产品
     */
    @Column(name = "sub_code")
    private Long subCode;

    /**
     * 子产品名称
     */
    @Column(name = "product_sub_name")
    private String productSubName;

    /**
     * 游玩日期
     */
    private Date day;

    @Column(name = "create_time")
    private Date createTime;

    /**
     * 总人数
     */
    private Integer sum;

    /**
     * 票务产品id
     */
    @Column(name = "ma_product_id")
    private Integer maProductId;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 产品订单状态
     */
    private Boolean state;


    /**
     * 实到人数
     */
    @UpdateIncrement
    @Column(name = "real_sum")
    private Integer realSum;

    @Column(name = "real_name")
    private Integer realName;

    private static final long serialVersionUID = 1L;
}
