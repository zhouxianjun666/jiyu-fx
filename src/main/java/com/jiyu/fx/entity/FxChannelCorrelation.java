package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_channel_correlation")
@Data
@Accessors(chain = true)
public class FxChannelCorrelation implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 申请关联者ID
     */
    @Column(name = "apply_id")
    private Integer applyId;

    /**
     * 被申请者ID
     */
    @Column(name = "be_applied_id")
    private Integer beAppliedId;

    /**
     * 类型：1-供应，2-销售
     */
    private Byte type;

    /**
     * 状态：1-待同意，2-已同意，3拒绝关联
     */
    private Byte status;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}