package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_group_consume")
@Data
@Accessors(chain = true)
public class FxGroupConsume implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 本次核销流水
     */
    @Column(name = "serial_no")
    private String serialNo;

    /**
     * 团队ID
     */
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * 凭证配置ID
     */
    @Column(name = "ma_config_id")
    private Integer maConfigId;

    /**
     * 子订单ID
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 核销票数
     */
    private Integer quantity;

    /**
     * 产品价格(供应商回传)
     */
    private Integer price;

    /**
     * 核销时间
     */
    @Column(name = "consume_time")
    private Date consumeTime;

    private static final long serialVersionUID = 1L;
}