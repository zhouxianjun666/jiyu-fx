package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_flow_ticket_log")
@Data
@Accessors(chain = true)
public class FxFlowTicket implements Serializable {
    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 订单ID
     */
    @Column(name = "order_no")
    private Long orderNo;

    /**
     * 票据ID
     */
    @Column(name = "ticket_id")
    private String ticketId;

    /**
     * 操作名称
     */
    @Column(name = "operation_name")
    private String operationName;

    /**
     * 游客ID
     */
    @Column(name = "tourist_id")
    private Integer touristId;

    /**
     * 凭据配置ID
     */
    @Column(name = "ma_config_id")
    private Integer maConfigId;

    /**
     * 申请时间
     */
    @Column(name = "apply_time")
    private Date applyTime;

    /**
     * 处理时间
     */
    @Column(name = "dealwith_time")
    private Date dealwithTime;

    /**
     * 流程方向
     */
    @Column(name = "flow_direction")
    private Boolean flowDirection;

    /**
     * 处理结果
     */
    @Column(name = "dealwith_result")
    private Boolean dealwithResult;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;
}