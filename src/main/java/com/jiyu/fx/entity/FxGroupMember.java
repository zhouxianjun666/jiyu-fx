package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_group_member")
@Data
@Accessors(chain = true)
public class FxGroupMember implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 团队ID
     */
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 证件类型
     */
    @Column(name = "card_type")
    private Integer cardType;

    /**
     * 证件号码
     */
    private String card;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 是否成人
     */
    private Boolean adult;

    /**
     * 创建用户ID
     */
    @Column(name = "create_user_id")
    private Integer createUserId;

    /**
     * 创建用户名称
     */
    @Column(name = "create_user_name")
    private String createUserName;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 子产品id
     */
    private Long code;

    /**
     * 是否到场
     */
    @Column(name = "is_present")
    private Boolean isPresent;

    /**
     * 产品预算id
     */
    @Column(name = "budget_id")
    private Integer budgetId;

    private static final long serialVersionUID = 1L;
}