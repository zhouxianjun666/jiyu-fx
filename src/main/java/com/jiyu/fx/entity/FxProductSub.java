package com.jiyu.fx.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_fx_product_sub")
@Data
@Accessors(chain = true)
public class FxProductSub implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 产品code
     */
    private Long code;

    /**
     * 名称
     */
    private String name;

    /**
     * 主商品id
     */
    @Column(name = "product_id")
    private Integer productId;

    /**
     * 主产品名
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 供应商id
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 供应商名称
     */
    @Column(name = "merchant_name")
    private String merchantName;

    /**
     * 票务产品id
     */
    @Column(name = "ma_id")
    private String maId;

    /**
     * 2001-散客;2002-团队
     */
    @Column(name = "ticket_flag")
    private Integer ticketFlag;

    /**
     * 3001:成人票; 3002:儿童票
     */
    @Column(name = "ticket_dict")
    private Integer ticketDict;

    /**
     * 市场价
     */
    @Column(name = "market_price")
    private Integer marketPrice;

    /**
     * 是否需要身份证（散客票）
     */
    @Column(name = "require_sid")
    private Integer requireSid;

    /**
     * 身份证次数限制/天
     */
    @Column(name = "sid_day_count")
    private Integer sidDayCount;

    /**
     * 身份证张数限制/天
     */
    @Column(name = "sid_day_quantity")
    private Integer sidDayQuantity;

    /**
     * 最低购买数 （团队票）
     */
    @Column(name = "min_buy_quantity")
    private Integer minBuyQuantity;

    /**
     * 最高购买数（散票）
     */
    @Column(name = "max_buy_quantity")
    private Integer maxBuyQuantity;

    /**
     * 最低使用限制(团队)
     */
    @Column(name = "low_use_quantity")
    private Integer lowUseQuantity;

    /**
     * 实名制要求（团队票）
     */
    @Column(name = "real_name")
    private Integer realName;

    /**
     * 预定须知
     */
    @Column(name = "book_notes")
    private String bookNotes;

    /**
     * 使用须知
     */
    @Column(name = "use_notes")
    private String useNotes;

    /**
     * 图片
     */
    private String imgs;

    /**
     * 凭证短信内容
     */
    @Column(name = "voucher_msg")
    private String voucherMsg;

    /**
     * 凭证短信模板ID
     */
    @Column(name = "voucher_template")
    private Integer voucherTemplate;

    /**
     * 小票内容
     */
    @Column(name = "ticket_msg")
    private String ticketMsg;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "operate_time")
    private Date operateTime;

    /**
     * 可住人数
     */
    private Integer person;

    /**
     * 所在楼层
     */
    private String floor;

    /**
     * 星期可用日期
     */
    private String week;

    /**
     * 1启用0禁用
     */
    private Boolean status;

    /**
     * 退订是否审核
     */
    @Column(name = "refund_audit")
    private Boolean refundAudit;

    /**
     * 票务配置id
     */
    @Column(name = "voucher_id")
    private Integer voucherId;

    /**
     * 是否有窗
     */
    private Boolean window;

    /**
     * 支付方式
     */
    @Column(name = "pay_type")
    private Integer payType;

    /**
     * 早饭类型
     */
    private Integer breakfast;

    /**
     * 是否需要最晚到店时间
     */
    @Column(name = "last_time")
    private Boolean lastTime;

    /**
     * 是否可加床
     */
    @Column(name = "is_bed")
    private Boolean isBed;

    /**
     * 价格上限
     */
    @Column(name = "price_limit")
    private Integer priceLimit;

    /**
     * 床型
     */
    @Column(name = "bed_type")
    private String bedType;

    /**
     * 上网类方式
     */
    @Column(name = "net_type")
    private String netType;

    @Column(name = "sort_num")
    private String sortNum;

    @Column(name = "last_time_content")
    private String lastTimeContent;

    private static final long serialVersionUID = 1L;
}