package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_ma_config")
@Data
@Accessors(chain = true)
public class FxMaConfig implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID(供应商)
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 名称
     */
    private String name;

    /**
     * 地址
     */
    private String link;

    /**
     * 是否启用
     */
    private Boolean enable;

    /**
     * 服务类型(票务系统)
     */
    private String service;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 额外配置JSON
     */
    private String config;

    private static final long serialVersionUID = 1L;
}