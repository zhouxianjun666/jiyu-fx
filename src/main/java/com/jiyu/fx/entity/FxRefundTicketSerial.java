package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_refund_ticket_serial")
@Data
@Accessors(chain = true)
public class FxRefundTicketSerial implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 第三方流水
     */
    @Column(name = "serial_no")
    private String serialNo;

    /**
     * 退订总数
     */
    private Integer quantity;

    /**
     * 退订订单ID
     */
    @Column(name = "refund_order_id")
    private Integer refundOrderId;

    /**
     * 票务&凭证配置ID
     */
    @Column(name = "ma_config_id")
    private Integer maConfigId;

    /**
     * 退票时间
     */
    @Column(name = "refund_time")
    private Date refundTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}
