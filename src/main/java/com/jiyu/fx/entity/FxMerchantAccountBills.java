package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_merchant_account_bills")
@Data
@Accessors(chain = true)
public class FxMerchantAccountBills implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 账户ID
     */
    @Column(name = "account_id")
    private Integer accountId;

    /**
     * 变动类型:1-充值;2-调整;3-订单分润(下单);4-核销分润(使用);5-订单退订;6-订单购买;
     */
    private Integer type;

    /**
     * 充值类型:0-余额;1-微信;2-支付宝;9-线下;
     */
    @Column(name = "recharge_type")
    private Integer rechargeType;

    /**
     * 关联ID
     */
    @Column(name = "ref_id")
    private String refId;

    /**
     * 订单号
     */
    @Column(name = "order_number")
    private Long orderNumber;

    /**
     * 变动之前余额
     */
    @Column(name = "before_balance")
    private Long beforeBalance;

    /**
     * 变动之后余额
     */
    @Column(name = "after_balance")
    private Long afterBalance;

    /**
     * 变动之前现金
     */
    @Column(name = "before_cash")
    private Long beforeCash;

    /**
     * 变动之后现金
     */
    @Column(name = "after_cash")
    private Long afterCash;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}
