package com.jiyu.fx.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_fx_scenery")
@Data
@Accessors(chain = true)
public class FxScenery implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 主产品code
     */
    private Integer productId;

    /**
     * 景点类型:["名胜古迹"]
     */
    private String category;

    /**
     * 景点级别
     */
    private Integer level;

    /**
     * 营业时间
     */
    @Column(name = "business_time")
    private String businessTime;

    /**
     * 交通线路
     */
    @Column(name = "transit_line")
    private String transitLine;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "operate_time")
    private Date operateTime;

    private static final long serialVersionUID = 1L;
}