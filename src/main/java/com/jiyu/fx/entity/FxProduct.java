package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_product")
@Data
@Accessors(chain = true)
public class FxProduct implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 产品code
     */
    private Long code;

    /**
     * 产品名
     */
    private String name;

    /**
     * 产品通知短信
     */
    private String phone;

    /**
     * 商户id
     */
    @Column(name = "merchant_id")
    private String merchantId;

    /**
     * 商户名
     */
    @Column(name = "merchant_name")
    private String merchantName;

    /**
     * 产品类型(景区：5001，酒店：5002)
     */
    private Integer type;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 图片
     */
    private String imgs;

    /**
     * 县
     */
    private Integer area;

    /**
     * 省市县str
     */
    private String pcastr;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 地理位置
     */
    private String map;

    /**
     * 服务电话
     */
    private String tel;

    /**
     * 简介
     */
    private String blurb;

    private static final long serialVersionUID = 1L;
}