package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.*;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jiyu.common.dto.OrderPay;
import com.jiyu.common.entity.base.Member;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_order")
@Data
@Accessors(chain = true)
public class FxOrder implements OrderPay, Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 订单编号
     */
    private Long number;

    /**
     * 订单金额(分)
     */
    private Integer amount;

    /**
     * 支付金额(分)
     */
    @Column(name = "pay_amount")
    private Integer payAmount;

    /**
     * 购买商户ID
     */
    @Column(name = "buy_merchant_id")
    private Integer buyMerchantId;

    /**
     * 购买企业名称
     */
    @Column(name = "buy_merchant_name")
    private String buyMerchantName;

    /**
     * 购买用户ID(平台内购买)
     */
    @Column(name = "buy_user_id")
    private Integer buyUserId;

    /**
     * 购买用户名称
     */
    @Column(name = "buy_user_name")
    private String buyUserName;

    /**
     * 第三方订单号
     */
    @Column(name = "outer_id")
    private String outerId;

    /**
     * 付款渠道:1-平台内;2-游客(代收);
     */
    @Column(name = "payment_channel")
    private Integer paymentChannel;

    /**
     * 支付类型: 0-余额;1-微信;2-支付宝
     */
    @Column(name = "payment_type")
    private Integer paymentType;

    /**
     * 支付时间
     */
    @Column(name = "payment_time")
    private Date paymentTime;

    /**
     * 来源
     */
    private Integer source;

    /**
     * 支付流水号
     */
    @Column(name = "payment_number")
    private String paymentNumber;

    /**
     * 备注
     */
    private String remark;

    /**
     * 下单时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 审核时间
     */
    @Column(name = "audit_time")
    private Date auditTime;

    /**
     * 审核企业ID
     */
    @Column(name = "audit_merchant_id")
    private Integer auditMerchantId;

    /**
     * 审核用户ID
     */
    @Column(name = "audit_user_id")
    private Integer auditUserId;

    /**
     * B2C会员ID
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 订单联系人(会员ID)
     */
    @Column(name = "shipping_member_id")
    private Integer shippingMemberId;

    /**
     * 购买失败退款是否成功
     */
    @Column(name = "buy_fail_refund_money")
    private Boolean buyFailRefundMoney;

    /**
     * 取消类型
     */
    @Column(name = "cancel_type")
    private Integer cancelType;

    /**
     * 取消原因
     */
    @Column(name = "cancel_reason")
    private String cancelReason;

    /**
     * 状态:1-待支付;2-支付中;3-已支付;4-已完成;5-已取消
     */
    private Integer status;

    /**
     * 团队ID，不为空则为团队订单
     */
    @Column(name = "group_id")
    private Integer groupId;

    @Transient
    private List<? extends FxOrderItem> items;

    @Transient
    private Member shipping;

    @Override
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String getBody() {
        return items == null ? null : items.stream().map(FxOrderItem::getProductSubName).collect(Collectors.joining(","));
    }

    @Override
    public long getMoney() {
        return Optional.ofNullable(this.payAmount).orElse(0);
    }

    @Override
    public String getTransactionId() {
        return this.paymentNumber;
    }

    @Override
    public Date getTimeExpire(int minute) {
        return DateUtil.offsetMinute(createTime, minute);
    }

    private static final long serialVersionUID = 1L;
}
