package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_product_chain")
@Data
@Accessors(chain = true)
public class FxProductChain implements Serializable {
    /**
     *  主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     *  子产品id
     */
    @Column(name = "product_sub_id")
    private Integer productSubId;

    /**
     * 卖家商户id
     */
    @Column(name = "sale_merchant_id")
    private Integer saleMerchantId;

    /**
     * 买家商户id
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 利润类型
     */
    @Column(name = "profit_type")
    private Integer profitType;

    /**
     * 利润值
     */
    private Integer profit;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}