package com.jiyu.fx.entity;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_ota_push_config")
@Data
@Accessors(chain = true)
public class FxOtaPushConfig implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 加密私钥
     */
    @Column(name = "access_key")
    private String accessKey;

    /**
     * 商户类型
     */
    private String type;

    /**
     * 推送地址
     */
    private String url;

    /**
     * OTA个性化配置
     */
    private String config;

    private static final long serialVersionUID = 1L;
}