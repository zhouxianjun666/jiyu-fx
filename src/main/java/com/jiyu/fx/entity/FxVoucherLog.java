package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_voucher_log")
@Data
@Accessors(chain = true)
public class FxVoucherLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 关联ID
     */
    @Column(name = "ref_id")
    private String refId;

    /**
     * 类型:1-出票;2-退票;3-核销;
     */
    private Integer type;

    /**
     * 请求 OR 返回
     */
    private Boolean request;

    /**
     * 凭证配置ID
     */
    @Column(name = "ma_config_id")
    private Integer maConfigId;

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 数据
     */
    private String data;

    /**
     * 异常信息
     */
    private String exception;

    private static final long serialVersionUID = 1L;
}
