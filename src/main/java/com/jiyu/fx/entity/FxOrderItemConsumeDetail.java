package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_order_item_consume_detail")
@Data
@Accessors(chain = true)
public class FxOrderItemConsumeDetail implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 核销ID
     */
    @Column(name = "consume_id")
    private Integer consumeId;

    /**
     * 凭证码
     */
    @Column(name = "voucher_number")
    private String voucherNumber;

    /**
     * 核销哪一天的票
     */
    private Date day;

    /**
     * 核销数量
     */
    private Integer quantity;

    private static final long serialVersionUID = 1L;
}