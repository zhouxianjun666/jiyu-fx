package com.jiyu.fx.entity;

import java.io.Serializable;
import javax.persistence.*;

import cn.hutool.core.util.StrUtil;
import com.jiyu.fx.ota.dto.CtripCfg;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Table(name = "t_fx_ctrip_config")
@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(value = "ctrip")
public class FxCtripConfig implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 携程加密私钥
     */
    @Column(name = "access_key")
    private String accessKey;

    /**
     * 携程账户
     */
    @Column(name = "account_id")
    private String accountId;

    /**
     * 携程AES加密密钥
     */
    @Column(name = "secret_key")
    private String secretKey;

    /**
     * 携程AES加密初始向量
     */
    @Column(name = "vector")
    private String vector;

    private static final long serialVersionUID = 1L;
}