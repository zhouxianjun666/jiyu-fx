package com.jiyu.fx.entity;

import com.jiyu.common.dto.ThreeGroup;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_fx_order_item_ticket")
@Data
@Accessors(chain = true)
public class FxOrderItemTicket implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 游客ID
     */
    @Column(name = "visitor_id")
    private Integer visitorId;

    /**
     * 游客详情ID
     */
    @Column(name = "visitor_detail_id")
    private Integer visitorDetailId;

    /**
     * 天
     */
    private Date day;

    /**
     * 凭证&票务配置ID
     */
    @Column(name = "ma_config_id")
    private Integer maConfigId;

    /**
     * 子订单ID
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 票据ID
     */
    @Column(name = "ticket_id")
    private String ticketId;

    /**
     * 凭证号
     */
    @Column(name = "voucher_number")
    private String voucherNumber;

    /**
     * 凭证号二维码地址
     */
    @Column(name = "voucher_url")
    private String voucherUrl;

    /**
     * 处理时间
     */
    @Column(name = "process_time")
    private Date processTime;

    @Transient
    private FxOrderItemVisitorDetail visitorDetail;

    @Transient
    private FxRefundVisitor refundVisitor;

    private static final long serialVersionUID = 1L;

    public ThreeGroup<Integer, Integer, Date> visitorItemDay() {
        ThreeGroup<Integer, Integer, Date> group = new ThreeGroup<>();
        group.setOne(visitorId).setTwo(orderItemId);
        return group.setThree(day);
    }
}
