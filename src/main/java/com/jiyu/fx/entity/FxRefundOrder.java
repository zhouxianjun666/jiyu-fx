package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_refund_order")
@Data
@Accessors(chain = true)
public class FxRefundOrder implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 退订编号
     */
    private Long number;

    /**
     * 子订单ID
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 退订数量
     */
    private Integer quantity;

    /**
     * 退款金额
     */
    private Integer money;

    /**
     * 手续费
     */
    private Integer fee;

    /**
     * 退订审核用户ID
     */
    @Column(name = "audit_user_id")
    private Integer auditUserId;

    /**
     * 退订审核用户名称
     */
    @Column(name = "audit_user_name")
    private String auditUserName;

    /**
     * 退订审核时间
     */
    @Column(name = "audit_time")
    private Date auditTime;

    /**
     * 退订是否需要审核
     */
    private Boolean audit;

    /**
     * 退订原因
     */
    private String cause;

    /**
     * 外部退订ID
     */
    @Column(name = "outer_id")
    private String outerId;

    /**
     * 状态:1-待审核;2-审核拒绝;3-退票中;4-退票失败;5-余票不足(票务已退票);6-退款中;7-已完成
     */
    private Integer status;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}
