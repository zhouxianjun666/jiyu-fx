package com.jiyu.fx.entity;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jiyu.common.annotation.UpdateIncrement;
import com.jiyu.common.dto.BetweenDate;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Table(name = "t_fx_product_plan")
@Data
@Accessors(chain = true)
public class FxProductPlan implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 子产品id
     */
    @Column(name = "product_sub_id")
    private Integer productSubId;

    /**
     * 产品id
     */
    @Column(name = "product_id")
    private Integer productId;

    /**
     * 市场价
     */
    @Column(name = "market_price")
    private Integer marketPrice;

    /**
     * 最低售价
     */
    @Column(name = "low_price")
    private Integer lowPrice;

    /**
     * 结算价
     */
    @Column(name = "settle_price")
    private Integer settlePrice;

    /**
     * 退货规则
     */
    @Column(name = "refund_id")
    private Integer refundId;

    /**
     * 售卖规则
     */
    @Column(name = "sale_rule_type")
    private Integer saleRuleType;

    /**
     * 预定时间是否限制
     */
    @Column(name = "booking_limit")
    private Boolean bookingLimit;

    /**
     * 预定时间限制天
     */
    @Column(name = "booking_day_limit")
    private Integer bookingDayLimit;

    /**
     * 预定时间限制时间
     */
    @Column(name = "booking_time_limit")
    private String bookingTimeLimit;

    /**
     * 使用时间限制 (预订后多少 秒)
     */
    @Column(name = "use_second_limit")
    private Integer useSecondLimit;

    /**
     * 有效天数
     */
    @Column(name = "effective_day")
    private Integer effectiveDay;

    /**
     * 有效开始日期
     */
    @Column(name = "effective_start_date")
    private Date effectiveStartDate;

    /**
     * 有效结束日期
     */
    @Column(name = "effective_end_date")
    private Date effectiveEndDate;

    /**
     * 有效日期类型
     */
    @Column(name = "effective_type")
    private Integer effectiveType;

    /**
     * 禁用星期
     */
    @Column(name = "disable_week")
    private Byte disableWeek;

    /**
     * 禁用日期
     */
    @Column(name = "disable_date")
    private String disableDate;

    /**
     * 库存
     */
    @UpdateIncrement
    private Integer stock;

    /**
     * 已售数量
     */
    @Column(name = "sale_quantity")
    @UpdateIncrement
    private Integer saleQuantity;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 售卖开始时间段
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 售卖结束时间段
     */
    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "status")
    private Boolean status;

    @Transient
    private Boolean refundRuleFree;
    @Transient
    private Boolean refundRuleRefund;
    @Transient
    private String refundRuleDetail;
    @Transient
    @JsonIgnore
    public String getRefundRule() {
        JSONObject object = new JSONObject();
        object.put("free", Optional.ofNullable(refundRuleFree).orElse(false));
        object.put("refund", Optional.ofNullable(refundRuleRefund).orElse(false));
        object.put("details", JSONUtil.parseArray(StrUtil.emptyToDefault(refundRuleDetail, "[]")));
        return object.toString();
    }

    @Transient
    @JsonIgnore
    public BetweenDate getBetweenDate() {
        return new BetweenDate().setBegin(new Date(startTime.getTime())).setEnd(new Date(endTime.getTime()));
    }

    private static final long serialVersionUID = 1L;
}
