package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_group")
@Data
@Accessors(chain = true)
public class FxGroup implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 团号
     */
    private String number;

    /**
     * 出行日期
     */
    @Column(name = "start_date")
    private Date startDate;

    /**
     * 旅行社名称
     */
    @Column(name = "travel_name")
    private String travelName;

    /**
     * 省
     */
    private Integer province;

    /**
     * 市
     */
    private Integer city;

    /**
     * 全地址
     */
    private String address;

    /**
     * 导游姓名
     */
    @Column(name = "guide_name")
    private String guideName;

    /**
     * 导游手机号
     */
    @Column(name = "guide_phone")
    private String guidePhone;

    /**
     * 导游身份证
     */
    @Column(name = "guide_sid")
    private String guideSid;

    /**
     * 导游证件号
     */
    @Column(name = "guide_card")
    private String guideCard;

    /**
     * 总人数
     */
    private Integer sum;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人ID
     */
    @Column(name = "create_user_id")
    private Integer createUserId;

    /**
     * 创建人名称
     */
    @Column(name = "create_user_name")
    private String createUserName;

    /**
     * 商户id
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 删除状态
     */
    private Boolean state;

    private static final long serialVersionUID = 1L;
}