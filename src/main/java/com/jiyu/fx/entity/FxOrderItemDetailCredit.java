package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_order_item_detail_credit")
@Data
@Accessors(chain = true)
public class FxOrderItemDetailCredit implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 天时间
     */
    private Date day;

    /**
     * 子订单编号
     */
    @Column(name = "item_number")
    private Long itemNumber;

    /**
     * 总使用积分
     */
    @Column(name = "used_credit")
    private Long usedCredit;

    /**
     * 每张使用积分
     */
    @Column(name = "unit_credit")
    private Long unitCredit;

    /**
     * 抵扣金额
     */
    @Column(name = "discount_money")
    private Long discountMoney;

    private static final long serialVersionUID = 1L;
}