package com.jiyu.fx.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_fx_refund_visitor")
@Data
@Accessors(chain = true)
public class FxRefundVisitor implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 退订订单ID
     */
    @Column(name = "refund_order_id")
    private Integer refundOrderId;

    /**
     * 子订单ID
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 游客ID
     */
    @Column(name = "visitor_id")
    private Integer visitorId;

    /**
     * 游客详细ID
     */
    @Column(name = "visitor_detail_id")
    private Integer visitorDetailId;

    /**
     * 天
     */
    private Date day;

    /**
     * 退订数量
     */
    private Integer quantity;

    /**
     * 手续费
     */
    private Integer fee;

    /**
     * 实际退款金额
     */
    private Integer money;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    @Transient
    private FxOrderItemTicket ticket;

    @Transient
    private FxOrderItemVisitor visitor;

    private static final long serialVersionUID = 1L;
}
