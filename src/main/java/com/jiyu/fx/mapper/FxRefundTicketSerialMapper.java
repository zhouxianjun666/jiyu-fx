package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxRefundTicketSerial;

public interface FxRefundTicketSerialMapper extends IMapper<FxRefundTicketSerial> {
}
