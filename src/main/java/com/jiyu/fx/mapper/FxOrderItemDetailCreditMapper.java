package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderItemDetailCredit;

public interface FxOrderItemDetailCreditMapper extends IMapper<FxOrderItemDetailCredit> {
}