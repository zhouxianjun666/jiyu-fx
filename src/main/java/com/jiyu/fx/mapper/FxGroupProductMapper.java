package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxGroupProduct;

import java.util.List;
import java.util.Map;

public interface FxGroupProductMapper extends IMapper<FxGroupProduct> {

    List<Map> selectbyGroupId(Integer id);

    int deleteByGroupId(Integer id);
}