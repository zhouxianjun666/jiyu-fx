package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderItemConsume;

public interface FxOrderItemConsumeMapper extends IMapper<FxOrderItemConsume> {
}
