package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderItemVisitorDetail;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 */
public interface FxOrderItemVisitorDetailMapper extends IMapper<FxOrderItemVisitorDetail> {
    /**
     * 退票
     * @param visitorId 游客ID
     * @param day 哪天的票
     * @param quantity 票数
     * @return
     */
    int refundQuantity(@Param("visitorId") int visitorId, @Param("day") Date day, @Param("quantity") int quantity);

    /**
     * 使用票
     * @param visitorId 游客ID
     * @param day 哪天的票
     * @param quantity 票数
     * @return
     */
    int usedQuantity(@Param("visitorId") int visitorId, @Param("day") Date day, @Param("quantity") int quantity);
}
