package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxVoucherLog;

public interface FxVoucherLogMapper extends IMapper<FxVoucherLog> {
}