package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxMerchantAccount;

import java.util.List;
import java.util.Map;

public interface FxMerchantAccountMapper extends IMapper<FxMerchantAccount> {
    List<Map<String, Object>> getChannelBalance(Integer id);
}
