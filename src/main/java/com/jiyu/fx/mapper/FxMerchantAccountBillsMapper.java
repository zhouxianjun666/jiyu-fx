package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxMerchantAccountBills;

public interface FxMerchantAccountBillsMapper extends IMapper<FxMerchantAccountBills> {
}
