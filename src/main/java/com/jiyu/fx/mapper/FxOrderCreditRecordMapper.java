package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderCreditRecord;

public interface FxOrderCreditRecordMapper extends IMapper<FxOrderCreditRecord> {
}