package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.entity.FxChannelMerchant;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FxChannelMerchantMapper extends IMapper<FxChannelMerchant> {
    List<FxChannelMerchant> selectByChannelId(Integer channelId);

    FxChannelMerchant selectByChannelAndMerchantId(@Param("channelId") Integer channelId, @Param("merchantId") Integer merchantId);

    int deleteByChannelIdAndProductId(@Param("channelId") Integer channelId,@Param("merchantId") Integer merchantId);

    int selectCountByMerchantId(Integer merchantId);

    List<Map<String, Object>> selectMerchantsByChannelId(@Param("channelId") Integer channelId);
    List<Merchant> selectMerchantsByProductSubId(@Param("productSubId") Integer productSubId, @Param("merchantId") Integer merchantId);

    FxChannelMerchant selectMerchantInGroup(@Param("id") Integer id, @Param("merchantId") Integer merchantId);
}
