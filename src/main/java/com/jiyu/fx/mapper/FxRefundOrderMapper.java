package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxRefundOrder;

import java.util.List;

public interface FxRefundOrderMapper extends IMapper<FxRefundOrder> {
    /**
     * 加载退票中的退订订单(未发起退票)
     * @return
     */
    List<FxRefundOrder> loadTicketingByNon();

    /**
     * 加载退款中的退订订单(未发起退款)
     * @return
     */
    List<FxRefundOrder> loadRefundMoneyByNon();

    /**
     * 加载退款中的退订订单(已发起退款)
     * @return
     */
    List<FxRefundOrder> loadRefundMoneyByHas();
}
