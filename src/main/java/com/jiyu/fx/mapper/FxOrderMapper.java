package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.dto.MerchantSalesOrderOfDay;
import com.jiyu.fx.entity.FxOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FxOrderMapper extends IMapper<FxOrder> {
    /**
     * 按天统计商户销售订单
     * @param dayCount 近多少天
     * @return
     */
    List<MerchantSalesOrderOfDay> statisticsMerchantSalesOrderOfDay(@Param("dayCount") int dayCount);
    int lastId();
}
