package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderItemVisitor;
import tk.mybatis.mapper.common.IdsMapper;

public interface FxOrderItemVisitorMapper extends IMapper<FxOrderItemVisitor>, IdsMapper<FxOrderItemVisitor> {
}
