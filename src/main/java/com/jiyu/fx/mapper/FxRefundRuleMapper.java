package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.common.dto.RefundRule;
import com.jiyu.fx.entity.FxRefundRule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FxRefundRuleMapper extends IMapper<FxRefundRule> {
    List<RefundRule> selectAllByEpId(@Param("merchantId") Integer merchantId, @Param("productType") Integer productType);
}
