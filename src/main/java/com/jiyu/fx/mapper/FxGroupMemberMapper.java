package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxGroupMember;

import java.util.List;

public interface FxGroupMemberMapper extends IMapper<FxGroupMember> {
    void deleteByGroupId(int id);

    List<FxGroupMember> selectBybudgetId(Integer id);
}