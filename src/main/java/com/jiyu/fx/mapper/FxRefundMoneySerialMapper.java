package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxRefundMoneySerial;

public interface FxRefundMoneySerialMapper extends IMapper<FxRefundMoneySerial> {
}
