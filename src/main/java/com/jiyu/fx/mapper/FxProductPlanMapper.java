package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxProductPlan;
import com.jiyu.fx.vo.CanSaleProductVO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface FxProductPlanMapper extends IMapper<FxProductPlan> {
    List<FxProductPlan> selectProductPlanList(@Param("productSubId") Integer productSubId, @Param("beginTime") String beginTime, @Param("endTime") String endTime);

    List<Map> selectProPlanByproductSubId(Integer productSubId);

    List<CanSaleProductVO> selectCanSaleProList(@Param("type") Integer type, @Param("name") String name, @Param("subName") String subName, @Param("merchantId") Integer merchantId, @Param("buySale") Boolean buySale);

    List<FxProductPlan> selectPlan(@Param("productSubId") Integer productSubId, @Param("beginTime") Date beginTime, @Param("endTime") Date endTime);

    int selectRepeatCount(@Param("beginDay") Date beginDay, @Param("productSubId") Integer productSubId);

    List<Map> selectCanSaleHotelList(@Param("province") String province, @Param("city") String city, @Param("timeRange") String timeRange, @Param("day") Integer day, @Param("name") String name, @Param("level") Integer level, @Param("lowPrice") Integer lowPrice,
                                     @Param("highPrice") Integer highPrice, @Param("hotelTheme") String hotelTheme, @Param("levelSort") String levelSort, @Param("priceSort") String priceSort, @Param("merchantId") Integer merchantId,@Param("buySale") Boolean buySale);

    int deleteBatchPlan(@Param("beginDay") Date beginDay, @Param("endDay") Date endDay, @Param("productSubId") Integer productSubId);
}
