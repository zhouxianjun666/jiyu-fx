package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxProduct;
import com.jiyu.fx.vo.FxProductVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Description: 类的功能
 * @Author: songe
 * @CreateDate: 2019/1/14 10:32
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/14 10:32
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
public interface FxProductMapper extends IMapper<FxProduct> {

    List<FxProductVO> selectSceneryProductList(@Param("type") Integer type, @Param("name") String name, @Param("subName") String subName, @Param("merchantId") Integer merchantId);

    int selectCountByidAndName(@Param("id") Integer id, @Param("name") String name, @Param("merchantId") Integer merchantId);

    Map selectDetail(long code);

}
