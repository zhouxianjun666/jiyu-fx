package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxGroupConsume;

public interface FxGroupConsumeMapper extends IMapper<FxGroupConsume> {
}