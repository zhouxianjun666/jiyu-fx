package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxMaConfig;

public interface FxMaConfigMapper extends IMapper<FxMaConfig> {
}