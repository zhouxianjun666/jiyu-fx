package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxChannel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FxChannelMapper extends IMapper<FxChannel> {

    List<Map<String, Object>> selectByPlatformId(Integer id);

    List<Map<String, Object>> selectByProductId(@Param("productSubId") int productSubId, @Param("merchantId") Integer merchantId);

    List<Map<String, Object>> findCanDownChannel(@Param("productSubId") int productSubId, @Param("merchantId") Integer merchantId);

    FxChannel selectChannelByChannelIdandPlatFormId(@Param("channelId") Integer channelId, @Param("merchantId") Integer merchantId);

    /**
     * 获取商户的所有商户
     * @param merchant
     * @return
     */
    List<Integer> getAllChannelMerchantByMerchant(@Param("merchant") int merchant);
}
