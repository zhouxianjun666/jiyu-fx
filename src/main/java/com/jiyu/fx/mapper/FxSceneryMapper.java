package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxScenery;

import java.util.List;
import java.util.Map;

public interface FxSceneryMapper extends IMapper<FxScenery> {
    int deleteByProductId(int id);

    List<Map> selectProductByid(int id);

    Map detail(String code);

    Integer selectSceneryByProcutId(Integer id);
}