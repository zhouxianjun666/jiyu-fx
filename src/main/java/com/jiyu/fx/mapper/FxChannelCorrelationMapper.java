package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxChannelCorrelation;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FxChannelCorrelationMapper extends IMapper<FxChannelCorrelation> {
    List<Map<String, Object>> canCorrelationList(@Param("merchantId") Integer merchantId, @Param("applyType") Integer applyType, @Param("beAppliedType") Integer beAppliedType, @Param("name") String name);

    List<Map<String, Object>> correlationList(@Param("merchantId") Integer merchantId, @Param("applyType") Integer applyType, @Param("beAppliedType") Integer beAppliedType, @Param("status") Integer status, @Param("name") String name);

    List<Map<String, Object>> canAddGroup(@Param("merchantId") Integer merchantId);
}