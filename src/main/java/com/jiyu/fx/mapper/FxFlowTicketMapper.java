package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxFlowTicket;

public interface FxFlowTicketMapper extends IMapper<FxFlowTicket> {
}