package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderItemConsumeDetail;

public interface FxOrderItemConsumeDetailMapper extends IMapper<FxOrderItemConsumeDetail> {
}