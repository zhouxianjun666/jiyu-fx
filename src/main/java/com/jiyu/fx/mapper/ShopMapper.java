package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.shop.Shop;

public interface ShopMapper extends IMapper<Shop> {
}