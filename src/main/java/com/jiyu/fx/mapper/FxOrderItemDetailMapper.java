package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderItemDetail;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 */
public interface FxOrderItemDetailMapper extends IMapper<FxOrderItemDetail> {
    /**
     * 退票
     * @param itemId 子订单ID
     * @param day 哪天的票
     * @param quantity 票数
     * @return
     */
    int refundQuantity(@Param("itemId") int itemId, @Param("day") Date day, @Param("quantity") int quantity);

    /**
     * 使用票
     * @param itemId 子订单ID
     * @param day 哪天的票
     * @param quantity 票数
     * @return
     */
    int usedQuantity(@Param("itemId") int itemId, @Param("day") Date day, @Param("quantity") int quantity);
}
