package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxGroup;
import com.jiyu.fx.vo.GroupBookVO;
import com.jiyu.fx.vo.GroupProductVO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface FxGroupMapper extends IMapper<FxGroup> {
    int deleteByLogic(int id);

    List<GroupBookVO> selectSceneryGroupList(@Param("startTime") Date startTime, @Param("number") String number, @Param("travelName") String travelName, @Param("guideName") String guideName, @Param("merchantId") Integer merchantId);

    List<GroupProductVO> queryGroupProducts(@Param("configId") String configId, @Param("sid") String sid, @Param("merchantId") Integer merchantId);
}
