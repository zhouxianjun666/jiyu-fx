package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxRefundVisitor;

public interface FxRefundVisitorMapper extends IMapper<FxRefundVisitor> {
}