package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxHotel;
import com.jiyu.fx.vo.FxHotelVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FxHotelMapper extends IMapper<FxHotel> {
    List<FxHotelVO> selectHotelList(@Param("name") String name, @Param("subName") String subName, @Param("merchantId") Integer merchantId);

    Integer selectHotelByproductId(Integer id);

    int deleteByProductId(int id);
}
