package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderItem;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhouxianjun(Alone)
 */
public interface FxOrderItemMapper extends IMapper<FxOrderItem> {
    /**
     * 修改退票数量
     * @param id 子订单ID
     * @param quantity 票数
     * @return
     */
    int changeRefundQuantity(@Param("id") int id, @Param("quantity") int quantity);

    /**
     * 使用票
     * @param id 子订单ID
     * @param quantity 票数
     * @return
     */
    int usedQuantity(@Param("id") int id, @Param("quantity") int quantity);

    /**
     * 重发
     * @param id 子订单ID
     * @param max 最大次数
     * @param interval 每次间隔
     * @return
     */
    int resend(@Param("id") int id, @Param("max") int max, @Param("interval") int interval);
}
