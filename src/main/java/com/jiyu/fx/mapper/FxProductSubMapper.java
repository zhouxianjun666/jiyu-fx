package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxProductSub;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FxProductSubMapper extends IMapper<FxProductSub> {
    List<FxProductSub> selectCanDistProSub(@Param("id") int id, @Param("merchantId") Integer merchantId);

    List<FxProductSub> selectByProductIdAndName(@Param("product_id") int productId, @Param("subName") String subName);

    List<Map<String, Object>> selectProSubByChannelId(int id);

    Map<String, Object> selectProDetailByCode(Long code);

    Map<String,Object> selectProPlanByProSubId(Integer proSubId);


}
