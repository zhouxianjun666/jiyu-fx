package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxCtripConfig;

public interface FxCtripConfigMapper extends IMapper<FxCtripConfig> {
}