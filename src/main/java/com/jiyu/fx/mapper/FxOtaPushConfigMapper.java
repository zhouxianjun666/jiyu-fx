package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOtaPushConfig;

public interface FxOtaPushConfigMapper extends IMapper<FxOtaPushConfig> {
}