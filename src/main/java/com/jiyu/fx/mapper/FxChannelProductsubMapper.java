package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxChannelProductsub;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FxChannelProductsubMapper extends IMapper<FxChannelProductsub> {
    int deleteByChannelAndSubId(@Param("channelId") int channelId,@Param("productSubId") int productSubId);

    List<FxChannelProductsub> selectByChannelId(Integer channelId);
}
