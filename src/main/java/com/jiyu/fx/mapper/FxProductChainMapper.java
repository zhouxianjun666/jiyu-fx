package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxProductChain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FxProductChainMapper extends IMapper<FxProductChain> {
    /**
     * 根据买家商户获取销售链
     * @param productSubId 子产品ID
     * @param merchantId 买家商户ID
     * @return
     */
    List<FxProductChain> selectByMerchant(@Param("productSubId") int productSubId, @Param("merchantId") int merchantId);

    List<Map> selectListInfo(@Param("productSubName") String productSubName, @Param("merchantId") Integer merchantId,@Param("productType") Integer productType);

    List<FxProductChain> selectChainBySubId(Integer productSubId);

    int deleteByChannelIdandProductId(@Param("productsubId") Integer productsubId, @Param("channelId") Integer channelId);

    FxProductChain selectChainByMerchantAndProId(@Param("merchantId") Integer merchantId, @Param("productsubId") Integer productsubId);

    int deleteByChannelIdAndMerchantId(@Param("channelId") Integer channelId, @Param("merchantId") Integer merchantId);

    int updateByProfitAndChannelId(@Param("profitType") Integer profitType, @Param("profit") Integer profit, @Param("id") Integer id);

    List<FxProductChain> selectProductChain(@Param("subId") Integer subId, @Param("merchantId") Integer merchantId,@Param("channelId") Integer channelId);
}
