package com.jiyu.fx.mapper;

import com.jiyu.common.IMapper;
import com.jiyu.fx.entity.FxOrderItemTicket;

public interface FxOrderItemTicketMapper extends IMapper<FxOrderItemTicket> {
}