package com.jiyu.fx.task;

import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiyu.fx.entity.FxRefundOrder;
import com.jiyu.common.util.Util;
import com.jiyu.fx.manager.RefundManager;
import com.jiyu.fx.mapper.FxRefundOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 检查退款中任务
 * @date 2019/1/23 14:12
 */
@Component
@Slf4j
public class RefundMoneySuccessTask {
    private final static int PAGE_SIZE = 20;
    @Autowired
    private RefundManager refundManager;
    @Resource
    private FxRefundOrderMapper refundOrderMapper;

    @Scheduled(fixedDelay = 60 * 1000 * 3, initialDelay = 1000 * 30)
    public void run() {
        this.pull(1);
    }

    private void pull(int page) {
        PageInfo<FxRefundOrder> p = null;
        try {
            p = PageHelper.startPage(page, PAGE_SIZE).doSelectPageInfo(() -> refundOrderMapper.loadRefundMoneyByHas());
            List<FxRefundOrder> list = p.getList();
            if (CollUtil.isEmpty(list)) {
                return;
            }
            list.forEach(order -> Util.runNoException(() -> refundManager.processRefundMoneyStatus(order)));
        } finally {
            if (p != null && p.isHasNextPage()) {
                this.pull(++page);
            }
        }
    }
}
