package com.jiyu.fx.task;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Snowflake;
import com.github.pagehelper.PageRowBounds;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.TicketSendInfo;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxOrderItemVisitorDetail;
import com.jiyu.fx.manager.TicketManager;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxOrderItemVisitorDetailMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.jiyu.common.Constant.ProductType.HOTEL;
import static com.jiyu.common.Constant.ProductType.LINE;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/12 11:26
 */
@Component
@Slf4j
public class SelfTicketSendTask {
    private final static int PAGE_SIZE = 20;
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private FxOrderItemVisitorDetailMapper visitorDetailMapper;
    @Autowired
    private TicketManager ticketManager;
    @Autowired
    private Snowflake snowflake;

    @Scheduled(fixedDelay = 60 * 1000 * 4, initialDelay = 1000 * 30)
    public void run() {
        this.pull(1);
    }

    private void pull(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<FxOrderItem> list = orderItemMapper.selectByExampleAndRowBounds(
                    Example.builder(FxOrderItem.class)
                            .where(
                                    WeekendSqls.<FxOrderItem>custom()
                                            .andIn(FxOrderItem::getProductType, Stream.of(HOTEL, LINE).collect(Collectors.toSet()))
                                            .andEqualTo(FxOrderItem::getStatus, Constant.OrderItemStatus.TICKETING)
                            )
                            .build(),
                    rowBounds
            );
            if (CollUtil.isEmpty(list)) {
                return;
            }
            list.forEach(orderItem -> Util.runNoException(() -> {
                List<FxOrderItemVisitorDetail> visitorDetails = visitorDetailMapper.select(new FxOrderItemVisitorDetail().setOrderItemId(orderItem.getId()));
                Result<?> result = ticketManager.sendResult(orderItem.getNumber(), visitorDetails.stream().map(vd -> new TicketSendInfo()
                        .setVisitorId(vd.getVisitorId())
                        .setDay(vd.getDay())
                        .setTicketId(String.valueOf(vd.getId()))
                        .setVoucherNumber(String.valueOf(snowflake.nextId()))
                ).collect(Collectors.toList()), true, new Date());
                if (!result.isSuccess()) {
                    log.warn("订单模拟出票失败: 订单号: {}, 原因: {}", orderItem.getNumber(), result.getMsg());
                }
            }));
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pull(++page);
            }
        }
    }
}
