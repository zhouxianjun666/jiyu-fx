package com.jiyu.fx.task;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.alone.tk.mybatis.JoinExample;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Config;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.mapper.FxOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单审核超时任务
 * @date 2019/1/18 8:50
 */
@Component
@Slf4j
public class OrderAuditTimeoutTask {
    private final static int PAGE_SIZE = 20;
    @Resource
    private FxOrderMapper orderMapper;
    @Autowired
    private Config config;
    @Autowired
    private OrderManager orderManager;

    @Scheduled(fixedDelay = 60 * 1000 * 5, initialDelay = 1000 * 30)
    public void run() {
        this.pull(1);
    }

    private void pull(int page) {
        PageInfo<FxOrder> p = null;
        try {
            p = PageHelper.startPage(page, PAGE_SIZE).doSelectPageInfo(() -> orderMapper.selectByJoinExampleEntity(
                    JoinExample.builder(FxOrder.class)
                            .addCol("fx_order.*")
                            .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getOrderId, FxOrder::getId))
                            .where(JoinExample.Where.custom()
                                    .andEqualTo(FxOrderItem::getStatus, Constant.OrderItemStatus.WAIT_AUDIT)
                                    .andLessThan(FxOrder::getPaymentTime, DateUtil.offsetMinute(new Date(), -config.getOrderAuditTimeout()))
                            )
                            .groupBy(FxOrder::getId)
                            .build()
            ));
            List<FxOrder> list = p.getList();
            if (CollUtil.isEmpty(list)) {
                return;
            }
            list.forEach(order -> Util.runNoException(() -> doCancel(order)));
        } finally {
            if (p != null && p.isHasNextPage()) {
                this.pull(++page);
            }
        }
    }

    private void doCancel(FxOrder order) {
        orderManager.newTransactionalCancel(order.getNumber(), Constant.OrderCancelType.AUDIT_TIMEOUT, "审核超时");
    }
}
