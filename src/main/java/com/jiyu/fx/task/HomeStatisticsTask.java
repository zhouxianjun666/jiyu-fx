package com.jiyu.fx.task;

import com.jiyu.fx.manager.HomeStatisticsManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 首页统计任务
 * @date 2019/5/20 16:59
 */
@Component
@Slf4j
public class HomeStatisticsTask {
    @Autowired
    private HomeStatisticsManager homeStatisticsManager;
    @Scheduled(fixedDelay = 60 * 1000 * 3, initialDelay = 1000 * 60 * 2)
    public void run() {
        if (!homeStatisticsManager.isLast()) {
            homeStatisticsManager.load();
        }
    }
}
