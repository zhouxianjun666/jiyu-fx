package com.jiyu.fx.task;

import com.jiyu.common.Constant;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.ChangeStock;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.manager.ProductPlanManager;
import com.jiyu.fx.mapper.FxOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 减可售库存检查任务
 * @date 2019/4/4 14:35
 */
@Component
@Slf4j
public class ReduceAvailableStockCheckTask {
    @Resource
    private FxOrderMapper orderMapper;
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private ProductPlanManager planManager;

    @Scheduled(fixedDelay = 60 * 1000, initialDelay = 1000 * 30)
    public void run() {
        RMap<Long, List<ChangeStock>> map = redissonClient.getMap(Constant.REDUCE_AVAILABLE_STOCK);
        Set<Map.Entry<Long, List<ChangeStock>>> entrySet = map.entrySet();
        Iterator<Map.Entry<Long, List<ChangeStock>>> it = entrySet.iterator();
        while (it.hasNext()) {
            Util.runNoException(() -> ((ReduceAvailableStockCheckTask) AopContext.currentProxy()).check(it.next(), it));
        }
    }

    @Locks("LOCK:RAS:#{#item.key}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class}, propagation = Propagation.REQUIRES_NEW)
    public void check(Map.Entry<Long, List<ChangeStock>> item, Iterator<Map.Entry<Long, List<ChangeStock>>> it) {
        int count = orderMapper.selectCount(new FxOrder().setNumber(item.getKey()));
        if (count <= 0) {
            item.getValue().forEach(c -> c.setAvailable(-c.getAvailable()));
            planManager.changeStock(item.getValue());
        }
        it.remove();
    }
}
