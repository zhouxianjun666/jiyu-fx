package com.jiyu.fx.task;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageRowBounds;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.Config;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.mapper.FxOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.jiyu.common.Constant.OrderStatus.PAYING;
import static com.jiyu.common.Constant.OrderStatus.PENDING_PAY;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单支付超时任务
 * @date 2019/1/18 8:50
 */
@Component
@Slf4j
public class OrderPayTimeoutTask {
    private final static int PAGE_SIZE = 20;
    @Resource
    private FxOrderMapper orderMapper;
    @Autowired
    private Config config;
    @Autowired
    private OrderManager orderManager;

    @Scheduled(fixedDelay = 60 * 1000 * 5, initialDelay = 1000 * 30)
    public void run() {
        this.pull(1);
    }

    private void pull(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<FxOrder> list = orderMapper.selectByExampleAndRowBounds(
                    Example.builder(FxOrder.class)
                            .where(
                                    WeekendSqls.<FxOrder>custom()
                                            .andIn(FxOrder::getStatus, Stream.of(PENDING_PAY, PAYING).collect(Collectors.toSet()))
                                            .andLessThan(FxOrder::getCreateTime, DateUtil.offsetMinute(new Date(), -config.getOrderPayTimeout()))
                            )
                            .build(),
                    rowBounds
            );
            if (CollUtil.isEmpty(list)) {
                return;
            }
            list.forEach(order -> Util.runNoException(() -> ((OrderPayTimeoutTask) AopContext.currentProxy()).doCancel(order)));
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pull(++page);
            }
        }
    }

    @Locks("#{#order.number}")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class, RuntimeException.class})
    public void doCancel(FxOrder order) {
        boolean paid = orderManager.processOrderPaymentStatus(order);
        if (!paid) {
            orderManager.cancel(order.getNumber(), Constant.OrderCancelType.PAY_TIMEOUT, "支付超时");
        }
    }
}
