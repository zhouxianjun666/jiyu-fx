package com.jiyu.fx.aop;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.jiyu.common.ExpressionParserManager;
import com.jiyu.common.util.Util;
import com.jiyu.fx.annotation.VoucherLog;
import com.jiyu.fx.entity.FxVoucherLog;
import com.jiyu.fx.manager.VoucherManager;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/22 11:42
 */
@Aspect
@Order(-1)
@Component
@Slf4j
public class VoucherLogAop {
    @Autowired
    private ExpressionParserManager parserManager;
    @Autowired
    private VoucherManager voucherManager;

    @Around(value = "execution(* com.jiyu..*.*(int, ..)) && @annotation(voucherLog) && args(id, ..)")
    public Object doAround(ProceedingJoinPoint point, VoucherLog voucherLog, int id) throws Throwable {
        Map<String, Object> map = parserManager.parseKeys(point, voucherLog.refId());
        FxVoucherLog fxVoucherLog = new FxVoucherLog()
                .setCreateTime(new Date())
                .setMaConfigId(id)
                .setRefId(StrUtil.utf8Str(CollUtil.getFirst(map.values())))
                .setRequest(voucherLog.request())
                .setType(voucherLog.type())
                .setSuccess(true)
                .setData(JSONUtil.toJsonStr(point.getArgs()));
        try {
            return point.proceed();
        } catch (Throwable e) {
            fxVoucherLog.setSuccess(false).setException(Util.cutStr(ExceptionUtil.stacktraceToString(e, 3000), 3000));
            throw e;
        } finally {
            voucherManager.saveLog(fxVoucherLog);
        }
    }
}
