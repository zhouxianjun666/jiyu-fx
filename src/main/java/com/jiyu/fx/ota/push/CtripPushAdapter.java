package com.jiyu.fx.ota.push;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.json.JSONUtil;
import com.jiyu.common.config.BigNumberSerializer;
import com.jiyu.common.config.OkHttpConfig;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxOtaPushConfig;
import com.jiyu.fx.entity.FxRefundOrder;
import com.jiyu.fx.ota.dto.CtripRequestHeader;
import com.jiyu.fx.vo.VoucherConsumeVO;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author zoujing
 * @Description: 携程消息推送处理器
 * @date 2019/3/26 14:03
 */
@Component
@Slf4j
public class CtripPushAdapter implements PushMsgAdapter{
    @Autowired
    private OkHttpClient okHttpClient;
    private static final String REFUND_SUCCESS = "REFUND_SUCCESS";
    private static final String REFUND_FAIL = "REFUND_FAIL";
    private static final String CONSUME = "CONSUME";

    /**
     * 消息推送
     */
    @Override
    @SneakyThrows
    public void pushMsg(Map<String, Object> map, FxOtaPushConfig fxOtaPushConfig, String pushCode){
        com.jiyu.fx.ota.dto.Request rsp = new com.jiyu.fx.ota.dto.Request();
        String pushUrl = fxOtaPushConfig.getUrl();
        Map<String, Object> body = new HashMap<>(10);
        Map<String, Object> request = new HashMap<>(2);
        CtripRequestHeader ctripRequestHeader = new CtripRequestHeader();
        if (REFUND_SUCCESS.equals(pushCode) || REFUND_FAIL.equals(pushCode)) {
            refundMsg(map, pushCode, body, ctripRequestHeader);
        }
        if (CONSUME.equals(pushCode)) {
            counsumeMsg(map, body, ctripRequestHeader);
        }
        Util.removeEmpty(body);
        Map<String, Object> config = JSONUtil.parseObj(fxOtaPushConfig.getConfig());
        ctripRequestHeader.setAccountId(MapUtils.getString(config, "accountId"));
        ctripRequestHeader.setRequestTime(DateUtil.formatDateTime(new Date()));
        ctripRequestHeader.setVersion(MapUtils.getString(config, "version"));
        String bodyStr = com.jiyu.fx.ota.dto.Request.encrypt(JSONUtil.toJsonStr(body), MapUtils.getString(config, "secretKey"), MapUtils.getString(config, "vector"));
        String value = ctripRequestHeader.getAccountId() + ctripRequestHeader.getServiceName() + ctripRequestHeader.getRequestTime() + bodyStr + ctripRequestHeader.getVersion();
        String jySign = DigestUtil.md5Hex(StrUtil.nullToEmpty(value) + fxOtaPushConfig.getAccessKey());
        ctripRequestHeader.setSign(jySign);
        request.put("header", ctripRequestHeader);
        request.put("body", bodyStr);
        if (CollUtil.isNotEmpty(request)) {
            request.entrySet().stream()
                    .filter(entry -> entry.getValue() instanceof Number
                            && ((Number) entry.getValue()).longValue() >= BigNumberSerializer.MAX_NUMBER)
                    .forEach(entry -> entry.setValue(String.valueOf(entry.getValue())));
        }
        String bodySign = Util.toDateTimeJson(request);
        RequestBody requestBody = RequestBody.create(OkHttpConfig.JSON, Optional.ofNullable(bodySign).orElse("{}"));
        Request httpRequest = new Request.Builder()
                .url(pushUrl)
                .method("POST", requestBody)
                .build();
        try (Response response = okHttpClient.newCall(httpRequest).execute()) {
            if (response.isSuccessful() && response.body() != null) {
                Map httpMap = JSONUtil.toBean(response.body().string(), Map.class);
                Map header = BeanUtil.toBean(httpMap.get("header"), Map.class);
                String resCode = header.get("resultCode").toString();
                String resMeg = header.get("resultMessage").toString();
                String error;
                if (!"0000".equalsIgnoreCase(resCode)) {
                    switch (resCode) {
                        case "0001":
                            error = "OTA账户信息不正确";
                            break;
                        case "0002":
                            error = "签名错误";
                            break;
                        case "0003":
                            error = "JSON解析失败";
                            break;
                        case "0004":
                            error = "请求方法为空";
                            break;
                        case "0005":
                            error = "系统异常";
                            break;
                        case "0006":
                            error = "请求数据异常";
                            break;
                        case "5001":
                        case "2001":
                            error = "携程订单号不存在";
                            break;
                        default:
                            error = resMeg;
                            break;
                    }
                    log.warn("携程推送信息URL:{} 推送失败:{}", new Object[]{pushUrl, error});
                } else {
                    log.info("推送信息URL:{} 推送成功:{}", new Object[]{pushUrl, resMeg});
                }
            }
        }
    }

    private void refundMsg (Map<String, Object> map, String pushCode, Map<String, Object> body, CtripRequestHeader ctripRequestHeader) {
        ctripRequestHeader.setServiceName("CancelOrderConfirm");
        FxOrder order = BeanUtil.toBean(map.get("order"), FxOrder.class);
        FxOrderItem orderItem = BeanUtil.toBean(map.get("orderItem"), FxOrderItem.class);
        FxRefundOrder refundOrder = BeanUtil.toBean(map.get("refundOrder"), FxRefundOrder.class);
        List<Map<String, Object>> items = new ArrayList<>();
        Map<String, Object> item = new HashMap<>(10);
        body.put("sequenceId", refundOrder.getOuterId());
        body.put("otaOrderId", order.getOuterId());
        body.put("supplierOrderId", order.getNumber());
        body.put("confirmResultCode", "REFUND_SUCCESS".equals(pushCode) ? "0000" : "0005");
        body.put("confirmResultMessage", "REFUND_SUCCESS".equals(pushCode) ? "确认成功" : "确认失败：退订审核被拒绝");
        item.put("itemId", orderItem.getOutItemId());
        items.add(item);
        body.put("items", items);
    }

    private void counsumeMsg (Map<String, Object> map, Map<String, Object> body, CtripRequestHeader ctripRequestHeader) {
        ctripRequestHeader.setServiceName("OrderConsumedNotice");
        FxOrder order = BeanUtil.toBean(map.get("order"), FxOrder.class);
        FxOrderItem orderItem = BeanUtil.toBean(map.get("item"), FxOrderItem.class);
        VoucherConsumeVO voucher = BeanUtil.toBean(map.get("vo"), VoucherConsumeVO.class);
        body.put("sequenceId", MapUtils.getString(map, "serialNo"));
        body.put("otaOrderId", order.getOuterId());
        body.put("supplierOrderId", order.getNumber());
        List<Map<String, Object>> items = new ArrayList<>();
        Map<String, Object> item = new HashMap<>(10);
        item.put("itemId", orderItem.getOutItemId());
        item.put("useStartDate", DateUtil.format(voucher.getTime(), "yyyy-MM-dd"));
        item.put("useEndDate", DateUtil.format(voucher.getTime(), "yyyy-MM-dd"));
        item.put("quantity", orderItem.getQuantity());
        item.put("useQuantity", voucher.getQuantity());
        items.add(item);
        body.put("items", items);
    }

    @Override
    public String otaType() {
        return "Ctrip";
    }
}
