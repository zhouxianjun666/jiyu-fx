package com.jiyu.fx.ota.push;

import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOtaPushConfig;

import java.util.Map;

public interface PushMsgAdapter {

    /**
     * 消息推送
     */
    void pushMsg(Map<String, Object> map, FxOtaPushConfig fxOtaPushConfig, String pushCode);

    /**
     * 推送OTA类型
     * @return
     */
    String otaType();
}
