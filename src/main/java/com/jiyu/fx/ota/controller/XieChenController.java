package com.jiyu.fx.ota.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.base.manager.UserManager;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.entity.base.UserIdentity;
import com.jiyu.fx.entity.FxCtripConfig;
import com.jiyu.fx.mapper.FxCtripConfigMapper;
import com.jiyu.fx.ota.adapter.CtripVerifyOrderNotifyAdapter;
import com.jiyu.fx.ota.dto.CtripRequestHeader;
import com.jiyu.fx.ota.dto.CtripResponseHeader;
import com.jiyu.fx.ota.dto.Request;
import com.jiyu.fx.ota.vo.CtripRequestVO;
import io.swagger.annotations.Api;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zoujing
 * @date 2019/3/26 10:34
 */
@Api(tags = "对接携程")
@Controller
@RequestMapping("client")
@Slf4j
public class XieChenController {
    private Map<String, CtripVerifyOrderNotifyAdapter> ctripVerifyOrderNotifyAdapterMap;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private UserManager userManager;

    @Resource
    private FxCtripConfigMapper fxCtripConfigMapper;

    @PostConstruct
    public void init() {
        ctripVerifyOrderNotifyAdapterMap = applicationContext.getBeansOfType(CtripVerifyOrderNotifyAdapter.class).values().stream().collect(
                Collectors.toMap(CtripVerifyOrderNotifyAdapter::type, Function.identity())
        );
    }

    private Map<String, FxCtripConfig> ctripConfigMap = new ConcurrentHashMap<>(1);

    private Map<Integer, AuthUser> userMap = new ConcurrentHashMap<>(1);

    @RequestMapping(value = "ctrip", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @SneakyThrows
    public void entrance(@RequestBody CtripRequestVO vo, HttpServletResponse response) {
        Request rsp = new Request();
        CtripResponseHeader ctripResponseHeader = new CtripResponseHeader();
        log.info("接收到携程请求{}",vo);
        CtripRequestHeader requestHeader = vo.getHeader();
        String accountId = requestHeader.getAccountId();
        FxCtripConfig ctripConfig = ctripConfigMap.computeIfAbsent(accountId, k -> fxCtripConfigMapper.selectOne(new FxCtripConfig().setAccountId(accountId)));
        if (ctripConfig == null) {
            log.error("供应商账户信息不正确{}", accountId);
            rsp.setHeader(ctripResponseHeader.setResultCode("0003").setResultMessage("供应商账户信息不正确"));
            rsp.write(response, null);
            return;
        }
        String decrypt = decrypt(vo.getBody(), ctripConfig.getSecretKey(), ctripConfig.getVector());
        if (decrypt == null) {
            log.info("报文解析失败");
            rsp.setHeader(ctripResponseHeader.setResultCode("0001").setResultMessage("报文解析失败"));
            rsp.write(response, ctripConfig);
            return;
        }
        JSONObject bodyMap = JSONUtil.parseObj(decrypt);
        String ctripSign = requestHeader.getSign();
        String value = requestHeader.getAccountId() + requestHeader.getServiceName() + requestHeader.getRequestTime() + vo.getBody() + requestHeader.getVersion();
        Integer merchantId = ctripConfig.getMerchantId();
        AuthUser user = userMap.computeIfAbsent(merchantId, k -> userManager.getAuthUser(b -> b, () -> JoinExample.Where.custom()
                .andEqualTo(UserIdentity::getMerchantId, k)
                .andEqualTo(UserIdentity::getType, Constant.UserIdentityType.EP_ADMIN)));
        String data = StrUtil.nullToEmpty(value) + ctripConfig.getAccessKey();
        String jySign = DigestUtil.md5Hex(data);
        if (!ctripSign.equals(jySign)) {
            log.info("签名错误: {} == {} == {}", ctripSign, jySign, data);
            rsp.setHeader(ctripResponseHeader.setResultCode("0002").setResultMessage("签名错误"));
            rsp.write(response, ctripConfig);
            return;
        }
        if (!ctripVerifyOrderNotifyAdapterMap.containsKey(requestHeader.getServiceName())) {
            log.warn("找不到处理适配器:{}", requestHeader.getServiceName());
            rsp.setHeader(ctripResponseHeader.setResultCode("0005").setResultMessage("找不到处理适配器:" + requestHeader.getServiceName()));
            rsp.write(response, ctripConfig);
            return;
        }
        ctripVerifyOrderNotifyAdapterMap.get(requestHeader.getServiceName()).process(bodyMap, user, rsp);
        rsp.write(response, ctripConfig);
    }

    private static String decrypt(String decData, String secretKey, String vector) {
        try {
            byte[] raw = secretKey.getBytes("utf-8");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(vector.getBytes("utf-8"));
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] encrypted1 = decodeBytes(decData);
            byte[] original = cipher.doFinal(encrypted1);
            return new String(original, "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 转字节数组
     *
     * @param str
     * @return
     */
    private static byte[] decodeBytes(String str) {
        byte[] bytes = new byte[str.length() / 2];
        for (int i = 0; i < str.length(); i += 2) {
            char c = str.charAt(i);
            bytes[i / 2] = (byte) ((c - 'a') << 4);
            c = str.charAt(i + 1);
            bytes[i / 2] += (c - 'a');
        }
        return bytes;
    }
}
