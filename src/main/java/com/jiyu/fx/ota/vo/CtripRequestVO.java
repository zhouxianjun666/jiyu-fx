package com.jiyu.fx.ota.vo;

import com.jiyu.fx.ota.dto.CtripRequestHeader;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CtripRequestVO {
    private CtripRequestHeader header;
    private String body;
}
