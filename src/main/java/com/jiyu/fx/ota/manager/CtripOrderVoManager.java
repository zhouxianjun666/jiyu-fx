package com.jiyu.fx.ota.manager;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.jiyu.common.Constant;
import com.jiyu.fx.vo.BookingItemVO;
import com.jiyu.fx.vo.BookingVO;
import com.jiyu.fx.vo.BookingVisitorVO;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zoujing
 * @date 2019/3/26 14:42
 */
@Component
public class CtripOrderVoManager {
    public BookingVO ctripOrderVo (Map params, JSONObject item, Map<String, Object> contact, String type) {
        BookingVO vo = new BookingVO();
        try {
            JSONObject passenger = item.getJSONArray("passengers").getJSONObject(0);
            BookingItemVO bookingItemVO = new BookingItemVO();
            BookingVisitorVO shipping = new BookingVisitorVO();
            BookingVisitorVO visitor = new BookingVisitorVO();
            List<BookingItemVO> list = new ArrayList<>();
            List<BookingVisitorVO> visitorList = new ArrayList<>();
            shipping.setName(MapUtils.getString(contact, "name"));
            shipping.setPhone(MapUtils.getString(contact, "mobile"));
            visitor.setName(MapUtils.getString(passenger, "name"));
            visitor.setPhone(MapUtils.getString(passenger, "mobile"));
            visitor.setCardType(("1").equals(MapUtils.getString(passenger, "cardType")) ? 1 : 2);
            visitor.setCardNo(MapUtils.getString(passenger, "cardNo"));
            visitor.setQuantity(MapUtils.getInteger(item, "quantity"));
            visitorList.add(visitor);
            bookingItemVO.setQuantity(MapUtils.getInteger(item, "count"));
            bookingItemVO.setProductSubNumber(MapUtils.getLong(item, "PLU"));
            bookingItemVO.setVisitors(visitorList);
            String useDate = MapUtils.getString(item, "useStartDate");
            bookingItemVO.setBooking(new SimpleDateFormat("yyyy-MM-dd").parse(useDate));
            list.add(bookingItemVO);
            vo.setShipping(shipping);
            vo.setItems(list);
            vo.setSource(Constant.OrderSource.OTA);
            vo.setRollback(("VerifyOrder").equals(type));
            vo.setCredit(false);
            vo.setOuterId(("VerifyOrder").equals(type) ? null : MapUtils.getString(params, "otaOrderId"));
            vo.setOutItemId(MapUtils.getString(item, "itemId"));
            return vo;
        } catch (Exception e) {
            return vo;
        }
    }
}
