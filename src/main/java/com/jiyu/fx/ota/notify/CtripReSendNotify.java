package com.jiyu.fx.ota.notify;

import cn.hutool.json.JSONObject;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Config;
import com.jiyu.common.dto.Result;
import com.jiyu.common.exception.ResultRuntimeException;
import com.jiyu.fx.dto.OrderDetailRequest;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.manager.SmsNotifyManager;
import com.jiyu.fx.manager.TicketManager;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.ota.adapter.CtripVerifyOrderNotifyAdapter;
import com.jiyu.fx.ota.dto.CtripResponseHeader;
import com.jiyu.fx.ota.dto.Request;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author zoujing
 * @date 2019/3/26 14:42
 */
@Component
@Slf4j
public class CtripReSendNotify implements CtripVerifyOrderNotifyAdapter {

    @Resource
    private FxOrderItemMapper orderItemMapper;

    @Autowired
    private Config config;

    @Autowired
    private SmsNotifyManager smsNotifyManager;

    @Autowired
    private TicketManager ticketManager;

    @Autowired
    private OrderManager orderManager;

    @Override
    public void process (JSONObject params, AuthUser user, Request rsp) {
        CtripResponseHeader ctripResponseHeader = new CtripResponseHeader();
        Result rs = null;
        try {
            OrderDetailRequest request = new OrderDetailRequest();
            request.setOuterId(MapUtils.getString(params, "otaOrderId"));
            request.setNumber(MapUtils.getLong(params, "supplierOrderId"));
            request.setShipping(false);
            FxOrder order = orderManager.detail(request, user, null);
            FxOrderItem orderItem = orderItemMapper.selectOne(new FxOrderItem().setOrderNumber(order.getNumber()));
            String phone = MapUtils.getString(params, "mobile");
            if (orderItem == null) {
                ctripResponseHeader.setResultCode("5001");
                ctripResponseHeader.setResultMessage("订单号不存在");
                rsp.setHeader(ctripResponseHeader);
                return;
            }
            if (phone == null) {
                ctripResponseHeader.setResultCode("5100");
                ctripResponseHeader.setResultMessage("手机号码为空");
                rsp.setHeader(ctripResponseHeader);
                return;
            }
            if (orderItem.getStatus().equals(3) || orderItem.getStatus().equals(4)) {
                if (orderItem.getLastRefundId() != null) {
                    ctripResponseHeader.setResultCode("5100");
                    ctripResponseHeader.setResultMessage("订单状态不正确不能重发");
                    rsp.setHeader(ctripResponseHeader);
                    return;
                }
                rs = ticketManager.sendTicket(orderItem);
            } else if (orderItem.getStatus().equals(6)) {
                int ret = orderItemMapper.resend(orderItem.getId(), config.getResendVoucherSmsMax(), config.getResendVoucherSmsInterval());
                if (ret <= 0) {
                    ctripResponseHeader.setResultCode("5002");
                    ctripResponseHeader.setResultMessage("重发已经超过最大次数");
                    rsp.setHeader(ctripResponseHeader);
                    return;
                }
                rs = smsNotifyManager.voucher(orderItem, phone);
            }
            if (rs.isSuccess()) {
                ctripResponseHeader.setResultCode("0000");
                ctripResponseHeader.setResultMessage("重发成功");
                rsp.setHeader(ctripResponseHeader);
            } else {
                ctripResponseHeader.setResultCode("5100");
                ctripResponseHeader.setResultMessage(rs.getMsg());
                rsp.setHeader(ctripResponseHeader);
            }
        } catch (ResultRuntimeException exception) {
            ctripResponseHeader.setResultCode("5100");
            ctripResponseHeader.setResultMessage(exception.getMessage());
            rsp.setHeader(ctripResponseHeader);
        }
    }

    @Override
    public String type() {
        return "SendVoucher";
    }
}
