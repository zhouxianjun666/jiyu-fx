package com.jiyu.fx.ota.notify;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.adapter.order.BookingOrder;
import com.jiyu.fx.ota.adapter.CtripVerifyOrderNotifyAdapter;
import com.jiyu.fx.ota.dto.Request;
import com.jiyu.fx.ota.dto.CtripResponseHeader;
import com.jiyu.fx.ota.manager.CtripOrderVoManager;
import com.jiyu.fx.service.ScenicProductService;
import com.jiyu.fx.vo.BookingVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zoujing
 * @date 2019/3/26 14:42
 */
@Component
@Slf4j
public class CtripVerifyOrderNotify implements CtripVerifyOrderNotifyAdapter {
    private Map<String, BookingOrder> bookingOrderMap = Collections.emptyMap();
    @Autowired
    private ApplicationContext applicationContext;
    @PostConstruct
    public void init() {
        bookingOrderMap = applicationContext.getBeansOfType(BookingOrder.class).values().stream()
                .collect(Collectors.toMap(BookingOrder::type, Function.identity()));
    }

    @Autowired
    private ScenicProductService productService;

    @Autowired
    private CtripOrderVoManager ctripOrderVoManager;

    @Override
    public void process (JSONObject params, AuthUser user, Request rsp) {
        CtripResponseHeader ctripResponseHeader = new CtripResponseHeader();
        String resultCode = "";
        String resultMessage = "";
        try {
            JSONObject item = params.getJSONArray("items").getJSONObject(0);
            JSONObject contact = params.getJSONArray("contacts").getJSONObject(0);
            if (item.get("PLU") == null || contact.get("name") == null
                    || contact.get("mobile") == null || item.get("useStartDate") == null) {
                ctripResponseHeader.setResultCode("1005");
                ctripResponseHeader.setResultMessage("参数为空");
                rsp.setHeader(ctripResponseHeader);
                return;
            }
            Result productResult = productService.selectSubProDetailByCode(MapUtils.getLong(item, "PLU"));
            if (productResult.getValue() == null) {
                ctripResponseHeader.setResultCode("1001");
                ctripResponseHeader.setResultMessage("产品不存在");
                rsp.setHeader(ctripResponseHeader);
                return;
            }
            BookingVO vo = ctripOrderVoManager.ctripOrderVo(params, item, contact, type());
            BookingOrder bookingOrder = bookingOrderMap.get("scenic");
            Result result = bookingOrder.validate(vo, user);
            if (result.isSuccess()) {
                ctripResponseHeader.setResultCode("0000");
                ctripResponseHeader.setResultMessage("验证成功，可以下单");
            } else {
                switch (result.getCode()) {
                    case 10009:
                        resultCode = "1002";
                        resultMessage = "产品已经下架";
                        break;
                    case 10007:
                        resultCode = "1003";
                        resultMessage = "库存不足";
                        break;
                    case 10010:
                        resultCode = "1007";
                        resultMessage = "产品价格不存在,当前日期没用销售计划";
                        break;
                    case 10011:
                        resultCode = "1008";
                        resultMessage = "账户余额不足";
                        break;
                    default:
                        resultCode = "1100";
                        resultMessage = result.getMsg();
                        break;
                }
                ctripResponseHeader.setResultCode(resultCode);
                ctripResponseHeader.setResultMessage(resultMessage);
            }
        } catch (Exception e) {
            ctripResponseHeader.setResultCode("0005");
            ctripResponseHeader.setResultMessage("系统异常：" + e.getMessage());
        }
        rsp.setHeader(ctripResponseHeader);
    }

    @Override
    public String type() {
        return "VerifyOrder";
    }
}
