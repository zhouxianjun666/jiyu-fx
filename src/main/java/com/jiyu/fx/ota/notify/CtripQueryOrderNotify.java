package com.jiyu.fx.ota.notify;

import cn.hutool.json.JSONObject;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.exception.ResultRuntimeException;
import com.jiyu.fx.dto.OrderDetailRequest;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxRefundOrder;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxRefundOrderMapper;
import com.jiyu.fx.ota.adapter.CtripVerifyOrderNotifyAdapter;
import com.jiyu.fx.ota.dto.CtripResponseHeader;
import com.jiyu.fx.ota.dto.Request;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zoujing
 * @date 2019/3/26 14:42
 */
@Component
@Slf4j
public class CtripQueryOrderNotify implements CtripVerifyOrderNotifyAdapter {
    @Autowired
    private OrderManager orderManager;

    @Resource
    private FxOrderItemMapper fxOrderItemMapper;

    @Resource
    private FxRefundOrderMapper fxRefundOrderMapper;

    @Override
    public void process (JSONObject params, AuthUser user, Request rsp) {
        CtripResponseHeader ctripResponseHeader = new CtripResponseHeader();
        Map<String, Object> body = new HashMap<>(10);
        try {
            OrderDetailRequest request = new OrderDetailRequest();
            request.setNumber(MapUtils.getLong(params, "supplierOrderId"));
            request.setShipping(false);
            FxOrder order = orderManager.detail(request, user, null);
            if (order == null) {
                rsp.setHeader(ctripResponseHeader.setResultCode("4001").setResultMessage("订单号不存在"));
                return;
            }
            if (!order.getOuterId().equals(MapUtils.getString(params, "otaOrderId"))) {
                rsp.setHeader(ctripResponseHeader.setResultCode("4001").setResultMessage("携程订单号异常"));
                return;
            }
            body.put("otaOrderId", order.getOuterId());
            body.put("supplierOrderId", order.getNumber());
            FxOrderItem fxOrderItem = fxOrderItemMapper.selectOne(new FxOrderItem().setOrderNumber(order.getNumber()));
            int orderStatus = parseStatus(fxOrderItem);
            List<Map<String, Object>> items = new ArrayList<>();
            Map<String, Object> map = new HashMap<>(10);
            map.put("itemId", fxOrderItem.getOutItemId());
            map.put("orderStatus", orderStatus);
            map.put("quantity", fxOrderItem.getQuantity());
            map.put("useQuantity", fxOrderItem.getUsedQuantity());
            map.put("cancelQuantity", fxOrderItem.getRefundQuantity());
            items.add(map);
            body.put("items", items);
            rsp.setHeader(ctripResponseHeader.setResultCode("0000").setResultMessage("查询成功"));
            rsp.setBody(body);
        } catch (ResultRuntimeException exception) {
            ctripResponseHeader.setResultMessage(exception.getMessage());
            rsp.setHeader(ctripResponseHeader.setResultCode("4100").setResultMessage(exception.getMessage()));
        }
    }

    private int parseStatus(FxOrderItem item) {
        if (item.getStatus() == Constant.OrderItemStatus.WAIT_AUDIT) {
            return 1;
        }
        if (item.getLastRefundId() != null) {
            FxRefundOrder fxRefundOrder = fxRefundOrderMapper.selectByPrimaryKey(item.getLastRefundId());
            if (fxRefundOrder.getStatus() == Constant.RefundOrderStatus.AUDIT_WAIT) {
                return 3;
            }
        }
        int quantity = item.getQuantity() * item.getDays();
        if (quantity == item.getRefundQuantity()) {
            return 5;
        }
        if (quantity == item.getUsedQuantity()) {
            return 8;
        }
        if (item.getRefundQuantity() > 0 && quantity > item.getRefundQuantity()) {
            return 4;
        }
        if (item.getUsedQuantity() > 0 && quantity > item.getUsedQuantity()) {
            return 7;
        }
        if (item.getInvalidQuantity() == quantity - item.getRefundQuantity() - item.getUsedQuantity()) {
            return 10;
        }
        return 2;
    }

    @Override
    public String type() {
        return "QueryOrder";
    }
}
