package com.jiyu.fx.ota.notify;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.exception.ResultRuntimeException;
import com.jiyu.fx.adapter.order.BookingOrder;
import com.jiyu.fx.dto.OrderDetailRequest;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxRefundOrder;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.manager.RefundManager;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxRefundOrderMapper;
import com.jiyu.fx.ota.adapter.CtripVerifyOrderNotifyAdapter;
import com.jiyu.fx.ota.dto.CtripResponseHeader;
import com.jiyu.fx.ota.dto.Request;
import com.jiyu.fx.vo.RefundVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zoujing
 * @date 2019/3/26 14:42
 */
@Component
@Slf4j
public class CtripCancelOrderNotify implements CtripVerifyOrderNotifyAdapter {
    private Map<String, BookingOrder> bookingOrderMap = Collections.emptyMap();
    @Autowired
    private ApplicationContext applicationContext;

    @Resource
    private FxOrderItemMapper fxOrderItemMapper;

    @Resource
    private FxRefundOrderMapper fxRefundOrderMapper;

    @Autowired
    private OrderManager orderManager;

    @Autowired
    private RefundManager refundManager;
    @PostConstruct
    public void init() {
        bookingOrderMap = applicationContext.getBeansOfType(BookingOrder.class).values().stream()
                .collect(Collectors.toMap(BookingOrder::type, Function.identity()));
    }


    @Override
    public void process (JSONObject params, AuthUser user, Request rsp) {
        CtripResponseHeader ctripResponseHeader = new CtripResponseHeader();
        Map<String, Object> body = new HashMap<>(5);
        RefundVO vo = new RefundVO();
        FxRefundOrder refundOrder;
        try {
            Map<String, Object> item = params.getJSONArray("items").getJSONObject(0);
            OrderDetailRequest request = new OrderDetailRequest();
            request.setNumber(MapUtils.getLong(params, "supplierOrderId"));
            request.setShipping(false);
            FxOrder order = orderManager.detail(request, user, null);
            if (order == null) {
                rsp.setHeader(ctripResponseHeader.setResultCode("2001").setResultMessage("取消失败：该订单号不存在"));
                return;
            }
            if (!order.getOuterId().equals(MapUtils.getString(params, "otaOrderId"))) {
                rsp.setHeader(ctripResponseHeader.setResultCode("2001").setResultMessage("取消失败：携程订单号异常"));
                return;
            }
            FxOrderItem fxOrderItem = fxOrderItemMapper.selectOne(new FxOrderItem().setOrderNumber(order.getNumber()));
            FxRefundOrder refOrder = fxRefundOrderMapper.selectOne(new FxRefundOrder().setOrderItemId(fxOrderItem.getId()));
            if (refOrder != null) {
                body.put("supplierConfirmType", refOrder.getStatus().equals(1) ? 2 : 1);
                rsp.setBody(body);
                rsp.setHeader(ctripResponseHeader.setResultCode("0000").setResultMessage("取消成功"));
                return;
            }
            vo.setOuterId(MapUtils.getString(params, "sequenceId"));
            vo.setNumber(fxOrderItem.getNumber());
            vo.setQuantity(MapUtils.getInteger(item, "quantity"));
            if (fxOrderItem.getQuantity().equals(fxOrderItem.getUsedQuantity())) {
                rsp.setHeader(ctripResponseHeader.setResultCode("2002").setResultMessage("取消失败：该订单已经使用"));
                return;
            }
            if ("2".equals(MapUtils.getString(params, "confirmType"))) {
                Integer canRefund = fxOrderItem.getQuantity() - fxOrderItem.getRefundQuantity() - fxOrderItem.getUsedQuantity();
                if (canRefund < vo.getQuantity()) {
                    rsp.setHeader(ctripResponseHeader.setResultCode("2004").setResultMessage("取消失败：可退数量小于申请退订数量"));
                    return;
                }
            }
            Result result = refundManager.apply(vo, "otaRefund", user);
            if (result.isSuccess()) {
                if (result.getValue() instanceof Boolean) {
                    refundOrder = BeanUtil.toBean(((Map) result.getData()).get("refundOrder"), FxRefundOrder.class);
                } else {
                    refundOrder = BeanUtil.toBean(result.getValue(), FxRefundOrder.class);
                }
                body.put("supplierConfirmType", refundOrder.getStatus().equals(1) ? 2 : 1);
                ctripResponseHeader.setResultCode("0000").setResultMessage("取消成功");
            } else {
                ctripResponseHeader.setResultCode("2100").setResultMessage("取消失败");
            }
            rsp.setBody(body);
            rsp.setHeader(ctripResponseHeader);
        } catch (ResultRuntimeException exception) {
            rsp.setHeader(ctripResponseHeader.setResultCode("2100").setResultMessage(exception.getMessage()));
        }
    }

    @Override
    public String type() {
        return "CancelOrder";
    }
}
