package com.jiyu.fx.ota.notify;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.exception.ResultRuntimeException;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.manager.BookingManager;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.ota.adapter.CtripVerifyOrderNotifyAdapter;
import com.jiyu.fx.ota.dto.CtripResponseHeader;
import com.jiyu.fx.ota.dto.Request;
import com.jiyu.fx.ota.manager.CtripOrderVoManager;
import com.jiyu.fx.service.ScenicProductService;
import com.jiyu.fx.vo.BookingVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zoujing
 * @date 2019/3/26 14:42
 */
@Component
@Slf4j
public class CtripCreateOrderNotify implements CtripVerifyOrderNotifyAdapter {
    @Autowired
    private BookingManager bookingManager;
    @Autowired
    private CtripOrderVoManager ctripOrderVoManager;
    @Resource
    private FxOrderItemMapper fxOrderItemMapper;
    @Autowired
    private ScenicProductService productService;

    @Override
    public void process (JSONObject params, AuthUser user, Request rsp  ) {
        params.getJSONArray("contacts");
        CtripResponseHeader ctripResponseHeader = new CtripResponseHeader();
        Map<String, Object> body = new HashMap<>(10);
        try {
            JSONObject item = params.getJSONArray("items").getJSONObject(0);
            JSONObject contact = params.getJSONArray("contacts").getJSONObject(0);
            Result productResult = productService.selectSubProDetailByCode(MapUtils.getLong(item, "PLU"));
            if (productResult.getValue() == null) {
                rsp.setHeader(ctripResponseHeader.setResultCode("1001").setResultMessage("产品不存在"));
                return;
            }
            JSONArray passengersList = item.getJSONArray("passengers");
            if (passengersList.size() <= 0) {
                rsp.setHeader(ctripResponseHeader.setResultCode("1005").setResultMessage("缺少出行人信息"));
                return;
            }
            if (passengersList.size() > 1) {
                rsp.setHeader(ctripResponseHeader.setResultCode("1003").setResultMessage("库存不足"));
                return;
            }
            JSONObject passenger = passengersList.getJSONObject(0);
            String cardNo = passenger.getStr("cardNo");
            if (cardNo.isEmpty()) {
                rsp.setHeader(ctripResponseHeader.setResultCode("1005").setResultMessage("出行人缺少证件"));
                return;
            }
            BookingVO vo = ctripOrderVoManager.ctripOrderVo(params, item, contact, type());
            vo.setRemark(MapUtils.getString(item, "remark"));
            Result result =  bookingManager.create(vo, "syncScenic", user);
            FxOrder order;
            FxOrderItem fxOrderItem;
            if (result.isSuccess()) {
                if (result.getValue() instanceof Boolean) {
                    order = BeanUtil.toBean(((Map) result.getData()).get("order"), FxOrder.class);
                    fxOrderItem = fxOrderItemMapper.selectOne(new FxOrderItem().setOrderNumber(order.getNumber()));
                } else {
                    List list = (List) result.getData().get("items");
                    fxOrderItem = BeanUtil.toBean(list.get(0), FxOrderItem.class);
                    order = BeanUtil.toBean(result.getValue(), FxOrder.class);
                }
                body.put("otaOrderId", order.getOuterId());
                body.put("supplierOrderId", order.getNumber());
                body.put("supplierConfirmType", fxOrderItem.getStatus().equals(1) ? 2 : 1);
                body.put("voucherSender", 2);
                ctripResponseHeader.setResultCode("0000");
                ctripResponseHeader.setResultMessage("下单成功");
                rsp.setHeader(ctripResponseHeader);
                rsp.setBody(body);
            } else {
                ctripResponseHeader.setResultCode("1100");
                ctripResponseHeader.setResultMessage(result.getMsg());
                rsp.setHeader(ctripResponseHeader);
            }
        } catch (ResultRuntimeException exception) {
            ctripResponseHeader.setResultCode("1100");
            ctripResponseHeader.setResultMessage(exception.getMessage());
            rsp.setHeader(ctripResponseHeader);
        }
    }

    @Override
    public String type() {
        return "CreateOrder";
    }
}
