package com.jiyu.fx.ota.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zoujing
 * @date 2019/3/26 10:34
 */
@Data
@Accessors(chain = true)
public class CtripResponseHeader {

    /**
     * 返回参数码
     */
    private String resultCode;

    /**
     * 返回消息
     */
    private String resultMessage;
}
