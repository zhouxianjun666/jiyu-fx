package com.jiyu.fx.ota.dto;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zoujing
 */
@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(value = "ctrip")
public class CtripCfg {
    private String merchantId;
    private String accessKey;
    private String accountId;
    private String secretKey;
    private String vector;

    public boolean equal(CtripCfg config) {
        return StrUtil.nullToEmpty(getMerchantId()).equals(StrUtil.nullToEmpty(config.getMerchantId())) &&
                StrUtil.nullToEmpty(getAccessKey()).equals(StrUtil.nullToEmpty(config.getAccessKey())) &&
                StrUtil.nullToEmpty(getAccountId()).equals(StrUtil.nullToEmpty(config.getAccountId())) &&
                StrUtil.nullToEmpty(getSecretKey()).equals(StrUtil.nullToEmpty(config.getSecretKey())) &&
                StrUtil.nullToEmpty(getVector()).equals(StrUtil.nullToEmpty(config.getVector()));
    };
}
