package com.jiyu.fx.ota.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zoujing
 * @date 2019/3/26 10:34
 */
@Data
@Accessors(chain = true)
public class CtripRequestHeader {

    /**
     * OTA分配给供应商的账户
     */
    private String accountId;

    /**
     * OTA请求接口名称
     */
    private String serviceName;

    /**
     * OTA请求时间
     */
    private String requestTime;

    /**
     * 版本号
     */
    private String version = "1.0";

    /**
     * 加密Sign
     */
    private String sign ;
}
