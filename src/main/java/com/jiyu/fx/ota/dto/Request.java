package com.jiyu.fx.ota.dto;

import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.json.JSONUtil;
import com.jiyu.fx.entity.FxCtripConfig;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

/**
 * @author zoujing
 * @date 2019/3/26 10:34
 */
@Data
public class Request {
    @ApiModelProperty("请求报文头")
    private Object header;

    @ApiModelProperty("请求体")
    private Object body;

    public void write (HttpServletResponse response, FxCtripConfig ctripConfig) {
        aes(ctripConfig);
        ServletUtil.write(response, JSONUtil.toJsonStr(this), "application/json;charset=UTF-8");
    }

    public void aes (FxCtripConfig ctripConfig) {
        if (body != null){
            this.setBody(encrypt(JSONUtil.toJsonStr(this.getBody()), ctripConfig.getSecretKey(), ctripConfig.getVector()));
        }
    }

    public static String encrypt(String encData, String secretKey, String vector) {
        try {
            byte[] raw = secretKey.getBytes("utf-8");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            // "算法/模式/补码方式"
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            // 使用 CBC模式，需要一个向量 iv，可增加加密算法的强度
            IvParameterSpec iv = new IvParameterSpec(vector.getBytes("utf-8"));
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(encData.getBytes("utf-8"));
            return encodeBytes(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 转 16 进制
     *
     * @param bytes
     * @return
     */
    public static String encodeBytes(byte[] bytes) {
        StringBuffer strBuf = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            strBuf.append((char) (((bytes[i] >> 4) & 0xF) + ((int) 'a')));
            strBuf.append((char) (((bytes[i]) & 0xF) + ((int) 'a')));
        }
        return strBuf.toString();
    }
}
