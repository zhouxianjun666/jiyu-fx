package com.jiyu.fx.ota.adapter;

import cn.hutool.json.JSONObject;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.fx.ota.dto.Request;

import java.util.Map;

/**
 * @author zoujing
 * @ClassName:
 * @Description: 携程接受请求适配器
 * @date 2019/3/26 14:03
 */
public interface CtripVerifyOrderNotifyAdapter {

    /**
     * 处理携程验证订单请求
     * @param params 请求参数
     */
    void process(JSONObject params, AuthUser user, Request rsp);

    /**
     * 请求类型
     * @return
     */
    String type();
}
