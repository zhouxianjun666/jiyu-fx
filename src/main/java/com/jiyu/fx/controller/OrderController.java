package com.jiyu.fx.controller;

import com.jiyu.base.mapper.MemberMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Config;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Member;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.OrderDetailRequest;
import com.jiyu.fx.dto.SearchOrder;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxOrderItemVisitor;
import com.jiyu.fx.manager.*;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxOrderItemVisitorMapper;
import com.jiyu.fx.mapper.FxOrderMapper;
import com.jiyu.fx.vo.BookingVO;
import com.jiyu.fx.vo.RefundVO;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/16 12:48
 */
@Api(tags = "订单接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/fx/order")
public class OrderController {
    @Resource
    private FxOrderMapper orderMapper;
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private MemberMapper memberMapper;
    @Resource
    private FxOrderItemVisitorMapper orderItemVisitorMapper;
    @Autowired
    private BookingManager bookingManager;
    @Autowired
    private RefundManager refundManager;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private TicketManager ticketManager;
    @Autowired
    private Config config;
    @Autowired
    private SmsNotifyManager smsNotifyManager;

    @ApiOperation(value = "创建订单", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("create/{type}")
    public Result<?> create(@ApiParam(required = true) @PathVariable String type,
                            @ApiParam(required = true) @RequestBody @Validated BookingVO vo) {
        return bookingManager.create(vo, type, Util.getUser());
    }

    @ApiOperation(value = "获取支付信息", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("payment/info/{number}")
    public Result<?> paymentInfo(@ApiParam(required = true) @PathVariable long number) {
        FxOrder order = orderMapper.selectOne(new FxOrder().setNumber(number));
        if (order == null) {
            return Result.fail("订单不存在");
        }
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderId(order.getId()));
        Member shipping = memberMapper.selectByPrimaryKey(order.getShippingMemberId());
        List<FxOrderItemVisitor> visitors = orderItemVisitorMapper.select(new FxOrderItemVisitor().setOrderItemId(items.get(0).getId()));
        return Result.ok(order).put("items", items).put("shipping", shipping).put("visitors", visitors);
    }

    @ApiOperation(value = "订单支付", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("payment/{number}")
    @Locks("#{#number}")
    public Result<?> payment(@ApiParam(required = true) @PathVariable long number,
                             @ApiParam(required = true) @RequestParam int type,
                             HttpServletRequest request) {
        FxOrder order = orderMapper.selectOne(new FxOrder().setNumber(number));
        if (order == null) {
            return Result.fail("订单不存在");
        }
        AuthUser user = Util.getUser();
        Integer merchantId = user.getMerchant().getId();
        if (!Objects.equals(order.getBuyMerchantId(), merchantId)) {
            return Result.fail("当前商户无法支付该订单");
        }
        return orderManager.payment(order, type, request, null);
    }

    @ApiOperation(value = "子订单审核", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("audit/{number}")
    @Locks("#{#number}")
    public Result<?> audit(@ApiParam(required = true, value = "子订单编号") @PathVariable long number,
                           @ApiParam(required = true) @RequestParam boolean success) {
        FxOrderItem item = orderItemMapper.selectOne(new FxOrderItem().setNumber(number));
        if (item == null) {
            return Result.fail("订单不存在");
        }
        if (item.getStatus() != Constant.OrderItemStatus.WAIT_AUDIT) {
            return Result.fail("该订单不在待审核状态");
        }
        AuthUser user = Util.getUser();
        Integer merchantId = user.getMerchant().getId();
        if (!Objects.equals(item.getSupplierMerchantId(), merchantId)) {
            return Result.fail("当前商户无法审核该订单");
        }
        return orderManager.audit(item, success);
    }

    @ApiOperation(value = "退订申请", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("refund/apply/{type}")
    public Result<?> refundApply(@ApiParam(required = true) @PathVariable String type,
                                 @ApiParam(required = true) @RequestBody @Validated RefundVO vo) {
        return refundManager.apply(vo, type, Util.getUser());
    }

    @ApiOperation(value = "退订审核", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("refund/audit/{number}")
    public Result<?> refundAudit(@ApiParam(required = true, value = "退订订单编号") @PathVariable long number,
                                 @ApiParam(required = true) @RequestParam boolean success) {
        return refundManager.audit(number, success, Util.getUser());
    }

    @ApiOperation(value = "订单列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")
    })
    public Result<?> list(@Valid SearchOrder searchOrder, Integer pageNum, Integer pageSize) {
        searchOrder.setShipping(true);
        return Result.ok(orderManager.list(searchOrder, Util.getUser(), pageNum, pageSize));
    }

    @ApiOperation(value = "订单详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("detail")
    public Result<?> detail(@Valid OrderDetailRequest request) {
        return Result.ok(orderManager.detail(request, Util.getUser(), null));
    }

    @ApiOperation(value = "出票", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("ticketing/{number}")
    @Locks("#{#number}")
    public Result<?> ticketing(@ApiParam(required = true, value = "子订单编号") @PathVariable long number) {
        FxOrderItem orderItem = orderItemMapper.selectOne(new FxOrderItem().setNumber(number));
        if (orderItem == null) {
            return Result.fail("订单不存在");
        }
        if (orderItem.getStatus() != Constant.OrderItemStatus.NON_SEND &&
                orderItem.getStatus() != Constant.OrderItemStatus.TICKET_FAIL) {
            return Result.fail("当前状态不能出票");
        }
        if (orderItem.getLastRefundId() != null) {
            return Result.fail("该子订单已发起退票");
        }
        return ticketManager.sendTicket(orderItem);
    }

    @ApiOperation(value = "重发短信", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("voucher-sms/resend/{number}")
    @Locks("#{#number}")
    public Result<?> resendVoucherSms(@ApiParam(required = true, value = "子订单编号") @PathVariable long number,
                                      @ApiParam String phone) {
        FxOrderItem orderItem = orderItemMapper.selectOne(new FxOrderItem().setNumber(number));
        if (orderItem == null) {
            return Result.fail("订单不存在");
        }
        if (orderItem.getStatus() != Constant.OrderItemStatus.SEND) {
            return Result.fail("出票成功后才可以重发短信");
        }
        int ret = orderItemMapper.resend(orderItem.getId(), config.getResendVoucherSmsMax(), config.getResendVoucherSmsInterval());
        if (ret <= 0) {
            return Result.fail("已超过最大次数或太快");
        }
        return smsNotifyManager.voucher(orderItem, phone);
    }
}
