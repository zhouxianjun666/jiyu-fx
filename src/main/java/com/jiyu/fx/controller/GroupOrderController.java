package com.jiyu.fx.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.SearchGroupOrder;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.manager.BookingManager;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.vo.*;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/22 15:43
 */
@Api(tags = "团队订单接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/fx/group/order")
public class GroupOrderController {
    @Resource
    private FxGroupMapper groupMapper;
    @Resource
    private FxGroupProductMapper groupProductMapper;
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private FxOrderItemDetailMapper detailMapper;
    @Resource
    private FxOrderItemVisitorDetailMapper visitorDetailMapper;
    @Resource
    private FxOrderMapper orderMapper;
    @Resource
    private FxGroupMemberMapper groupMemberMapper;
    @Autowired
    private BookingManager bookingManager;
    @Autowired
    private OrderManager orderManager;

    @ApiOperation(value = "创建团队订单", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("create/{type}")
    @Locks("LOCK:GROUP:BOOKING:#{#vo.budgetId}")
    public Result<?> create(@ApiParam(required = true) @PathVariable String type,
                                 @ApiParam(required = true) @RequestBody @Validated GroupBookingVO vo) {
        FxGroupProduct product = groupProductMapper.selectByPrimaryKey(vo.getBudgetId());
        Assert.notNull(product, "产品不存在");
        FxGroup group = groupMapper.selectByPrimaryKey(product.getGroupId());
        Assert.notNull(group, "团队不存在");
        BookingVO bookingVO = BeanUtil.toBean(vo, BookingVO.class)
                .setGroup(group)
                .setGroupProduct(product)
                .setSource(Constant.OrderSource.FX_PLATFORM)
                .setShipping(new BookingVisitorVO()
                        .setCardType(Constant.CardType.ID)
                        .setCardNo(group.getGuideSid())
                        .setName(group.getGuideName())
                        .setPhone(group.getGuidePhone())
                )
                .setItems(Collections.singletonList(new BookingItemVO()
                        .setDays(vo.getDays())
                        .setBooking(vo.getBooking())
                        .setProductSubNumber(product.getSubCode())
                        .setVisitors(Collections.singletonList(new BookingVisitorVO()
                                .setCardType(Constant.CardType.ID)
                                .setCardNo(group.getGuideSid())
                                .setName(group.getGuideName())
                                .setPhone(group.getGuidePhone())
                                .setQuantity(vo.getQuantity())
                        ))
                ));
        return bookingManager.create(bookingVO, type, Util.getUser());
    }

    @ApiOperation(value = "订单列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")
    })
    public Result<?> list(@Valid SearchGroupOrder search, Integer pageNum, Integer pageSize) {
        return Result.ok(orderManager.groupOrderList(search, Util.getUser(), pageNum, pageSize));
    }

    @ApiOperation(value = "订单详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("detail/{number}")
    public Result<?> detail(@PathVariable long number) {
        return Result.ok(orderManager.groupOrderDetail(number, Util.getUser(), null));
    }

    @ApiOperation(value = "修改成员", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("modify/member")
    @Locks("#{#vo.itemNumber}")
    public Result<?> modifyMember(@RequestBody @Validated ModifyGroupOrderMemberVO vo) {
        FxOrderItem orderItem = orderItemMapper.selectOne(new FxOrderItem().setNumber(vo.getItemNumber()));
        Assert.notNull(orderItem, "订单不存在");
        if (orderItem.getUsedQuantity() > 0 || orderItem.getRefundQuantity() > 0) {
            return Result.fail("当前订单已取票");
        }
        FxOrder order = orderMapper.selectByPrimaryKey(orderItem.getOrderId());
        if (order.getPaymentType() != Constant.PaymentType.LOCAL_PAY) {
            return Result.fail("当前订单非现场到付");
        }
        List<FxOrderItemDetail> details = detailMapper.select(new FxOrderItemDetail().setOrderItemId(orderItem.getId()));
        if (details.size() > 1) {
            return Result.fail("暂不支持修改多天订单");
        }
        int total = orderItem.getQuantity();
        if (CollUtil.isNotEmpty(vo.getDelete())) {
            groupMemberMapper.deleteByExample(Example.builder(FxGroupMember.class)
                    .where(WeekendSqls.<FxGroupMember>custom()
                            .andIn(FxGroupMember::getId, vo.getDelete())
                    )
                    .build()
            );
            total -= vo.getDelete().size();
        }
        if (CollUtil.isNotEmpty(vo.getAdd())) {
            AuthUser user = Util.getUser();
            try {
                groupMemberMapper.insert(vo.getAdd().stream().map(item -> BeanUtil.toBean(item, FxGroupMember.class)
                        .setGroupId(order.getGroupId())
                        .setBudgetId(vo.getBudgetId())
                        .setCreateTime(new Date())
                        .setCreateUserId(user.getId())
                        .setCreateUserName(user.getFullName())
                        .setIsPresent(false)
                        .setCode(orderItem.getProductSubNumber())
                ).collect(Collectors.toList()));
            } catch (DuplicateKeyException e) {
                throw Result.fail("证件号码重复").exception();
            }
            total += vo.getAdd().size();
        }
        int count = groupMemberMapper.selectCount(new FxGroupMember().setBudgetId(vo.getBudgetId()));
        int settlePrice = orderItem.getSettlePrice() / orderItem.getQuantity() * total;
        int lowPrice = orderItem.getLowPrice() / orderItem.getQuantity() * total;
        orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem().setId(orderItem.getId()).setQuantity(total)
                .setSettlePrice(settlePrice)
                .setLowPrice(lowPrice)
        );
        groupProductMapper.updateByPrimaryKeySelective(new FxGroupProduct().setId(vo.getBudgetId()).setSum(count));
        orderMapper.updateByPrimaryKeySelective(new FxOrder().setId(order.getId()).setAmount(lowPrice).setPayAmount(settlePrice));
        detailMapper.updateByPrimaryKeySelective(new FxOrderItemDetail().setId(details.get(0).getId()).setQuantity(total));
        visitorDetailMapper.updateByExampleSelective(new FxOrderItemVisitorDetail().setQuantity(total), Example.builder(FxOrderItemVisitorDetail.class)
                .where(WeekendSqls.<FxOrderItemVisitorDetail>custom()
                        .andEqualTo(FxOrderItemVisitorDetail::getOrderItemId, orderItem.getId())
                        .andEqualTo(FxOrderItemVisitorDetail::getDay, details.get(0).getDay())
                )
                .build()
        );
        return Result.ok();
    }
}
