package com.jiyu.fx.controller;

import cn.hutool.core.bean.BeanUtil;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.AddProductPlanDto;
import com.jiyu.fx.entity.FxProductPlan;
import com.jiyu.fx.entity.FxProductSub;
import com.jiyu.fx.manager.ProductPlanManager;
import com.jiyu.fx.mapper.FxProductSubMapper;
import com.jiyu.fx.service.ProductPlanService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 销售计划
 * @Author: songe
 * @CreateDate: 2019/1/15 14:27
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/15 14:27
 * @UpdateRemark:
 * @Version: 1.0
 */
@RestController
@RequestMapping("api/product/plan")
@Api(tags = "景区销售计划接口")
@CrossOrigin(allowCredentials = "true")
public class ScenicProductPlanSaleController {

    @Autowired
    private ProductPlanService planService;
    @Autowired
    private ProductPlanManager productPlanManager;
    @Resource
    private FxProductSubMapper productSubMapper;

    @PostMapping(value = "add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> add(@RequestBody AddProductPlanDto fxProductPlan) {
        return planService.add(fxProductPlan);
    }

    @PostMapping(value = "update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> update(@RequestBody FxProductPlan fxProductPlan) {
        return planService.update(fxProductPlan);
    }

    @PostMapping(value = "batch/update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> updateBatch(@RequestBody List<Map> list) {
        for (Map plan : list) {

            planService.update(BeanUtil.mapToBean(plan, FxProductPlan.class, false));
        }
        return Result.ok();
    }

    @PostMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> delete(@ApiParam(required = true) @PathVariable int id) {
        return planService.delete(id);
    }

    @GetMapping(value = "list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> list(Integer productSubId, String beginTime, String endTime, Integer pageNum, Integer pageSize) {
        return planService.list(productSubId, beginTime, endTime, pageNum, pageSize);
    }

    @ApiOperation(value = "获取景区可售列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "sale/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "主产品名"),
            @ApiImplicitParam(name = "subName", value = "子产品名"),
            @ApiImplicitParam(name = "type", value = "团/散类型"),
            @ApiImplicitParam(name = "buySale", value = "是否购买自己产品"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")
    })
    public Result<?> canSaleList(String name, String subName, Integer type, Integer pageNum, Integer pageSize, @RequestParam(defaultValue = "false") Boolean buySale) {
        return planService.canSaleList(name, subName, type, pageNum, pageSize, buySale);
    }


    @ApiOperation(value = "获取价格日历", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "sale/calendar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productSubId", value = "子产品ID"),
            @ApiImplicitParam(name = "productSubNumber", value = "子产品编号"),
            @ApiImplicitParam(name = "beginTime", value = "开始时间"),
            @ApiImplicitParam(name = "endTime", value = "结束时间")
    })
    public Result<?> saleCalendar(Integer productSubId, Long productSubNumber, Date beginTime, Date endTime) {
        if (productSubId == null && productSubNumber == null) {
            return Result.fail("子产品不能为空");
        }
        if (productSubId == null) {
            FxProductSub sub = productSubMapper.selectOne(new FxProductSub().setCode(productSubNumber));
            if (sub == null) {
                return Result.fail("产品不存在");
            }
            productSubId = sub.getId();
        }
        int merchantId = Util.getUser().getMerchant().getId();
        return productPlanManager.priceCalendar(productSubId, merchantId, beginTime, endTime);
    }
}
