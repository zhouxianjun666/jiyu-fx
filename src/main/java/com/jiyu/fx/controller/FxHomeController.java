package com.jiyu.fx.controller;

import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.manager.HomeStatisticsManager;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/5/20 15:15
 */
@Api(tags = "首页接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/fx/home")
public class FxHomeController {
    @Autowired
    private HomeStatisticsManager homeStatisticsManager;

    @GetMapping("statistics")
    public Result<?> statisticsData() {
        return homeStatisticsManager.home(Util.getUser());
    }
}
