package com.jiyu.fx.controller;

import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxProductSub;
import com.jiyu.fx.service.ScenicProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Description: 景区产品管理类
 * @Author: songe
 * @CreateDate: 2019/1/4 14:16
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/4 14:16 @UpdateRemark: 修改内容 @Version: 1.0
 */
@RestController
@RequestMapping("api/product/scenic")
@Api(tags = "景区主产品接口")
@CrossOrigin(allowCredentials = "true")
public class ScenicProductController {
    @Autowired
    private ScenicProductService productService;

    @PostMapping(value = "update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> update(@RequestBody Map<String, Object> map) {
        return productService.update(map);
    }

    @PostMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> delete(@ApiParam(required = true) @PathVariable int id) {
        return productService.delete(id);
    }

    @GetMapping(value = "list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> list(String name, String subName, Integer pageNum, Integer pageSize) {
        return productService.list(name, subName, pageNum, pageSize);
    }

    @GetMapping(value = "select/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> info(@PathVariable int id) {
        return productService.info(id);
    }

    @GetMapping(value = "detail/query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> detail(String code) {
        return productService.detail(code);
    }

    @PostMapping(value = "sub/update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> updateSub(@RequestBody Map map) {
        AuthUser user = Util.getUser();
        Merchant merchant = user.getMerchant();
        map.put("merchantId", merchant.getId());
        map.put("merchantName", merchant.getName());
        return productService.updateSub(map);
    }

    @GetMapping(value = "sub/select/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> selectSubProDetailByid(@PathVariable int id) {
        return productService.selectSubProDetailByid(id);
    }

    @GetMapping(value = "sub/select/code", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> selectSubProDetailByid(Long code) {
        return productService.selectSubProDetailByCode(code);
    }

    @PostMapping(value = "sub/upordown", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> upOrDownSub(@RequestBody FxProductSub fxProductSub) {
        return productService.upOrDownSub(fxProductSub);
    }

    @PostMapping(value = "sub/delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> deletesub(@ApiParam(required = true) @PathVariable int id) {
        return productService.deleteSub(id);
    }

}
