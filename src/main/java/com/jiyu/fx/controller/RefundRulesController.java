package com.jiyu.fx.controller;

import cn.hutool.core.lang.Assert;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxRefundRule;
import com.jiyu.fx.mapper.FxRefundRuleMapper;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Description: 退货规则类
 * @Author: songe
 * @CreateDate: 2019/2/11 17:11
 * @UpdateUser: songe
 * @UpdateDate: 2019/2/11 17:11
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */

@Api(tags = "退货规则")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/refund/rules")
public class RefundRulesController {

    @Resource
    FxRefundRuleMapper refundRuleMapper;

    @PostMapping(value = "add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> add(@RequestBody FxRefundRule fxRefundRule) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        int isRepeat = refundRuleMapper.selectCount(fxRefundRule.setMerchantId(merchantId));
        Assert.isFalse(isRepeat > 0, "名字重复请重新输入!");

        Date now = new Date();
        fxRefundRule.setMerchantId(merchantId);
        fxRefundRule.setCreateTime(now);
        fxRefundRule.setUpdateTime(now);
        refundRuleMapper.insertSelective(fxRefundRule);
        return Result.ok();
    }

    @PostMapping(value = "update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> update(@RequestBody FxRefundRule fxRefundRule) {
        refundRuleMapper.updateByPrimaryKeySelective(fxRefundRule);
        return Result.ok();
    }

    @GetMapping(value = "list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> list(Integer pageNum, Integer pageSize, Integer productType) {
        return Result.ok(Util.pageOrOffsetOrNone(() -> refundRuleMapper.selectAllByEpId(Util.getUser().getMerchant().getId(), productType), pageNum, pageSize));

    }

}
