package com.jiyu.fx.controller;

import com.jiyu.common.Constant;
import com.jiyu.common.dto.Config;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxGroupMember;
import com.jiyu.fx.entity.FxMaConfig;
import com.jiyu.fx.mapper.FxGroupMemberMapper;
import com.jiyu.fx.mapper.FxMaConfigMapper;
import com.jiyu.fx.service.FxGroupService;
import com.jiyu.fx.vo.GroupBookVO;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Description: 团队类
 * @Author: songe
 * @CreateDate: 2019/3/21 19:29
 * @UpdateUser: songe
 * @UpdateDate: 2019/3/21 19:29
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
@Api(tags = "团队管理")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("/api/product/group")
public class FxGroupController {
    @Resource
    private FxGroupMemberMapper groupMemberMapper;

    @Autowired
    private FxGroupService fxGroupService;

    @Autowired
    private Config config;

    @Resource
    private FxMaConfigMapper maConfigMapper;

    @ApiOperation(value = "团队更新", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping(value = "update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> update(@RequestBody GroupBookVO vo) {
        return fxGroupService.update(vo);
    }

    @PostMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> delete(@ApiParam(required = true) @PathVariable int id) {
        return fxGroupService.delete(id);
    }


    @ApiOperation(value = "团队详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "select/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> showDetail(@PathVariable Integer id) {
        return fxGroupService.showDetail(id);
    }

    @ApiOperation(value = "团员列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("member/list/{budgetId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")
    })
    public Result<?> list(@PathVariable int budgetId, Integer pageNum, Integer pageSize) {
        return Result.ok(Util.pageOrOffsetOrNone(() -> groupMemberMapper.select(new FxGroupMember().setBudgetId(budgetId)), pageNum, pageSize));
    }

    @ApiOperation(value = "团队到付查询接口", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("group/present/query/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "凭证配置ID"),
            @ApiImplicitParam(name = "sid", value = "导游身份证id")
    })
    public Result<?> queryGroupProducts(String id, String sid) {
        FxMaConfig maConfig = maConfigMapper.selectByPrimaryKey(id);
        if (maConfig == null || !maConfig.getMerchantId().equals(Util.getUser().getMerchant().getId())) {
            return Result.fail("标识错误");
        }
        if (!maConfig.getService().equals(Constant.VoucherServiceType.JY) && !config.isDev()) {
            return Result.fail("对接系统错误");
        }

        return fxGroupService.queryGroupProducts(id, sid);
    }
}
