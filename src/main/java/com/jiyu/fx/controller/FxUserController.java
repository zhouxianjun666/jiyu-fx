package com.jiyu.fx.controller;

import cn.hutool.core.util.RandomUtil;
import com.jiyu.base.manager.SmsManager;
import com.jiyu.base.mapper.UserMapper;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * @Author: 娄楚帅
 * @UpdateRemark: 更新说明
 */
@Api(tags = "分销用户接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("/fx/user")
public class FxUserController {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private SmsManager smsManager;

    @ApiOperation(value="获取随机验证码",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("randCode")
    public Result<?> randCode(HttpSession httpSession){
        String code = RandomUtil.randomString(4);
        httpSession.setAttribute("fxRandCode", code);
        return Result.ok(code.toUpperCase());
    }

    @ApiOperation(value="检验手机号和随机验证码",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("telAndCode")
    public Result<?> verifyTelAndCode( @ApiParam(required = true) String phone, @ApiParam(required = true) String randCode, HttpSession httpSession){
        String verifyCode = (String) httpSession.getAttribute("fxRandCode");
        if(!randCode.equals(verifyCode.toUpperCase())){
            return Result.fail("验证码不正确");
        }
        User user = userMapper.selectOne(new User().setPhone(phone));
        if (user == null) {
            return Result.fail("该手机号未注册");
        }
        return Result.ok();
    }

    @ApiOperation(value="获取手机验证码", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("getIdentifyCode")
    public Result<?> getIdentifyCode(String tel){
        return smsManager.sendVerificationCode(tel, null);
    }

    @ApiOperation(value="验证手机验证码对错",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("verifyCode")
    public Result<?> verifyCode(@ApiParam(required = true) String cellPhone ,@ApiParam(required = true) String IdentifyCode){
        System.out.println(cellPhone);
        System.out.println(IdentifyCode);
        Boolean isCode =  smsManager.validateCode(cellPhone, IdentifyCode);
        return isCode ? Result.ok() : Result.fail("验证码有误");
    }

    @ApiOperation(value="基于手机号修改密码",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("updatePass")
    public Result<?> updatePass(
            @ApiParam(required = true)  String phone,
            @ApiParam(required = true)  String newPass)
    {
        User user = userMapper.selectOne(new User().setPhone(phone));
        String password = new SimpleHash("MD5", newPass, user.getSalt()).toHex();
        userMapper.updateByPrimaryKeySelective(new User().setId(user.getId()).setPassword(password));
        return Result.ok();
    }


}
