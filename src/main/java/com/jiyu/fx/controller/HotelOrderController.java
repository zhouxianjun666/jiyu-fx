package com.jiyu.fx.controller;

import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.manager.RefundManager;
import com.jiyu.fx.manager.TicketManager;
import com.jiyu.fx.vo.HotelConsumeVO;
import com.jiyu.fx.vo.RefundVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/11 10:10
 */
@Api(tags = "酒店订单接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/fx/order/hotel")
public class HotelOrderController {
    @Autowired
    private TicketManager ticketManager;
    @Autowired
    private RefundManager refundManager;

    @ApiOperation(value = "消费/使用", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("consume")
    @Locks("#{#vo.number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @CheckResult
    public Result<?> refundApply(@ApiParam(required = true) @RequestBody @Validated HotelConsumeVO vo) {
        AuthUser user = Util.getUser();
        Result<Boolean> result = ticketManager.hotelConsume(vo, user);
        if (!result.isSuccess() || result.getValue()) {
            return result;
        }
        RefundVO refundVO = new RefundVO().setNumber(vo.getNumber()).setRefundTime(new Date()).setCause("酒店核销自动退订");
        return refundManager.apply(refundVO, "hotel-consume-remainder", user);
    }
}
