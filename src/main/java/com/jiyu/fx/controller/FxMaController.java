package com.jiyu.fx.controller;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AddGroup;
import com.jiyu.common.dto.Result;
import com.jiyu.common.dto.UpdateGroup;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.JYTicketConfig;
import com.jiyu.fx.entity.FxMaConfig;
import com.jiyu.fx.manager.VoucherManager;
import com.jiyu.fx.mapper.FxMaConfigMapper;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * @author larry.page
 * @ClassName:
 * @Description:
 * @date 2019-1-25 11:45:58
 */

@Api(tags = "凭证&票务配置接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/fx/ma")
public class FxMaController {
    @Resource
    private FxMaConfigMapper fxMaConfigMapper;
    @Autowired
    private Validator validator;
    @Autowired
    private VoucherManager voucherManager;


    @ApiOperation(value = "获取凭证&票务配置列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("list")
    @RequiresPermissions("a:fx:ma:list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
            @ApiImplicitParam(name = "name", value = "名称"),
            @ApiImplicitParam(name = "service", value = "票务系统")
    })
    public Result<?> list(String name, String service, Integer pageNum, Integer pageSize) {
        return Result.ok(Util.pageOrOffsetOrNone(() -> {
            JoinExample.Builder builder = JoinExample.builder(FxMaConfig.class)
                    .where(JoinExample.Where.custom()
                            .andEqualTo(FxMaConfig::getMerchantId, Util.getUser().getMerchant().getId())
                            .andLike(FxMaConfig::getName, name)
                            .optional(StrUtil.isNotBlank(service), b -> b.andEqualTo(FxMaConfig::getService, service))
                    );
            return fxMaConfigMapper.selectByJoinExampleEntity(builder.build()
            );
        }, pageNum, pageSize));
    }

    @ApiOperation(value = "获取凭证&票务产品列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("product/list/{maConfigId}")
    @RequiresPermissions("a:fx:ma:product-list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    public Result<?> productList(@ApiParam(required = true) @PathVariable int maConfigId, Integer pageNum, Integer pageSize) {
        return voucherManager.pullProduct(maConfigId, pageNum, pageSize);
    }


    @ApiOperation(value = "修改凭证&票务配置", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("update")
    @RequiresPermissions("a:fx-ma:update")
    public Result<?> update(@ApiParam(required = true) @RequestBody @Validated(UpdateGroup.class) FxMaConfig vo) {
        Result<FxMaConfig> result = check(vo.getId());
        if (!result.isSuccess()) {
            return result;
        }
        FxMaConfig maConfig = result.getValue();
        if (maConfig.getService().equals(Constant.VoucherServiceType.JY) && StrUtil.isNotBlank(vo.getConfig())) {
            Result<?> validate = validateJY(vo.getConfig());
            if (!validate.isSuccess()) {
                return validate;
            }
        }
        fxMaConfigMapper.updateByPrimaryKeySelective(vo.setCreateTime(null).setService(null));
        return Result.ok();
    }

    @ApiOperation(value = "删除凭证&票务配置", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("remove/{id}")
    @RequiresPermissions("a:fx-ma:remove")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> remove(@ApiParam(required = true) @PathVariable int id) {
        Result<?> result = check(id);
        if (!result.isSuccess()) {
            return result;
        }
        fxMaConfigMapper.deleteByPrimaryKey(id);
        return Result.ok();
    }


    @ApiOperation(value = "新增凭证&票务配置", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("add")
    @RequiresPermissions("a:fx-ma:add")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> add(@ApiParam(required = true) @RequestBody @Validated(AddGroup.class) FxMaConfig vo) {
        if (vo.getService().equals(Constant.VoucherServiceType.JY)) {
            if (!JSONUtil.isJsonObj(vo.getConfig())) {
                return Result.fail("缺少配置信息");
            }
            Result<?> validate = validateJY(vo.getConfig());
            if (!validate.isSuccess()) {
                return validate;
            }
        }
        fxMaConfigMapper.insertSelective(vo.setMerchantId(Util.getUser().getMerchant().getId()).setCreateTime(new Date()));
        return Result.ok();
    }

    private Result<FxMaConfig> check(int id) {
        FxMaConfig maConfig = fxMaConfigMapper.selectByPrimaryKey(id);
        if (maConfig == null) {
            return Result.fail("不存在");
        }
        if (!Objects.equals(maConfig.getMerchantId(), Util.getUser().getMerchant().getId())) {
            return Result.fail("当前商户没有权限");
        }
        return Result.ok(maConfig);
    }

    private Result<?> validateJY(String config) {
        Set<ConstraintViolation<JYTicketConfig>> validate = validator.validate(JSONUtil.toBean(config, JYTicketConfig.class));
        if (!validate.isEmpty()) {
            return Result.fail(CollUtil.getFirst(validate).getMessage());
        }
        return Result.ok();
    }
}
