package com.jiyu.fx.controller;

import cn.hutool.core.util.StrUtil;
import com.jiyu.base.dto.SearchBills;
import com.jiyu.base.mapper.MerchantPaymentConfigMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.MerchantPaymentConfig;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxMerchantAccount;
import com.jiyu.fx.manager.FxMerchantAccountManager;
import com.jiyu.fx.mapper.FxMerchantAccountMapper;
import com.qiniu.util.StringUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/29 9:40
 */
@Api(tags = "分销商户账户接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/fx/merchant/account")
public class FxMerchantAccountController {
    @Resource
    private FxMerchantAccountMapper fxMerchantAccountMapper;
    @Autowired
    private FxMerchantAccountManager merchantAccountManager;
    @Resource
    private MerchantPaymentConfigMapper merchantPaymentConfigMapper;

    @ApiOperation(value = "获取当前商户账户", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("get")
    public Result<?> get(@ApiParam Integer boss) {
        int merchantId = Util.getUser().getMerchant().getId();
        return Result.ok(boss == null ?
                fxMerchantAccountMapper.select(new FxMerchantAccount().setMerchantId(merchantId)) :
                fxMerchantAccountMapper.selectOne(new FxMerchantAccount().setMerchantId(merchantId).setBossMerchantId(boss)));
    }

    @ApiOperation(value = "获取指定商户账户", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("get/{merchantId}")
    public Result<?> get(@ApiParam(required = true) @PathVariable int merchantId) {
        return Result.ok(fxMerchantAccountMapper.selectOne(new FxMerchantAccount().setMerchantId(merchantId).setBossMerchantId(Util.getUser().getMerchant().getId())));
    }

    @ApiOperation(value = "获取商户账户流水", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("bills")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")
    })
    public Result<?> bills(SearchBills searchBills, Integer pageNum, Integer pageSize) {
        AuthUser user = Util.getUser();
        if (searchBills.getBossMerchantId() == null) {
            searchBills.setBossMerchantId(user.getMerchant().getId());
        }
        return Result.ok(Util.pageOrOffsetOrNone(() -> merchantAccountManager.bills(searchBills), pageNum, pageSize));
    }

    @ApiOperation(value = "授信", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("credit/{merchantId}")
    public Result<?> credit(@ApiParam(required = true) @PathVariable int merchantId, @ApiParam(required = true) @RequestParam int quota) {
        merchantAccountManager.creditGranting(Util.getUser().getMerchant().getId(), merchantId, quota);
        return Result.ok();
    }

    @ApiOperation(value = "充值", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("recharge/{merchantId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "money", value = "金额"),
            @ApiImplicitParam(name = "remark", value = "备注")
    })
    public Result<?> recharge(@ApiParam(required = true) @PathVariable int merchantId, @RequestParam int money, String remark) {
        merchantAccountManager.recharge(Util.getUser().getMerchant().getId(), merchantId, money, remark);
        return Result.ok();
    }

    @ApiOperation(value = "扣除", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("deduct/{merchantId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "money", value = "金额"),
            @ApiImplicitParam(name = "remark", value = "备注")
    })
    public Result<?> deduct(@ApiParam(required = true) @PathVariable int merchantId, @RequestParam int money, String remark) {
        merchantAccountManager.deduct(Util.getUser().getMerchant().getId(), merchantId, money, remark);
        return Result.ok();
    }

    @ApiOperation(value = "调整", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("adjustment/{merchantId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "balance", value = "余额值"),
            @ApiImplicitParam(name = "cash", value = "可提现值"),
            @ApiImplicitParam(name = "remark", value = "备注")
    })
    public Result<?> adjustment(@ApiParam(required = true) @PathVariable int merchantId, Long balance, Long cash, String remark) {
        merchantAccountManager.adjustment(Util.getUser().getMerchant().getId(), merchantId, balance, cash, remark);
        return Result.ok();
    }

    @ApiOperation(value = "获取已关联渠道账户资金", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("channel/balance")
    public Result<?> getChannelBalance(Integer pageNum, Integer pageSize) {
        return Result.ok(Util.pageOrOffsetOrNone(() -> fxMerchantAccountMapper.getChannelBalance(Util.getUser().getMerchant().getId()), pageNum, pageSize));
    }

    @ApiOperation(value = "获取指定商户账户的支付方式", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("get/config/{merchantId}")
    public Result<?> getPayConfig(@ApiParam(required = true) @PathVariable int merchantId) {
        Map<String, Object> map = new HashMap<>(10);
        map.put("wxPay", false);
        map.put("aliPay", false);
        FxMerchantAccount fxMerchantAccount = fxMerchantAccountMapper.selectOne(new FxMerchantAccount().setMerchantId(merchantId).setBossMerchantId(Util.getUser().getMerchant().getId()));
        List<MerchantPaymentConfig> merchantPaymentConfigList = merchantPaymentConfigMapper.select(new MerchantPaymentConfig().setMerchantId(Util.getUser().getMerchant().getId()));
        merchantPaymentConfigList.forEach( merchantPaymentConfig -> {
            if (merchantPaymentConfig.getType().equals(Constant.PaymentConfigType.WX) && merchantPaymentConfig.getStatus()) {
                map.put("wxPay", true);
            } else if (merchantPaymentConfig.getType().equals(Constant.PaymentConfigType.ALIPAY) && merchantPaymentConfig.getStatus()) {
                map.put("aliPay", true);
            }
        } );
        map.put("payConfigStr", fxMerchantAccount.getPayConfig());
        return Result.ok(map);
    }

    @ApiOperation(value = "获取可用的支付方式", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("get/use/config/{merchantId}")
    public Result<?> getCanUsePayConfig(@ApiParam(required = true) @PathVariable int merchantId) {
        Map<String, Object> map = new HashMap<>(10);
        FxMerchantAccount fxMerchantAccount = fxMerchantAccountMapper.selectOne(new FxMerchantAccount().setMerchantId(Util.getUser().getMerchant().getId()).setBossMerchantId(merchantId));
        map.put("payConfigStr", fxMerchantAccount.getPayConfig());
        return Result.ok(map);
    }

    @ApiOperation(value = "给制定商户设置支付方式", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("set/config/{merchantId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status", value = "关闭/开启"),
            @ApiImplicitParam(name = "type", value = "支付方式"),
    })
    public Result<?> setConfig(@ApiParam(required = true) @PathVariable int merchantId, Boolean status, String type) {
        FxMerchantAccount fxMerchantAccount = fxMerchantAccountMapper.selectOne(new FxMerchantAccount().setMerchantId(merchantId).setBossMerchantId(Util.getUser().getMerchant().getId()));
        Set<String> arrList = Arrays.stream(StrUtil.nullToEmpty(fxMerchantAccount.getPayConfig()).split(",")).collect(Collectors.toSet());
        if (status) {
            arrList.add(type);
        } else if (status.equals(false)) {
            arrList.remove(type);
        }
        String payConfig = StringUtils.join(arrList, ",");
        fxMerchantAccount.setPayConfig(payConfig);
        return Result.ok(fxMerchantAccountMapper.updateByPrimaryKeySelective(fxMerchantAccount));
    }
}
