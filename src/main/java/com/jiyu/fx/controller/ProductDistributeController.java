package com.jiyu.fx.controller;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.DistributeDto;
import com.jiyu.fx.entity.FxProductChain;
import com.jiyu.fx.service.ProductDistributeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 产品分销类
 * @Author: songe
 * @CreateDate: 2019/1/16 17:35
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/16 17:35
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
@RestController
@RequestMapping("api/product/distribute")
@Api(tags = "产品分销接口")
@CrossOrigin(allowCredentials = "true")
public class ProductDistributeController {


    @Autowired
    private ProductDistributeService distributeService;

    @PostMapping(value = "distribute", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> distribute2ep(@RequestBody DistributeDto fxProductPlan) {

        return distributeService.distribute2ep(fxProductPlan);
    }

    @PostMapping(value = "update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> update(@RequestBody FxProductChain fxProductPlan) {
        return distributeService.update(fxProductPlan);
    }

    @PostMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> delete(@ApiParam(required = true) @PathVariable int id) {
        return distributeService.delete(id);
    }

    @GetMapping(value = "list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> list(String productSubName,Integer productType, Integer pageNum, Integer pageSize) {

        return distributeService.list(productSubName,productType, pageNum, pageSize);
    }

    /*@GetMapping(value = "ep/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> addEpsFrSubPro(int productSubId) {
        return distributeService.findEpsFrSubPro(productSubId);
    }*/


}
