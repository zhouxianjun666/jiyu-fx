package com.jiyu.fx.controller.shop;

import cn.hutool.core.collection.CollUtil;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.mapper.FxProductSubMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;


/**
 * @author lou
 * @ClassName:
 * @Description:
 */
@Api(tags = "产品详情")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("/api/shop/product")
public class ShopProductDetailController {
    @Resource
    private FxProductSubMapper productSubMapper;

    @ApiOperation(value = "产品详情", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "itemId", value = "产品id", required = true),
    })
    @GetMapping("productDetail")
    public Result<?> productDetail(Long code) {
        Map<String, Object> proSubDetail = productSubMapper.selectProDetailByCode(code);
        if(CollUtil.isEmpty(proSubDetail)){
            return Result.fail("此产品不存在");
        }
        Map<String, Object> proPlan = productSubMapper.selectProPlanByProSubId((Integer) proSubDetail.get("id"));
        proSubDetail.put("proPlan",proPlan);
        return Result.ok(proSubDetail);
    }

}
