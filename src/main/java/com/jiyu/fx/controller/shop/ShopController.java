package com.jiyu.fx.controller.shop;

import cn.hutool.core.bean.BeanUtil;
import com.jiyu.common.dto.AddGroup;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.dto.UpdateGroup;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.shop.Shop;
import com.jiyu.fx.manager.ShopManager;
import com.jiyu.fx.mapper.ShopMapper;
import com.jiyu.fx.vo.shop.ShopOperationVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/27 16:03
 */
@Api(tags = "店铺接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("/api/shop")
public class ShopController {
    @Resource
    private ShopMapper shopMapper;
    @Autowired
    private ShopManager shopManager;

    @ApiOperation(value = "开通店铺", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("open")
    @RequiresPermissions("a:shop:open")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> open(@ApiParam(required = true) @RequestBody @Validated(AddGroup.class) ShopOperationVO vo) {
        Shop shop = shopManager.currentShop(true);
        if (shop != null) {
            return Result.fail("当前商户已开通店铺");
        }
        AuthUser user = Util.getUser();
        Integer merchantId = user.getMerchant().getId();
        shopMapper.insertSelective(BeanUtil.toBean(vo, Shop.class).setCreateTime(new Date()).setMerchantId(merchantId));
        return Result.ok();
    }

    @ApiOperation(value = "修改店铺", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("update")
    @RequiresPermissions("a:shop:update")
    public Result<?> update(@ApiParam(required = true) @RequestBody @Validated(UpdateGroup.class) ShopOperationVO vo) {
        Shop shop = shopManager.currentShop();
        shopMapper.updateByPrimaryKeySelective(BeanUtil.toBean(vo, Shop.class).setId(shop.getId()));
        return Result.ok();
    }

    @ApiOperation(value = "获取店铺信息", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("info")
    @RequiresPermissions("a:shop:info")
    public Result<?> info() {
        return Result.ok(shopManager.currentShop(true));
    }

    @ApiOperation(value = "店铺状态", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("status/{id}/{status}")
    @RequiresPermissions("a:shop:status")
    public Result<?> update(@ApiParam(required = true) @PathVariable Integer id,
                            @ApiParam(required = true) @PathVariable Boolean status) {
        shopMapper.updateByPrimaryKeySelective(new Shop().setId(id).setStatus(status));
        return Result.ok();
    }
}
