package com.jiyu.fx.controller.shop;

import cn.hutool.core.util.StrUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.base.manager.MerchantPaymentConfigManager;
import com.jiyu.base.manager.UserManager;
import com.jiyu.base.manager.WxManager;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.dto.WxPaymentConfig;
import com.jiyu.common.dto.WxUser;
import com.jiyu.common.entity.base.Member;
import com.jiyu.common.entity.base.UserIdentity;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.OrderDetailRequest;
import com.jiyu.fx.dto.SearchOrder;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.entity.shop.Shop;
import com.jiyu.fx.manager.BookingManager;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.manager.RefundManager;
import com.jiyu.fx.manager.ShopManager;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxOrderMapper;
import com.jiyu.fx.vo.BookingVO;
import com.jiyu.fx.vo.RefundVO;
import io.swagger.annotations.*;
import org.apache.commons.collections.MapUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/28 15:27
 */
@Api(tags = "店铺订单接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("/api/shop/order")
public class ShopOrderController {
    @Resource
    private FxOrderMapper orderMapper;
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private ShopManager shopManager;
    @Autowired
    private BookingManager bookingManager;
    @Autowired
    private UserManager userManager;
    @Autowired
    private RefundManager refundManager;
    @Autowired
    private MerchantPaymentConfigManager paymentConfigManager;
    @Autowired
    private WxManager wxManager;

    @ApiOperation(value = "创建订单", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("create/{type}")
    public Result<?> create(@ApiParam(required = true) @PathVariable String type,
                            @ApiParam(required = true) @RequestBody @Validated BookingVO vo) {
        WxUser wxUser = Util.getWxUser();
        Shop shop = shopManager.currentShop();
        AuthUser user = userManager.getAuthUser(b -> b, () -> JoinExample.Where.custom()
                .andEqualTo(UserIdentity::getMerchantId, shop.getMerchantId())
                .andEqualTo(UserIdentity::getType, Constant.UserIdentityType.EP_ADMIN));
        user.setWxUser(wxUser);
        return bookingManager.create(vo.setSource(Constant.OrderSource.SELF_MEDIA).setPaymentChannel(Constant.PaymentChannel.RETAIL), type, user);
    }

    @ApiOperation(value = "订单支付检查", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("payment/check/{merchant}")
    public Result<?> checkPayment(@ApiParam(required = true) @PathVariable int merchant) {
        WxPaymentConfig config = paymentConfigManager.get(merchant, Constant.PaymentConfigType.WX);
        if (config == null) {
            return Result.fail("微信支付未配置");
        }
        Shop shop = shopManager.currentShop();
        if (!config.getAppId().equals(shop.getAppId())) {
            return Result.fail(Result.WX_PAYMENT_MISMATCH);
        }
        return Result.ok();
    }

    @ApiOperation(value = "获取支付openid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("payment/openid/{code}")
    public Result<?> getPaymentOpenid(@ApiParam(required = true) @PathVariable String code,
                                      @ApiParam(required = true) @RequestParam int merchantId) {
        WxPaymentConfig config = paymentConfigManager.get(merchantId, Constant.PaymentConfigType.WX);
        if (config == null) {
            return Result.fail("微信支付未配置");
        }
        Map<String, Object> map = wxManager.getOpenid(config.getAppId(), config.getAppSecret(), code);
        String openid = MapUtils.getString(map, "openid");
        if (StrUtil.isBlank(openid)) {
            return Result.fail("获取openid失败");
        }
        return Result.ok().setValue(openid);
    }

    @ApiOperation(value = "订单支付", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("payment/{number}")
    @Locks("#{#number}")
    public Result<?> payment(@ApiParam(required = true) @PathVariable long number,
                             String openid,
                             HttpServletRequest request) {
        FxOrder order = orderMapper.selectOne(new FxOrder().setNumber(number));
        if (order == null) {
            return Result.fail("订单不存在");
        }

        return orderManager.payment(order, Constant.PaymentType.WX, request, StrUtil.blankToDefault(openid, Util.getWxUser().getUsername()));
    }

    @ApiOperation(value = "退订申请", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("refund/apply/{type}")
    public Result<?> refundApply(@ApiParam(required = true) @PathVariable String type,
                                 @ApiParam(required = true) @RequestBody @Validated RefundVO vo) {
        WxUser wxUser = Util.getWxUser();
        Shop shop = shopManager.currentShop();
        AuthUser user = userManager.getAuthUser(b -> b, () -> JoinExample.Where.custom()
                .andEqualTo(UserIdentity::getMerchantId, shop.getMerchantId())
                .andEqualTo(UserIdentity::getType, Constant.UserIdentityType.EP_ADMIN));
        user.setWxUser(wxUser);
        return refundManager.apply(vo, type, user);
    }

    @ApiOperation(value = "获取会员自己订单列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "search", value = "查询条件"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list/member")
    public Result<?> listByMember(SearchOrder search, Integer pageNum, Integer pageSize) {
        WxUser wxUser = Util.getWxUser();
        Object list = orderManager.list(search, getListBuilder(search).andWhere(JoinExample.Where.custom()
                .andEqualTo(FxOrder::getMemberId, wxUser.getMember().getId())
        ), pageNum, pageSize);
        return Result.ok(list);
    }

    @ApiOperation(value = "获取店铺订单列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "search", value = "查询条件"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list/shop")
    @RequiresPermissions("a:shop:order:list-shop")
    public Result<?> listByShop(SearchOrder search, Integer pageNum, Integer pageSize) {
        Shop shop = shopManager.currentShop();
        Object list = orderManager.list(search, getListBuilder(search).andWhere(JoinExample.Where.custom()
                .andEqualTo(FxOrder::getBuyMerchantId, shop.getMerchantId())
                .andEqualTo(FxOrder::getSource, Constant.OrderSource.SELF_MEDIA)
        ), pageNum, pageSize);
        return Result.ok(list);
    }

    private JoinExample.Builder getListBuilder(SearchOrder search) {
        return JoinExample.builder(FxOrder.class)
                .all(FxOrder.class)
                .optional(search.isShipping(), b -> b.addCol(Member.class, FxOrder::getShipping))
                .optional(search.isJoinMember(), b -> b
                        .addTable(new JoinExample.Table(Member.class, Member::getId, FxOrder::getShippingMemberId))
                )
                .optional(search.isJoinItem(), b -> b
                        .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getOrderId, FxOrder::getId))
                )
                .optional(search.isJoinRefund(), b -> b
                        .addTable(new JoinExample.Table(FxRefundOrder.class, FxRefundOrder::getOrderItemId, FxOrderItem::getId))
                )
                .optional(search.isJoinVisitor(), b -> b
                        .addTable(new JoinExample.Table(FxOrderItemVisitor.class, FxOrderItemVisitor::getOrderItemId, FxOrderItem::getId))
                )
                .optional(search.isJoinTicket(), b -> {
                    JoinExample.JoinType joinType = StrUtil.isNotBlank(search.getVoucherNumber()) ? JoinExample.JoinType.JOIN : JoinExample.JoinType.LEFT;
                    b.addTable(new JoinExample.Table(FxOrderItemTicket.class, joinType, FxOrderItemTicket::getOrderItemId, FxOrderItem::getId));
                })
                .where(JoinExample.Where.custom()
                        .bean(search, JoinExample.tableAlias(FxOrder.class))
                        .andIsNull(FxOrder::getGroupId)
                )
                .groupBy(FxOrder::getId)
                .optional(search.isDesc(),  b -> b.desc(FxOrder::getCreateTime))
                .optional(!search.isDesc(),  b -> b.asc(FxOrder::getCreateTime));
    }

    @ApiOperation(value = "店铺订单详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("detail")
    public Result<?> detail(@Valid OrderDetailRequest request) {
        Shop shop = shopManager.currentShop();
        AuthUser user = userManager.getAuthUser(b -> b, () -> JoinExample.Where.custom()
                .andEqualTo(UserIdentity::getMerchantId, shop.getMerchantId())
                .andEqualTo(UserIdentity::getType, Constant.UserIdentityType.EP_ADMIN));
        return Result.ok(orderManager.detail(request, user, null));
    }
}
