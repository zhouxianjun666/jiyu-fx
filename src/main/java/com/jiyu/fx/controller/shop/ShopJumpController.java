package com.jiyu.fx.controller.shop;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.jiyu.base.manager.MerchantPaymentConfigManager;
import com.jiyu.base.manager.WxOauthManager;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Config;
import com.jiyu.common.dto.Result;
import com.jiyu.common.dto.WxCfg;
import com.jiyu.common.dto.WxPaymentConfig;
import com.jiyu.fx.entity.shop.Shop;
import com.jiyu.fx.mapper.ShopMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/4/9 14:56
 */
@Controller
@RequestMapping("wx/shop/jump")
@Slf4j
public class ShopJumpController {
    @Resource
    private ShopMapper shopMapper;
    @Autowired
    private Config config;
    @Autowired
    private WxOauthManager wxOauthManager;
    @Autowired
    private MerchantPaymentConfigManager paymentConfigManager;

    @RequestMapping("go/{shopId}")
    public void jump(@PathVariable String shopId, HttpServletRequest request, HttpServletResponse response) {
        WxCfg wxCfg = getWxConfig(shopId);
        if (wxCfg == null) {
            return;
        }
        WxOauthManager.oauthGo(request, response, config.getDomain() + "/wx/shop/jump/back", wxCfg.getAppId());
    }

    @RequestMapping("back")
    public void back(@RequestParam String code, @RequestParam("_") String to, HttpServletRequest request, HttpServletResponse response) {
        String shopId = StrUtil.subBetween(to, "__sid=", to.contains("#/") ? "#/" : null);
        if (StrUtil.isEmpty(shopId)) {
            Result.fail("非法访问").write();
            return;
        }
        WxCfg wxCfg = getWxConfig(shopId);
        if (wxCfg == null) {
            return;
        }
        Map<String, String> params = HttpUtil.decodeParamMap(to, CharsetUtil.CHARSET_UTF_8.name());
        wxOauthManager.oauthBack(wxCfg, code, to, params.get("scope"), request, response);
    }

    @RequestMapping("go/code/{merchant}")
    public void jumpCode(@PathVariable int merchant, HttpServletRequest request, HttpServletResponse response) {
        WxPaymentConfig wxPaymentConfig = paymentConfigManager.get(merchant, Constant.PaymentConfigType.WX);
        WxOauthManager.oauthGo(request, response, config.getDomain() + "/wx/shop/jump/back/code", wxPaymentConfig.getAppId(), "snsapi_base");
    }

    @RequestMapping("back/code")
    @SneakyThrows
    public void backCode(@RequestParam String code, @RequestParam("_") String to, HttpServletResponse response) {
        int index = to.lastIndexOf("?");
        if (index > -1) {
            to += "&code=" + code;
        } else {
            to += "?code=" + code;
        }
        to = HttpUtil.decode(to, CharsetUtil.CHARSET_UTF_8);
        log.info("wx jump back code url: {}", to);
        response.sendRedirect(to);
    }

    private WxCfg getWxConfig(@PathVariable String shopId) {
        Shop shop = shopMapper.selectByPrimaryKey(Base64.decodeStr(shopId));
        if (shop == null) {
            Result.fail("非法访问，无法识别店铺标识").write();
            return null;
        }
        return new WxCfg().setAppId(shop.getAppId()).setAppSecret(shop.getAppSecret());
    }
}
