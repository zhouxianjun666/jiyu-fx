package com.jiyu.fx.controller.shop;

import com.alone.tk.mybatis.JoinExample;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxProduct;
import com.jiyu.fx.entity.FxProductChain;
import com.jiyu.fx.entity.FxProductPlan;
import com.jiyu.fx.entity.FxProductSub;
import com.jiyu.fx.entity.shop.Shop;
import com.jiyu.fx.manager.ProductPlanManager;
import com.jiyu.fx.manager.ShopManager;
import com.jiyu.fx.mapper.FxProductSubMapper;
import com.jiyu.fx.service.ScenicProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/4/1 9:56
 */
@Api(tags = "店铺产品接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("/api/shop/product")
public class ShopProductController {
    @Resource
    private FxProductSubMapper productSubMapper;
    @Autowired
    private ShopManager shopManager;
    @Autowired
    private ProductPlanManager productPlanManager;
    @Autowired
    private ScenicProductService productService;

    @ApiOperation(value = "获取可售产品列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "产品类型:5001-景点;5002-酒店;5003-线路", required = true),
            @ApiImplicitParam(name = "name", value = "产品名称"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list/sale")
    public Result<?> listBySale(@RequestParam int type, String name,
                                Integer pageNum, Integer pageSize) {
        Shop shop = shopManager.currentShop();
        return Result.ok(Util.pageOrOffsetOrNone(() -> productSubMapper.selectByJoinExample(JoinExample.builder(FxProductSub.class)
                .all(FxProductSub.class)
                .addCol("MIN(fx_product_plan.low_price)", FxProductPlan::getLowPrice)
                .addCol("MAX(fx_product_plan.market_price)", FxProductPlan::getMarketPrice)
                .addCol(FxProduct::getType)
                .addTable(new JoinExample.Table(FxProduct.class, FxProduct::getId, FxProductSub::getProductId))
                .addTable(new JoinExample.Table(FxProductPlan.class, FxProductPlan::getProductSubId, FxProductSub::getId))
                .addTable(new JoinExample.Table(FxProductChain.class, JoinExample.JoinType.LEFT, FxProductChain::getProductSubId, FxProductSub::getId))
                .where(JoinExample.Where.custom()
                        .andLike(FxProduct::getName, name)
                        .andEqualTo(FxProduct::getType, type)
                        .andEqualTo(FxProductSub::getTicketFlag, Constant.TicketFlag.SINGLE)
                        .andEqualTo(FxProductSub::getStatus, true)
                        .andEqualTo(FxProductPlan::getStatus, true)
                )
                .andWhere(JoinExample.Where.custom()
                        .andEqualTo(FxProductSub::getMerchantId, shop.getMerchantId())
                        .orEqualTo(FxProductChain::getMerchantId, shop.getMerchantId())
                )
                .groupBy(FxProductSub::getId)
                .build()
        ), pageNum, pageSize));
    }

    @ApiOperation(value = "获取价格日历", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "sale/calendar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productSubNumber", value = "子产品编号"),
            @ApiImplicitParam(name = "beginTime", value = "开始时间"),
            @ApiImplicitParam(name = "endTime", value = "结束时间")
    })
    public Result<?> saleCalendar(Long productSubNumber, Date beginTime, Date endTime) {
        Shop shop = shopManager.currentShop();
        if (productSubNumber == null) {
            return Result.fail("子产品不能为空");
        }
        FxProductSub sub = productSubMapper.selectOne(new FxProductSub().setCode(productSubNumber));
        if (sub == null) {
            return Result.fail("产品不存在");
        }
        Integer productSubId = sub.getId();
        int merchantId = shop.getMerchantId();
        return productPlanManager.priceCalendar(productSubId, merchantId, beginTime, endTime);
    }

    @ApiOperation(value = "获取子产品详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "sub/select/code", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> selectSubProDetailByCode(Long code) {
        return productService.selectSubProDetailByCode(code);
    }
}
