package com.jiyu.fx.controller;

import com.jiyu.base.manager.MerchantManager;
import com.jiyu.base.mapper.EpMapper;
import com.jiyu.common.dto.Result;
import com.jiyu.common.dto.WxCfg;
import com.jiyu.common.entity.base.Ep;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "demoS接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("demo")
public class DemoController {
    @Autowired
    private MerchantManager merchantManager;
    @Autowired
    private WxCfg wxCfg;
    @Resource
    private EpMapper epMapper;

    @ApiOperation(value = "demo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("demo")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> demo(String url) {
        epMapper.updateByPrimaryKeySelective(new Ep().setId(18).setLegalPerson("同程"));
        int i = 1 / 0;
        return Result.ok();
    }
}
