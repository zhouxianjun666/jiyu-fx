package com.jiyu.fx.controller;

import cn.hutool.core.collection.CollUtil;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Config;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.common.util.Binary62Util;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxMaConfig;
import com.jiyu.fx.manager.VoucherManager;
import com.jiyu.fx.mapper.FxMaConfigMapper;
import com.jiyu.fx.vo.VoucherConsumeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/23 16:01
 */
@Api(tags = "凭证通知接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/fx/voucher/notify")
@Slf4j
public class VoucherNotifyController {
    @Autowired
    private Config config;
    @Autowired
    private VoucherManager voucherManager;
    @Resource
    private FxMaConfigMapper maConfigMapper;

    @ApiOperation(value = "核销通知(基裕开放平台)", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("consume/{id}")
    @RequiresPermissions("a:fx:voucher:notify:consume")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "凭证配置ID", required = true)
    })
    public Result<?> consume(@PathVariable String id, @RequestBody @Validated VoucherConsumeVO vo) {
        long maConfigId = Binary62Util.decode(id);
        FxMaConfig maConfig = maConfigMapper.selectByPrimaryKey(maConfigId);
        if (maConfig == null || !maConfig.getMerchantId().equals(Util.getUser().getMerchant().getId())) {
            return Result.fail("标识错误");
        }
        if (!maConfig.getService().equals(Constant.VoucherServiceType.JY) && !config.isDev()) {
            return Result.fail("对接系统错误");
        }
        return voucherManager.consumeNotify(maConfig.getId(), vo);
    }

    @ApiOperation(value = "核销通知(基裕开放平台)", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("consume")
    @RequiresPermissions("a:fx:voucher:notify:consume")
    public Result<?> consume(@RequestBody @Validated VoucherConsumeVO vo) {
        Merchant merchant = Util.getUser().getMerchant();
        int type = merchant.getType();
        if (type != Constant.MerchantType.FX_SUPPLIER && type != Constant.MerchantType.FX_PLATFORM) {
            return Result.fail("只有供应商才能核销");
        }
        List<FxMaConfig> list = maConfigMapper.select(new FxMaConfig().setMerchantId(merchant.getId()).setService(Constant.VoucherServiceType.JY));
        if (CollUtil.isEmpty(list)) {
            return Result.fail("当前供应商没有已对接的票务系统");
        }
        if (list.size() > 1) {
            return Result.fail("当前供应商已对接多套票务系统,请传入标识.");
        }
        return voucherManager.consumeNotify(list.get(0).getId(), vo);
    }
}
