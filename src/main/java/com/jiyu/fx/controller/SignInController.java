package com.jiyu.fx.controller;

import com.jiyu.base.dto.CreateMerchantResult;
import com.jiyu.base.manager.MerchantManager;
import com.jiyu.base.manager.SmsManager;
import com.jiyu.base.mapper.DicMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AddGroup;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Dic;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.entity.FxMaConfig;
import com.jiyu.fx.mapper.FxMaConfigMapper;
import com.jiyu.fx.vo.CreateFxMerchantVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zoujing
 * @date 2019/4/30 10:48
 */
@Api(tags = "企业会员注册接口")
@RestController
@RequestMapping("/sign")
@CrossOrigin(allowCredentials = "true")
public class SignInController {
    @Autowired
    private SmsManager smsManager;
    @Autowired
    private MerchantManager merchantManager;
    @Resource
    private DicMapper dicMapper;
    @Resource
    private FxMaConfigMapper fxMaConfigMapper;

    @ApiOperation(value = "根据常量类型获取所有常量", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("list/type/{type}")
    public Result<?> getByCode(@ApiParam(required = true) @PathVariable Integer type) {
        return Result.ok(dicMapper.select(new Dic().setType(type)));
    }

    @ApiOperation(value = "注册会员发送短信验证码", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("/sms/{phone}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> sendSmsCode(@PathVariable("phone") String phone) {
        return smsManager.sendVerificationCode(phone, null);
    }

    @ApiOperation(value = "注册会员，创建商户", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("in")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> create(@ApiParam(required = true) @RequestBody @Validated(AddGroup.class) CreateFxMerchantVO vo) {
        Boolean isCode =  smsManager.validateCode(vo.getAdmin().getPhone(), vo.getCode());
        if (!isCode) {
            return Result.fail("验证码错误！");
        }
        Result<CreateMerchantResult> result = merchantManager.create(vo);
        if (!result.isSuccess()) {
            return result;
        }
        CreateMerchantResult merchantResult = result.getValue();
        Merchant merchant = merchantResult.getMerchant();
        FxMaConfig maConfig = fxMaConfigMapper.selectOne(new FxMaConfig().setService(Constant.VoucherServiceType.AUTO).setMerchantId(merchant.getId()));
        if (maConfig == null) {
            fxMaConfigMapper.insertSelective(new FxMaConfig()
                    .setMerchantId(merchant.getId())
                    .setCreateTime(new Date())
                    .setEnable(true)
                    .setService(Constant.VoucherServiceType.AUTO)
                    .setName("自动"));
        }
        return result;
    }
}
