package com.jiyu.fx.controller;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.entity.FxProductSub;
import com.jiyu.fx.service.FxHotelProductService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Description: 酒店管理类
 * @Author: songe
 * @CreateDate: 2019/1/4 16:25
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/4 16:25
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */

@RestController
@RequestMapping("api/product/hotel")
@Api(tags = "酒店主产品接口")
@CrossOrigin(allowCredentials = "true")
public class FxHotelProductController {

    @Autowired
    private FxHotelProductService service;


    @PostMapping(value = "update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> update(@RequestBody Map<String, Object> map) {
        return service.update(map);
    }

    @PostMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> delete(@ApiParam(required = true) @PathVariable int id) {
        return service.delete(id);
    }

    @GetMapping(value = "list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> list(String name, String subName, Integer pageNum, Integer pageSize) {
        return service.list(name, subName, pageNum, pageSize);
    }

    @GetMapping(value = "/detail/query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> queryDetail(String code) {
        return service.queryDetail(code);
    }

    @PostMapping(value = "sub/upordown", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> upOrDownSub(@RequestBody FxProductSub fxProductSub) {
        return service.upOrDownSub(fxProductSub);
    }

    @PostMapping(value = "sub/delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> deletesub(@ApiParam(required = true) @PathVariable int id) {
        return service.deleteSub(id);
    }

    @PostMapping(value = "sub/update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> updateSub(@RequestBody FxProductSub map) {
        return service.updateSub(map);
    }

    @GetMapping(value = "sub/detail/query", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> selectSubInfo(String code) {
        return service.selectSubInfo(code);
    }

    @ApiOperation(value = "获取酒店可售列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "sale/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "province", value = "省份"),
            @ApiImplicitParam(name = "city", value = "城市"),
            @ApiImplicitParam(name = "inTime", value = "入住时间"),
            @ApiImplicitParam(name = "outTime", value = "退房"),
            @ApiImplicitParam(name = "name", value = "酒店名"),
            @ApiImplicitParam(name = "level", value = "酒店等级"),
            @ApiImplicitParam(name = "lowPrice", value = "最低价格"),
            @ApiImplicitParam(name = "highPrice", value = "最高价格"),
            @ApiImplicitParam(name = "hotelTheme", value = "酒店主题"),
            @ApiImplicitParam(name = "ouTime", value = "退房"),
            @ApiImplicitParam(name = "levelSort", value = "星级排序"),
            @ApiImplicitParam(name = "priceSort", value = "价格排序"),
            @ApiImplicitParam(name = "buySale", value = "是否购买自己产品"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")})
    public Result<?> selectCanSaleHotelList(String province, String city, String inTime,
                                            String outTime, String name, Integer level,
                                            Integer lowPrice, Integer highPrice, String hotelTheme, String levelSort, String priceSort, Integer pageNum, Integer pageSize, @RequestParam(defaultValue = "false") Boolean buySale) {
        return service.selectCanSaleHotelList(province, city, inTime, outTime, name, level, lowPrice, highPrice, hotelTheme, levelSort, priceSort, pageNum, pageSize, buySale);
    }

}
