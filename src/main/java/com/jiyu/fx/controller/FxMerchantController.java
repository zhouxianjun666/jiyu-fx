package com.jiyu.fx.controller;

import com.jiyu.base.controller.UserController;
import com.jiyu.base.manager.MerchantManager;
import com.jiyu.base.manager.SmsManager;
import com.jiyu.base.mapper.EpMapper;
import com.jiyu.base.mapper.MerchantMapper;
import com.jiyu.base.mapper.UserMapper;
import com.jiyu.base.vo.ModifyPasswordVo;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.*;
import com.jiyu.common.util.Util;
import com.jiyu.fx.mapper.FxChannelMapper;
import com.jiyu.fx.mapper.FxMaConfigMapper;
import com.jiyu.fx.service.ChannelGroupService;
import com.jiyu.fx.vo.UpdateInfoVO;
import io.swagger.annotations.*;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.expression.spel.support.StandardTypeConverter;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/13 10:48
 */
@Api(tags = "分销企业接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/fx/merchant")
public class FxMerchantController {
    @Autowired
    private MerchantManager merchantManager;
    @Autowired
    private ChannelGroupService channelGroupService;
    @Resource
    private FxChannelMapper channelMapper;
    @Resource
    private EpMapper epMapper;
    @Resource
    private FxMaConfigMapper fxMaConfigMapper;
    @Autowired
    private SmsManager smsManager;
    @Resource
    private UserMapper userMapper;
    @Resource
    private MerchantMapper merchantMapper;

    @ApiOperation(value = "获取商户列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "商户名称"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")
    })
    @SuppressWarnings("unchecked")
    public Result<?> list(String name, Integer pageNum, Integer pageSize) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        Object res = Util.pageOrOffsetOrNone(() -> merchantMapper.list(name, merchantId), pageNum, pageSize);
        return Result.ok(res);
    }


    @ApiOperation(value = "修改企业信息", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("updateInfo")
    public Result<?> updateInfo(@RequestBody @Validated UpdateInfoVO vo) {
        Merchant merchant = Util.getUser().getMerchant();
        merchantMapper.updateByPrimaryKeySelective(new Merchant()
                .setId(merchant.getId())
                .setNickname(vo.getAccountName())
                .setAddressDetail(vo.getAddress())
                .setType(vo.getTypeOfComp())
                .setName(vo.getRealName())
                .setAddressCode(vo.getPcastr())
        );
        return Result.ok();
    }

    @ApiOperation(value = "获取手机验证码", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("getIdentifyCode")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> getIdentifyCode(String tel) {
        return smsManager.sendVerificationCode(tel, "CHANGE_PHONE", 2);
    }

    @ApiOperation(value = "验证手机验证码对错", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("verifyCode")
    public Result<?> verifyCode(String cellPhone, String identifyCode) {
        boolean isCode = smsManager.validateCode(cellPhone, "CHANGE_PHONE", identifyCode);
        return isCode ? Result.ok() : Result.fail("验证码有误");
    }

    @ApiOperation(value = "绑定新手机号", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("bindNewTel")
    public Result<?> bindNewTel(String cellPhone, String code) {
        Result<?> result = verifyCode(cellPhone, code);
        if (!result.isSuccess()) {
            return Result.fail("验证码错误！");
        }
        AuthUser user = Util.getUser();
        userMapper.updateByPrimaryKeySelective(new User().setId(user.getId()).setPhone(cellPhone));
        return Result.ok();
    }
}

