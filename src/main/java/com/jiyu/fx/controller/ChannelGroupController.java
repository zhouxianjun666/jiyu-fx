package com.jiyu.fx.controller;

import cn.hutool.core.map.MapUtil;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.mapper.FxChannelCorrelationMapper;
import com.jiyu.fx.mapper.FxChannelMapper;
import com.jiyu.fx.mapper.FxChannelMerchantMapper;
import com.jiyu.fx.mapper.FxMerchantAccountMapper;
import com.jiyu.fx.service.ChannelGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Description: 渠道组类
 * @Author: songe
 * @CreateDate: 2019/1/28 15:08
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/28 15:08
 * @UpdateRemark:
 * @Version: 1.0
 */

@Api(tags = " 渠道分组接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/channel/group")
public class ChannelGroupController {

    @Autowired
    ChannelGroupService groupService;

    @Resource
    FxChannelMapper mapper;

    @Resource
    private FxChannelMerchantMapper channelMerchantMapper;

    @Resource
    private FxChannelCorrelationMapper fxChannelCorrelationMapper;

    @Resource
    private FxMerchantAccountMapper fxMerchantAccountMapper;

    @PostMapping(value = "add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> add(@RequestBody FxChannel fxchannel) {
        return groupService.update(fxchannel);
    }

    @PostMapping(value = "update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> update(@RequestBody FxChannel fxchannel) {
        return groupService.update(fxchannel);
    }

    @PostMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> delete(@ApiParam(required = true) @PathVariable int id) {
        return groupService.delete(id);
    }

    @GetMapping(value = "list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> list(Integer pageNum, Integer pageSize) {

        return groupService.list(pageNum, pageSize);
    }

    @GetMapping(value = "ep/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> eplist(Integer channelId, Integer pageNum, Integer pageSize) {

        return groupService.eplist(channelId, pageNum, pageSize);
    }

    @GetMapping(value = "can/sub/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> selectCanDistProSub(int id) {
        return groupService.selectCanDistProSub(id);
    }

    @GetMapping(value = "sub/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> selectDistProSub(int id) {
        return groupService.selectDistProSub(id);
    }

    @PostMapping(value = "sub/up", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)

    public Result<?> upProSub(@RequestBody FxChannelProductsub channelProductsub) {
        return groupService.upProSub(channelProductsub);
    }

    @PostMapping(value = "sub/down", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> downProSub(@RequestBody Map map) {
        return groupService.downProSub(map);
    }

    @ApiOperation(value = "获取渠道分组列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "show", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> showList() {
        return groupService.showList();
    }

    @PostMapping(value = "move/ep", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> moveEp2OtherChannelGroup(@RequestBody Map map) {
        Integer channelId = MapUtil.getInt(map, "channelId");
        Integer tarChannelId = MapUtil.getInt(map, "tarCahnnelId");
        Integer epId = MapUtil.getInt(map, "epId");
        return groupService.moveEp2OtherChannelGroup(channelId, tarChannelId, epId);
    }

    @PostMapping(value = "move/sub", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> moveSub2OtherChannelGroup(@RequestParam Integer channelId, @RequestParam Integer tarChannelId, @RequestParam Integer productsubId) {
        return groupService.moveSub2OtherChannelGroup(channelId, tarChannelId, productsubId);
    }

    @GetMapping(value = "sub/channel/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> addEpsFrSubPro(int productSubId) {
        return groupService.findCanUpChannel(productSubId);
    }

    @GetMapping(value = "sub/channel/down/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> findCanDownChannel(int productSubId) {
        return groupService.findCanDownChannel(productSubId);
    }

    @PostMapping(value = "sub/channel/up", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> upPro2Channel(@RequestBody Map map) {
        return groupService.upPro2Channel(map);
    }

    @PostMapping(value = "sub/channel/down", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> downProFrChannel(@RequestBody Map map) {
        return groupService.downProFrChannel(map);

    }

    @ApiOperation(value = "获取可关联渠道列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "can/correlation/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> canCorrelationList(Integer type, String name, Integer pageNum, Integer pageSize) {
        Integer applyType = 1, beAppliedType = 2;
        if (type.equals(Constant.ChannelCorrelationType.SUPPLY)) {
            applyType = 2;
            beAppliedType = 1;
        }
        return groupService.canCorrelationList(applyType, beAppliedType, name, pageNum, pageSize);
    }

    @ApiOperation(value = "获取已关联或待同意关联渠道列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "correlation/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> correlationList(Integer type, Integer status, String name, Integer pageNum, Integer pageSize) {
        Integer applyType = 1, beAppliedType = 2;
        if (type.equals(Constant.ChannelCorrelationType.SUPPLY)) {
            applyType = 2;
            beAppliedType = 1;
        }
        return groupService.correlationList(applyType, beAppliedType, status, name, pageNum, pageSize);
    }

    @ApiOperation(value = "申请关联渠道", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping(value = "apply/correlation", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> applyCorrelation(@RequestBody Map<String, Object> params) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        Byte status = 1;
        fxChannelCorrelationMapper.insert(new FxChannelCorrelation().setApplyId(merchantId).setBeAppliedId(MapUtils.getIntValue(params, "id")).setStatus(status).setType(MapUtils.getByteValue(params, "type")).setCreateTime(new Date()));
        return Result.ok();
    }

    @ApiOperation(value = "修改关联状态", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping(value = "update/status", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> updateStatus(@RequestBody Map<String, Object> params) {
        FxChannelCorrelation fxChannelCorrelation = fxChannelCorrelationMapper.selectOne(new FxChannelCorrelation().setId(MapUtils.getIntValue(params, "id")));
        fxChannelCorrelation.setStatus(MapUtils.getByteValue(params, "status"));
        fxChannelCorrelationMapper.updateByPrimaryKeySelective(fxChannelCorrelation);
        //同意关联创建对应商户账户
        if (MapUtils.getByteValue(params, "status") == 2) {
            Integer bossId = fxChannelCorrelation.getType() == 1 ? fxChannelCorrelation.getApplyId() : fxChannelCorrelation.getBeAppliedId();
            Integer merchantId = fxChannelCorrelation.getType() == 1 ? fxChannelCorrelation.getBeAppliedId() : fxChannelCorrelation.getApplyId();
            fxMerchantAccountMapper.insertSelective(new FxMerchantAccount().setBossMerchantId(bossId).setMerchantId(merchantId).setCreateTime(new Date()));
        }
        return Result.ok();
    }

    @ApiOperation(value = "获取可添加至组成员", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "can/add/group", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> canAddGroup() {
        return groupService.canAddGroup();
    }

    @ApiOperation(value = "添加渠道到组", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping(value = "add/group", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    public Result<?> addGroup(@RequestBody Map<String, Object> params) {
        ArrayList<Integer> ids = (ArrayList<Integer>) params.get("ids");
        Integer channelId = MapUtils.getIntValue(params, "channelId");
        return groupService.addGroup(ids, channelId);
    }

    @ApiOperation(value = "获取所有渠道组并且判断渠道是否已在组内", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping(value = "get/group/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> getGroup(@ApiParam(required = true) @PathVariable int id) {
        try {
            Map<String, Object> map = new HashMap<>(2);
            List<Map<String, Object>> list = mapper.selectByPlatformId(Util.getUser().getMerchant().getId());
            FxChannelMerchant fxChannelMerchant = channelMerchantMapper.selectMerchantInGroup(id, Util.getUser().getMerchant().getId());
            for (Map<String, Object> objectMap : list) {
                if (fxChannelMerchant.getChannelId().equals(MapUtils.getIntValue(objectMap, "id"))) {
                    map.put("inId", fxChannelMerchant.getChannelId());
                    map.put("name", objectMap.get("name"));
                    map.put("in", true);
                    break;
                } else {
                    map.put("in", false);
                }
            }
            return Result.ok(list).setData(map);
        }catch (Exception e){
            throw Result.fail(e.getMessage()).exception();
        }
    }

    @ApiOperation(value = "渠道组设置返佣", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping(value = "set/rebate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("unchecked")
    public Result<?> setRebate(@RequestBody Map<String, Object> params) {
        List<Map<String, Object>> list = (List<Map<String, Object>>) params.get("proList");
        Integer channelId = MapUtils.getIntValue(params, "channelId");
        return groupService.setRebate(list, channelId);
    }
}
