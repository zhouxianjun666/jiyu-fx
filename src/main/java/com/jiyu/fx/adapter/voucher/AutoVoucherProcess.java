package com.jiyu.fx.adapter.voucher;

import cn.hutool.core.lang.Snowflake;
import com.github.pagehelper.PageInfo;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.*;
import com.jiyu.fx.entity.FxMaConfig;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 自动凭证处理
 * @date 2019/2/22 16:07
 */
@Component
public class AutoVoucherProcess implements VoucherProcess {
    @Autowired
    @Getter
    private ApplicationContext applicationContext;
    @Autowired
    private Snowflake snowflake;

    @Override
    public String type() {
        return Constant.VoucherServiceType.AUTO;
    }

    @Override
    public Result<SendTicketResult> ticketing(FxMaConfig maConfig, SendTicketInfo info) {
        return Result.ok(new SendTicketResult()
                .setNumber(info.getNumber())
                .setProcTime(new Date())
                .setVouchers(info.getDayInfos().stream()
                        .flatMap(dInfo -> dInfo.getVisitors().stream().map(v -> new TicketSendInfo()
                                .setVoucherNumber(String.valueOf(snowflake.nextId()))
                                .setVisitorId(v.getId())
                                .setDay(dInfo.getDay())
                        ))
                        .collect(Collectors.toList())
                )
        );
    }

    @Override
    public Result<RefundTicketResult> refund(FxMaConfig maConfig, RefundTicketInfo info) {
        return Result.ok(new RefundTicketResult()
                .setNumber(Long.valueOf(info.getSerialNo()))
                .setProcTime(new Date())
                .setSerialNo(info.getSerialNo())
        );
    }

    @Override
    public Result<?> pullProduct(FxMaConfig maConfig, Integer pageNum, Integer pageSize) {
        List<MaProduct> list = Collections.singletonList(new MaProduct().setId("AT").setName("自动"));
        return Result.ok(pageNum != null && pageSize != null ? PageInfo.of(list) : list);
    }
}
