package com.jiyu.fx.adapter.voucher;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.*;
import com.jiyu.fx.entity.FxMaConfig;
import org.springframework.context.ApplicationContext;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/22 15:38
 */
public interface VoucherProcess {
    /**
     * 获取spring上下文
     * @return
     */
    ApplicationContext getApplicationContext();

    /**
     * 类型
     *
     * @return
     */
    String type();

    /**
     * 出票
     * @param maConfig 凭证配置
     * @param info 出票信息
     * @return
     */
    Result<SendTicketResult> ticketing(FxMaConfig maConfig, SendTicketInfo info);

    /**
     * 退票
     * @param maConfig 凭证配置
     * @param info 退票信息
     * @return
     */
    Result<RefundTicketResult> refund(FxMaConfig maConfig, RefundTicketInfo info);

    /**
     * 拉取产品列表
     * @param maConfig 凭证配置
     * @param pageNum 页码
     * @param pageSize 当前页条数
     * @return List(MaProduct) OR PageInfo(MaProduct)
     */
    Result<?> pullProduct(FxMaConfig maConfig, Integer pageNum, Integer pageSize);
}
