package com.jiyu.fx.adapter.voucher;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONUtil;
import com.jiyu.base.dto.UserIdentityUser;
import com.jiyu.base.manager.JiYuRequestManager;
import com.jiyu.base.manager.UserManager;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.*;
import com.jiyu.fx.entity.FxMaConfig;
import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 基裕采购开放对接平台处理
 * @date 2019/2/25 11:02
 */
@Component
public class JYVoucherProcess implements VoucherProcess {
    @Autowired
    @Getter
    private ApplicationContext applicationContext;
    private Map<Integer, UserIdentityUser> merchantMap = new HashMap<>(1);
    @Autowired
    private JiYuRequestManager requestManager;
    @Autowired
    private UserManager userManager;

    @Override
    public String type() {
        return Constant.VoucherServiceType.JY;
    }

    @Override
    public Result<SendTicketResult> ticketing(FxMaConfig maConfig, SendTicketInfo info) {
        JYTicketConfig config = JSONUtil.toBean(maConfig.getConfig(), JYTicketConfig.class);
        return request(maConfig, config.getCreateUrl(), info, HttpMethod.POST, SendTicketResult.class);
    }

    @Override
    public Result<RefundTicketResult> refund(FxMaConfig maConfig, RefundTicketInfo info) {
        JYTicketConfig config = JSONUtil.toBean(maConfig.getConfig(), JYTicketConfig.class);
        return request(maConfig, config.getRefundUrl(), info, HttpMethod.POST, RefundTicketResult.class);
    }

    @Override
    public Result<?> pullProduct(FxMaConfig maConfig, Integer pageNum, Integer pageSize) {
        JYTicketConfig config = JSONUtil.toBean(maConfig.getConfig(), JYTicketConfig.class);
        return request(maConfig, config.getProductUrl(), MapUtil.builder(new HashMap<String, Object>(2))
                .put("pageNum", pageNum)
                .put("pageSize", pageSize)
                .build(), HttpMethod.GET
        );
    }

    @SneakyThrows
    private <T> Result<T> request(FxMaConfig maConfig, String url, Object params, HttpMethod method, Class<T> valueType) {
        return request(maConfig, url, BeanUtil.beanToMap(params), method, valueType);
    }

    @SneakyThrows
    private <T> Result<T> request(FxMaConfig maConfig, String url, Object params, HttpMethod method) {
        return request(maConfig, url, BeanUtil.beanToMap(params), method, null);
    }

    @SneakyThrows
    private <T> Result<T> request(FxMaConfig maConfig, String url, HttpMethod method) {
        return request(maConfig, url, null, method, null);
    }

    @SneakyThrows
    private <T> Result<T> request(FxMaConfig maConfig, String url, Map<String, Object> params, HttpMethod method, Class<T> valueType) {
        UserIdentityUser identityUser = merchantMap.computeIfAbsent(maConfig.getMerchantId(), this::getAuthInfo);
        Assert.notNull(identityUser, "商户信息异常");
        return requestManager.request(identityUser.getIdentity().getAccessKey(), identityUser.getIdentity().getAccessSecret(), url, params, method, valueType);
    }

    private UserIdentityUser getAuthInfo(int merchantId) {
        return userManager.getAdminByMerchant(merchantId);
    }
}
