package com.jiyu.fx.adapter.order;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.StrUtil;
import com.jiyu.base.mapper.MemberMapper;
import com.jiyu.base.mapper.MerchantMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.dto.*;
import com.jiyu.common.entity.base.Member;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.dto.ChangeStock;
import com.jiyu.fx.dto.ItemPriceDTO;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.manager.ProductPlanManager;
import com.jiyu.fx.manager.ProductSalesManager;
import com.jiyu.fx.mapper.FxOrderMapper;
import com.jiyu.fx.mapper.FxProductChainMapper;
import com.jiyu.fx.vo.BookingItemVO;
import com.jiyu.fx.vo.BookingVO;
import com.jiyu.fx.vo.BookingVisitorVO;
import lombok.SneakyThrows;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopContext;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单预定接口, 实现接口如需要事务或者检查Result则必须在目标方法增加相应注解
 * @date 2019/1/14 14:21
 */
public interface BookingOrder {
    /**
     * 获取spring上下文
     *
     * @return
     */
    ApplicationContext getApplicationContext();

    /**
     * 类型
     *
     * @return
     */
    String type();

    /**
     * 校验
     *
     * @param vo   预定信息
     * @param user 当前用户信息
     * @return
     */
    @CheckResult
    default Result<?> validate(BookingVO vo, AuthUser user) {
        MerchantMapper merchantMapper = getApplicationContext().getBean(MerchantMapper.class);
        Merchant merchant = merchantMapper.selectByPrimaryKey(user.getMerchant().getId());
        Assert.notNull(merchant, "商户不存在");
        vo.setPaymentChannel(Optional.ofNullable(vo.getPaymentChannel()).orElse(Constant.PaymentChannel.PLATFORM));
        return Result.ok();
    }

    /**
     * 检查是否存在
     *
     * @param vo   预定信息
     * @param user 当前用户信息
     * @return 返回为true则为已存在
     */
    @CheckResult
    default Result<Boolean> isExist(BookingVO vo, AuthUser user) {
        if (StrUtil.isNotBlank(vo.getOuterId())) {
            FxOrderMapper fxOrderMapper = getApplicationContext().getBean(FxOrderMapper.class);
            FxOrder order = fxOrderMapper.selectOne(new FxOrder().setBuyMerchantId(user.getMerchant().getId()).setOuterId(vo.getOuterId()));
            if (order != null) {
                return Result.ok(true).put("order", order);
            }
        }
        return Result.ok(false);
    }

    /**
     * 模拟扣减可售库存
     *
     * @param items
     * @param oversell
     * @return
     */
    default Map<Long, Map<Date, Boolean>> mockReducedAvailableStock(List<BookingItemVO> items, boolean oversell) {
        Map<Long, Map<Date, Boolean>> result = new HashMap<>(items.size());
        for (BookingItemVO item : items) {
            for (int i = 0; i < item.getDays(); i++) {
                DateTime time = DateUtil.offsetDay(item.getBooking(), i);
                result.computeIfAbsent(item.getProductSubNumber(), k -> new HashMap<>(item.getDays()))
                        .putIfAbsent(time.toJdkDate(), oversell);
            }
        }
        return result;
    }

    /**
     * 减可售库存
     * 独立事务，缩短事务链，提高性能。在任务中做回滚补偿
     *
     * @param items 预定产品列表
     * @param order 订单
     * @param infos 产品
     * @return
     */
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class}, propagation = Propagation.REQUIRES_NEW)
    default Map<Long, Map<Date, Boolean>> reducedAvailableStock(List<BookingItemVO> items, FxOrder order, List<ProductSalesInfo> infos) {
        RedissonClient redissonClient = getApplicationContext().getBean(RedissonClient.class);
        RMap<Long, List<ChangeStock>> map = redissonClient.getMap(Constant.REDUCE_AVAILABLE_STOCK);
        if (map.containsKey(order.getNumber())) {
            throw Result.fail("请稍候再试").exception();
        }
        ProductPlanManager planManager = getApplicationContext().getBean(ProductPlanManager.class);
        // 扣减可售库存
        List<ChangeStock> changeStocks = items.stream().map(item -> new ChangeStock()
                .setCode(item.getProductSubNumber())
                .setDays(item.getDays())
                .setStart(item.getBooking())
                .setAvailable(-item.getQuantity())
        ).collect(Collectors.toList());
        Map<Long, Integer> subQuantityMap = changeStocks.stream().collect(Collectors.toMap(ChangeStock::getCode, ChangeStock::getAvailable));
        Map<Long, Map<Date, Boolean>> stock = planManager.changeStock(changeStocks);
        List<ChangeStock> list = stock.entrySet().stream().flatMap(entry -> entry.getValue().entrySet().stream()
                .filter(entry2 -> !entry2.getValue())
                .map(entry2 -> new ChangeStock()
                        .setStart(entry2.getKey())
                        .setAvailable(subQuantityMap.get(entry.getKey()))
                        .setCode(entry.getKey())
                )
        ).collect(Collectors.toList());
        map.fastPut(order.getNumber(), list);
        return stock;
    }

    /**
     * 验证产品是否可售
     *
     * @param vo    预定信息
     * @param order 订单
     * @param user  当前用户信息
     * @return key-子产品编号,value-验证返回信息
     */
    @CheckResult(emptyValue = false)
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = {Exception.class, RuntimeException.class})
    default Result<Map<Long, ProductSalesInfo>> validateAvailableSale(BookingVO vo, FxOrder order, AuthUser user) {
        Set<Long> codes = vo.getItems().stream().map(BookingItemVO::getProductSubNumber).collect(Collectors.toSet());
        FxProductChainMapper chainMapper = getApplicationContext().getBean(FxProductChainMapper.class);
        ProductPlanManager planManager = getApplicationContext().getBean(ProductPlanManager.class);
        ProductSalesManager salesManager = getApplicationContext().getBean(ProductSalesManager.class);
        List<ProductSalesInfo> infos = salesManager.getBasicProductSalesInfos(codes);
        Optional<Long> optional = codes.stream().filter(code -> infos.stream()
                .noneMatch(info -> Objects.equals(code, info.getProductSubNumber()))).findFirst();
        if (optional.isPresent()) {
            return Result.fail(StrUtil.format("产品:{}不存在", optional.get()));
        }
        Map<Long, Map<Date, Boolean>> stockMap;
        if (vo.getRollback()) {
            stockMap = mockReducedAvailableStock(vo.getItems(), true);
        } else {
            stockMap = ((BookingOrder) AopContext.currentProxy()).reducedAvailableStock(vo.getItems(), order, infos);
        }

        int merchantId = user.getMerchant().getId();
        Map<Long, BookingItemVO> itemVOMap = vo.getItems().stream().collect(Collectors.toMap(BookingItemVO::getProductSubNumber, Function.identity()));
        for (ProductSalesInfo info : infos) {
            MerchantSalesInfo salesInfo;
            if (info.getSupplierMerchantId() == merchantId) {
                salesInfo = new MerchantSalesInfo().setMerchantId(merchantId).setSupplyMerchantId(merchantId).setRebate(0).setRebateType(Constant.ProfitType.FIXED);
            } else {
                FxProductChain chain = chainMapper.selectOne(new FxProductChain().setProductSubId(info.getProductSubId()).setMerchantId(merchantId));
                if (chain == null) {
                    return Result.fail(StrUtil.format("产品:{}该企业未被分销", info.getProductSubName()));
                }
                salesInfo = salesManager.chainToSalesInfo(chain);
            }
            info.setSalesInfos(salesInfo);

            BookingItemVO itemVO = itemVOMap.get(info.getProductSubNumber());
            List<FxProductPlan> plans = planManager.getByTime(info.getProductSubId(), itemVO.getBooking(), itemVO.getDays());
            if (CollUtil.isEmpty(plans) || plans.size() != itemVO.getDays()) {
                return Result.fail(StrUtil.format("产品:{}销售计划不全", info.getProductSubName()));
            }
            Map<Date, Boolean> oversellMap = stockMap.get(info.getProductSubNumber());
            Map<BetweenDate, ProductPlanInfo> everyDayInfo = planManager.planToEveryDayInfo(plans);
            everyDayInfo.forEach((day, plan) -> plan.setOversell(oversellMap.get(day.getBegin())));
            info.setDayPlanMap(everyDayInfo);
        }
        return Result.ok(infos.stream().collect(Collectors.toMap(ProductSalesInfo::getProductSubNumber, Function.identity())));
    }

    /**
     * 保存主订单信息
     *
     * @param vo   预定信息
     * @param user 当前用户信息
     * @return
     */
    @CheckResult(emptyValue = false)
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = {Exception.class, RuntimeException.class})
    default Result<FxOrder> makeOrder(BookingVO vo, AuthUser user) {
        Snowflake snowflake = getApplicationContext().getBean(Snowflake.class);
        FxOrder order = new FxOrder()
                .setNumber(snowflake.nextId())
                .setStatus(Constant.OrderStatus.PENDING_PAY)
                .setBuyMerchantId(user.getMerchant().getId())
                .setBuyMerchantName(user.getMerchant().getName())
                .setBuyUserId(user.getId())
                .setBuyUserName(user.getFullName())
                .setSource(vo.getSource())
                .setOuterId(vo.getOuterId())
                .setPaymentChannel(vo.getPaymentChannel())
                .setRemark(vo.getRemark())
                .setCreateTime(new Date());
        if (user.getWxUser() != null) {
            order.setMemberId(user.getWxUser().getMember().getId());
        }
        return Result.ok(order);
    }

    /**
     * 验证预定游玩日期
     *
     * @param info         产品信息
     * @param item         子订单信息
     * @param vo           预定信息
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult
    default Result<?> validateBookingDate(ProductSalesInfo info, BookingItemVO item, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        Date now = new Date();
        info.getDayPlanMap().forEach((betweenDate, plan) -> {
            if (plan.isBookingLimit()) {
                int dayLimit = plan.getBookingDayLimit();
                DateTime dateTime = DateUtil.offsetDay(betweenDate.getBegin(), -dayLimit);
                String timeLimit = plan.getBookingTimeLimit();
                if (StrUtil.isNotBlank(timeLimit)) {
                    try {
                        String[] timeArray = timeLimit.split(":");
                        dateTime.setField(DateField.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
                        dateTime.setField(DateField.MINUTE, Integer.parseInt(timeArray[1]));
                    } catch (Exception e) {
                        LoggerFactory.getLogger(getClass()).warn("验证预定游玩日期解析时间限制异常", e);
                    }
                }
                if (now.after(dateTime)) {
                    String dayMsg = dayLimit == 0 ? "当天" : "前" + dayLimit + "天";
                    throw Result.fail(StrUtil.format("需在订购时间的{}的{}前预订", dayMsg, DateUtil.format(dateTime, "HH:mm"))).exception();
                }
            } else {
                int useSecondLimit = plan.getUseSecondLimit();
                if (useSecondLimit > 0) {
                    DateTime dateTime = DateUtil.offsetSecond(now, useSecondLimit);
                    if (dateTime.after(DateUtil.endOfDay(now))) {
                        throw Result.fail(StrUtil.format("需在当天订购时间的{}小时前预订", TimeUnit.SECONDS.toHours(useSecondLimit))).exception();
                    }
                }
            }
        });
        return beforeResult;
    }

    /**
     * 验证游客信息
     *
     * @param info         产品信息
     * @param item         子订单信息
     * @param vo           预定信息
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult
    default Result<?> validateVisitor(ProductSalesInfo info, BookingItemVO item, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        if (CollUtil.isEmpty(item.getVisitors())) {
            return Result.fail("请填写游客信息", beforeResult);
        }
        if (info.getRealName() == Constant.RealNameType.NONE || info.getRealName() == 0) {
            return beforeResult;
        }
        Set<String> cardNos = item.getVisitors().stream().map(BookingVisitorVO::getCardNo).filter(Objects::nonNull).collect(Collectors.toSet());
        if (CollUtil.isEmpty(cardNos)) {
            return Result.fail("该产品需实名认证,请填写游客身份信息。", beforeResult);
        }
        if (cardNos.size() < item.getVisitors().size()) {
            return Result.fail("游客身份信息重复。", beforeResult);
        }
        if (info.getRealName() == Constant.RealNameType.ONE_TO_MANY) {
            if (item.getVisitors().size() != 1) {
                return Result.fail("当前产品为一证多票,只需要一个游客信息");
            }
            BookingVisitorVO visitorVO = item.getVisitors().get(0);
            if (StrUtil.isEmpty(visitorVO.getCardNo())) {
                return Result.fail("当前产品为一证多票,缺少实名信息");
            }
            if (visitorVO.getCardType() == Constant.CardType.ID && !IdcardUtil.isValidCard(visitorVO.getCardNo())) {
                return Result.fail("当前产品为一证多票,请填写正确的证件信息");
            }
        }
        if (info.getRealName() == Constant.RealNameType.ONE_TO_ONE) {
            if (item.getVisitors().size() != item.getQuantity()) {
                return Result.fail("当前产品为一证一票,游客数量与票数不符");
            }
            if (item.getVisitors().stream().anyMatch(v -> v.getCardType() == Constant.CardType.ID && !IdcardUtil.isValidCard(v.getCardNo()))) {
                return Result.fail("当前产品为一证一票,请填写正确的证件信息");
            }
        }
        int sidDayQuantity = info.getSidDayQuantity();
        if (sidDayQuantity > 0) {
            OrderManager orderManager = getApplicationContext().getBean(OrderManager.class);
            Map<String, Integer> cardNoQuantityMao = orderManager.getCardNoQuantityByDay(item.getProductSubNumber(), item.getBooking(), cardNos);
            Optional<BookingVisitorVO> optional = item.getVisitors().stream()
                    .filter(v -> v.getQuantity() + cardNoQuantityMao.getOrDefault(v.getCardNo(), 0) > sidDayQuantity)
                    .findFirst();
            if (optional.isPresent()) {
                return Result.fail(StrUtil.format("身份信息:{}超出产品【{}】当天最大购票数,已定张数{},最大购票张数{}",
                        optional.get().getCardNo(),
                        info.getProductSubName(),
                        cardNoQuantityMao.getOrDefault(optional.get().getCardNo(), 0),
                        sidDayQuantity), beforeResult);
            }
        }
        return beforeResult;
    }

    /**
     * 计算产品每天抵扣金额
     * @param info                  产品售卖信息
     * @param item                  子订单预定信息
     * @param quantity              子产品预定总数(不X天)
     * @param merchantSalesPriceMap 商户每日销售价格信息
     * @param vo                    订单预定信息
     * @param user                  当前用户信息
     * @param beforeResult          上一个阶段返回信息
     * @return
     */
    @CheckResult
    default Result<Map<Date, Long>> calcItemDiscount(ProductSalesInfo info, BookingItemVO item, int quantity, Map<Date,
                                                     MerchantSalesInfoPrice> merchantSalesPriceMap, long itemNumber,
                                                     BookingVO vo, AuthUser user, Result<?> beforeResult) {
        return Result.ok();
    }

    /**
     * 计算产品售价
     *
     * @param info                  产品售卖信息
     * @param item                  子订单预定信息
     * @param quantity              子产品预定总数(不X天)
     * @param merchantSalesPriceMap 商户每日销售价格信息
     * @param discountMap           抵扣总金额
     * @param vo                    订单预定信息
     * @param user                  当前用户信息
     * @param beforeResult          上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<ItemPriceDTO> calcItemPrice(ProductSalesInfo info, BookingItemVO item, int quantity, Map<Date, MerchantSalesInfoPrice> merchantSalesPriceMap,
                                               Map<Date, Long> discountMap, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        int lowPrice = 0, settlePrice = 0, rebate = 0;
        long discountMoney = 0;
        for (Map.Entry<Date, MerchantSalesInfoPrice> entry : merchantSalesPriceMap.entrySet()) {
            MerchantSalesInfoPrice infoPrice = entry.getValue();
            ProductPlanInfo plan = OrderManager.getPlanByTime(info.getDayPlanMap(), entry.getKey());
            Assert.notNull(plan, "该时间:{}没有销售计划", entry.getKey());
            int dayLowPrice = plan.getLowPrice();
            if (infoPrice.getRebateValue() > dayLowPrice) {
                return Result.fail("超出售价");
            }
            int price = dayLowPrice * quantity;
            long discount = discountMap.getOrDefault(entry.getKey(), 0L);
            if (discount > price) {
                return Result.fail("抵扣金额超出支付金额");
            }
            lowPrice += price;
            settlePrice += plan.getSettlePrice() * quantity;
            discountMoney += discount;
            rebate += infoPrice.getRebateValue() * quantity;
        }
        return Result.ok(new ItemPriceDTO().setLowPrice(lowPrice).setSettlePrice(settlePrice).setDiscountMoney(discountMoney).setRebate(rebate));
    }

    /**
     * 保存订单联系人信息
     *
     * @param order        订单信息
     * @param vo           预定信息
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    default Result<Member> saveOrderShipping(FxOrder order, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        MemberMapper memberMapper = getApplicationContext().getBean(MemberMapper.class);
        BookingVisitorVO shipping = vo.getShipping();
        Member member = memberMapper.selectOne(new Member().setPhone(shipping.getPhone()));
        member = Optional.ofNullable(member).orElse(new Member());
        member.setPhone(shipping.getPhone())
                .setNickname(shipping.getName())
                .setGender(shipping.getGender());
        if (shipping.getCardType() != null && StrUtil.isNotEmpty(shipping.getCardNo())) {
            member.setIdCard(shipping.getCardType() == Constant.CardType.ID ? shipping.getCardNo() : null);
        }
        if (member.getId() == null) {
            memberMapper.insertSelective(member);
        } else {
            memberMapper.updateByPrimaryKeySelective(member);
        }
        return Result.ok(member);
    }

    /**
     * 保存子订单信息
     *
     * @param order        订单信息
     * @param price        价格信息
     * @param quantity     数量
     * @param info         售卖信息
     * @param item         子产品预定信息
     * @param vo           预定信息
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    default Result<FxOrderItem> makeOrderItem(FxOrder order, ItemPriceDTO price, long itemNumber, int quantity, ProductSalesInfo info,
                                              BookingItemVO item, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        boolean isAudit = info.getDayPlanMap().values().stream().anyMatch(ProductPlanInfo::isAudit);
        FxOrderItem orderItem = new FxOrderItem()
                .setOrderNumber(order.getNumber())
                .setCreateTime(new Date())
                .setDays(item.getDays())
                .setStartTime(info.getDayPlanMap().keySet().iterator().next().getBegin())
                .setEndTime(CollUtil.getLast(info.getDayPlanMap().keySet()).getEnd())
                .setLowPrice(price.getLowPrice())
                .setLowUesQuantity(info.getLowUseQuantity())
                .setMaConfigId(info.getVoucherId())
                .setMaProductId(info.getMaProductId())
                .setNumber(itemNumber)
                .setProductName(info.getProductName())
                .setProductNumber(info.getProductNumber())
                .setProductSubName(info.getProductSubName())
                .setProductSubNumber(info.getProductSubNumber())
                .setProductType(info.getProductType())
                .setQuantity(quantity)
                .setSettlePrice(price.getSettlePrice())
                .setDiscountMoney(price.getDiscountMoney())
                .setRebate(price.getRebate())
                .setSmsTemplateId(info.getSmsTemplateId())
                .setSupplierMerchantId(info.getSupplierMerchantId())
                .setSupplierMerchantName(info.getSupplierMerchantName())
                .setSupplierPhone(info.getSupplierPhone())
                .setTicketFlag(info.getTicketFlag())
                .setTicketMsg(info.getTicketMsg())
                .setRefundAudit(info.isRefundAudit())
                .setAuditSuccess(!isAudit)
                .setStatus(isAudit ? Constant.OrderItemStatus.WAIT_AUDIT : Constant.OrderItemStatus.NON_SEND)
                .setOutItemId(vo.getOutItemId());
        return Result.ok(orderItem);
    }

    /**
     * 保存子订单详情信息
     *
     * @param order             订单信息
     * @param orderItem         子订单
     * @param info              产品售卖信息
     * @param salesInfoPriceMap 每日销售链信息
     * @param item              子订单信息
     * @param vo                预定信息
     * @param user              当前用户信息
     * @param beforeResult      上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @SneakyThrows
    default Result<List<FxOrderItemDetail>> makeOrderItemDetail(FxOrder order, FxOrderItem orderItem, ProductSalesInfo info,
                                                                Map<Date, MerchantSalesInfoPrice> salesInfoPriceMap,
                                                                BookingItemVO item, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        return Result.ok(info.getDayPlanMap().entrySet().stream().map(entry -> {
            Date now = new Date();
            Date effectiveDate;
            Date expiryDate;
            ProductPlanInfo plan = entry.getValue();
            Date booking = entry.getKey().getBegin();
            if (plan.getEffectiveType() == Constant.EffectiveType.DAY) {
                effectiveDate = booking.after(now) ? booking : now;
                expiryDate = DateUtil.offsetDay(booking, plan.getEffectiveDay() - 1)
                        .setField(DateField.HOUR_OF_DAY, DateUtil.hour(entry.getKey().getEnd(), true))
                        .setField(DateField.MINUTE, DateUtil.minute(entry.getKey().getEnd()))
                        .setField(DateField.SECOND, DateUtil.second(entry.getKey().getEnd()));
            } else {
                // 如果预定游玩日期大于生效日期则使用预定游玩日期否则使用生效日期
                Date effectiveStartDate = plan.getEffectiveStartDate();
                if (effectiveStartDate == null) {
                    effectiveStartDate = booking;
                }
                effectiveDate = booking.after(effectiveStartDate) ? booking : effectiveStartDate;
                expiryDate = plan.getEffectiveEndDate();
            }
            //如果有使用时间限制,生效时间往后顺延
            if (plan.getUseSecondLimit() > 0) {
                Date newer = DateUtil.offsetSecond(now, plan.getUseSecondLimit());
                effectiveDate = newer.after(effectiveDate) ? newer : effectiveDate;
            }

            if (effectiveDate.after(expiryDate)) {
                throw Result.fail("该产品已过期", beforeResult).exception();
            }
            // 不能购买已过销售计划的产品
            if (now.after(entry.getKey().getEnd())) {
                throw Result.fail(StrUtil.format("您订购的产品[{}]预定时间已过期", info.getProductSubName()), beforeResult).exception();
            }
            return new FxOrderItemDetail()
                    .setCreateTime(now)
                    .setDay(booking)
                    .setOversell(plan.isOversell())
                    .setDisableDay(plan.getDisableDate())
                    .setDisableWeek(plan.getDisableWeek())
                    .setLowUseQuantity(info.getLowUseQuantity())
                    .setQuantity(orderItem.getQuantity())
                    .setRefundRule(plan.getRefundRule())
                    .setUseLimitSecond(plan.getUseSecondLimit())
                    .setRefundQuantity(0)
                    .setUsedQuantity(0)
                    .setEffectiveDate(effectiveDate)
                    .setExpiryDate(expiryDate)
                    .setSettlePrice(plan.getSettlePrice())
                    .setLowPrice(plan.getLowPrice())
                    .setRebate(salesInfoPriceMap.get(booking).getRebateValue());
        }).collect(Collectors.toList()));
    }

    /**
     * 保存子订单游客信息
     *
     * @param order        订单信息
     * @param orderItem    子订单信息
     * @param item         子产品预定信息
     * @param vo           预定信息
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    default Result<List<FxOrderItemVisitor>> makeOrderItemVisitor(FxOrder order, FxOrderItem orderItem, BookingItemVO item,
                                                                  BookingVO vo, AuthUser user, Result<?> beforeResult) {
        return Result.ok(item.getVisitors().stream().map(v -> new FxOrderItemVisitor()
                .setName(v.getName())
                .setPhone(v.getPhone())
                .setTempQuantity(v.getQuantity())
                .setCardType(StrUtil.isBlank(v.getCardNo()) ? null : v.getCardType())
                .setCardNo(v.getCardNo())
                .setGender(Optional.ofNullable(v.getGender()).orElse(0))
        ).collect(Collectors.toList()));
    }

    /**
     * 是否回滚本次操作
     *
     * @param order        订单信息
     * @param vo           预定信息
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult
    default Result<Boolean> isRollback(FxOrder order, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        return Result.ok(Optional.ofNullable(vo.getRollback()).orElse(false));
    }

    /**
     * 后期处理
     *
     * @param order          订单信息
     * @param items          子订单信息集合
     * @param salesInfoMap   售卖信息
     * @param shipping       订单联系人
     * @param vo             预定信息
     * @param user           当前用户信息
     * @param rollbackResult 上一个阶段返回信息
     * @return
     */
    @CheckResult
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    default Result<FxOrder> after(FxOrder order, List<FxOrderItem> items, Map<Long, ProductSalesInfo> salesInfoMap, Member shipping, BookingVO vo, AuthUser user, Result<Boolean> rollbackResult) {
        if (rollbackResult.getValue()) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return Result.ok(order).put("items", items).put("shipping", shipping).put("visitors", vo.getItems().get(0).getVisitors());
    }
}
