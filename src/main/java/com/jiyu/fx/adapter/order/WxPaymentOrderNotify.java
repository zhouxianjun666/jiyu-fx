package com.jiyu.fx.adapter.order;

import cn.hutool.core.util.StrUtil;
import com.jiyu.base.controller.notify.adapter.WxPaymentNotifyAdapter;
import com.jiyu.base.manager.MerchantPaymentConfigManager;
import com.jiyu.base.manager.WxManager;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.common.dto.WxPaymentConfig;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.TreeMap;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/30 9:42
 */
@Component
@Slf4j
public class WxPaymentOrderNotify implements WxPaymentNotifyAdapter {
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private MerchantPaymentConfigManager epPaymentConfigManager;
    @Resource
    private FxOrderMapper orderMapper;
    @Resource
    private FxOrderItemMapper orderItemMapper;

    @Override
    public void process(String number, String transactionId, TreeMap<String, Object> map, HttpServletResponse response) {
        FxOrder order = orderMapper.selectOne(new FxOrder().setNumber(Long.valueOf(number)));
        Assert.notNull(order, "订单不存在");
        int total = MapUtils.getIntValue(map, "total_fee");
        if (total != order.getMoney()) {
            throw Result.fail(StrUtil.format("微信通知: 订单号={} 支付金额不匹配 => {} -> {}", number, total, order.getMoney())).exception();
        }
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderNumber(order.getNumber()));
        WxPaymentConfig paymentConfig = epPaymentConfigManager.get(items.get(0).getSupplierMerchantId(), Constant.PaymentConfigType.WX);
        if (paymentConfig == null) {
            log.warn("微信通知: 订单号={} 微信支付配置不存在, 不检查签名.", number);
        } else {
            boolean verify = WxManager.verify(map, paymentConfig.toCfg());
            if (!verify) {
                throw Result.fail(StrUtil.format("微信通知: 订单号={} 签名验证失败", number)).exception();
            }
        }
        orderManager.paid(order.getNumber(), transactionId);
    }

    @Override
    public String type() {
        return "order";
    }
}
