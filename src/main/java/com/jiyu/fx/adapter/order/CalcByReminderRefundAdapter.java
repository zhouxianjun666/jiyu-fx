package com.jiyu.fx.adapter.order;

import cn.hutool.core.collection.CollUtil;
import com.jiyu.common.dto.RefundRule;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxOrderItemVisitor;
import com.jiyu.fx.entity.FxRefundOrder;
import com.jiyu.fx.entity.FxRefundVisitor;
import com.jiyu.fx.mapper.FxOrderItemVisitorMapper;
import com.jiyu.fx.vo.RefundVO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 退订计算(退剩余所有票)
 * @date 2019/1/31 12:38
 */
@Component
public class CalcByReminderRefundAdapter extends RemainderRefundAdapter {
    @Resource
    private FxOrderItemVisitorMapper visitorMapper;

    @Override
    public String type() {
        return "calc-" + super.type();
    }

    @Override
    public void save(RefundVO vo, FxOrderItem orderItem, Result<List<FxRefundVisitor>> visitorResult, FxRefundOrder refundOrder) {

    }

    @Override
    @SuppressWarnings("unchecked")
    public Result<?> after(RefundVO vo, FxRefundOrder refundOrder) {
        Optional<Result<?>> refundRuleRes = vo.getResults().stream()
                .filter(result -> result.getValue() instanceof Map)
                .filter(result -> CollUtil.getFirst(((Map) result.getValue()).values()) instanceof RefundRule)
                .findFirst();
        Optional<Result<?>> refundVisitorRes = vo.getResults().stream()
                .filter(result -> result.getValue() instanceof Collection)
                .filter(result -> CollUtil.getFirst((Collection) result.getValue()) instanceof FxRefundVisitor)
                .findFirst();
        Result<FxOrderItem> orderItemResult = vo.get(FxOrderItem.class);
        return Result.ok(refundOrder)
                .putIf("orderItem", r -> Objects.nonNull(orderItemResult), orderItemResult.getValue())
                .putIf("refundRule", r -> refundRuleRes.isPresent(), refundRuleRes.map(Result::getValue).orElse(null))
                .putIf("refundVisitor", r -> refundVisitorRes.isPresent() && r.getValue() != null, r -> {
                    List<FxRefundVisitor> refundVisitors = (List<FxRefundVisitor>) refundVisitorRes.map(Result::getValue).orElse(null);
                    if (refundVisitors == null) {
                        return null;
                    }
                    String ids = refundVisitors.stream().map(FxRefundVisitor::getVisitorId).map(String::valueOf).collect(Collectors.joining(","));
                    List<FxOrderItemVisitor> visitors = visitorMapper.selectByIds(ids);
                    Map<Integer, FxOrderItemVisitor> visitorMap = visitors.stream().collect(Collectors.toMap(FxOrderItemVisitor::getId, Function.identity()));
                    refundVisitors.forEach(rv -> rv.setVisitor(visitorMap.get(rv.getVisitorId())));
                    return refundVisitors;
                });
    }
}
