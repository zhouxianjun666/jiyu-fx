package com.jiyu.fx.adapter.order;

import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.ProductSalesInfo;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.vo.BookingItemVO;
import com.jiyu.fx.vo.BookingVO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/7 16:49
 */
@Component
public class HotelBookingAdapter implements BookingOrder {
    @Autowired
    @Getter
    private ApplicationContext applicationContext;

    @Override
    public String type() {
        return "hotel";
    }

    @Override
    @CheckResult
    public Result<?> validate(BookingVO vo, AuthUser user) {
        boolean check = vo.getItems().stream().anyMatch(item -> item.getDays() == null || item.getDays() <= 0);
        if (check) {
            return Result.fail("预定天数错误");
        }
        return BookingOrder.super.validate(vo, user);
    }

    @Override
    @CheckResult
    public Result<?> validateVisitor(ProductSalesInfo info, BookingItemVO item, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        return beforeResult;
    }
}
