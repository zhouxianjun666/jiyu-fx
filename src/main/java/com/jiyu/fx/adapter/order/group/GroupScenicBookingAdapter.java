package com.jiyu.fx.adapter.order.group;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.ProductSalesInfo;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Member;
import com.jiyu.fx.adapter.order.ScenicBookingAdapter;
import com.jiyu.fx.dto.ItemPriceDTO;
import com.jiyu.fx.entity.FxGroupMember;
import com.jiyu.fx.entity.FxGroupProduct;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.mapper.FxGroupMemberMapper;
import com.jiyu.fx.mapper.FxGroupProductMapper;
import com.jiyu.fx.mapper.FxOrderMapper;
import com.jiyu.fx.vo.BookingItemVO;
import com.jiyu.fx.vo.BookingVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/21 15:17
 */
@Component
@Slf4j
public class GroupScenicBookingAdapter extends ScenicBookingAdapter {
    @Resource
    private FxGroupMemberMapper groupMemberMapper;
    @Resource
    private FxGroupProductMapper groupProductMapper;
    @Resource
    private FxOrderMapper orderMapper;

    @Override
    public String type() {
        return "group-" + super.type();
    }

    @Override
    public Map<Long, Map<Date, Boolean>> reducedAvailableStock(List<BookingItemVO> items, FxOrder order, List<ProductSalesInfo> infos) {
        Map<Long, Map<Date, Boolean>> result = new HashMap<>(items.size());
        Map<Long, ProductSalesInfo> map = infos.stream().collect(Collectors.toMap(ProductSalesInfo::getProductSubNumber, Function.identity()));
        List<BookingItemVO> localPayList = items.stream().filter(item -> map.get(item.getProductSubNumber()).getPayType() == 2).collect(Collectors.toList());
        List<BookingItemVO> payList = items.stream().filter(item -> map.get(item.getProductSubNumber()).getPayType() == 1).collect(Collectors.toList());
        result.putAll(this.mockReducedAvailableStock(localPayList, true));
        result.putAll(super.reducedAvailableStock(payList, order, infos));
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = {Exception.class, RuntimeException.class})
    @CheckResult(emptyValue = false)
    public Result<Map<Long, ProductSalesInfo>> validateAvailableSale(BookingVO vo, FxOrder order, AuthUser user) {
        Result<Map<Long, ProductSalesInfo>> result = super.validateAvailableSale(vo, order, user);
        result.ifFailThrow();
        boolean checkFlag = result.getValue().values().stream().anyMatch(info -> info.getTicketFlag() != Constant.TicketFlag.GROUP);
        if (checkFlag) {
            return Result.fail("只能购买团队产品");
        }
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = {Exception.class, RuntimeException.class})
    @CheckResult(emptyValue = false)
    public Result<FxOrder> makeOrder(BookingVO vo, AuthUser user) {
        Result<FxOrder> result = super.makeOrder(vo, user);
        result.ifFailThrow();
        FxOrder order = result.getValue();
        order.setGroupId(vo.getGroup().getId());
        return result;
    }

    @Override
    @CheckResult
    public Result<?> validateBookingDate(ProductSalesInfo info, BookingItemVO item, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        Date startDate = vo.getGroup().getStartDate();
        if (startDate != null && item.getBooking().getTime() < startDate.getTime()) {
            return Result.fail("订购时间需大于团队出行日期");
        }
        return super.validateBookingDate(info, item, vo, user, beforeResult);
    }

    @Override
    @CheckResult
    public Result<?> validateVisitor(ProductSalesInfo info, BookingItemVO item, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        int minBuyQuantity = info.getMinBuyQuantity();
        if (minBuyQuantity > 0 && minBuyQuantity > item.getQuantity()) {
            return Result.fail("低于当前产品最低购买票数: " + minBuyQuantity);
        }
        if (info.getRealName() == Constant.RealNameType.NONE) {
            return beforeResult;
        }
        List<FxGroupMember> members = groupMemberMapper.select(new FxGroupMember().setBudgetId(vo.getGroupProduct().getId()));
        if (CollUtil.isEmpty(members)) {
            return Result.fail("当前产品需要实名认证");
        }
        if (members.size() > item.getQuantity()) {
            return Result.fail("游客信息与票数不符");
        }
        if (info.getRealName() == Constant.RealNameType.ONE_TO_MANY) {
            if (members.stream().noneMatch(m -> StrUtil.isNotBlank(m.getCard()))) {
                return Result.fail("当前产品为一证多票,缺少实名信息");
            }
        }
        if (info.getRealName() == Constant.RealNameType.ONE_TO_ONE) {
            if (members.size() != item.getQuantity() || !members.stream().allMatch(m -> StrUtil.isNotBlank(m.getCard()))) {
                return Result.fail("当前产品为一证一票,缺少实名信息");
            }
        }
        return beforeResult;
    }

    @Override
    @CheckResult(emptyValue = false)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<FxOrderItem> makeOrderItem(FxOrder order, ItemPriceDTO price, long itemNumber, int quantity, ProductSalesInfo info, BookingItemVO item, BookingVO vo, AuthUser user, Result<?> beforeResult) {
        if (info.getPayType() == Constant.ProductPayType.LOCAL_PAY) {
            orderMapper.updateByPrimaryKeySelective(order.setPaymentType(Constant.PaymentType.LOCAL_PAY).setStatus(Constant.OrderStatus.PAID));
        }
        return super.makeOrderItem(order, price, itemNumber, quantity, info, item, vo, user, beforeResult);
    }

    @Override
    @CheckResult
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<FxOrder> after(FxOrder order, List<FxOrderItem> items, Map<Long, ProductSalesInfo> salesInfoMap, Member shipping, BookingVO vo, AuthUser user, Result<Boolean> rollbackResult) {
        if (!rollbackResult.getValue()) {
            groupProductMapper.updateByPrimaryKeySelective(new FxGroupProduct()
                    .setId(vo.getGroupProduct().getId())
                    .setState(true)
                    .setRealName(CollUtil.getFirst(salesInfoMap.values()).getRealName())
                    .setOrderId(order.getId())
            );
        }
        return super.after(order, items, salesInfoMap, shipping, vo, user, rollbackResult);
    }
}
