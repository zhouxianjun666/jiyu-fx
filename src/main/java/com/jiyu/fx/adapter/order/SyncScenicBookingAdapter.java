package com.jiyu.fx.adapter.order;

import com.jiyu.common.Constant;
import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.ProductSalesInfo;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Member;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.vo.BookingVO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author zoujing
 * @ClassName:
 * @Description:
 * @date 2019/1/16 12:45
 */
@Component
public class SyncScenicBookingAdapter extends ScenicBookingAdapter {
    @Autowired
    @Getter
    private ApplicationContext applicationContext;

    @Autowired
    private OrderManager orderManager;

    @Override
    public String type() {
        return "syncScenic";
    }

    @Override
    @CheckResult
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<FxOrder> after(FxOrder order, List<FxOrderItem> items, Map<Long, ProductSalesInfo> salesInfoMap, Member shipping, BookingVO vo, AuthUser user, Result<Boolean> rollbackResult) {
        if (!rollbackResult.getValue()) {
            HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
            orderManager.payment(order, Constant.PaymentType.BALANCE, request, null);
        }
        return super.after(order, items, salesInfoMap, shipping, vo, user, rollbackResult);
    }
}
