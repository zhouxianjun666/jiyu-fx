package com.jiyu.fx.adapter.order;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.base.mapper.MerchantMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.dto.*;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.dto.DayRefundTemp;
import com.jiyu.fx.dto.DetailDiscountDTO;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.manager.RefundManager;
import com.jiyu.fx.manager.SmsNotifyManager;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxOrderMapper;
import com.jiyu.fx.mapper.FxRefundOrderMapper;
import com.jiyu.fx.mapper.FxRefundVisitorMapper;
import com.jiyu.fx.vo.RefundVO;
import com.jiyu.fx.vo.VisitorQuantityVO;
import lombok.SneakyThrows;
import org.springframework.context.ApplicationContext;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单退订接口, 实现接口如需要事务或者检查Result则必须在目标方法增加相应注解
 * @date 2019/1/21 16:53
 */
public interface RefundOrder {
    /**
     * 获取spring上下文
     * @return
     */
    ApplicationContext getApplicationContext();

    /**
     * 类型
     *
     * @return
     */
    String type();

    /**
     * 验证商户
     * @param vo 退订信息
     * @param order 订单信息
     * @param user 当前用户信息
     * @return
     */
    default Result<Merchant> validateMerchant(RefundVO vo, FxOrder order, AuthUser user) {
        MerchantMapper merchantMapper = getApplicationContext().getBean(MerchantMapper.class);
        Merchant merchant = merchantMapper.selectByPrimaryKey(user.getMerchant().getId());
        Assert.notNull(merchant, "商户不存在");
        if (!Objects.equals(order.getBuyMerchantId(), merchant.getId())) {
            return Result.fail("当前商户不能申请退订");
        }
        return Result.ok(merchant);
    }

    /**
     * 校验
     *
     * @param vo   退订信息
     * @param user 当前用户信息
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<FxOrder> validate(RefundVO vo, AuthUser user) {
        FxOrderMapper orderMapper = getApplicationContext().getBean(FxOrderMapper.class);
        OrderManager orderManager = getApplicationContext().getBean(OrderManager.class);
        FxOrder order = orderManager.getOrderByItem(vo.getNumber());
        if (order == null) {
            return Result.fail("订单不存在");
        }
        if (order.getPaymentType() == Constant.PaymentType.LOCAL_PAY) {
            return Result.fail("现场支付订单不允许退订");
        }
        if (order.getStatus() != Constant.OrderStatus.PAID) {
            return Result.fail("当前订单状态不能申请退订");
        }
        Result<Merchant> merchantResult = validateMerchant(vo, order, user);
        if (!merchantResult.isSuccess()) {
            throw merchantResult.exception();
        }
        int count = orderMapper.selectByJoinExampleTransform(JoinExample.builder(FxOrderItem.class)
                        .count()
                        .addTable(new JoinExample.Table(FxRefundOrder.class, FxRefundOrder::getOrderItemId, FxOrderItem::getId))
                        .where(JoinExample.Where.custom()
                                .andEqualTo(FxOrderItem::getOrderId, order.getId())
                                .andNotIn(FxRefundOrder::getStatus, Stream.of(
                                        Constant.RefundOrderStatus.AUDIT_REFUSE,
                                        Constant.RefundOrderStatus.COMPLETE,
                                        Constant.RefundOrderStatus.REFUND_TICKET_FAIL
                                        ).collect(Collectors.toSet())
                                )
                        )
                        .build(),
                int.class
        );
        if (count > 0) {
            return Result.fail("您还有未处理完的退订订单");
        }
        return Result.ok(order);
    }

    /**
     * 检查子订单
     *
     * @param vo    退订信息
     * @param order 订单
     * @param user  当前用户
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<FxOrderItem> checkOrderItem(RefundVO vo, FxOrder order, AuthUser user) {
        FxOrderItemMapper orderItemMapper = getApplicationContext().getBean(FxOrderItemMapper.class);
        FxOrderItem orderItem = orderItemMapper.selectOne(new FxOrderItem().setNumber(vo.getNumber()));
        if (!ArrayUtil.contains(new int[]{
                Constant.OrderItemStatus.SEND,
                Constant.OrderItemStatus.NON_SEND,
                Constant.OrderItemStatus.TICKET_FAIL
        }, orderItem.getStatus())) {
            return Result.fail("当前订单状态不允许退订");
        }
        return Result.ok(orderItem);
    }

    /**
     * 检查是否存在
     *
     * @param vo   退订信息
     * @param item 子订单信息
     * @param user 当前用户信息
     * @return 返回为true则为已存在
     */
    @CheckResult
    default Result<Boolean> isExist(RefundVO vo, FxOrderItem item, AuthUser user) {
        if (StrUtil.isNotBlank(vo.getOuterId())) {
            FxRefundOrderMapper refundOrderMapper = getApplicationContext().getBean(FxRefundOrderMapper.class);
            FxRefundOrder refundOrder = refundOrderMapper.selectOne(new FxRefundOrder().setOrderItemId(item.getId()).setOuterId(vo.getOuterId()));
            if (refundOrder != null) {
                return Result.ok(true).put("refundOrder", refundOrder);
            }
        }
        return Result.ok(false);
    }

    /**
     * 解析游客退订信息
     *
     * @param vo           退订细腻
     * @param item         子订单信息
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<Map<Date, List<VisitorQuantityVO>>> parseDayRefund(RefundVO vo, FxOrderItem item,
                                                                    AuthUser user, Result<?> beforeResult) {
        Map<Date, List<VisitorQuantityVO>> visitorMap = vo.getVisitorMap();
        if (CollUtil.isEmpty(visitorMap)) {
            OrderManager orderManager = getApplicationContext().getBean(OrderManager.class);
            visitorMap = orderManager.autoAssemblingVisitorQuantity(item.getId(), vo.getQuantity(), vo.getDay());
        }
        return Result.ok(visitorMap);
    }

    /**
     * 解析每日退货规则
     *
     * @param vo           退订信息
     * @param item         子订单信息
     * @param detailMap    每日详情
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<Map<Date, RefundRule<AbstractRefundRuleDetail>>> parseDayRefundRule(RefundVO vo, FxOrderItem item,
                                                                                       Map<Date, DetailDiscountDTO> detailMap,
                                                                                       AuthUser user, Result<?> beforeResult) {
        return Result.ok(vo.getVisitorMap().keySet().stream().collect(
                Collectors.toMap(day -> day, day ->
                        Optional.ofNullable(detailMap.get(day))
                                .map(detail -> parseRefundRule(vo, item, detail))
                                .orElseThrow(() -> Result.fail(
                                        StrUtil.format("{} 没有该日期的订单",
                                                DateUtil.formatDateTime(day)),
                                        beforeResult
                                ).exception())))
        );
    }

    /**
     * 解析退货规则
     *
     * @param vo        退订信息
     * @param orderItem 子订单信息
     * @param detail    每日详情
     * @param <T>       退货规则详情
     * @return
     */
    @SneakyThrows
    default <T extends AbstractRefundRuleDetail> RefundRule<T> parseRefundRule(RefundVO vo, FxOrderItem orderItem, FxOrderItemDetail detail) {
        return Constant.DATE_MAPPER.readValue(detail.getRefundRule(), Constant.DATE_MAPPER.getTypeFactory().constructParametricType(RefundRule.class, CustomRefundRuleDetail.class));
    }

    /**
     * 是否可退
     *
     * @param vo           退订信息
     * @param item         子订单信息
     * @param detailMap    每日详情
     * @param ruleMap      退订游玩日期的每日退货规则
     * @param user         当前用户信息
     * @param beforeResult 上一个阶段返回信息
     * @return
     */
    @CheckResult
    default Result<Boolean> canRefund(RefundVO vo, FxOrderItem item, Map<Date, DetailDiscountDTO> detailMap,
                                      Map<Date, RefundRule<AbstractRefundRuleDetail>> ruleMap, AuthUser user, Result<?> beforeResult) {
        return Result.ok(ruleMap.values().stream().allMatch(RefundRule::isRefund));
    }

    /**
     * 验证游客信息
     *
     * @param vo                 退订信息
     * @param item               子订单信息
     * @param visitors           游客列表
     * @param detailMap          每日详情
     * @param visitorQuantityMap 退订游客票数集合
     * @param user               当前用户信息
     * @param beforeResult       上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<List<FxRefundVisitor>> validateVisitor(RefundVO vo, FxOrderItem item, List<FxOrderItemVisitorDetail> visitors,
                                                          Map<Date, DetailDiscountDTO> detailMap,
                                                          Map<Integer, Integer> visitorQuantityMap, AuthUser user, Result<?> beforeResult) {
        Map<Integer, Integer> map = visitors.stream().collect(Collectors.groupingBy(FxOrderItemVisitorDetail::getVisitorId, Collectors.summingInt(FxOrderItemVisitorDetail::getRemainQuantity)));
        boolean check = visitorQuantityMap.entrySet().stream().anyMatch(entry -> map.getOrDefault(entry.getKey(), 0) < entry.getValue());
        if (check) {
            return Result.fail("余票不足");
        }
        Map<TwoGroup<Integer, Integer>, FxOrderItemVisitorDetail> vdMap = visitors.stream().collect(Collectors.toMap(FxOrderItemVisitorDetail::unique, Function.identity()));
        List<FxRefundVisitor> refundVisitors = vo.getVisitorMap().entrySet().stream()
                .flatMap(entry -> entry.getValue().stream()
                        .map(v -> new FxRefundVisitor()
                                .setCreateTime(vo.getRefundTime())
                                .setOrderItemId(item.getId())
                                .setVisitorId(v.getId())
                                .setQuantity(v.getQuantity())
                                .setDay(entry.getKey())
                                .setVisitorDetailId(vdMap.get(new TwoGroup<>(v.getId(), detailMap.get(entry.getKey()).getId())).getId())
                        )
                ).collect(Collectors.toList());
        return Result.ok(refundVisitors);
    }

    /**
     * 每日临时集合(当日最终价格&当日手续费百分比)
     *
     * @param vo             预定信息
     * @param item           子订单信息
     * @param dayQuantityMap 每日退票数
     * @param detailMap      每日详情
     * @param ruleMap        每日退订规则
     * @param buyMerchant    购买商户ID
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<Map<Date, DayRefundTemp>> getDayRefundTempMap(RefundVO vo, FxOrderItem item, Map<Date, Integer> dayQuantityMap,
                                                                 Map<Date, DetailDiscountDTO> detailMap,
                                                                 Map<Date, RefundRule<AbstractRefundRuleDetail>> ruleMap, int buyMerchant) {
        FxOrder order = vo.get(FxOrder.class).getValue();
        boolean isRetail = order.getPaymentChannel() == Constant.PaymentChannel.RETAIL;
        Map<Date, DayRefundTemp> tempMap = new HashMap<>(dayQuantityMap.size());
        for (Map.Entry<Date, Integer> entry : dayQuantityMap.entrySet()) {
            Date key = entry.getKey();
            DetailDiscountDTO detail = detailMap.get(key);
            int finalPrice = isRetail ? detail.getLowPrice() : detail.getSettlePrice();
            tempMap.put(key, new DayRefundTemp()
                    .setPrice(finalPrice)
                    .setCredit(detail.getCredit())
                    .setDetail(detail)
                    .setPercent(getDayRefundFeePercent(vo, ruleMap.get(key), item, detail, finalPrice)));
        }
        return Result.ok(tempMap);
    }

    /**
     * 获取天的退订手续费百分比
     *
     * @param vo         退订信息
     * @param refundRule 退货规则
     * @param item       子订单
     * @param detail     订单详情
     * @param finalPrice 最终售价
     * @return
     */
    default double getDayRefundFeePercent(RefundVO vo, RefundRule<AbstractRefundRuleDetail> refundRule, FxOrderItem item, FxOrderItemDetail detail, int finalPrice) {
        return OrderManager.getRefundFeePercent(refundRule, finalPrice, new RefundRuleCheckInfo().setBooking(detail.getDay()).setTime(vo.getRefundTime()));
    }

    /**
     * 计算退支付金额与手续费
     *
     * @param vo             退订信息
     * @param orderItem      子订单信息
     * @param buyMerchant    购买商户ID
     * @param refundVisitors 退订游客信息
     * @param tempMap        每日退票临时集合
     * @param user           当前用户信息
     * @param beforeResult   上一个阶段返回信息
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<int[]> calcRefundMoneyAndFee(RefundVO vo, FxOrderItem orderItem, int buyMerchant,
                                                List<FxRefundVisitor> refundVisitors,
                                                Map<Date, DayRefundTemp> tempMap,
                                                AuthUser user, Result<?> beforeResult) {
        double money = 0, fee = 0;
        for (FxRefundVisitor refundVisitor : refundVisitors) {
            Date day = refundVisitor.getDay();
            DayRefundTemp refundTemp = tempMap.get(day);
            int quantity = refundVisitor.getQuantity();
            double percent = refundTemp.getPercent();
            int price = refundTemp.getPrice();
            double m = 0, f = 0;
            // 积分抵扣
            if (refundTemp.getCredit() != null) {
                FxOrderItemDetailCredit credit = refundTemp.getCredit();
                int count = (int) (credit.getUsedCredit() / credit.getUnitCredit());
                // 抵扣后当天总支付金额
                FxOrderItemDetail detail = refundTemp.getDetail();
                long discountPrice = detail.getQuantity() * price - credit.getDiscountMoney();
                // 全部使用积分抵扣，则每张价格平分：总支付金额 / 总张数 = 每张价格
                if (count == detail.getQuantity()) {
                    price = (int) (discountPrice / detail.getQuantity());
                } else {
                    // 先退积分的票: (总支付金额 - 没有抵扣的张数的金额) / 抵扣张数 = 抵扣每张金额
                    int remainder = count - detail.getRefundQuantity();
                    if (remainder > 0) {
                        int dPrice = (int) ((discountPrice - ((detail.getQuantity() - count) * price)) / count);
                        if (remainder < quantity) {
                            for (int i = 0; i < remainder; i++) {
                                double[] pa = RefundManager.calcRefundMoneyAndFee(percent, dPrice, 1);
                                m += pa[0];
                                f += pa[1];
                                quantity--;
                            }
                        } else {
                            price = dPrice;
                        }
                    }
                }
            }

            if (quantity > 0) {
                double[] pa = RefundManager.calcRefundMoneyAndFee(percent, price, quantity);
                m += pa[0];
                f += pa[1];
            }
            money += m;
            fee += f;
            refundVisitor.setMoney((int) m).setFee((int) f);
        }
        return Result.ok(new int[]{(int) money, (int) fee});
    }

    /**
     * 创建退订订单
     *
     * @param vo        退订信息
     * @param orderItem 子订单信息
     * @param fee       手续费
     * @param money     退支付者金额
     * @param quantity  退订总数
     * @return
     */
    @CheckResult(emptyValue = false)
    default Result<FxRefundOrder> makeRefundOrder(RefundVO vo, FxOrderItem orderItem, int fee, int money, int quantity) {
        Snowflake snowflake = getApplicationContext().getBean(Snowflake.class);
        FxRefundOrder refundOrder = new FxRefundOrder()
                .setOrderItemId(orderItem.getId())
                .setOuterId(vo.getOuterId())
                .setAudit(orderItem.getRefundAudit())
                .setCause(vo.getCause())
                .setCreateTime(vo.getRefundTime())
                .setFee(fee)
                .setMoney(money)
                .setNumber(snowflake.nextId())
                .setQuantity(quantity)
                .setStatus(orderItem.getRefundAudit() ? Constant.RefundOrderStatus.AUDIT_WAIT : Constant.RefundOrderStatus.REFUNDING);
        return Result.ok(refundOrder);
    }

    /**
     * 入库
     *
     * @param vo            退订信息
     * @param orderItem     子订单信息
     * @param visitorResult 退订游客信息
     * @param refundOrder   退订订单
     */
    default void save(RefundVO vo, FxOrderItem orderItem, Result<List<FxRefundVisitor>> visitorResult,
                      FxRefundOrder refundOrder) {
        FxRefundOrderMapper refundOrderMapper = getApplicationContext().getBean(FxRefundOrderMapper.class);
        FxOrderItemMapper orderItemMapper = getApplicationContext().getBean(FxOrderItemMapper.class);
        FxRefundVisitorMapper refundVisitorMapper = getApplicationContext().getBean(FxRefundVisitorMapper.class);
        refundOrderMapper.insertSelective(refundOrder);
        orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem()
                .setId(orderItem.getId())
                .setLastRefundId(refundOrder.getId())
                .setCurrentPreRefundQuantity(refundOrder.getQuantity())
        );
        visitorResult.getValue().forEach(visitor -> visitor.setRefundOrderId(refundOrder.getId()));
        refundVisitorMapper.insert(visitorResult.getValue());
    }

    /**
     * 检查手续费与退款金额
     *
     * @param vo          退订信息
     * @param refundOrder 退订订单
     * @return
     */
    @CheckResult
    default Result<?> checkFeeAndMoney(RefundVO vo, FxRefundOrder refundOrder) {
        if (refundOrder.getMoney() < 0) {
            return Result.fail("退款金额不能小于0");
        }
        FxOrder order = vo.get(FxOrder.class).getValue();
        int price = order.getPayAmount();
        if (refundOrder.getMoney() > price) {
            return Result.fail("退款金额不能大于订单支付金额");
        }
        if (refundOrder.getFee() > price) {
            return Result.fail("手续费不能大于订单支付金额");
        }
        return Result.ok();
    }

    /**
     * 后置操作(返回信息)
     *
     * @param vo          退订信息
     * @param refundOrder 退订订单
     * @return
     */
    @CheckResult
    default Result<?> after(RefundVO vo, FxRefundOrder refundOrder) {
        if (refundOrder.getStatus() == Constant.RefundOrderStatus.AUDIT_WAIT) {
            Result<FxOrderItem> orderItemResult = vo.get(FxOrderItem.class);
            if (orderItemResult != null) {
                SmsNotifyManager smsNotifyManager = getApplicationContext().getBean(SmsNotifyManager.class);
                smsNotifyManager.sendRefundApplyNotify(orderItemResult.getValue());
            }
        }
        return Result.ok(refundOrder);
    }
}
