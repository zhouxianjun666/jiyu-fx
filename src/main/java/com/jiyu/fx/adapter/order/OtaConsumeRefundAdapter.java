package com.jiyu.fx.adapter.order;

import cn.hutool.core.lang.Assert;
import com.jiyu.base.mapper.MerchantMapper;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.vo.RefundVO;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class OtaConsumeRefundAdapter extends RemainderRefundAdapter {
    @Override
    public String type() {
        return "otaRefund";
    }

    @Override
    public Result<Merchant> validateMerchant(RefundVO vo, FxOrder order, AuthUser user) {
        MerchantMapper merchantMapper = getApplicationContext().getBean(MerchantMapper.class);
        Merchant merchant = merchantMapper.selectByPrimaryKey(user.getMerchant().getId());
        Assert.notNull(merchant, "商户不存在");
        if (!Objects.equals(order.getBuyMerchantId(), merchant.getId())) {
            return Result.fail("当前商户不能申请退订");
        }
        return Result.ok(merchant);
    }
}
