package com.jiyu.fx.adapter.order;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/23 14:54
 */
@Component
public class CommonRefundAdapter implements RefundOrder {
    @Autowired
    @Getter
    private ApplicationContext applicationContext;

    @Override
    public String type() {
        return "common";
    }
}
