package com.jiyu.fx.adapter.order;

import com.jiyu.common.dto.*;
import com.jiyu.fx.entity.FxOrderItemDetailCredit;
import com.jiyu.fx.mapper.FxOrderItemDetailCreditMapper;
import com.jiyu.fx.vo.BookingItemVO;
import com.jiyu.fx.vo.BookingVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/5/13 10:38
 */
@Component
public class TestScenicCreditBookingAdapter extends ScenicBookingAdapter {
    @Autowired
    private Config config;
    @Resource
    private FxOrderItemDetailCreditMapper detailCreditMapper;
    @Override
    public String type() {
        return "test-credit-" + super.type();
    }

    @Override
    public Result<?> validate(BookingVO vo, AuthUser user) {
        if (!config.isDev()) {
            return Result.fail("不支持当前环境");
        }
        return super.validate(vo, user);
    }

    @Override
    public Result<Map<Date, Long>> calcItemDiscount(ProductSalesInfo info, BookingItemVO item, int quantity,
                                                    Map<Date, MerchantSalesInfoPrice> merchantSalesPriceMap, long itemNumber,
                                                    BookingVO vo, AuthUser user, Result<?> beforeResult) {
        Map<Date, Long> map = new HashMap<>(merchantSalesPriceMap.size());
        for (Map.Entry<Date, MerchantSalesInfoPrice> entry : merchantSalesPriceMap.entrySet()) {
            long count = (quantity > 1 ? quantity - 1 : quantity);
            long money = (long) (count * 0.1 * 100);
            map.put(entry.getKey(), money);
            detailCreditMapper.insertSelective(new FxOrderItemDetailCredit()
                    .setDay(entry.getKey()).setItemNumber(itemNumber)
                    .setUnitCredit(1L).setUsedCredit(count).setDiscountMoney(money)
            );
        }
        return Result.ok(map);
    }
}
