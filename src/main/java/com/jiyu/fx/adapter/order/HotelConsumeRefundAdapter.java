package com.jiyu.fx.adapter.order;

import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.vo.RefundVO;
import org.springframework.stereotype.Component;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/11 17:02
 */
@Component
public class HotelConsumeRefundAdapter extends RemainderRefundAdapter {
    @Override
    public String type() {
        return "hotel-consume-" + super.type();
    }

    @Override
    public Result<Merchant> validateMerchant(RefundVO vo, FxOrder order, AuthUser user) {
        return Result.ok(user.getMerchant());
    }
}
