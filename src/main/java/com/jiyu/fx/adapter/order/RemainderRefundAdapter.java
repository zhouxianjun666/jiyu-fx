package com.jiyu.fx.adapter.order;

import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.manager.OrderManager;
import com.jiyu.fx.vo.RefundVO;
import com.jiyu.fx.vo.VisitorQuantityVO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 退剩余所有票
 * @date 2019/1/23 14:54
 */
@Component
public class RemainderRefundAdapter implements RefundOrder {
    @Autowired
    @Getter
    private ApplicationContext applicationContext;
    @Autowired
    private OrderManager orderManager;

    @Override
    public String type() {
        return "remainder";
    }

    @Override
    @CheckResult(emptyValue = false)
    public Result<Map<Date, List<VisitorQuantityVO>>> parseDayRefund(RefundVO vo, FxOrderItem item, AuthUser user, Result<?> beforeResult) {
        return Result.ok(orderManager.autoAssemblingVisitorQuantity(item.getId(), null, null));
    }
}
