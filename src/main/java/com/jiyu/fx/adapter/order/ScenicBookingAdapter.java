package com.jiyu.fx.adapter.order;

import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.User;
import com.jiyu.fx.vo.BookingVO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/16 12:45
 */
@Component
public class ScenicBookingAdapter implements BookingOrder {
    @Autowired
    @Getter
    private ApplicationContext applicationContext;

    @Override
    public String type() {
        return "scenic";
    }

    @Override
    @CheckResult
    public Result<?> validate(BookingVO vo, AuthUser user) {
        vo.getItems().forEach(item -> item.setDays(1));
        return BookingOrder.super.validate(vo, user);
    }
}
