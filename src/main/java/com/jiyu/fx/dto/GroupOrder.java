package com.jiyu.fx.dto;

import com.jiyu.fx.entity.FxGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/22 15:21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class GroupOrder extends FxGroup {
    private List<Map<String, Object>> orders;
}
