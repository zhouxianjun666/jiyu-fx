package com.jiyu.fx.dto;

import com.alone.tk.mybatis.JoinExample;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/6 12:04
 */
@Data
@Accessors(chain = true)
public class FillItemParam {
    private Class<?> type;
    private JoinExample.Builder builder;
    private Set<Integer> ids;
    private Set<Long> numbers;
    private OrderItemFill fill;
}
