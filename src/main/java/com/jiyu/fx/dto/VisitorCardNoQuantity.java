package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/8 15:32
 */
@Data
@Accessors(chain = true)
public class VisitorCardNoQuantity {
    private String cardNo;
    private int quantity;
}
