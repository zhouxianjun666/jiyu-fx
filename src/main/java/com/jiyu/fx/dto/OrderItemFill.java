package com.jiyu.fx.dto;

import com.jiyu.common.annotation.SelectEntityList;
import com.jiyu.fx.entity.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Transient;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/6 11:45
 */
@Data
@Accessors(chain = true)
public class OrderItemFill {
    @ApiModelProperty("是否查询游客信息")
    @Transient
    @SelectEntityList(FxOrderItemVisitor.class)
    private boolean visitor;

    @ApiModelProperty("是否查询游客天(详情)信息")
    @Transient
    @SelectEntityList(FxOrderItemVisitorDetail.class)
    private boolean visitorDetail;

    @ApiModelProperty("是否查询票据&凭证信息")
    @Transient
    @SelectEntityList(FxOrderItemTicket.class)
    private boolean voucher;

    @ApiModelProperty("是否查询订单详情信息")
    @Transient
    @SelectEntityList(FxOrderItemDetail.class)
    private boolean detail;

    @ApiModelProperty("是否查询退订信息")
    @Transient
    @SelectEntityList(FxRefundOrder.class)
    private boolean refund;

    @ApiModelProperty("是否查询退订游客信息")
    @Transient
    @SelectEntityList(FxRefundVisitor.class)
    private boolean refundVisitor;

    @ApiModelProperty("是否查询积分抵扣信息")
    @Transient
    @SelectEntityList(FxOrderItemDetailCredit.class)
    private boolean credit;
}
