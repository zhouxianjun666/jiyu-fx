package com.jiyu.fx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/21 20:30
 */
@Data
@Accessors(chain = true)
public class JYTicketConfig {
    @ApiModelProperty(value = "下单、出票请求地址", required = true)
    @NotBlank
    private String createUrl;

    @ApiModelProperty(value = "拉取产品票据请求地址", required = true)
    @NotBlank
    private String productUrl;

    @ApiModelProperty(value = "退票请求地址", required = true)
    @NotBlank
    private String refundUrl;

    @ApiModelProperty(value = "查询下单出票结果请求地址")
    private String queryCreateResultUrl;

    @ApiModelProperty(value = "查询退票结果请求地址")
    private String queryRefundResultUrl;
}
