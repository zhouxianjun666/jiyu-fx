package com.jiyu.fx.dto;

import com.alone.tk.mybatis.OperationTypes;
import com.alone.tk.mybatis.annotation.Operation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Transient;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/22 14:19
 */
@Data
@Accessors(chain = true)
public class SearchGroupOrder {
    @ApiModelProperty("团队出行时间开始")
    @Operation(value = OperationTypes.GREATER_THAN_OR_EQUAL, column = "start_date")
    private Date start;

    @ApiModelProperty("团队出行时间结束")
    @Operation(value = OperationTypes.LESS_THAN_OR_EQUAL, column = "start_date")
    private Date end;

    @ApiModelProperty("团号")
    private String number;

    @ApiModelProperty("旅行社")
    @Operation(value = OperationTypes.LIKE, column = "travel_name")
    private String travelName;

    @ApiModelProperty("导游")
    @Operation(value = OperationTypes.LIKE, column = "guide_name")
    private String guideName;

    @ApiModelProperty("是否以供应的视角查询")
    @Transient
    private boolean isSupplier;
}
