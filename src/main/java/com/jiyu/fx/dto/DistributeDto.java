package com.jiyu.fx.dto;

import lombok.Data;

@Data
public class DistributeDto {

    /**
     * 主键id
     */
    private Integer id;
    /**
     * 子产品id
     */
    private Integer productSubId;

    /**
     * 卖家企业id
     */
    private Integer saleMerchantId;

    /**
     * 卖家平台ID
     */
    private Integer salePlatformId;

    /**
     * 买家企业id
     */
    private Integer merchantId;

    /**
     * 买家平台ID
     */
    private Integer platformId;

    /**
     * 利润类型
     */
    private Integer profitType;

    /**
     * 利润值
     */
    private Integer profit;

    /**
     * 所有上级ID
     */
    private String pids;

    /**
     * 当前销售链等级
     */
    private Integer level;
}
