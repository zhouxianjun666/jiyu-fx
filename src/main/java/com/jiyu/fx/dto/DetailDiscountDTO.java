package com.jiyu.fx.dto;

import com.jiyu.fx.entity.FxOrderItemDetail;
import com.jiyu.fx.entity.FxOrderItemDetailCredit;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/5/13 9:31
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class DetailDiscountDTO extends FxOrderItemDetail {
    /**
     * 积分抵扣
     */
    private FxOrderItemDetailCredit credit;
}
