package com.jiyu.fx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 出票信息
 * @date 2019/2/15 9:14
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@RequiredArgsConstructor
public class SendTicketInfo {
    @ApiModelProperty("订单号")
    @NonNull
    @NotNull
    private Long number;

    @ApiModelProperty("产品ID")
    @NonNull
    @NotNull
    private Long productNumber;

    @ApiModelProperty("票务产品ID")
    @Nullable
    private String maProductId;

    @ApiModelProperty("产品名称")
    @Nullable
    private String productName;

    @ApiModelProperty("票据名称")
    @Nullable
    private String ticketName;

    @ApiModelProperty("小票内容")
    @Nullable
    private String ticketContent;

    @ApiModelProperty("渠道名称")
    @Nullable
    private String channelName;

    @ApiModelProperty("每天的票据信息")
    @NonNull
    @NotEmpty
    @Valid
    private List<SendTicketDayInfo> dayInfos;
}
