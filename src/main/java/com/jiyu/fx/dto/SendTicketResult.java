package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 出票结果
 * @date 2019/2/22 15:47
 */
@Data
@Accessors(chain = true)
public class SendTicketResult {
    /**
     * 订单号
     */
    private long number;
    /**
     * 是否异步
     */
    private boolean async;
    /**
     * 处理时间
     */
    private Date procTime;
    /**
     * 凭证码列表
     */
    private List<TicketSendInfo> vouchers;
}
