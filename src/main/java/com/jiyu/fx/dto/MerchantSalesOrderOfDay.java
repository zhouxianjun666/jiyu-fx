package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/5/20 15:38
 */
@Data
@Accessors(chain = true)
public class MerchantSalesOrderOfDay implements Serializable {
    /**
     * 购买商户ID
     */
    private int merchant;
    /**
     * 购买日期
     */
    private Date days;
    /**
     * 支付金额
     */
    private long payAmount;
    /**
     * 订单数量
     */
    private int num;
}
