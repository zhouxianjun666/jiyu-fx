package com.jiyu.fx.dto;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.jiyu.fx.vo.VisitorQuantityVO;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/23 15:11
 */
public class VisitorQuantityDayMapDeserializer extends JsonDeserializer<Map<Object, List<VisitorQuantityVO>>> {
    private TypeReference<HashMap<Object, List<VisitorQuantityVO>>> typeRef = new TypeReference<HashMap<Object, List<VisitorQuantityVO>>>() {};

    @Override
    public Map<Object, List<VisitorQuantityVO>> deserialize(JsonParser p, DeserializationContext ctxt, Map<Object, List<VisitorQuantityVO>> intoValue) throws IOException {
        Map<Object, List<VisitorQuantityVO>> map = p.getCodec().readValue(p, typeRef);

        for(Map.Entry<Object, List<VisitorQuantityVO>> e : map.entrySet()){
            List<VisitorQuantityVO> value = e.getValue();
            Object key = e.getKey();
            intoValue.put(DateUtil.parse(key.toString()).toJdkDate(), value);
        }

        return intoValue;
    }

    @Override
    public Map<Object, List<VisitorQuantityVO>> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return this.deserialize(p, ctxt, new HashMap<>(1));
    }
}
