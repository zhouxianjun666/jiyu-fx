package com.jiyu.fx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/15 9:40
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@RequiredArgsConstructor
public class RefundTicketInfo {
    @ApiModelProperty("订单号")
    @NonNull
    @NotNull
    private Long number;

    @ApiModelProperty("本次退订流水号(退订订单号)")
    @NonNull
    @NotBlank
    private String serialNo;

    @ApiModelProperty("申请时间")
    @NonNull
    @NotNull
    private Date applyTime;

    @ApiModelProperty("原因")
    @Nullable
    private String reason;

    @ApiModelProperty("退票游客信息")
    @NonNull
    @NotEmpty
    @Valid
    private List<RefundTicketVisitorInfo> visitors;
}
