package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/5/10 17:58
 */
@Data
@Accessors(chain = true)
public class ItemPriceDTO {
    /**
     * 最低售价（零售价）
     */
    private int lowPrice;
    /**
     * 结算价
     */
    private int settlePrice;
    /**
     * 抵扣金额
     */
    private long discountMoney;
    /**
     * 返佣
     */
    private int rebate;
}
