package com.jiyu.fx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/15 9:19
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@RequiredArgsConstructor
public class RefundTicketVisitorInfo {
    @ApiModelProperty("游客ID")
    @Nullable
    private Integer id;

    @ApiModelProperty("票据ID")
    @Nullable
    private String ticketId;

    @ApiModelProperty("凭证码")
    @NonNull
    @NotBlank
    private String voucherNumber;

    @ApiModelProperty("退票数量")
    @NonNull
    @Min(1)
    @NotNull
    private Integer quantity;
}
