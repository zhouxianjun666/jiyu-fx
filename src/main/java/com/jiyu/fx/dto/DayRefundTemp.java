package com.jiyu.fx.dto;

import com.jiyu.fx.entity.FxOrderItemDetail;
import com.jiyu.fx.entity.FxOrderItemDetailCredit;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/22 11:38
 */
@Data
@Accessors(chain = true)
public class DayRefundTemp {
    private int price;
    private double percent;
    private FxOrderItemDetailCredit credit;
    private FxOrderItemDetail detail;
}
