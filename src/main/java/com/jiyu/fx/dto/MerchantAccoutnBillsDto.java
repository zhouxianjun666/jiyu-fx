package com.jiyu.fx.dto;

import com.jiyu.fx.entity.FxMerchantAccount;
import com.jiyu.fx.entity.FxMerchantAccountBills;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zoujing
 * @ClassName:
 * @Description:
 * @date 2019/5/21 14:49
 */
@Data
@Accessors(chain = true)
public class MerchantAccoutnBillsDto extends FxMerchantAccountBills {
    /**
     * 子产品编号
     */
    private int MerchantId;
    /**
     * 可售
     */
    private String MerchantName;
}
