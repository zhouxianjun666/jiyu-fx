package com.jiyu.fx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/15 9:19
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@RequiredArgsConstructor
public class SendTicketVisitorInfo {
    @ApiModelProperty("游客ID")
    @NonNull
    @NotNull
    private Integer id;

    @ApiModelProperty("游客姓名")
    @NonNull
    @NotBlank
    private String name;

    @ApiModelProperty("手机号")
    @NonNull
    @NotBlank
    private String phone;

    @ApiModelProperty(value = "证件类型", notes = "1-身份证;2-其它")
    @Nullable
    @Range(min = 1, max = 2)
    private Integer cardType;

    @ApiModelProperty("证件号")
    @Nullable
    private String cardNo;

    @ApiModelProperty(value = "性别", notes = "0-未知;1-男;2-女")
    @Nullable
    @Range(min = 0, max = 2)
    private Integer gender;

    @ApiModelProperty("票数")
    @NonNull
    @Min(1)
    @NotNull
    private Integer quantity;
}
