package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 出票信息
 * @date 2019/1/29 14:27
 */
@Data
@Accessors(chain = true)
public class TicketSendInfo {
    /**
     * 游客ID
     */
    private Integer visitorId;
    /**
     * 票据ID
     */
    private String ticketId;
    /**
     * 哪一天
     */
    private Date day;
    /**
     * 凭证码
     */
    private String voucherNumber;
    /**
     * 凭证码URL
     */
    private String voucherUrl;
}
