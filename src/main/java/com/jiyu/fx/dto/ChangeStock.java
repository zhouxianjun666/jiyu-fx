package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/16 14:49
 */
@Data
@Accessors(chain = true)
public class ChangeStock implements Serializable {
    /**
     * 子产品编号
     */
    private long code;
    /**
     * 可售
     */
    private int available;
    /**
     * 已售
     */
    private int sold;
    /**
     * 开始时间
     */
    private Date start;
    /**
     * 天数
     */
    private int days = 1;
}
