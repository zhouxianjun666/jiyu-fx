package com.jiyu.fx.dto;

import com.jiyu.fx.entity.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/18 14:21
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderItemInfo extends FxOrderItem {
    private List<FxOrderItemVisitor> visitors;

    private List<FxOrderItemVisitorDetail> visitorDetails;

    private List<FxOrderItemTicket> tickets;

    private List<FxOrderItemDetail> details;

    private List<FxRefundOrder> refundOrders;

    private List<FxRefundVisitor> refundVisitors;

    private List<FxOrderItemDetailCredit> detailCredits;
}
