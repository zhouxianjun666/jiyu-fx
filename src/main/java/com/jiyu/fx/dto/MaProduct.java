package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/22 15:57
 */
@Data
@Accessors(chain = true)
public class MaProduct {
    /**
     * 票据产品ID
     */
    private String id;
    /**
     * 票据产品名称
     */
    private String name;
    /**
     * 是否为团队票
     */
    private boolean group;
    /**
     * 是否为成人
     */
    private boolean adult = true;
    /**
     * 是否实名制要求(一证一票)
     */
    private boolean realName;
    /**
     * 预定须知
     */
    private String bookNotes;
    /**
     * 使用须知
     */
    private String useNotes;
}
