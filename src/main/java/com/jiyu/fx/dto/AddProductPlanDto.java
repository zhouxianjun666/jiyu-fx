package com.jiyu.fx.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 销售计划前端接受类
 * @Author: songe
 * @CreateDate: 2019/1/17 9:21
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/17 9:21
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */

@Data
public class AddProductPlanDto implements Serializable {

    /**
     * 子产品id
     */
    private Integer productSubId;

    /**
     * 产品id
     */
    private Integer productId;

    /**
     * 退货规则id
     */
    private Integer refundId;

    /**
     * 市场价
     */
    private Integer marketPrice;

    /**
     * 最低售价
     */
    private Integer lowPrice;

    /**
     * 结算价
     */
    private Integer settlePrice;

    /**
     * 退货规则
     */
    private String refundRule;

    /**
     * 售卖规则
     */
    private Integer saleRuleType;

    /**
     * 预定时间是否限制
     */
    private Boolean bookingLimit;

    /**
     * 预定时间限制天
     */
    private Integer bookingDayLimit;

    /**
     * 预定时间限制时间
     */
    private String bookingTimeLimit;

    /**
     * 使用时间限制 (预订后多少 秒)
     */
    private Integer useSecondLimit;

    /**
     * 有效天数
     */
    private Integer effectiveDay;

    /**
     * 有效开始日期
     */
    private Date effectiveStartDate;

    /**
     * 有效结束日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date effectiveEndDate;

    /**
     * 有效日期类型
     */
    private Integer effectiveType;

    /**
     * 禁用星期
     */
    private Byte disableWeek;

    /**
     * 禁用日期
     */
    private String disableDate;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 时间段
     */
    private List<Date> dateRange;
    /**
     * 时分时间段
     */
    private List<Map> timeRange;


}
