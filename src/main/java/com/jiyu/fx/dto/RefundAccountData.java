package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/18 11:34
 */
@Data
@Accessors(chain = true)
public class RefundAccountData {
    /**
     * 天
     */
    private Date day;
    /**
     * 游客ID
     */
    private int visitor;
    /**
     * 张数
     */
    private int quantity;
    /**
     * 商户ID
     */
    private int merchant;
    /**
     * 退余额
     */
    private double money;
    /**
     * 进手续费
     */
    private double fee;
}
