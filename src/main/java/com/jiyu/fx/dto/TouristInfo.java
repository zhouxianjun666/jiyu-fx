package com.jiyu.fx.dto;


import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author larry.page
 * @Description: 游客信息
 * @date 2019-1-29 17:24:56
 */
@Data
@Accessors(chain = true)
public class TouristInfo {
    /**
     * 游客id
     */
    private Integer touristId;
    /**
     * 游客姓名
     */
    private String touristName;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 证件号
     */
    private String identity;
    /**
     * 证件类型
     */
    private Integer identityType;
    /**
     * 票数
     */
    private Integer ticketCount;
    /**
     * 退票申请时间
     */
    private Date applTimeRorRefundTime;
    /**
     * 退票原因
     */
    private String reasonForRefund;
    /**
     * 退票申请数量
     */
    private Integer applTimeRorRefundCount;
    /**
     * 凭证码
     */
    private String voucherNumber;
    /**
     * 凭证码URL
     */
    private String voucherUrl;
}
