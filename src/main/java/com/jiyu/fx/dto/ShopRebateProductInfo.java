package com.jiyu.fx.dto;

import com.jiyu.fx.entity.FxProduct;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/29 12:52
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ShopRebateProductInfo extends FxProduct {
    private List<Map<String, Object>> subs;
}
