package com.jiyu.fx.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 退票结果
 * @date 2019/2/22 15:50
 */
@Data
@Accessors(chain = true)
public class RefundTicketResult {
    /**
     * 订单号(退订订单)
     */
    private long number;
    /**
     * 是否异步
     */
    private boolean async;
    /**
     * 处理时间
     */
    private Date procTime;
    /**
     * 第三方流水号
     */
    private String serialNo;
}
