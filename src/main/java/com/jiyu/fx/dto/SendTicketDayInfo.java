package com.jiyu.fx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.lang.Nullable;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/9 9:50
 */
@Data
@Accessors(chain = true)
public class SendTicketDayInfo {
    @ApiModelProperty("哪一天的票")
    @NotNull
    private Date day;

    @ApiModelProperty("生效时间")
    @NotNull
    private Date effectiveTime;

    @ApiModelProperty("到期时间")
    @NotNull
    private Date expiryTime;

    @ApiModelProperty(value = "禁用星期", notes = "例如:0100100,则代表周二与周五为禁用")
    @Nullable
    private String disableWeeks;

    @ApiModelProperty(value = "禁用日期", notes = "例如:2019-02-15,2019-02-16")
    @Nullable
    private String disableDays;

    @ApiModelProperty("出票游客信息")
    @NotNull
    @NotEmpty
    @Valid
    private List<SendTicketVisitorInfo> visitors;
}
