package com.jiyu.fx.dto;

import com.alone.tk.mybatis.annotation.Operation;
import com.jiyu.common.annotation.HasOneOf;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Transient;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/6 10:47
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@HasOneOf(fields = {"outerId", "number"}, message = "第三方订单号与基裕订单号必须有一项不为空")
public class OrderDetailRequest extends OrderItemFill {
    @ApiModelProperty("第三方订单号")
    @Operation(column = "outer_id")
    private String outerId;

    @ApiModelProperty("基裕订单号")
    private Long number;

    @ApiModelProperty("是否查询订单联系人信息")
    @Transient
    private boolean shipping;

    @ApiModelProperty(readOnly = true, hidden = true)
    public boolean isJoinMember() {
        return isShipping();
    }
}
