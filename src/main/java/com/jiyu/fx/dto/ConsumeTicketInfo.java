package com.jiyu.fx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/15 9:46
 */
@Data
@Accessors(chain = true)
public class ConsumeTicketInfo {
    @ApiModelProperty("订单号")
    @Nullable
    private Long number;

    @ApiModelProperty("本次核销流水号")
    @NonNull
    @NotBlank
    private String serialNo;

    @ApiModelProperty("游客ID")
    @Nullable
    private Integer visitorId;

    @ApiModelProperty("凭证号")
    @NonNull
    @NotBlank
    private String voucherNumber;

    @ApiModelProperty("票据ID")
    @Nullable
    private String ticketId;

    @ApiModelProperty("本次核销数量")
    @NonNull
    @Min(1)
    @NotNull
    private Integer quantity;
}
