package com.jiyu.fx.dto;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alone.tk.mybatis.OperationTypes;
import com.alone.tk.mybatis.annotation.Operation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Transient;
import java.util.Date;
import java.util.Set;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/14 11:47
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class SearchOrder extends OrderItemFill {
    @ApiModelProperty("下单时间开始")
    @Operation(value = OperationTypes.GREATER_THAN_OR_EQUAL, column = "create_time")
    private Date start;

    @ApiModelProperty("下单时间结束")
    @Operation(value = OperationTypes.LESS_THAN_OR_EQUAL, column = "create_time")
    private Date end;

    @ApiModelProperty("联系人手机号")
    @Operation(value = OperationTypes.LIKE, alias = "member")
    private String phone;

    @ApiModelProperty("联系人名称")
    @Operation(value = OperationTypes.LIKE, alias = "member", column = "nickname")
    private String shippingName;

    @ApiModelProperty("产品名称")
    @Operation(value = OperationTypes.LIKE, alias = "fx_order_item", column = "product_name")
    private String productName;

    @ApiModelProperty("票据名称")
    @Operation(value = OperationTypes.LIKE, alias = "fx_order_item", column = "product_sub_name")
    private String productSubName;

    @ApiModelProperty("产品类型")
    @Operation(alias = "fx_order_item", column = "product_type")
    private Integer productType;

    @ApiModelProperty("使用数量")
    @Operation(alias = "fx_order_item", column = "used_quantity")
    private Integer usedQuantity;

    @ApiModelProperty("团散")
    @Operation(alias = "fx_order_item", column = "ticket_flag")
    private Integer ticketFlag;

    @ApiModelProperty("订单状态")
    @Operation(OperationTypes.IN)
    private Set<Integer> status;

    @ApiModelProperty("子订单状态")
    @Operation(value = OperationTypes.IN, alias = "fx_order_item", column = "status")
    private Set<Integer> itemStatus;

    @ApiModelProperty("退订订单状态")
    @Operation(value = OperationTypes.IN, alias = "fx_refund_order", column = "status")
    private Set<Integer> refundStatus;

    @ApiModelProperty("订单号")
    private Long number;

    @ApiModelProperty("外部订单号")
    @Operation(column = "outer_id")
    private String outerId;

    @ApiModelProperty("凭证号")
    @Operation(alias = "fx_order_item_ticket", column = "voucher_number")
    private String voucherNumber;

    @ApiModelProperty("游客证件号")
    @Operation(alias = "fx_order_item_visitor", column = "card_no")
    private String cardNo;

    @ApiModelProperty("购买渠道名称")
    @Operation(value = OperationTypes.LIKE, column = "buy_merchant_name")
    private String merchantName;

    @ApiModelProperty("支付方式")
    @Operation(column = "payment_type")
    private String paymentType;

    @ApiModelProperty("来源")
    private String source;

    @ApiModelProperty("是否查询订单联系人信息")
    @Transient
    private boolean shipping;

    @ApiModelProperty("是否只查询自己订单")
    @Transient
    private boolean self;

    @ApiModelProperty("是否倒序")
    @Transient
    private boolean desc;

    @ApiModelProperty(readOnly = true, hidden = true)
    public boolean isJoinMember() {
        return isShipping() || StrUtil.isNotBlank(phone) || StrUtil.isNotBlank(shippingName);
    }

    @ApiModelProperty(readOnly = true, hidden = true)
    public boolean isJoinItem() {
        return CollUtil.isNotEmpty(itemStatus) || isJoinRefund() || isJoinVisitor() || isJoinTicket()
                || StrUtil.isNotBlank(productName) || StrUtil.isNotBlank(productSubName)
                || productType != null || ticketFlag != null;
    }

    @ApiModelProperty(readOnly = true, hidden = true)
    public boolean isJoinRefund() {
        return CollUtil.isNotEmpty(refundStatus);
    }

    @ApiModelProperty(readOnly = true, hidden = true)
    public boolean isJoinTicket() {
        return StrUtil.isNotBlank(voucherNumber);
    }

    @ApiModelProperty(readOnly = true, hidden = true)
    public boolean isJoinVisitor() {
        return StrUtil.isNotBlank(cardNo);
    }
}
