package com.jiyu.fx.event;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.jiyu.base.mapper.MerchantMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.dto.ChangeStock;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.event.bo.*;
import com.jiyu.fx.manager.*;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.ota.push.PushMsgAdapter;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单预定事件服务
 * 在事件处理器中由于返回值发布的事件都是事务隔离的
 * @date 2019/3/14 19:33
 */
@Service
@Slf4j
public class BookingOrderEventService {
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private FxOrderMapper orderMapper;
    @Resource
    private FxGroupProductMapper groupProductMapper;
    @Resource
    private FxGroupMemberMapper groupMemberMapper;
    @Resource
    private FxGroupConsumeMapper groupConsumeMapper;
    @Autowired
    private ProductPlanManager productPlanManager;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private TicketManager ticketManager;
    @Autowired
    private SmsNotifyManager smsNotifyManager;
    @Autowired
    private RefundManager refundManager;
    @Autowired
    private RedissonClient redissonClient;
    @Resource
    private MerchantMapper merchantMapper;
    @Resource
    private FxOtaPushConfigMapper fxOtaPushConfigMapper;

    @Autowired
    private ButtJointNmediaManager buttJointNmediaManager;

    private Map<String, PushMsgAdapter> pushMsgAdapter;
    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init() {
        pushMsgAdapter = applicationContext.getBeansOfType(PushMsgAdapter.class).values().stream().collect(
                Collectors.toMap(PushMsgAdapter::otaType, Function.identity())
        );
    }

    /**
     * 下单同步通知新媒体使用积分
     *
     * @param event
     */
    @EventListener(condition = "#event.vo.credit != null && #event.vo.credit")
    public void createUseCredit(BookingOrderEvent event) {
        // 查询商户信息
        Merchant merchant = merchantMapper.selectOne(new Merchant().setId(event.getOrder().getBuyMerchantId()));
        // 下单请求新媒体消费积分
        log.info("下单请求新媒体消费积分开始，产品：{}，分发商：{}，订单号：{}", event.getItems().get(0).getProductSubNumber(), merchant.getEp(), event.getOrder().getNumber());
        buttJointNmediaManager.creditConsume(event.getItems().get(0).getProductSubNumber(), merchant.getId(), event.getItems().get(0).getNumber(), event.getItems().get(0).getQuantity());
    }

    /**
     * 团队现场到付发送已支付事件
     *
     * @param event
     */
    @EventListener(condition = "#event.order.groupId != null && #event.order.paymentType == 3")
    public PaidOrderEvent createOfGroupLocalPay(BookingOrderEvent event) {
        return new PaidOrderEvent(event.getOrder());
    }

    /**
     * 订单创建完成后删除库存检查标记
     * @param event
     */
    @TransactionalEventListener
    public void createOfClearStock(BookingOrderEvent event) {
        redissonClient.getMap(Constant.REDUCE_AVAILABLE_STOCK).fastRemove(event.getOrder().getNumber());
    }

    /**
     * 支付成功处理
     * 在事件处理器中由于返回值发布的事件都是事务隔离的
     * @param event
     */
    @EventListener(condition = "#event.order.paymentType != 3")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @Order(1)
    @Async
    public void paid(PaidOrderEvent event) {
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderNumber(event.getOrder().getNumber()));
        // 加已售
        List<ChangeStock> changeStocks = items.stream().map(item -> new ChangeStock()
                .setCode(item.getProductSubNumber())
                .setDays(item.getDays())
                .setStart(item.getStartTime())
                .setSold(item.getQuantity())
        ).collect(Collectors.toList());
        productPlanManager.changeStock(changeStocks);
        // 检查是否全部审核并出票
        orderManager.checkAuditAndSend(items, event.getOrder().getNumber());
    }

    /**
     * 支付成功后发送待审核短信通知
     *
     * @param event
     */
    @TransactionalEventListener(fallbackExecution = true)
    @Order(2)
    @Async
    public void paidOfSmsNotify(PaidOrderEvent event) {
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderNumber(event.getOrder().getNumber()));
        items.stream().filter(item -> item.getStatus() == Constant.OrderItemStatus.WAIT_AUDIT).forEach(item -> {
            Result result = Result.fail("暂不支持该产品类型");
            if (item.getProductType() == Constant.ProductType.SCENIC) {
                result = smsNotifyManager.orderAuditScenic(item);
            } else if (item.getProductType() == Constant.ProductType.HOTEL) {
                result = smsNotifyManager.orderAuditHotel(item);
            } else if (item.getProductType() == Constant.ProductType.LINE) {
                result = smsNotifyManager.orderAuditLine(item);
            }
            if (!result.isSuccess()) {
                log.warn("发送订单审核通知失败: 订单号: {}, 原因:{}", item.getOrderNumber(), result.getMsg());
            }
        });
    }

    /**
     * 预定审核后事件
     *
     * @param event
     */
    @EventListener
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void audit(AuditOrderEvent event) {
        FxOrderItem item = event.getItem();
        if (!event.isSuccess()) {
            orderManager.cancel(item.getOrderNumber(), Constant.OrderCancelType.AUDIT_REFUSE, StrUtil.format("产品:{}审核失败", item.getProductSubName()));
            return;
        }
        // 检查是否全部审核并出票
        orderManager.checkAuditAndSend(item.getOrderNumber());
    }

    /**
     * 待出票事件出票
     * @param event
     */
    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void waitSendOfSend(WaitSendEvent event) {
        Result result = ticketManager.sendTicket(event.getItem());
        if (!result.isSuccess()) {
            log.warn("出票失败: 订单号: {}, 原因: {}", event.getItem().getOrderNumber(), result.getMsg());
        }
    }

    /**
     * 取消还库存
     *
     * @param event
     */
    @EventListener
    @Order(1)
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void cancelOfStock(CancelOrderEvent event) {
        List<FxOrderItemDetail> details = orderManager.getDetailsByNumber(event.getOrder().getNumber());
        // 加可售(没有超卖的才加)
        List<ChangeStock> availableStocks = details.stream()
                .filter(detail -> !detail.getOversell())
                .map(detail -> new ChangeStock()
                        .setCode(detail.getProductSubNumber())
                        .setDays(1)
                        .setStart(detail.getDay())
                        .setAvailable(detail.getQuantity()))
                .collect(Collectors.toList());
        productPlanManager.changeStock(availableStocks);
        // 减已售(分账成功才加已售，所以这里要分账成功才减已售)
        if (event.getOrder().getPaymentType() != null && event.getOrder().getPaymentTime() != null) {
            List<ChangeStock> soldStocks = event.getItems().stream().map(item -> new ChangeStock()
                    .setCode(item.getProductSubNumber())
                    .setDays(item.getDays())
                    .setStart(item.getStartTime())
                    .setSold(-item.getQuantity()))
                    .collect(Collectors.toList());
            productPlanManager.changeStock(soldStocks);
        }
    }

    /**
     * 取消订单退款
     */
    @TransactionalEventListener(fallbackExecution = true, condition = "#event.order.paymentType != null && #event.order.paymentTime != null")
    @Async
    @Order(3)
    public void cancelOfRefundMoney(CancelOrderEvent event) {
        FxOrder order = event.getOrder();
        Result<?> result = refundManager.refundMoney(order, order.getPayAmount(), String.valueOf(order.getNumber()),
                Constant.AccountBillsType.ORDER_BUY_FAIL_REFUND, "订单购买失败原路退款");
        if (!result.isSuccess()) {
            throw result.exception();
        }
        orderMapper.updateByPrimaryKeySelective(new FxOrder().setId(order.getId())
                .setBuyFailRefundMoney(true)
        );
    }

    /**
     * 取消订单发送短信通知
     *
     * @param event
     */
    @TransactionalEventListener(fallbackExecution = true)
    @Order(4)
    @Async
    public void cancelOfSmsNotify(CancelOrderEvent event) {
        Result result = smsNotifyManager.orderFail(event.getItems().get(0), StrUtil.blankToDefault(event.getReason(), "购买失败"));
        if (!result.isSuccess()) {
            log.warn("发送预定失败通知失败: 订单号: {}, 原因: {}", event.getOrder().getNumber(), result.getMsg());
        }
    }

    @Async
    @TransactionalEventListener(fallbackExecution = true)
    public void cancelOfSmsCredit(CancelOrderEvent event) {
        log.info("订单取消请求新媒体积分取消，订单号：{}", event.getOrder().getNumber());
        buttJointNmediaManager.creditCancel(event.getItems().get(0).getNumber());
    }

    /**
     * 出票短信通知
     *
     * @param event
     */
    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void ticketSendOfSmsNotify(TicketSendEvent event) {
        Result result = smsNotifyManager.voucher(event.getItem());
        if (!result.isSuccess()) {
            log.warn("发送出票通知失败: 订单号:{}, 原因:{}", event.getItem().getOrderNumber(), result.getMsg());
        }
    }

    /**
     * 核销后处理
     *
     * @param event
     */
    @EventListener
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void ticketConsume(TicketConsumeEvent event) {
        // 检查订单是否已完成
        orderManager.checkOrderComplete(event.getItem().getId());
    }

    /**
     * 核销团队处理
     *
     * @param event
     */
    @EventListener
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @SuppressWarnings("unchecked")
    public void ticketConsumeOfGroup(TicketConsumeEvent event) {
        FxOrder order = orderMapper.selectOne(new FxOrder().setId(event.getItem().getOrderId()));
        if (order.getGroupId() == null) {
            return;
        }
        FxGroupProduct product = groupProductMapper.selectOne(new FxGroupProduct().setOrderId(order.getId()));
        groupProductMapper.updateIncrement(new FxGroupProduct().setRealSum(event.getVo().getQuantity()).setId(product.getId()));
        Integer price = event.getVo().get("price", Integer.class);
        groupConsumeMapper.insertSelective(BeanUtil.toBean(event.getRecord(), FxGroupConsume.class)
                .setId(null)
                .setGroupId(order.getGroupId())
                .setPrice(price)
                .setQuantity(event.getVo().getQuantity())
        );
        List<String> sid = event.getVo().get("sid", List.class);
        if (CollUtil.isNotEmpty(sid)) {
            sid.stream().distinct().forEach(id ->
                    groupMemberMapper.updateByExampleSelective(new FxGroupMember().setIsPresent(true), Example.builder(FxGroupMember.class)
                            .where(WeekendSqls.<FxGroupMember>custom()
                                    .andEqualTo(FxGroupMember::getCardType, Constant.CardType.ID)
                                    .andEqualTo(FxGroupMember::getBudgetId, product.getId())
                            )
                            .build()
                    ));
        }
    }

    @Async
    @TransactionalEventListener(fallbackExecution = true)
    public void ticketConsumeOfCredit(TicketConsumeEvent event) {
        log.info("订单核销请求新媒体积分生效，订单号：{}", event.getItem().getOrderNumber());
        buttJointNmediaManager.creditConfirm(event.getItem().getNumber());
    }

    @Async
    @TransactionalEventListener(fallbackExecution = true)
    public void ticketConsumeOfPushMsg(TicketConsumeEvent event) {
        FxOrder order = orderManager.getOrderByItem(event.getItem().getNumber());
        if (order.getSource().equals(Constant.OrderSource.OTA)) {
            FxOtaPushConfig fxOtaPushConfig = fxOtaPushConfigMapper.selectOne(new FxOtaPushConfig().setMerchantId(order.getBuyMerchantId()));
            if (!pushMsgAdapter.containsKey(fxOtaPushConfig.getType())) {
                log.warn("找不到OTA消息处理适配器:{}", fxOtaPushConfig.getType());
            } else {
                Map<String, Object> map = BeanUtil.beanToMap(event);
                map.put("order", order);
                map.put("serialNo", event.getRecord().getSerialNo());
                pushMsgAdapter.get(fxOtaPushConfig.getType()).pushMsg(map, fxOtaPushConfig, "CONSUME");
            }
        }
    }
}
