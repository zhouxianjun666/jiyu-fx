package com.jiyu.fx.event;

import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.ChangeStock;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxOrderItemDetail;
import com.jiyu.fx.entity.FxOtaPushConfig;
import com.jiyu.fx.event.bo.RefundAuditEvent;
import com.jiyu.fx.event.bo.RefundMoneyEvent;
import com.jiyu.fx.event.bo.TicketRefundEvent;
import com.jiyu.fx.manager.*;
import com.jiyu.fx.mapper.FxOrderItemDetailMapper;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxOtaPushConfigMapper;
import com.jiyu.fx.ota.push.PushMsgAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/15 10:54
 */
@Service
@Slf4j
public class RefundOrderEventService {
    @Resource
    private FxOtaPushConfigMapper fxOtaPushConfigMapper;
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private FxOrderItemDetailMapper detailMapper;
    @Autowired
    private SmsNotifyManager smsNotifyManager;
    @Autowired
    private ProductPlanManager productPlanManager;
    @Autowired
    private RefundManager refundManager;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private ButtJointNmediaManager buttJointNmediaManager;

    private Map<String, PushMsgAdapter> pushMsgAdapter;
    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init() {
        pushMsgAdapter = applicationContext.getBeansOfType(PushMsgAdapter.class).values().stream().collect(
                Collectors.toMap(PushMsgAdapter::otaType, Function.identity())
        );
    }

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void auditOfSmsNotify(RefundAuditEvent event) {
        Result result = smsNotifyManager.refundFail(event.getItem(), event.getRefundOrder(), "审核被拒绝");
        if (!result.isSuccess()) {
            log.warn("发送退订失败通知失败: 订单号: {}, 退订订单号: {}, 原因: {}", event.getItem().getOrderNumber(), event.getRefundOrder().getNumber(), result.getMsg());
        }
    }

    @EventListener(condition = "#event.success == false ")
    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void  auditOfMsgPush(RefundAuditEvent event) {
        FxOrder order = orderManager.getOrderByItem(event.getItem().getNumber());
        if (order.getSource().equals(Constant.OrderSource.OTA)) {
            FxOtaPushConfig fxOtaPushConfig = fxOtaPushConfigMapper.selectOne(new FxOtaPushConfig().setMerchantId(order.getBuyMerchantId()));
            if (!pushMsgAdapter.containsKey(fxOtaPushConfig.getType())) {
                log.warn("找不到OTA消息处理适配器:{}", fxOtaPushConfig.getType());
            } else {
                String pushCode = "REFUND_FAIL";
                Map<String, Object> map = new HashMap<>(10);
                map.put("order", order);
                map.put("orderItem",  event.getItem());
                map.put("refundOrder", event.getRefundOrder());
                pushMsgAdapter.get(fxOtaPushConfig.getType()).pushMsg(map, fxOtaPushConfig, pushCode);
            }
        }
    }

    @EventListener(condition = "#event.success == true")
    @Order(1)
    public void refundTicket(TicketRefundEvent event) {
        FxOrderItem orderItem = orderItemMapper.selectByPrimaryKey(event.getRefundOrder().getOrderItemId());
        List<FxOrderItemDetail> details = detailMapper.select(new FxOrderItemDetail().setOrderItemId(orderItem.getId()));
        Map<Date, FxOrderItemDetail> detailMap = details.stream().collect(Collectors.toMap(FxOrderItemDetail::getDay, Function.identity()));
        List<ChangeStock> changeStocks = event.getDayMap().entrySet().stream()
                .map(entry -> new ChangeStock()
                        .setCode(orderItem.getProductSubNumber())
                        .setDays(1)
                        .setStart(entry.getKey())
                        .setAvailable(!detailMap.get(entry.getKey()).getOversell() ? entry.getValue() : 0)
                        .setSold(-entry.getValue())
                )
                .collect(Collectors.toList());
        productPlanManager.changeStock(changeStocks);
    }

    @TransactionalEventListener(fallbackExecution = true, condition = "#event.success == false")
    @Order(2)
    @Async
    public void refundTicketOfSmsNotify(TicketRefundEvent event) {
        FxOrderItem orderItem = orderItemMapper.selectByPrimaryKey(event.getRefundOrder().getOrderItemId());
        Result result = smsNotifyManager.refundFail(orderItem, event.getRefundOrder(), "余票不足");
        if (!result.isSuccess()) {
            log.warn("发送退订失败通知失败: 订单号: {}, 退订订单号: {}, 原因: {}", orderItem.getOrderNumber(), event.getRefundOrder().getNumber(), result.getMsg());
        }
    }

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void  refundTicketOfPushMsg(TicketRefundEvent event) {
        FxOrder order = orderManager.getOrderByItem(event.getRefundOrder().getOrderItemId());
        FxOrderItem orderItem = orderItemMapper.selectByPrimaryKey(event.getRefundOrder().getOrderItemId());
        if (order.getSource().equals(Constant.OrderSource.OTA)) {
            FxOtaPushConfig fxOtaPushConfig = fxOtaPushConfigMapper.selectOne(new FxOtaPushConfig().setMerchantId(order.getBuyMerchantId()));
            if (!pushMsgAdapter.containsKey(fxOtaPushConfig.getType())) {
                log.warn("找不到OTA消息处理适配器:{}", fxOtaPushConfig.getType());
            } else {
                String pushCode = "REFUND_SUCCESS";
                Map<String, Object> map = new HashMap<>(10);
                map.put("order", order);
                map.put("orderItem",  orderItem);
                map.put("refundOrder", event.getRefundOrder());
                pushMsgAdapter.get(fxOtaPushConfig.getType()).pushMsg(map, fxOtaPushConfig, pushCode);
            }
        }
    }

    @EventListener
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void refundMoney(RefundMoneyEvent event) {
        // 检查订单是否已完成
        orderManager.checkOrderComplete(event.getItem().getId());
    }

    @TransactionalEventListener
    @Async
    public void refundMoneyOfSmsNotify(RefundMoneyEvent event) {
        Result<?> result = smsNotifyManager.refundSuccess(event.getItem(), event.getRefundOrder());
        if (!result.isSuccess()) {
            log.warn("发送退订成功通知失败: 订单号:{}, 退订订单号:{}, 原因:{}", event.getItem().getOrderNumber(), event.getRefundOrder().getNumber(), result.getMsg());
        }
    }

    @TransactionalEventListener
    @Async
    public void refundMoneyOfCredit(RefundMoneyEvent event) {
        log.info("退订成功请求新媒体消费积分取消，订单号：{}", event.getItem().getOrderNumber());
        buttJointNmediaManager.creditCancel(event.getItem().getNumber());
    }
}
