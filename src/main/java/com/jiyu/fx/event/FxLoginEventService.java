package com.jiyu.fx.event;

import cn.hutool.core.codec.Base64;
import com.jiyu.base.dto.WxToken;
import com.jiyu.base.event.LoginEvent;
import com.jiyu.fx.entity.shop.Shop;
import com.jiyu.fx.mapper.ShopMapper;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/28 9:33
 */
@Service
public class FxLoginEventService {
    @Resource
    private ShopMapper shopMapper;

    @EventListener(condition = "#event.token instanceof T(com.jiyu.base.dto.WxToken) && #event.token.type == T(com.jiyu.common.Constant.WxLoginType).SHOP")
    public void loginOfShowWx(LoginEvent event) {
        WxToken wxToken  = (WxToken) event.getToken();
        String sid = wxToken.getRequest().getParameter("__sid");
        Shop shop = shopMapper.selectByPrimaryKey(Base64.decodeStr(sid));
        if (shop == null) {
            throw new UnknownAccountException("店铺不存在");
        }
        event.getUser().put("shop", shop.setAppSecret(null));
    }
}
