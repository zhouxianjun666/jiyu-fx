package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxRefundOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 退订审核事件业务对象
 * @date 2019/3/14 18:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class RefundAuditEvent extends ExpandData {
    /**
     * 退订订单
     */
    private FxRefundOrder refundOrder;
    /**
     * 子订单
     */
    private FxOrderItem item;
    /**
     * 审核结果
     */
    private boolean success;
}
