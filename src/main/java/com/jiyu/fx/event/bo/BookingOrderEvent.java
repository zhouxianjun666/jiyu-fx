package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.vo.BookingVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单预定事件业务对象
 * 订单创建成功后触发
 * @date 2019/3/14 18:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class BookingOrderEvent extends ExpandData {
    /**
     * 预定信息
     */
    private BookingVO vo;
    /**
     * 类型
     */
    private String type;
    /**
     * 订单信息
     */
    private FxOrder order;
    /**
     * 子订单列表
     */
    private List<FxOrderItem> items;
    /**
     * 当前用户
     */
    private AuthUser currentUser;
}
