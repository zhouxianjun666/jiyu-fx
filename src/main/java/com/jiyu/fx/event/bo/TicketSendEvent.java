package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxOrderItemTicket;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单出票事件业务对象
 * 订单出票后触发
 * @date 2019/3/14 18:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TicketSendEvent extends ExpandData {
    /**
     * 子订单信息
     */
    private FxOrderItem item;
    /**
     * 出票结果
     */
    private boolean success;
    /**
     * 票据信息
     */
    private List<FxOrderItemTicket> tickets;
}
