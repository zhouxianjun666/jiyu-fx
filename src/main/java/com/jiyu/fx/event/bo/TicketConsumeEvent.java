package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxOrderItemConsume;
import com.jiyu.fx.vo.VoucherConsumeVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单核销事件业务对象
 * 订单出票后触发
 * @date 2019/3/14 18:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TicketConsumeEvent extends ExpandData {
    /**
     * 子订单信息
     */
    private FxOrderItem item;
    /**
     * 每天核销票数
     */
    private Map<Date, Integer> dayMap;
    /**
     * 核销数据
     */
    private VoucherConsumeVO vo;
    /**
     * 核销流水记录
     */
    private FxOrderItemConsume record;
}
