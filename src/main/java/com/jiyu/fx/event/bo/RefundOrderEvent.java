package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxRefundOrder;
import com.jiyu.fx.vo.RefundVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 退订申请事件业务对象
 * @date 2019/3/14 18:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class RefundOrderEvent extends ExpandData {
    /**
     * 退订信息
     */
    private RefundVO vo;
    /**
     * 类型
     */
    private String type;
    /**
     * 退订订单
     */
    private FxRefundOrder refundOrder;
    /**
     * 订单信息
     */
    private FxOrder order;
    /**
     * 子订单
     */
    private FxOrderItem item;
    /**
     * 当前用户
     */
    private AuthUser currentUser;
}
