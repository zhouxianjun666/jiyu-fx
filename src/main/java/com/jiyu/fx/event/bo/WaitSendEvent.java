package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrderItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 待出票事件
 * @date 2019/4/2 12:34
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class WaitSendEvent extends ExpandData {
    private FxOrderItem item;
}
