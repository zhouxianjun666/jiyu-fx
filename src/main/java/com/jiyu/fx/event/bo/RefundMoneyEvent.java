package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrderItem;
import com.jiyu.fx.entity.FxRefundMoneySerial;
import com.jiyu.fx.entity.FxRefundOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单退款成功事件业务对象
 * @date 2019/3/25 18:23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class RefundMoneyEvent extends ExpandData {
    /**
     * 退订订单
     */
    private FxRefundOrder refundOrder;
    /**
     * 退款流水
     */
    private FxRefundMoneySerial serial;
    /**
     * 子订单
     */
    private FxOrderItem item;
}
