package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrder;
import com.jiyu.fx.entity.FxOrderItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单取消事件业务对象
 * 订单取消后触发
 * @date 2019/3/14 18:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class CancelOrderEvent extends ExpandData {
    /**
     * 订单信息
     */
    private FxOrder order;
    /**
     * 子订单列表
     */
    private List<FxOrderItem> items;
    /**
     * 取消类型
     */
    private Integer type;
    /**
     * 原因
     */
    private String reason;
}
