package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxOrderItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单审核事件业务对象
 * 订单预定审核后触发
 * @date 2019/3/14 18:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class AuditOrderEvent extends ExpandData {
    /**
     * 子订单信息
     */
    private FxOrderItem item;
    /**
     * 审核结果
     */
    private boolean success;
}
