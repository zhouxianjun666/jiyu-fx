package com.jiyu.fx.event.bo;

import com.jiyu.common.dto.ExpandData;
import com.jiyu.fx.entity.FxRefundOrder;
import com.jiyu.fx.entity.FxRefundTicketSerial;
import com.jiyu.fx.entity.FxRefundVisitor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单退票事件业务对象
 * 订单退票后触发
 * @date 2019/3/14 18:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TicketRefundEvent extends ExpandData {
    /**
     * 退订订单
     */
    private FxRefundOrder refundOrder;
    /**
     * 流水
     */
    private FxRefundTicketSerial serial;
    /**
     * 退票结果
     */
    private boolean success;
    /**
     * 退订游客信息
     */
    private List<FxRefundVisitor> visitors;
    /**
     * 每天退订票数
     */
    private Map<Date, Integer> dayMap;
}
