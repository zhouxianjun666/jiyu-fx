package com.jiyu.fx.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GroupBaseMemberVO {
    @ApiModelProperty("姓名")
    @NotNull(message = "姓名不能为空")
    private String name;

    @ApiModelProperty("证件类型")
    private Integer cardType;

    @ApiModelProperty("证件号码")
    private String card;

    @ApiModelProperty("手机号")
    @NotNull(message = "手机号不能为空")
    private String phone;

    @ApiModelProperty("是否成人")
    @NotNull
    private Boolean adult;
}
