package com.jiyu.fx.vo;

import com.jiyu.common.dto.ExpandData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/23 16:33
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class VoucherConsumeVO extends ExpandData {
    @ApiModelProperty(required = true, value = "本次核销流水号")
    @NotBlank(message = "核销流水号不能为空")
    private String serialNo;

    @ApiModelProperty(required = true, value = "凭证码")
    @NotBlank(message = "凭证码不能为空")
    private String voucherNumber;

    @ApiModelProperty(required = true, value = "数量")
    @NotNull(message = "核销数量不能为空")
    @Min(value = 1, message = "核销数量必须大于0")
    private Integer quantity;

    @ApiModelProperty("核销时间 yyyy-MM-dd HH:mm:ss")
    private Date time;
}
