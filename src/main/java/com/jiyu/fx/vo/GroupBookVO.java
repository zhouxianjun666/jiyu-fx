package com.jiyu.fx.vo;

import com.jiyu.common.dto.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class GroupBookVO {

    @ApiModelProperty("id")
    @NotNull(groups = {UpdateGroup.class})
    private Integer id;

    @ApiModelProperty("团号")
    @Valid
    @NotNull(message = "团号不能为空")
    private String number;

    @ApiModelProperty("旅行社名称")
    @Valid
    @NotNull(message = "旅行社名称不能为空")
    private String travelName;

    @ApiModelProperty("开始时间")
    @Valid
    @NotNull(message = "开始时间不能为空")
    private Date startDate;

    @ApiModelProperty("省")
    @Valid
    @NotNull(message = "省不能为空")
    private Integer province;

    @ApiModelProperty("市")
    @Valid
    @NotNull(message = "市不能为空")
    private Integer city;

    @ApiModelProperty("全地址")
    private String address;

    @ApiModelProperty("导游姓名")
    @Valid
    @NotNull(message = "导游姓名不能为空")
    private String guideName;

    @ApiModelProperty("导游手机号")
    @Valid
    @NotNull(message = "导游手机号不能为空")
    private String guidePhone;

    @ApiModelProperty("导游身份证")
    @Valid
    @NotNull(message = "导游身份证不能为空")
    private String guideSid;

    @ApiModelProperty("导游证件号")
    private String guideCard;

    @ApiModelProperty("总人数")
    @Valid
    @NotNull(message = "总人数不能为空")
    private Integer sum;

    @ApiModelProperty("预订产品")
    @Valid
    private List<GroupProductVO> products;

}
