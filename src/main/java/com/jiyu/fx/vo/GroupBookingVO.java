package com.jiyu.fx.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/22 9:16
 */
@Data
public class GroupBookingVO {
    @ApiModelProperty("产品预算id")
    @NotNull
    private Integer budgetId;

    @ApiModelProperty("预定游玩时间")
    @NotNull(message = "预定游玩时间不能为空")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date booking;

    @ApiModelProperty("预定天数")
    private Integer days;

    @ApiModelProperty("票数")
    @NotNull
    @Min(1)
    private Integer quantity;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("是否使用积分")
    private Boolean credit;
}
