package com.jiyu.fx.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Package: com.jiyu.fx.vo
 * @Author: 娄楚帅
 * @CreateDate: 2019/5/13 15:10
 */
@Data
public class UpdateInfoVO {

    @ApiModelProperty("账号信息")
    @NotBlank
    private String accountName;

    @ApiModelProperty("详细地址")
    @NotBlank
    private String address;

    @ApiModelProperty("商户类型")
    private Integer typeOfComp;

    @ApiModelProperty("企业名称")
    @NotBlank
    private String realName;

    @ApiModelProperty("企业地址")
    @NotBlank
    private String pcastr;

}
