package com.jiyu.fx.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jiyu.fx.manager.OrderManager;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/14 10:29
 */
@Data
@Accessors(chain = true)
public class BookingItemVO {
    @ApiModelProperty("子产品编号")
    @NotNull(message = "子产品编号不能为空")
    private Long productSubNumber;

    @ApiModelProperty("预定游玩时间")
    @NotNull(message = "预定游玩时间不能为空")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date booking;

    @ApiModelProperty("预定天数")
    private Integer days;

    @ApiModelProperty("游客列表")
    @NotEmpty(message = "游客不能为空")
    @Valid
    private List<BookingVisitorVO> visitors;

    @ApiModelProperty(readOnly = true, hidden = true)
    private Integer quantity;

    public int getQuantity() {
        if (quantity == null) {
            quantity = OrderManager.getTotalQuantity(visitors);
        }
        return quantity;
    }
}
