package com.jiyu.fx.vo;

import com.jiyu.fx.entity.FxProductSub;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description: 主产品VO类
 * @Author: songe
 * @CreateDate: 2019/1/9 8:50
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/9 8:50
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
@Data
@Accessors(chain = true)
public class FxProductVO implements Serializable {
    private Integer id;

    /**
     * 产品code
     */
    private String code;

    /**
     * 产品名
     */
    private String name;

    /**
     * 产品通知短信
     */
    private String phone;

    /**
     * 企业id
     */
    private String merchantId;

    /**
     * 企业名
     */
    private String merchantName;

    /**
     * 产品类型(景区：5001，酒店：5002)
     */
    private Integer type;


    private Date createTime;

    private Date updateTime;
    /**
     * 图片
     */
    private String imgs;

    /**
     * 县
     */
    private Integer area;

    /**
     * 省市县str
     */
    private String pcastr;

    /**
     * 详细地址
     */
    private String address;

    private Integer productId;


    /**
     * 景点类型:["名胜古迹"]
     */
    private String category;

    /**
     * 景点级别
     */
    private Integer level;

    /**
     * 营业时间
     */
    private String businessTime;

    /**
     * 服务电话
     */
    private String tel;

    /**
     * 地图坐标
     */
    private String map;

    /**
     * 交通线路
     */
    private String transitLine;

    /**
     * 景点简介
     */
    private String blurb;


    private List<FxProductSub> subs;
}
