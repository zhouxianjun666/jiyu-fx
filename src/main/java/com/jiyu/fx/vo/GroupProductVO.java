package com.jiyu.fx.vo;

import com.jiyu.common.dto.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class GroupProductVO {

    @ApiModelProperty("产品id")
    @NotNull(groups = {UpdateGroup.class})
    private Integer id;

    @ApiModelProperty("团队id")
    @Valid
    @NotNull(message = "导团队id不能为空")
    private Integer groupId;

    @ApiModelProperty("产品code")
    @Valid
    @NotNull(message = "产品code不能为空")
    private Long code;

    @ApiModelProperty("产品名称")
    @Valid
    @NotNull(message = "产品名称不能为空")
    private String productName;

    @ApiModelProperty("子产品")
    @Valid
    @NotNull(message = "子产品不能为空")
    private Long subCode;

    @ApiModelProperty("子产品名称")
    @Valid
    @NotNull(message = "子产品名称不能为空")
    private String productSubName;

    @ApiModelProperty("游玩日期")
    @Valid
    @NotNull(message = "游玩日期不能为空")
    private Date day;

    @ApiModelProperty("总人数")
    @Valid
    @NotNull(message = "总人数不能为空")
    private Integer sum;

    private Integer realSum;

    @ApiModelProperty("票务产品id")
    private Integer maProductId;

    @ApiModelProperty("订单id")
    private Integer orderId;

    @ApiModelProperty("产品状态")
    private Boolean state;

    @ApiModelProperty("团队产品实名制")
    private Integer realName;

    @ApiModelProperty("成员列表")
    @Valid
    List<GroupMemberVO> members;
}
