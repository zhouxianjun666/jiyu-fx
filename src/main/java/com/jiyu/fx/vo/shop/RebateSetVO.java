package com.jiyu.fx.vo.shop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/27 18:42
 */
@Data
public class RebateSetVO {
    @ApiModelProperty("子产品编号")
    @NotNull(message = "产品编号不能为空")
    private Long code;

    @ApiModelProperty("返利百分比")
    @NotNull(message = "请设置返利百分比")
    @Min(value = 0, message = "返利百分比最低为0%")
    @Max(value = 100, message = "返利百分比最高为100%")
    private Integer rebate;
}
