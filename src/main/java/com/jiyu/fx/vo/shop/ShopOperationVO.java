package com.jiyu.fx.vo.shop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/27 16:13
 */
@Data
@Accessors(chain = true)
public class ShopOperationVO {
    @ApiModelProperty("店铺名称")
    @NotBlank(message = "店铺名称不能为空")
    private String name;

    @ApiModelProperty("微信公众号二维码")
    @NotBlank(message = "请上传微信公众号二维码")
    private String wxQr;

    @ApiModelProperty("微信应用ID")
    @NotBlank(message = "请设置微信应用ID")
    private String appId;

    @ApiModelProperty("微信应用秘钥")
    @NotBlank(message = "请设置微信应用秘钥")
    private String appSecret;
}
