package com.jiyu.fx.vo.shop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/7 15:07
 */
@ApiModel("提现申请")
@Data
public class WithdrawApplyVO {
    @ApiModelProperty(value = "收款方式", notes = "1-微信;2-支付宝;3-银行卡", required = true)
    @NotNull(message = "收款方式不能为空")
    private Integer type;

    @ApiModelProperty(value = "提现金额", required = true)
    @NotNull(message = "提现金额不能为空")
    private Integer money;
}
