package com.jiyu.fx.vo.shop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/28 11:54
 */
@Data
public class WithdrawConfigVO {

    @ApiModelProperty("每周几(1~7)")
    @Range(min = 1, max = 7)
    private Integer weekDay;

    @ApiModelProperty("每月多少号(1~31)")
    @Range(min = 1, max = 31)
    private Integer monthDay;

    @ApiModelProperty("类型: 1 - week; 2 - month")
    @NotNull
    private Integer type;

    @ApiModelProperty("最低限额(分)")
    private Integer limit;

    @ApiModelProperty("开始时间")
    private Date start;

    @ApiModelProperty("结束时间")
    private Date end;
}
