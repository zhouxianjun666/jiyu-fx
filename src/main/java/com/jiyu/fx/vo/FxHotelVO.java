package com.jiyu.fx.vo;


import com.jiyu.fx.entity.FxProductSub;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class FxHotelVO implements Serializable {
    private Integer id;

    /**
     * 产品code
     */
    private Long code;

    /**
     * 产品名
     */
    private String name;

    /**
     * 产品通知短信
     */
    private String phone;

    /**
     * 商户id
     */
    private String merchantId;

    /**
     * 商户名
     */
    private String merchantName;

    /**
     * 产品类型(景区：5001，酒店：5002)
     */
    private String type;


    private Date createTime;

    private Date updateTime;
    /**
     * 图片
     */
    private String imgs;

    /**
     * 县
     */
    private Integer area;

    /**
     * 省市县str
     */
    private String pcastr;

    /**
     * 详细地址
     */
    private String address;

    private Integer productId;

    /**
     * 酒店级别
     */
    private Integer levelType;

    /**
     * 预订电话
     */
    private String tel;

    /**
     * 团队起订数
     */
    private Integer teamNum;

    /**
     * 简介
     */
    private String blurb;

    private String map;

    /**
     * 入住时间
     */
    private String inTime;

    /**
     * 离店时间
     */
    @Column(name = "out_time")
    private String outTime;

    /**
     * 装修时间
     */
    private Date decorateTime;

    /**
     * 开业时间
     */
    private Date openTime;

    /**
     * '支付方式5960微信5961支付宝5962境内银联5963现金5964境外银行卡',
     */
    private String canpayType;

    /**
     * 是否可带宠物
     */
    private Byte petType;

    /**
     * '5950Master5951Visa5952国内银联卡5953信用卡'
     */
    private String cardType;

    /**
     * 网络状况5933免费WIF5934免费宽带5935收费宽带5936无网络
     */
    private String netType;

    /**
     * 酒店标签 5900	顶级奢华酒店5901	商务型酒店5902	连锁品牌5903	假日酒店5904	公寓酒店5905	快捷酒店5906	经济型酒店5907	假日酒店5908	公寓酒店5909	快捷酒店5910	经济型酒店
     */
    private String labelType;

    /**
     * 停车场5940免费停车场5941收费停车场5942无停车场
     */
    private Integer parkType;

    /**
     * 酒店设施5920	西式餐厅5921	中式餐厅5922	室外游泳池5923	室内游泳池5924	残疾人设施5925	温泉5926	会议室5927	健身房5928	SPA5929	棋牌室5930	酒吧5931	商务中心
     */
    private String facilityType;

    /**
     * 服务5911	接机服务5912	接站服务5913	接待外宾5914	行李寄存5915	洗衣5916	看护小孩5917	租车5918	叫醒服务
     */
    private String serverType;

    /**
     * 排序号？
     */
    private Integer sortNum;


    private List<FxProductSub> subs;

}
