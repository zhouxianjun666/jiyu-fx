package com.jiyu.fx.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/25 19:44
 */
@Data
public class ModifyGroupOrderMemberVO {
    @ApiModelProperty("产品预算ID")
    @NotNull
    private Integer budgetId;

    @ApiModelProperty("子订单编号")
    @NotNull
    private Long itemNumber;

    @ApiModelProperty("要删除的成员ID")
    private List<Integer> delete;

    @ApiModelProperty("新增的成员列表")
    @Valid
    private List<GroupBaseMemberVO> add;
}
