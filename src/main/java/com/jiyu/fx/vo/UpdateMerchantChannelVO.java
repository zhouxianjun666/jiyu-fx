package com.jiyu.fx.vo;

import com.jiyu.base.vo.UpdateMerchantVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/28 20:37
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UpdateMerchantChannelVO extends UpdateMerchantVO {
    @ApiModelProperty("销售渠道")
    private Integer channel;
}
