package com.jiyu.fx.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 游客票数量信息
 * @date 2019/1/21 16:45
 */
@Data
@Accessors(chain = true)
public class VisitorQuantityVO {
    @ApiModelProperty("游客ID")
    @NotNull(message = "游客ID不能为空")
    private Integer id;
    @ApiModelProperty("数量")
    @Min(1)
    private Integer quantity;
}
