package com.jiyu.fx.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.jiyu.fx.dto.VisitorQuantityDayMapDeserializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/21 16:37
 */
@Data
@Accessors(chain = true)
public class HotelConsumeVO {
    @ApiModelProperty("核销子订单编号")
    @NotNull(message = "核销子订单编号不能为空")
    private Long number;

    @ApiModelProperty("核销数量:不传游客时有效")
    @Min(1)
    private Integer quantity;

    @ApiModelProperty("核销哪一天:不传游客时有效")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date day;

    @ApiModelProperty("游客核销信息")
    @JsonDeserialize(using = VisitorQuantityDayMapDeserializer.class)
    private Map<Date, List<VisitorQuantityVO>> visitorMap;
}
