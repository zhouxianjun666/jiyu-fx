package com.jiyu.fx.vo;


import com.jiyu.common.dto.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class GroupMemberVO extends GroupBaseMemberVO {

    @ApiModelProperty("成员id")
    @NotNull(groups = {UpdateGroup.class})
    private Integer id;

    @ApiModelProperty("团队ID")
    @NotNull(message = "团队ID不能为空")
    private Integer groupId;

    @ApiModelProperty("子产品id")
    private Long code;

    @ApiModelProperty("产品预算id,GroupProduct的id")
    @NotNull(message = "产品预算id不能为空")
    private Integer budgetId;
}
