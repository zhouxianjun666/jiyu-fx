package com.jiyu.fx.vo;

import com.jiyu.base.vo.CreateMerchantVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/14 8:42
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class CreateFxMerchantVO extends CreateMerchantVO {
    @ApiModelProperty("销售渠道")
    private Integer channel;
}
