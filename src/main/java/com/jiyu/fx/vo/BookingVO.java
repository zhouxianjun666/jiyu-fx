package com.jiyu.fx.vo;

import com.jiyu.common.Constant;
import com.jiyu.fx.entity.FxGroup;
import com.jiyu.fx.entity.FxGroupProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/14 10:26
 */
@Data
@Accessors(chain = true)
public class BookingVO {
    @ApiModelProperty("订单联系人")
    @Valid
    @NotNull(message = "订单联系人不能为空")
    private BookingVisitorVO shipping;

    @ApiModelProperty("预定产品列表")
    @Valid
    @NotNull(message = "预定产品不能为空")
    private List<BookingItemVO> items;

    @ApiModelProperty("来源")
    private Integer source;

    @ApiModelProperty("支付渠道")
    private Integer paymentChannel;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("第三方订单号")
    @Length(max = 50)
    private String outerId;

    @ApiModelProperty("第三方子订单号")
    @Length(max = 50)
    private String outItemId;

    @ApiModelProperty("是否回滚")
    private Boolean rollback;

    @ApiModelProperty("是否使用积分")
    private Boolean credit;

    @ApiModelProperty(accessMode = ApiModelProperty.AccessMode.READ_ONLY, hidden = true)
    private FxGroup group;
    @ApiModelProperty(accessMode = ApiModelProperty.AccessMode.READ_ONLY, hidden = true)
    private FxGroupProduct groupProduct;

    public Boolean getRollback() {
        this.rollback = Optional.ofNullable(this.rollback).orElse(false);
        return this.rollback;
    }

    public Integer getSource() {
        this.source = Optional.ofNullable(this.source).orElse(Constant.OrderSource.FX_PLATFORM);
        return this.source;
    }
}
