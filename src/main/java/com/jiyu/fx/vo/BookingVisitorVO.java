package com.jiyu.fx.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/14 10:29
 */
@Data
@Accessors(chain = true)
public class BookingVisitorVO {
    @ApiModelProperty("游客姓名")
    @NotBlank(message = "游客姓名不能为空")
    private String name;

    @ApiModelProperty("手机号")
    @NotBlank(message = "游客手机号不能为空")
    private String phone;

    @ApiModelProperty("票数")
    private Integer quantity;

    @ApiModelProperty("证件类型")
    private Integer cardType;

    @ApiModelProperty("证件号")
    private String cardNo;

    @ApiModelProperty("性别")
    private Integer gender;
}
