package com.jiyu.fx.vo;

import cn.hutool.core.collection.CollUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.VisitorQuantityDayMapDeserializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/21 16:37
 */
@Data
@Accessors(chain = true)
public class RefundVO {
    @ApiModelProperty("退订子订单编号")
    @NotNull(message = "退订子订单编号不能为空")
    private Long number;
    @ApiModelProperty("退订理由")
    private String cause;
    @ApiModelProperty("第三方退订订单号")
    @Length(max = 50)
    private String outerId;
    @ApiModelProperty("退订数量:不传游客时有效")
    @Min(1)
    private Integer quantity;
    @ApiModelProperty("退订哪一天:不传游客时有效")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date day;
    @ApiModelProperty("游客退订信息")
    @JsonDeserialize(using = VisitorQuantityDayMapDeserializer.class)
    private Map<Date, List<VisitorQuantityVO>> visitorMap;

    @ApiModelProperty(readOnly = true)
    private Date refundTime;
    @ApiModelProperty(readOnly = true)
    private List<Result<?>> results;

    public RefundVO addResult(Result<?> result) {
        if (results == null) {
            results = new ArrayList<>(1);
        }
        results.add(result);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> Result<T> get(Class<T> t) {
        if (CollUtil.isEmpty(results)) {
            return null;
        }
        Optional<Result<?>> optional = results.stream().filter(r -> t.isInstance(r.getValue())).findFirst();
        return (Result<T>) optional.orElse(null);
    }

    public Result<?> get(int index) {
        if (CollUtil.isEmpty(results) || results.size() <= index) {
            return null;
        }
        return results.get(index);
    }
}
