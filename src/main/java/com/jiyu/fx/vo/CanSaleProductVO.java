package com.jiyu.fx.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class CanSaleProductVO {

    @ApiModelProperty("产品id")
    Integer id;

    @ApiModelProperty("产品名")
    String name;

    Long code;

    @ApiModelProperty("供应商名")
    String merchantName;

    @ApiModelProperty("地址")
    String address;

    @ApiModelProperty("商户id")
    Integer merchantId;

    @ApiModelProperty("图片")
    String imgs;

    @ApiModelProperty("子产品")
    List<Map> subs;

}
