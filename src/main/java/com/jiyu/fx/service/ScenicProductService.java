package com.jiyu.fx.service;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.entity.FxProductSub;

import java.util.Map;

/**
 * @Description: 景区产品服务接口
 * @Author: songe
 * @CreateDate: 2019/1/4 14:50
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/4 14:50
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
public interface ScenicProductService {
    Result<?> delete(int id);

    Result<?> list(String name, String subName, Integer pageNum, Integer pageSize);

    Result<?> update(Map map);

    Result<?> info(int id);

    Result<?> updateSub(Map map);

    Result<?> deleteSub(int id);

    Result<?> upOrDownSub(FxProductSub fxProductSub);

    Result<?> selectSubProDetailByid(int id);

    Result<?> selectSubProDetailByCode(Long code);

    Result<?> detail(String code);
}
