package com.jiyu.fx.service;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.AddProductPlanDto;
import com.jiyu.fx.entity.FxProductPlan;

/**
 * @Description: 产品销售计划接口
 * @Author: songe
 * @CreateDate: 2019/1/17 9:09
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/17 9:09
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
public interface ProductPlanService {
    Result<?> add(AddProductPlanDto fxProductPlan);

    Result<?> delete(int id);

    Result<?> list(Integer productSubId, String beginTime, String endTime, Integer pageNum, Integer pageSize);

    Result<?> update(FxProductPlan fxProductPlan);

    Result<?> canSaleList(String name, String subName, Integer type, Integer pageNum, Integer pageSize, Boolean buySale);
}
