package com.jiyu.fx.service;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.entity.FxChannel;
import com.jiyu.fx.entity.FxChannelProductsub;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ChannelGroupService {
    Result<?> update(FxChannel fxProductPlan);

    Result<?> delete(int id);

    Result<?> list(Integer pageNum, Integer pageSize);

    /*Result<?> listPage(Integer pageNum, Integer pageSize, String name, Long phone);*/

    Result<?> selectDistProSub(int id);

    Result<?> selectCanDistProSub(int id);

    Result<?> upProSub(FxChannelProductsub channelProductsub);

    Result<?> downProSub(Map map);

    Result<?> upEp2Channel(Integer channelId, Integer merchantId);

    Result<?> showList();

    Result<?> moveEp2OtherChannelGroup(Integer channelId, Integer tarChannelId, Integer merchantId);

    Result<?> moveSub2OtherChannelGroup(Integer channelId, Integer tarChannelId, Integer productsubId);

    Result<?> findCanUpChannel(int productSubId);

    Result<?> upPro2Channel(Map map);

    Result<?> findCanDownChannel(int productSubId);

    Result<?> downProFrChannel(Map map);

    Result<?> eplist(Integer channelId, Integer pageNum, Integer pageSize);

    Result<?> canCorrelationList(Integer applyType, Integer beAppliedType, String name, Integer pageNum, Integer pageSize);

    Result<?> correlationList(Integer applyType, Integer beAppliedType, Integer status, String name, Integer pageNum, Integer pageSize);

    Result<?> canAddGroup();

    Result<?> addGroup(ArrayList<Integer> ids, Integer channelId);

    Result<?> setRebate(List<Map<String, Object>> list, Integer channelId);
}
