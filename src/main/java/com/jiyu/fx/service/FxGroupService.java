package com.jiyu.fx.service;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.vo.GroupBookVO;

import java.util.Date;

public interface FxGroupService {
    Result<?> update(GroupBookVO vo);

    Result<?> delete(int id);

    Result<?> list(Date startTime, String number, String travelName, String guideName, Integer pageNum, Integer pageSize);

    Result<?> showDetail(Integer id);

    Result<?> queryGroupProducts(String configid, String sid);
}
