package com.jiyu.fx.service;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.DistributeDto;
import com.jiyu.fx.entity.FxProductChain;

/**
 * @Description: 分销服务类接口
 * @Author: songe
 * @CreateDate: 2019/1/16 17:51
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/16 17:51
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
public interface ProductDistributeService {

    Result<?> distribute2ep(DistributeDto fxProductPlan);

    Result<?> update(FxProductChain fxProductPlan);

    Result<?> delete(int id);

    Result<?> list(String productSubName, Integer productType, Integer pageNum, Integer pageSize);

    /*Result<?> findEpsFrSubPro(int productSubId);*/
}
