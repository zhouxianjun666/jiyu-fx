package com.jiyu.fx.service;

import com.jiyu.common.dto.Result;
import com.jiyu.fx.entity.FxProductSub;

import java.util.Map;

public interface FxHotelProductService {
    Result<?> update(Map<String, Object> map);

    Result<?> list(String name, String subName, Integer pageNum, Integer pageSize);

    Result<?> delete(int id);

    Result<?> deleteSub(int id);

    Result<?> upOrDownSub(FxProductSub fxProductSub);

    Result<?> updateSub(FxProductSub map);

    Result<?> selectCanSaleHotelList(String province, String city, String inTime, String ouTime, String name, Integer level, Integer lowPrice, Integer highPrice, String hotelTheme, String leveSort, String priceSort, Integer pageNum, Integer pageSize, Boolean buySale);

    Result<?> queryDetail(String code);

    Result<?> selectSubInfo(String code);
}
