package com.jiyu.fx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import com.jiyu.base.manager.MerchantManager;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.MerchantEp;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.service.ChannelGroupService;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ChannelGroupServiceImpl implements ChannelGroupService {


    @Resource
    FxChannelMapper mapper;

    @Resource
    FxProductSubMapper subMapper;

    @Resource
    FxChannelProductsubMapper channelProductsubMapper;

    @Resource
    FxProductChainMapper chainMapper;
    @Resource
    private FxChannelMerchantMapper channelMerchantMapper;

    @Resource
    private FxChannelCorrelationMapper fxChannelCorrelationMapper;

    @Resource
    MerchantManager merchantManager;

    @Override
    public Result<?> update(FxChannel fxProductPlan) {
        Date now = new Date();
        Integer merchantId = Util.getUser().getMerchant().getId();
        Integer userId = Util.getUser().getId();
        if (fxProductPlan.getId() == null) {
            fxProductPlan.setCreateTime(now);
            fxProductPlan.setUpdateTime(now);
            fxProductPlan.setMerchantId(merchantId);
            fxProductPlan.setUserId(userId);
            mapper.insertSelective(fxProductPlan);
        } else {
            fxProductPlan.setUpdateTime(now);
            mapper.updateByPrimaryKeySelective(fxProductPlan);
            //chainMapper.updateByProfitAndChannelId(fxProductPlan.getProfitType(), fxProductPlan.getProfit(), fxProductPlan.getId());
        }
        return Result.ok();
    }

    @Override
    public Result<?> delete(int id) {
        mapper.deleteByPrimaryKey(id);
        return Result.ok();
    }

    @Override
    public Result<?> list(Integer pageNum, Integer pageSize) {
        Object value = Util.pageOrOffsetOrNone(() -> mapper.selectByPlatformId(Util.getUser().getMerchant().getId()), pageNum, pageSize);
        return Result.ok(value);
    }

/*    @Override
    public Result<?> listPage(Integer pageNum, Integer pageSize, String name, Long phone) {
        Object value = Util.pageOrOffsetOrNone(() -> mapper.selectByMerchantIdAndSearch(Util.getUser().getMerchant().getId(), name, phone), pageNum, pageSize);
        return Result.ok(value);
    }*/



    @Override
    public Result<?> selectDistProSub(int id) {
        List<Map<String, Object>> list = subMapper.selectProSubByChannelId(id);
        return Result.ok(list);
    }

    @Override
    public Result<?> selectCanDistProSub(int id) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        List<FxProductSub> list = subMapper.selectCanDistProSub(id, merchantId);
        return Result.ok(list);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> upProSub(FxChannelProductsub channelProductsub) {
        AuthUser user = Util.getUser();
        Integer merchantId = user.getMerchant().getId();
        channelProductsub.setCreateTime(new Date());
        channelProductsubMapper.insertSelective(channelProductsub);
        List<FxChannelMerchant> list = channelMerchantMapper.selectByChannelId(channelProductsub.getChannelId());
        list.forEach(t -> chainMapper.insertSelective(new FxProductChain()
                .setSaleMerchantId(merchantId)
                .setMerchantId(t.getMerchantId())
                .setProfitType(channelProductsub.getProfitType())
                .setProductSubId(channelProductsub.getProductsubId())
                .setCreateTime(new Date())
                .setProfit(channelProductsub.getProfit()))
        );

        return Result.ok();
    }

    @Override
    public Result<?> downProSub(Map map) {
        int channelId = MapUtil.getInt(map, "channelId");
        int productSubId = MapUtil.getInt(map, "productsubId");
        channelProductsubMapper.deleteByChannelAndSubId(channelId, productSubId);
        chainMapper.deleteByChannelIdandProductId(productSubId, channelId);
        return Result.ok();
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @Locks("EP-TO-CHANNEL:#{#merchantId}")
    public Result<?> upEp2Channel(Integer channelId, Integer merchantId) {
        Integer saleMerchantId = Util.getUser().getMerchant().getId();
        FxChannel fxChannel = mapper.selectChannelByChannelIdandPlatFormId(channelId, merchantId);
        Assert.notNull(fxChannel, "该渠道不属于当前商户");
        FxChannelMerchant channelEp = channelMerchantMapper.selectOne(new FxChannelMerchant().setMerchantId(merchantId));
        if (channelEp != null) {
            channelMerchantMapper.deleteByChannelIdAndProductId(channelEp.getChannelId(), merchantId);
            chainMapper.deleteByChannelIdAndMerchantId(channelEp.getChannelId(), merchantId);
        }
        channelMerchantMapper.insertSelective(new FxChannelMerchant().setMerchantId(merchantId).setChannelId(channelId).setCreateTime(new Date()));

        List<FxChannelProductsub> subList = channelProductsubMapper.selectByChannelId(channelId);

        subList.forEach(t -> {
            chainMapper.insertSelective(new FxProductChain()
                    .setSaleMerchantId(saleMerchantId)
                    .setMerchantId(merchantId)
                    .setProfitType(t.getProfitType())
                    .setProductSubId(t.getProductsubId())
                    .setCreateTime(new Date())
                    .setProfit(t.getProfit()));
        });
        return Result.ok();
    }

    @Override
    public Result<?> showList() {
        return Result.ok(mapper.selectByPlatformId(Util.getUser().getMerchant().getId()));
    }

    @Override
    public Result<?> moveEp2OtherChannelGroup(Integer channelId, Integer tarChannelId, Integer merchantId) {
        channelMerchantMapper.deleteByChannelIdAndProductId(channelId, merchantId);
        chainMapper.deleteByChannelIdAndMerchantId(channelId, merchantId);
        return null;
    }

    @Override
    public Result<?> moveSub2OtherChannelGroup(Integer channelId, Integer tarChannelId, Integer productsubId) {
        return null;
    }

    @Override
    public Result<?> findCanUpChannel(int productSubId) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        List<Map<String, Object>> list = mapper.selectByProductId(productSubId, merchantId);
        list.forEach(item -> {
            item.put("profitType", 1);
            item.put("profit", 0);
        });
        return Result.ok(list);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> upPro2Channel(Map map) {
        int productSubId = MapUtil.getInt(map, "productSubId");
        Map[] channels = MapUtil.get(map, "channels", Map[].class);
        Date now = new Date();
        Integer merchantId = Util.getUser().getMerchant().getId();
        for (Map channel : channels) {
            List<FxChannelMerchant> list = channelMerchantMapper.selectByChannelId(MapUtils.getIntValue(channel, "id"));
            channelProductsubMapper.insertSelective(new FxChannelProductsub()
                    .setCreateTime(now)
                    .setProductsubId(productSubId)
                    .setChannelId(MapUtils.getIntValue(channel, "id"))
                    .setProfitType(MapUtils.getIntValue(channel, "profitType"))
                    .setProfit(MapUtils.getIntValue(channel, "profit")));
            list.forEach(t -> {
                chainMapper.insertSelective(new FxProductChain()
                        .setSaleMerchantId(merchantId)
                        .setMerchantId(t.getMerchantId())
                        .setProfitType(MapUtils.getIntValue(channel, "profitType"))
                        .setProductSubId(productSubId)
                        .setCreateTime(now)
                        .setProfit(MapUtils.getIntValue(channel, "profit")));

            });
        }

        return Result.ok();
    }

    @Override
    public Result<?> findCanDownChannel(int productSubId) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        List<Map<String, Object>> list = mapper.findCanDownChannel(productSubId, merchantId);
        return Result.ok(list);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> downProFrChannel(Map map) {
        int productSubId = MapUtil.getInt(map, "productSubId");
        FxChannel[] channels = MapUtil.get(map, "channels", FxChannel[].class);
        for (FxChannel channel : channels) {
            channelProductsubMapper.deleteByChannelAndSubId(channel.getId(), productSubId);
            chainMapper.deleteByChannelIdandProductId(productSubId, channel.getId());
        }
        return Result.ok();
    }

    @Override
    public Result<?> eplist(Integer channelId, Integer pageNum, Integer pageSize) {
        Object value = Util.pageOrOffsetOrNone(() -> channelMerchantMapper.selectMerchantsByChannelId(channelId), pageNum, pageSize);
        return Result.ok(value);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> canCorrelationList(Integer applyType, Integer beAppliedType, String name, Integer pageNum, Integer pageSize) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        Object value = Util.pageOrOffsetOrNone(() -> fxChannelCorrelationMapper.canCorrelationList(merchantId, applyType, beAppliedType, name), pageNum, pageSize);
        return Result.ok(value);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> correlationList(Integer applyType, Integer beAppliedType, Integer status, String name, Integer pageNum, Integer pageSize) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        Object value = Util.pageOrOffsetOrNone(() -> fxChannelCorrelationMapper.correlationList(merchantId, applyType, beAppliedType, status, name), pageNum, pageSize);
        return Result.ok(value);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> canAddGroup() {
        Integer merchantId = Util.getUser().getMerchant().getId();
        return Result.ok(fxChannelCorrelationMapper.canAddGroup(merchantId));
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> addGroup(ArrayList<Integer> ids, Integer channelId) {
        List<FxChannelProductsub> subList = channelProductsubMapper.selectByChannelId(channelId);
        Integer merchantId = Util.getUser().getMerchant().getId();
        ids.forEach(item -> {
            channelMerchantMapper.delete(new FxChannelMerchant().setMerchantId(item));
            chainMapper.delete(new FxProductChain().setSaleMerchantId(merchantId).setMerchantId(item));
            channelMerchantMapper.insert(
                new FxChannelMerchant()
                        .setChannelId(channelId)
                        .setMerchantId(item)
                        .setCreateTime(new Date())
            );
            subList.forEach(temp -> {
                chainMapper.insertSelective(new FxProductChain()
                        .setSaleMerchantId(merchantId)
                        .setMerchantId(item)
                        .setProfitType(temp.getProfitType())
                        .setProductSubId(temp.getProductsubId())
                        .setCreateTime(new Date())
                        .setProfit(temp.getProfit()));
            });
        });
        return Result.ok();
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> setRebate(List<Map<String, Object>> list, Integer channelId) {
        list.forEach(item -> {
            channelProductsubMapper.updateByPrimaryKeySelective(new FxChannelProductsub()
                    .setId(MapUtils.getIntValue(item, "refId"))
                    .setChannelId(channelId)
                    .setProductsubId(MapUtils.getIntValue(item, "subId"))
                    .setProfit(MapUtils.getIntValue(item, "profit"))
                    .setProfitType(MapUtils.getIntValue(item, "profitType")));
            List<FxProductChain> productChainList = chainMapper.selectProductChain(MapUtils.getIntValue(item, "subId"), Util.getUser().getMerchant().getId(), channelId);
            productChainList.forEach(temp -> {
                chainMapper.updateByPrimaryKeySelective(temp.setProfit(MapUtils.getIntValue(item, "profit")).setProfitType(MapUtils.getIntValue(item, "profitType")));
            });
        });
        return Result.ok();
    }
}
