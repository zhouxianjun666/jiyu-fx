package com.jiyu.fx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.StrUtil;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.common.exception.ResultRuntimeException;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.service.FxHotelProductService;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 酒店实现类
 * @Author: songe
 * @CreateDate: 2019/3/14 10:27
 * @UpdateUser: songe
 * @UpdateDate: 2019/3/14 10:27
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
@Service
public class FxHotelProductServiceImpl implements FxHotelProductService {

    @Resource
    private FxHotelMapper hotelMapper;

    @Resource
    private FxProductMapper productMapper;

    @Resource
    private FxProductSubMapper subMapper;

    @Autowired
    private Snowflake snowflake;

    @Resource
    private FxProductChainMapper chainMapper;
    @Resource
    private FxProductPlanMapper planMapper;
    @Resource
    private FxChannelProductsubMapper channelProductsubMapper;

    @Resource
    private FxProductMapper fxProductMapper;

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> update(Map<String, Object> map) {
        AuthUser user = Util.getUser();
        Merchant merchant = user.getMerchant();
        String merchantName = merchant.getName();
        map.put("merchantId", merchant.getId());
        FxProduct fxProduct = BeanUtil.mapToBean(map, FxProduct.class, false);
        FxHotel hotel = BeanUtil.mapToBean(map, FxHotel.class, false);
        if (fxProduct.getId() != null) {
            int count = fxProductMapper.selectCountByidAndName(fxProduct.getId(), fxProduct.getName(), merchant.getId());
            Assert.isFalse(count > 0, "产品名重复");
            fxProduct.setCreateTime(null);
            fxProduct.setUpdateTime(null);
            hotel.setCreateTime(null);
            productMapper.updateByPrimaryKeySelective(fxProduct);
            Integer hotelId = hotelMapper.selectHotelByproductId(fxProduct.getId());
            hotelMapper.updateByPrimaryKeySelective(hotel.setId(hotelId));
        } else {
            int count = productMapper.selectCount(new FxProduct().setName(fxProduct.getName()).setMerchantId(merchant.getId().toString()));
            Assert.isFalse(count > 0, "产品名重复");
            Date now = new Date();
            long code = snowflake.nextId();
            fxProduct.setCode(code);
            fxProduct.setType(Constant.ProductType.HOTEL);
            fxProduct.setCreateTime(now);
            fxProduct.setUpdateTime(now);
            fxProduct.setMerchantName(merchantName);
            hotel.setCreateTime(now);
            productMapper.insertSelective(fxProduct);
            hotel.setProductId(fxProduct.getId());
            hotelMapper.insert(hotel);
        }
        return Result.ok();
    }

    @Override
    public Result<?> list(String name, String subName, Integer pageNum, Integer pageSize) {
        AuthUser user = Util.getUser();
        Merchant merchant = user.getMerchant();
        Integer merchantId = merchant.getId();
        Object value = Util.pageOrOffsetOrNone(() -> hotelMapper.selectHotelList(name, subName, merchantId), pageNum, pageSize);
        return Result.ok(value);
    }

    @Override
    public Result<?> delete(int id) {
        List<FxProductSub> subList = subMapper.selectByProductIdAndName(id, null);
        if (CollectionUtil.isNotEmpty(subList)) {
            throw new ResultRuntimeException("该产品下有子产品,不能删除");
        }
        productMapper.deleteByPrimaryKey(id);
        hotelMapper.deleteByProductId(id);
        return Result.ok();
    }

    @Override
    public Result<?> deleteSub(int id) {
        FxProductSub fxProductSub = subMapper.selectByPrimaryKey(id);
        Assert.isFalse(fxProductSub.getStatus(), "请先下架该产品");
        planMapper.delete(new FxProductPlan().setProductSubId(id));
        chainMapper.delete(new FxProductChain().setProductSubId(id));
        channelProductsubMapper.delete(new FxChannelProductsub().setProductsubId(id));
        subMapper.deleteByPrimaryKey(id);
        return Result.ok();
    }

    @Override
    public Result<?> upOrDownSub(FxProductSub fxProductSub) {
        return null;
    }

    @Override
    public Result<?> updateSub(FxProductSub sub) {
        AuthUser user = Util.getUser();
        Merchant merchant = user.getMerchant();
        sub.setMerchantId(merchant.getId());
        sub.setMerchantName(merchant.getName());
        Date now = new Date();
        if (sub.getId() != null) {
            sub.setOperateTime(null);
            sub.setCreateTime(null);
            subMapper.updateByPrimaryKeySelective(sub);
        } else {
            long code = snowflake.nextId();
            sub.setOperateTime(now);
            sub.setCreateTime(now);
            sub.setCode(code);
            subMapper.insertSelective(sub);
        }

        return Result.ok();
    }

    @Override
    public Result<?> selectCanSaleHotelList(String province, String city, String inTime, String ouTime, String name, Integer level, Integer lowPrice, Integer highPrice, String hotelTheme, String levelSort, String priceSort, Integer pageNum, Integer pageSize, Boolean buySale) {
        AuthUser user = Util.getUser();
        Integer merchantId = user.getMerchant().getId();
        if (!StrUtil.isEmpty(levelSort)) {
            Assert.isTrue("desc".equalsIgnoreCase(levelSort) || "asc".equalsIgnoreCase(levelSort), "星级排序传入参数不合法");
        }
        if (!StrUtil.isEmpty(priceSort)) {
            Assert.isTrue("desc".equalsIgnoreCase(priceSort) || "asc".equalsIgnoreCase(priceSort), "价格排序传入参数不合法");

        }
        Object value = Util.pageOrOffsetOrNone(() -> {
            String timeRange = null;
            int day = 0;
            if (StringUtil.isNotBlank(inTime) && StringUtil.isNotBlank(ouTime)) {
                List<DateTime> list = DateUtil.rangeToList(DateUtil.parse(inTime, "yyyy-MM-dd"), DateUtil.parse(ouTime, "yyyy-MM-dd"), DateField.DAY_OF_YEAR);
                if (CollectionUtil.isEmpty(list)) {
                    list.add(DateUtil.parse(inTime, "yyyy-MM-dd"));
                }
                StringBuilder temp = new StringBuilder();
                temp.append("(");
                for (DateTime time : list) {
                    temp.append("'").append(time.toString("yyyy-MM-dd")).append("'").append(",");
                }
                timeRange = temp.toString().subSequence(0, temp.length() - 1) + ")";
                day = list.size();
            }
            return planMapper.selectCanSaleHotelList(province, city, timeRange, day, name, level, lowPrice, highPrice, hotelTheme, levelSort, priceSort, merchantId, buySale);
        }, pageNum, pageSize);
        return Result.ok(value);
    }

    @Override
    public Result<?> queryDetail(String code) {
        return Result.ok(productMapper.selectDetail(Long.valueOf(code)));
    }

    @Override
    public Result<?> selectSubInfo(String code) {
        return Result.ok(subMapper.selectOne(new FxProductSub().setCode(Long.valueOf(code))));
    }
}
