package com.jiyu.fx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.DistributeDto;
import com.jiyu.fx.entity.FxProductChain;
import com.jiyu.fx.mapper.FxChannelMerchantMapper;
import com.jiyu.fx.mapper.FxProductChainMapper;
import com.jiyu.fx.service.ProductDistributeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Description: 分销实现类
 * @Author: songe
 * @CreateDate: 2019/1/16 18:02
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/16 18:02
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
@Service
public class ProductDistributeServiceImpl implements ProductDistributeService {

    @Resource
    private FxProductChainMapper chainMapper;

    @Resource
    private FxChannelMerchantMapper channelMerchantMapper;

    @Override
    public Result<?> distribute2ep(DistributeDto distributeDto) {
        FxProductChain fxProductChain = new FxProductChain();
        BeanUtil.copyProperties(distributeDto, fxProductChain);
        Date now = new Date();
        fxProductChain.setCreateTime(now);
        chainMapper.insertSelective(fxProductChain);

        return Result.ok();
    }

    @Override
    public Result<?> update(FxProductChain fxProductPlan) {
        return null;
    }

    @Override
    public Result<?> delete(int id) {
        return null;
    }

    @Override
    public Result<?> list(String productSubName, Integer productType, Integer pageNum, Integer pageSize) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        Object value = Util.pageOrOffsetOrNone(() -> chainMapper.selectListInfo(productSubName, merchantId,productType), pageNum, pageSize);
        return Result.ok(value);
    }

    /*@Override
    public Result<?> findEpsFrSubPro(int productSubId) {
        Integer Merchant = Util.getUser().getMerchant().getId();
        List<Merchant> list = channelMerchantMapper.selectMerchantsByProductSubId(productSubId, platformId);
        return Result.ok(list);
    }*/
}
