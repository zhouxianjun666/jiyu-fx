package com.jiyu.fx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Snowflake;
import com.jiyu.base.manager.MerchantManager;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.common.exception.ResultRuntimeException;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.service.ScenicProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 类的功能
 * @Author: songe
 * @CreateDate: 2019/1/10 9:01
 * @UpdateUser: songe
 * @UpdateDate: 2019/1/10 9:01
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
@Service
public class ScenicProductServiceImpl implements ScenicProductService {

    @Resource
    private FxProductMapper fxProductMapper;

    @Resource
    private FxSceneryMapper fxSceneryMapper;

    @Autowired
    private Snowflake snowflake;

    @Resource
    private FxProductSubMapper subMapper;

    @Autowired
    MerchantManager merchantManager;

    @Resource
    private FxProductChainMapper chainMapper;

    @Resource
    FxProductPlanMapper planMapper;

    @Resource
    FxChannelProductsubMapper channelProductsubMapper;

    @Override
    public Result<?> delete(int id) {
        List<FxProductSub> subList = subMapper.selectByProductIdAndName(id, null);
        if (CollectionUtil.isNotEmpty(subList)) {
            throw new ResultRuntimeException("该产品下有子产品,不能删除");
        }
        fxProductMapper.deleteByPrimaryKey(id);
        fxSceneryMapper.deleteByProductId(id);
        return Result.ok();
    }

    @Override
    public Result<?> list(String name, String subName, Integer pageNum, Integer pageSize) {
        AuthUser user = Util.getUser();
        Merchant merchant = user.getMerchant();
        Object value = Util.pageOrOffsetOrNone(() -> fxProductMapper.selectSceneryProductList(5001, name, subName, merchant.getId()), pageNum, pageSize);
        return Result.ok(value);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> update(Map map) {
        Merchant merchant = Util.getUser().getMerchant();
        map.put("merchantId", merchant.getId());
        String merchantName = merchant.getName();

        FxProduct fxProduct = BeanUtil.mapToBean(map, FxProduct.class, false);
        FxScenery fxScenery = BeanUtil.mapToBean(map, FxScenery.class, false);
        if (fxProduct.getId() != null) {
            int count = fxProductMapper.selectCountByidAndName(fxProduct.getId(), fxProduct.getName(), merchant.getId());
            Assert.isFalse(count > 0, "产品名重复");
            fxProduct.setCreateTime(null);
            fxProduct.setUpdateTime(null);
            fxScenery.setCreateTime(null);
            fxScenery.setOperateTime(null);
            fxProductMapper.updateByPrimaryKeySelective(fxProduct);
            Integer sceneryId = fxSceneryMapper.selectSceneryByProcutId(fxProduct.getId());
            fxSceneryMapper.updateByPrimaryKeySelective(fxScenery.setId(sceneryId));
        } else {
            int count = fxProductMapper.selectCount(new FxProduct().setName(fxProduct.getName()).setMerchantId(merchant.getId().toString()));
            Assert.isFalse(count > 0, "产品名重复");
            long code = snowflake.nextId();
            Date now = new Date();
            fxProduct.setCode(code);
            fxProduct.setMerchantName(merchantName);
            fxProduct.setCreateTime(now);
            fxProduct.setUpdateTime(now);
            fxProduct.setType(Constant.ProductType.SCENIC);
            fxScenery.setCreateTime(now);
            fxScenery.setOperateTime(now);
            fxProductMapper.insertSelective(fxProduct);
            fxScenery.setProductId(fxProduct.getId());
            fxSceneryMapper.insert(fxScenery);
        }
        return Result.ok();
    }

    @Override
    public Result<?> info(int id) {
        List<Map> list = fxSceneryMapper.selectProductByid(id);
        return Result.ok(list);
    }

    @Override
    public Result<?> updateSub(Map map) {
        FxProductSub fxProductsub = BeanUtil.mapToBean(map, FxProductSub.class, false);
        Date now = new Date();
        if (fxProductsub.getId() != null) {
            fxProductsub.setOperateTime(null);
            fxProductsub.setCreateTime(null);
            subMapper.updateByPrimaryKeySelective(fxProductsub);
        } else {
            fxProductsub.setOperateTime(now);
            fxProductsub.setCreateTime(now);
            long code = snowflake.nextId();
            fxProductsub.setCode(code);
            subMapper.insertSelective(fxProductsub);
        }
        return Result.ok();
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> deleteSub(int id) {
        FxProductSub fxProductSub = subMapper.selectByPrimaryKey(id);
        Assert.isFalse(fxProductSub.getStatus(), "请先下架该产品");
        subMapper.deleteByPrimaryKey(id);
        chainMapper.delete(new FxProductChain().setProductSubId(id));
        planMapper.delete(new FxProductPlan().setProductSubId(id));
        channelProductsubMapper.delete(new FxChannelProductsub().setProductsubId(id));
        return Result.ok();
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> upOrDownSub(FxProductSub fxProductSub) {
        subMapper.updateByPrimaryKeySelective(fxProductSub);
        return Result.ok();
    }

    @Override
    public Result<?> selectSubProDetailByid(int id) {
        return Result.ok(subMapper.selectByPrimaryKey(id));
    }

    @Override
    public Result<?> selectSubProDetailByCode(Long code) {
        if (code == null) {
            return Result.fail("子产品Code不能为空");
        }
        return Result.ok(subMapper.selectOne(new FxProductSub().setCode(code)));
    }

    @Override
    public Result<?> detail(String code) {
        return Result.ok(fxSceneryMapper.detail(code));
    }
}
