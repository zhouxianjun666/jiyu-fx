package com.jiyu.fx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.AddProductPlanDto;
import com.jiyu.fx.entity.FxProductPlan;
import com.jiyu.fx.mapper.FxProductPlanMapper;
import com.jiyu.fx.service.ProductPlanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProductPlanServiceImpl implements ProductPlanService {

    @Resource
    private FxProductPlanMapper productPlanMapper;

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> add(AddProductPlanDto fxProductPlan) {
        if (fxProductPlan.getSaleRuleType() == null) {
            fxProductPlan.setSaleRuleType(0);
        }
        Date beginDay = fxProductPlan.getDateRange().get(0);
        Date endDay = fxProductPlan.getDateRange().get(1);
        productPlanMapper.deleteBatchPlan(beginDay, DateUtil.endOfDay(endDay), fxProductPlan.getProductSubId());
        int days = (int) DateUtil.betweenDay(beginDay, endDay, true);
        List<Map> timeRange = fxProductPlan.getTimeRange();

        Date now = new Date();
        String weeks = StrUtil.padPre(NumberUtil.getBinaryStr(fxProductPlan.getDisableWeek() == null ? 0 : fxProductPlan.getDisableWeek()), 7, '0');
        String disableWeeks = Stream.iterate(BigInteger.ZERO, n -> n.add(BigInteger.ONE)).limit(7).map(j -> weeks.charAt(j.intValue()) == '1' ? "1" : "0").collect(Collectors.joining(""));
        for (int i = 0; i <= days; i++) {
            DateTime day = DateUtil.offsetDay(beginDay, i).setMutable(false);
            if (DateUtil.dayOfWeek(day) == 0) {
                if (disableWeeks.charAt(6) == '1') {
                    continue;
                }
            } else {
                if (disableWeeks.charAt(DateUtil.dayOfWeek(day) - 1) == '1') {
                    continue;
                }
            }
            if (CollectionUtil.isNotEmpty(timeRange)) {
                timeRange.forEach(t -> {
                    String[] timeRanges = MapUtil.get(t, "val", String[].class);
                    Integer marketPrice = MapUtil.get(t, "marketPrice", Integer.class);
                    Integer lowPrice = MapUtil.get(t, "lowPrice", Integer.class);
                    Integer settlePrice = MapUtil.get(t, "settlePrice", Integer.class);
                    String startTime = timeRanges[0];
                    String endTime = timeRanges[1];
                    FxProductPlan plan = new FxProductPlan();
                    BeanUtil.copyProperties(fxProductPlan, plan);
                    plan.setCreateTime(now);
                    plan.setUpdateTime(now);
                    plan.setMarketPrice(marketPrice);
                    plan.setLowPrice(lowPrice);
                    plan.setSettlePrice(settlePrice);
                    plan.setStartTime(day.setField(DateField.HOUR, Integer.valueOf(startTime.split(":")[0])).setField(DateField.MINUTE, Integer.valueOf(startTime.split(":")[1])).setField(DateField.SECOND, 0));
                    plan.setEndTime(day.setField(DateField.HOUR, Integer.valueOf(endTime.split(":")[0])).setField(DateField.MINUTE, Integer.valueOf(endTime.split(":")[1])).setField(DateField.SECOND, 0));
                    productPlanMapper.insertSelective(plan);
                });
            } else {
                FxProductPlan plan = new FxProductPlan();
                BeanUtil.copyProperties(fxProductPlan, plan);
                plan.setCreateTime(now);
                plan.setUpdateTime(now);
                plan.setStartTime(DateUtil.beginOfDay(DateUtil.offsetDay(beginDay, i)).setField(DateField.MILLISECOND, 0));
                plan.setEndTime(DateUtil.endOfDay(DateUtil.offsetDay(beginDay, i)).setField(DateField.MILLISECOND, 0));
                productPlanMapper.insertSelective(plan);
            }

        }
        return Result.ok();
    }


    @Override
    public Result<?> delete(int id) {
        productPlanMapper.deleteByPrimaryKey(id);
        return Result.ok();
    }

    @Override
    public Result<?> list(Integer productSubId, String beginTime, String endTime, Integer pageNum, Integer pageSize) {
        Object value = Util.pageOrOffsetOrNone(() -> productPlanMapper.selectProductPlanList(productSubId, beginTime, endTime), pageNum, pageSize);
        return Result.ok(value);
    }

    @Override
    public Result<?> update(FxProductPlan fxProductPlan) {
        productPlanMapper.updateByPrimaryKeySelective(fxProductPlan);
        return Result.ok();
    }

    @Override
    public Result<?> canSaleList(String name, String subName, Integer type, Integer pageNum, Integer pageSize, Boolean buySale) {
        AuthUser user = Util.getUser();
        Integer merchantId = user.getMerchant().getId();
        Object value = Util.pageOrOffsetOrNone(() -> productPlanMapper.selectCanSaleProList(type, name, subName, merchantId, buySale), pageNum, pageSize);
        return Result.ok(value);
    }


    public static void main(String[] args) {
        String weeks = StrUtil.padPre(NumberUtil.getBinaryStr(0), 7, '0');
        String disableWeeks = Stream.iterate(BigInteger.ZERO, n -> n.add(BigInteger.ONE)).limit(7).map(j -> weeks.charAt(j.intValue()) == '1' ? "1" : "0").collect(Collectors.joining(""));
        System.out.println(disableWeeks);
    }
}
