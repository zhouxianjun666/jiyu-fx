package com.jiyu.fx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.FxGroup;
import com.jiyu.fx.entity.FxGroupMember;
import com.jiyu.fx.entity.FxGroupProduct;
import com.jiyu.fx.mapper.FxGroupMapper;
import com.jiyu.fx.mapper.FxGroupMemberMapper;
import com.jiyu.fx.mapper.FxGroupProductMapper;
import com.jiyu.fx.mapper.FxProductMapper;
import com.jiyu.fx.service.FxGroupService;
import com.jiyu.fx.vo.GroupBookVO;
import com.jiyu.fx.vo.GroupProductVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FxGroupServiceImpl implements FxGroupService {

    @Resource
    FxGroupMapper groupMapper;

    @Resource
    FxGroupProductMapper fxGroupProductMapper;

    @Resource
    FxGroupMemberMapper fxGroupMemberMapper;

    @Resource
    FxProductMapper productMapper;

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> update(GroupBookVO vo) {
        FxGroup fxGroup = new FxGroup();
        BeanUtil.copyProperties(vo, fxGroup);
        Date now = new Date();
        Integer merchantId = Util.getUser().getMerchant().getId();
        String userName = Util.getUser().getUsername();
        if (fxGroup.getId() == null) {
            FxGroup group = groupMapper.selectOne(new FxGroup().setNumber(vo.getNumber()).setState(true).setMerchantId(merchantId));
            Assert.isNull(group, "团队有重复");
            fxGroup.setCreateTime(now);
            fxGroup.setUpdateTime(now);
            fxGroup.setMerchantId(merchantId);
            fxGroup.setCreateUserName(userName);
            groupMapper.insertSelective(fxGroup);
        } else {
            fxGroup.setCreateTime(null);
            fxGroup.setUpdateTime(null);
            fxGroup.setMerchantId(null);
            fxGroup.setCreateUserName(null);
            groupMapper.updateByPrimaryKeySelective(fxGroup);
            fxGroupMemberMapper.deleteByGroupId(fxGroup.getId());
            fxGroupProductMapper.deleteByGroupId(fxGroup.getId());
        }
        vo.getProducts().forEach(t -> {
            FxGroupProduct groupProduct = new FxGroupProduct();
            BeanUtil.copyProperties(t, groupProduct);
            groupProduct.setCreateTime(now);
            groupProduct.setGroupId(fxGroup.getId());
            fxGroupProductMapper.insertSelective(groupProduct);
            t.getMembers().forEach(h -> {
                FxGroupMember groupMember = new FxGroupMember();
                BeanUtil.copyProperties(h, groupMember);
                groupMember.setGroupId(fxGroup.getId());
                groupMember.setBudgetId(groupProduct.getId());
                groupMember.setUpdateTime(now);
                groupMember.setCreateTime(now);
                groupMember.setCreateUserId(merchantId);
                groupMember.setCreateUserName(Util.getUser().getUsername());
                fxGroupMemberMapper.insertSelective(groupMember);
            });
        });

        return Result.ok(fxGroup.getId());
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> delete(int id) {
        groupMapper.deleteByLogic(id);
        fxGroupMemberMapper.deleteByGroupId(id);
        fxGroupProductMapper.deleteByGroupId(id);
        return Result.ok();
    }

    @Override
    public Result<?> list(Date startTime, String number, String travelName, String guideName, Integer pageNum, Integer pageSize) {

        Object value = Util.pageOrOffsetOrNone(() -> {
            Integer merchantId = null;
            if (Util.getUser().getMerchant().getType() == Constant.MerchantType.FX_SELLER) {
                merchantId = Util.getUser().getMerchant().getId();
            }
            return groupMapper.selectSceneryGroupList(startTime, number, travelName, guideName, merchantId);
        }, pageNum, pageSize);
        return Result.ok(value);
    }

    @Override
    public Result<?> showDetail(Integer id) {
        FxGroup fxGroup = groupMapper.selectOne(new FxGroup().setId(id));
        List<Map> products = fxGroupProductMapper.selectbyGroupId(id);
        products.forEach(t -> {
                    List<FxGroupMember> members = fxGroupMemberMapper.selectBybudgetId(Integer.valueOf(t.get("id").toString()));
                    t.put("members", members);
                }
        );
        Map<String, Object> map = new HashMap<>(2);
        map.put("group", fxGroup);
        map.put("products", products);
        return Result.ok(map);
    }

    @Override
    public Result<?> queryGroupProducts(String configId, String sid) {
        Integer merchantId = Util.getUser().getMerchant().getId();
        List<GroupProductVO> list = groupMapper.queryGroupProducts(configId, sid, merchantId);
        return Result.ok(list);
    }
}
