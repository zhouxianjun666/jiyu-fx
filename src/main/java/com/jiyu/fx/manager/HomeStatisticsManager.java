package com.jiyu.fx.manager;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.fx.dto.MerchantSalesOrderOfDay;
import com.jiyu.fx.mapper.FxChannelMapper;
import com.jiyu.fx.mapper.FxOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/5/20 15:44
 */
@Component
@Slf4j
public class HomeStatisticsManager {
    public static final String TODAY_KEY = "HOME:TODAY";
    public static final String DAY7_KEY = "HOME:DAY7";
    public static final String DAY30_KEY = "HOME:DAY30";
    public static final String LAST_KEY = "HOME:LAST";
    private static final int DAYS = 30;
    @Autowired
    private RedissonClient redissonClient;
    @Resource
    private FxOrderMapper orderMapper;
    @Resource
    private FxChannelMapper channelMapper;

    public boolean isLast() {
        RBucket<String> last = redissonClient.getBucket(LAST_KEY);
        try {
            String value = StrUtil.nullToEmpty(last.get());
            String[] arr = value.split(",");
            long time = Long.valueOf(arr[0]);
            if (!DateUtil.isSameDay(new Date(), new Date(time))) {
                return false;
            }
            int oldLastId = Integer.parseInt(arr[1]);
            int lastId = orderMapper.lastId();
            return oldLastId >= lastId;
        } catch (Exception e) {
            log.warn("解析首页统计LAST异常", e);
            last.delete();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Map<Integer, MerchantSalesOrderOfDay>> get() {
        Map<Integer, MerchantSalesOrderOfDay> today = redissonClient.getMap(TODAY_KEY);
        Map<Integer, MerchantSalesOrderOfDay> day7 = redissonClient.getMap(DAY7_KEY);
        Map<Integer, MerchantSalesOrderOfDay> day30 = redissonClient.getMap(DAY30_KEY);
        boolean isEmpty = today.isEmpty() || day7.isEmpty() || day30.isEmpty();
        if (isEmpty) {
            if (!isLast()) {
                List<Map<Integer, MerchantSalesOrderOfDay>> list = load();
                today = list.get(0);
                day7 = list.get(1);
                day30 = list.get(2);
            }
        }
        return Lists.newArrayList(today, day7, day30);
    }

    /**
     * 获取首页数据
     * @param user
     * @return
     */
    public Result<?> home(AuthUser user) {
        int merchant = user.getMerchant().getId();
        List<Integer> ids = channelMapper.getAllChannelMerchantByMerchant(merchant);
        Set<Integer> self = Collections.singleton(merchant);
        List<Map<Integer, MerchantSalesOrderOfDay>> list = get();
        Map<Integer, MerchantSalesOrderOfDay> todayMap = list.get(0);
        Map<Integer, MerchantSalesOrderOfDay> day7Map = list.get(1);
        Map<Integer, MerchantSalesOrderOfDay> day30Map = list.get(2);
        MerchantSalesOrderOfDay salesToday = calc(todayMap, ids);
        MerchantSalesOrderOfDay salesDay7 = calc(day7Map, ids);
        MerchantSalesOrderOfDay salesDay30 = calc(day30Map, ids);
        MerchantSalesOrderOfDay selfToday = calc(todayMap, self);
        MerchantSalesOrderOfDay selfDay7 = calc(day7Map, self);
        MerchantSalesOrderOfDay selfDay30 = calc(day30Map, self);
        return Result.ok()
                .put("salesToday", salesToday)
                .put("salesDay7", salesDay7)
                .put("salesDay30", salesDay30)
                .put("selfToday", selfToday)
                .put("selfDay7", selfDay7)
                .put("selfDay30", selfDay30);
    }

    @Locks
    @SuppressWarnings("unchecked")
    public List<Map<Integer, MerchantSalesOrderOfDay>> load() {
        Date now = new Date();
        List<MerchantSalesOrderOfDay> list = orderMapper.statisticsMerchantSalesOrderOfDay(DAYS);
        Map<Integer, MerchantSalesOrderOfDay> today = groupOfDays(now, list, 0);
        Map<Integer, MerchantSalesOrderOfDay> day7 = groupOfDays(now, list, 7);
        Map<Integer, MerchantSalesOrderOfDay> day30 = groupOfDays(now, list, DAYS);
        RMap<Object, Object> todayMap = redissonClient.getMap(TODAY_KEY);
        todayMap.clear();
        todayMap.putAll(today);
        RMap<Object, Object> day7Map = redissonClient.getMap(DAY7_KEY);
        day7Map.clear();
        day7Map.putAll(day7);
        RMap<Object, Object> day30Map = redissonClient.getMap(DAY30_KEY);
        day30Map.clear();
        day30Map.putAll(day30);
        int lastId = orderMapper.lastId();
        redissonClient.getBucket(LAST_KEY).set(now.getTime() + "," + lastId);
        return Lists.newArrayList(today, day7, day30);
    }

    private Map<Integer, MerchantSalesOrderOfDay> groupOfDays(Date now, List<MerchantSalesOrderOfDay> list, int days) {
        return list.stream()
                .filter(item -> days == DAYS || DateUtil.betweenDay(item.getDays(), now, true) <= days)
                .collect(Collectors.groupingBy(MerchantSalesOrderOfDay::getMerchant))
                .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> {
                            List<MerchantSalesOrderOfDay> value = entry.getValue();
                            return new MerchantSalesOrderOfDay()
                                    .setPayAmount(value.stream().mapToLong(MerchantSalesOrderOfDay::getPayAmount).sum())
                                    .setNum(value.stream().mapToInt(MerchantSalesOrderOfDay::getNum).sum());
                        })
                );

    }

    private MerchantSalesOrderOfDay calc(Map<Integer, MerchantSalesOrderOfDay> map, Collection<Integer> ids) {
        List<MerchantSalesOrderOfDay> list = map.entrySet().stream().filter(entry -> ids.contains(entry.getKey())).map(Map.Entry::getValue).collect(Collectors.toList());
        long payAmount = list.stream().mapToLong(MerchantSalesOrderOfDay::getPayAmount).sum();
        int count = list.stream().mapToInt(MerchantSalesOrderOfDay::getNum).sum();
        return new MerchantSalesOrderOfDay().setPayAmount(payAmount).setNum(count);
    }
}
