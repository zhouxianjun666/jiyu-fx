package com.jiyu.fx.manager;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.StrUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AccountChangeInfo;
import com.jiyu.common.exception.UpdateVersionException;
import com.jiyu.fx.entity.FxMerchantAccount;
import com.jiyu.fx.entity.FxMerchantAccountBills;
import com.jiyu.fx.mapper.FxMerchantAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Optional;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/4/2 15:44
 */
@Component
public class UpdateVersionRetryManager {
    @Autowired
    private Snowflake snowflake;
    @Autowired
    private FxMerchantAccountManager fxMerchantAccountManager;
    @Resource
    private FxMerchantAccountMapper fxMerchantAccountMapper;

    @Retryable(value = UpdateVersionException.class, backoff = @Backoff(delay = 100))
    public FxMerchantAccountBills merchantAccountChange(AccountChangeInfo info) {
        FxMerchantAccount account = fxMerchantAccountMapper.selectOne(new FxMerchantAccount().setMerchantId(info.getMerchantId()).setBossMerchantId(info.getBossMerchantId()));
        FxMerchantAccount update = new FxMerchantAccount().setBalance((long) info.getBalance()).setCash((long) info.getCash()).setVersion(account.getVersion());
        // 根据版本号乐观锁更新
        fxMerchantAccountMapper.updateByVersion(update, ea -> fxMerchantAccountMapper.updateIncrementWhereExample(ea,
                new JoinExample.JustWhere()
                        .where(JoinExample.Where.custom(false)
                                .andEqualTo(FxMerchantAccount::getId, account.getId())
                                .andEqualTo(FxMerchantAccount::getVersion, account.getVersion())
                        )
                        .andWhere(JoinExample.Where.custom(false).allowNullValue()
                                .andIncrement(FxMerchantAccount::getBalance, info.getBalance(), ">= -credit", (Object)null)
                                .orLessThan(FxMerchantAccount::getCredit, "0")
                        )
                        .andWhere(JoinExample.Where.custom(false).allowNullValue()
                                .andIncrement(FxMerchantAccount::getCash, info.getCash(), ">= -credit", (Object)null)
                                .orLessThan(FxMerchantAccount::getCredit, "0")
                        )
                        .buildExampleWhere()
        ), StrUtil.format("账户余额不足,流水:{}", info.getNumber()));
        return new FxMerchantAccountBills()
                .setAccountId(account.getId())
                .setRefId(info.getNumber())
                .setOrderNumber(info.getOrderNumber())
                .setType(info.getType())
                .setRechargeType(info.getRechargeType())
                .setRemark(info.getRemark())
                .setBeforeBalance(account.getBalance())
                .setAfterBalance(account.getBalance() + info.getBalance())
                .setBeforeCash(account.getCash())
                .setAfterCash(account.getCash() + info.getCash())
                .setCreateTime(new Date());
    }

    @Retryable(UpdateVersionException.class)
    public FxMerchantAccountBills adjustment(int boss, int merchantId, Long balance, Long cash, String remark) {
        FxMerchantAccount account = fxMerchantAccountManager.getOrCreate(boss, merchantId);
        fxMerchantAccountMapper.updateByVersion(new FxMerchantAccount().setId(account.getId()).setBalance(balance).setCash(cash).setVersion(account.getVersion()));
        return new FxMerchantAccountBills()
                .setAccountId(account.getId())
                .setRefId(String.valueOf(snowflake.nextId()))
                .setType(Constant.AccountBillsType.CHANGE)
                .setRemark(remark)
                .setBeforeBalance(account.getBalance())
                .setAfterBalance(Optional.ofNullable(balance).orElse(account.getBalance()))
                .setBeforeCash(account.getCash())
                .setAfterCash(Optional.ofNullable(cash).orElse(account.getCash()))
                .setCreateTime(new Date());
    }
}
