package com.jiyu.fx.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.dto.ThreeGroup;
import com.jiyu.common.dto.TwoGroup;
import com.jiyu.fx.dto.SendTicketDayInfo;
import com.jiyu.fx.dto.SendTicketInfo;
import com.jiyu.fx.dto.SendTicketVisitorInfo;
import com.jiyu.fx.dto.TicketSendInfo;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.event.bo.TicketConsumeEvent;
import com.jiyu.fx.event.bo.TicketRefundEvent;
import com.jiyu.fx.event.bo.TicketSendEvent;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.vo.HotelConsumeVO;
import com.jiyu.fx.vo.VisitorQuantityVO;
import com.jiyu.fx.vo.VoucherConsumeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/29 14:25
 */
@Component
@Slf4j
public class TicketManager {
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private FxOrderItemVisitorMapper visitorMapper;
    @Resource
    private FxOrderItemTicketMapper ticketMapper;
    @Resource
    private FxRefundOrderMapper refundOrderMapper;
    @Resource
    private FxRefundTicketSerialMapper refundTicketSerialMapper;
    @Resource
    private FxOrderItemConsumeMapper consumeMapper;
    @Resource
    private FxOrderItemConsumeDetailMapper consumeDetailMapper;
    @Resource
    private FxOrderItemDetailMapper orderItemDetailMapper;
    @Resource
    private FxOrderItemVisitorDetailMapper visitorDetailMapper;
    @Resource
    private FxRefundVisitorMapper refundVisitorMapper;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private RefundManager refundManager;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private VoucherManager voucherManager;
    @Autowired
    private Snowflake snowflake;

    /**
     * 发起出票
     *
     * @param orderItem 子订单
     * @return
     */
    @CheckResult
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> sendTicket(FxOrderItem orderItem) {
        FxOrder order = orderManager.getOrderByItem(orderItem.getId());
        if (order.getPaymentType() == Constant.PaymentType.LOCAL_PAY) {
            return Result.fail("现场到付产品不出票");
        }
        List<FxOrderItemDetail> details = orderItemDetailMapper.select(new FxOrderItemDetail().setOrderItemId(orderItem.getId()));
        List<FxOrderItemVisitor> visitors = visitorMapper.select(new FxOrderItemVisitor().setOrderItemId(orderItem.getId()));
        List<FxOrderItemVisitorDetail> visitorDetails = visitorDetailMapper.select(new FxOrderItemVisitorDetail().setOrderItemId(orderItem.getId()));
        Map<TwoGroup<Integer, Integer>, FxOrderItemVisitorDetail> visitorDetailMap = visitorDetails.stream().collect(Collectors.toMap(FxOrderItemVisitorDetail::unique, Function.identity()));
        orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem().setId(orderItem.getId()).setStatus(Constant.OrderItemStatus.TICKETING));
        return voucherManager.ticketing(orderItem.getMaConfigId(), new SendTicketInfo()
                .setNumber(orderItem.getNumber())
                .setProductNumber(orderItem.getProductSubNumber())
                .setChannelName(order.getBuyMerchantName())
                .setMaProductId(orderItem.getMaProductId())
                .setProductName(orderItem.getProductName())
                .setTicketName(orderItem.getProductSubName())
                .setTicketContent(orderItem.getTicketMsg())
                .setDayInfos(details.stream().map(d -> {
                    String weeks = StrUtil.padPre(NumberUtil.getBinaryStr(d.getDisableWeek()), 7, '0');
                    String disableWeeks = Stream.iterate(BigInteger.ZERO, n -> n.add(BigInteger.ONE)).limit(7)
                            .map(i -> weeks.charAt(i.intValue()) == '1' ? "1" : "0")
                            .collect(Collectors.joining(""));
                    return new SendTicketDayInfo()
                            .setDay(d.getDay())
                            .setEffectiveTime(d.getEffectiveDate())
                            .setExpiryTime(d.getExpiryDate())
                            .setDisableDays(String.join(",", d.getDisableDays()))
                            .setDisableWeeks(disableWeeks)
                            .setVisitors(visitors.stream()
                                    .map(v -> BeanUtil.toBean(v, SendTicketVisitorInfo.class)
                                            .setQuantity(visitorDetailMap.get(new TwoGroup<>(v.getId(), d.getId())).getQuantity())
                                    )
                                    .collect(Collectors.toList())
                            );
                }).collect(Collectors.toList()))
        );
    }

    /**
     * 出票结果
     * 如果订单只有一个游客，票据信息的游客ID方可为空
     *
     * @param number   订单号(子订单)
     * @param infos    票据信息
     * @param success  是否成功
     * @param procTime 处理时间
     * @return
     */
    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> sendResult(long number, List<TicketSendInfo> infos, boolean success, Date procTime) {
        FxOrderItem orderItem = orderItemMapper.selectOne(new FxOrderItem().setNumber(number));
        Assert.notNull(orderItem, "订单不存在");
        if (orderItem.getStatus() != Constant.OrderItemStatus.TICKETING) {
            return Result.fail("当前订单状态不在出票中");
        }
        List<FxOrderItemTicket> tickets = null;
        if (success) {
            List<FxOrderItemVisitorDetail> visitorDetails = visitorDetailMapper.select(new FxOrderItemVisitorDetail().setOrderItemId(orderItem.getId()));
            Map<TwoGroup<Integer, Date>, FxOrderItemVisitorDetail> visitorDetailMap = visitorDetails.stream().collect(Collectors.toMap(FxOrderItemVisitorDetail::uniqueDate, Function.identity()));
            Set<Integer> vIds = visitorDetails.stream().map(FxOrderItemVisitorDetail::getVisitorId).collect(Collectors.toSet());
            boolean nonVisitorId = infos.stream().anyMatch(info -> Objects.isNull(info.getVisitorId()));
            if (nonVisitorId && vIds.size() > 1) {
                return Result.fail("缺少游客ID");
            }
            // 是否没有匹配的游客
            if (!nonVisitorId) {
                boolean nonMatch = infos.stream().map(TicketSendInfo::getVisitorId).noneMatch(vIds::contains);
                if (nonMatch) {
                    return Result.fail("游客信息错误");
                }
            }
            List<FxOrderItemDetail> details = orderItemDetailMapper.select(new FxOrderItemDetail().setOrderItemId(orderItem.getId()));
            Map<Date, FxOrderItemDetail> detailMap = details.stream().collect(Collectors.toMap(FxOrderItemDetail::getDay, Function.identity()));
            if (details.size() > 1) {
                boolean nonMatch = infos.stream().map(TicketSendInfo::getDay).noneMatch(detailMap::containsKey);
                if (nonMatch) {
                    return Result.fail("游客天的信息错误");
                }
            }
            orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem()
                    .setId(orderItem.getId())
                    .setSendMaTime(procTime)
                    .setStatus(Constant.OrderItemStatus.SEND));
            tickets = infos.stream().map(info -> {
                        Integer visitorId = Optional.ofNullable(info.getVisitorId()).orElse(CollUtil.getLast(vIds));
                        Date day = Optional.ofNullable(info.getDay()).orElse(details.get(0).getDay());
                        FxOrderItemVisitorDetail visitorDetail = visitorDetailMap.get(new TwoGroup<>(visitorId, day));
                        return new FxOrderItemTicket()
                                .setMaConfigId(orderItem.getMaConfigId())
                                .setOrderItemId(orderItem.getId())
                                .setProcessTime(procTime)
                                .setTicketId(info.getTicketId())
                                .setVoucherNumber(info.getVoucherNumber())
                                .setVoucherUrl(info.getVoucherUrl())
                                .setVisitorId(visitorDetail.getVisitorId())
                                .setVisitorDetailId(visitorDetail.getId())
                                .setDay(info.getDay());
                    }
            ).collect(Collectors.toList());
            ticketMapper.insert(tickets);
        } else {
            orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem().setId(orderItem.getId()).setStatus(Constant.OrderItemStatus.TICKET_FAIL));
        }
        applicationContext.publishEvent(new TicketSendEvent(orderItem, success, tickets));
        return Result.ok();
    }

    /**
     * 退票结果
     *
     * @param number   订单号(退订订单)
     * @param serialNo 第三方流水
     * @param success  是否成功
     * @param procTime 处理时间
     * @return
     */
    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @CheckResult
    public Result<?> refundResult(long number, String serialNo, boolean success, Date procTime) {
        FxRefundOrder refundOrder = refundOrderMapper.selectOne(new FxRefundOrder().setNumber(number));
        if (refundOrder == null) {
            return Result.fail("退订订单不存在");
        }
        FxRefundTicketSerial serial = refundTicketSerialMapper.selectOne(new FxRefundTicketSerial().setRefundOrderId(refundOrder.getId()));
        if (serial == null) {
            return Result.fail("该退订订单未发起退票");
        }
        if (StrUtil.isNotBlank(serial.getSerialNo()) || serial.getRefundTime() != null) {
            return Result.fail("重复退票结果");
        }
        refundTicketSerialMapper.updateByPrimaryKeySelective(serial.setSerialNo(serialNo).setRefundTime(procTime));
        orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem().setId(refundOrder.getOrderItemId()).setCurrentPreRefundQuantity(0));
        List<FxRefundVisitor> visitors = refundVisitorMapper.select(new FxRefundVisitor().setRefundOrderId(refundOrder.getId()));
        Map<Date, Integer> dayMap = visitors.stream().collect(Collectors.groupingBy(FxRefundVisitor::getDay, Collectors.summingInt(FxRefundVisitor::getQuantity)));
        if (success) {
            boolean check = refundManager.refundTicketSuccess(refundOrder, serial, visitors, dayMap);
            if (!check) {
                refundOrderMapper.updateByPrimaryKeySelective(new FxRefundOrder()
                        .setId(refundOrder.getId())
                        .setStatus(Constant.RefundOrderStatus.REFUND_TICKET_SUCCESS_NOT_ENOUGH)
                );
            }
        } else {
            refundOrderMapper.updateByPrimaryKeySelective(new FxRefundOrder().setId(refundOrder.getId()).setStatus(Constant.RefundOrderStatus.REFUND_TICKET_FAIL));
        }
        applicationContext.publishEvent(new TicketRefundEvent(refundOrder, serial, success, visitors, dayMap));
        return Result.ok();
    }

    /**
     * 核销通知
     *
     * @param maConfigId 凭证&票务配置ID
     * @param vo         核销信息
     * @return
     */
    @Locks("ORDER:CONSUME:#{#maConfigId}:#{#serialNo}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> consumeNotify(int maConfigId, VoucherConsumeVO vo) {
        FxOrderItemConsume consume = consumeMapper.selectOne(new FxOrderItemConsume().setMaConfigId(maConfigId).setSerialNo(vo.getSerialNo()));
        if (consume != null) {
            return Result.fail(Result.REPEAT_CONSUME, "重复核销");
        }
        FxOrderItemTicket ticket = ticketMapper.selectOne(new FxOrderItemTicket().setMaConfigId(maConfigId).setVoucherNumber(vo.getVoucherNumber()));
        if (ticket == null) {
            return Result.fail(Result.NOT_FOUND_VOUCHER_NUMBER, "凭证号不存在");
        }
        FxOrderItem orderItem = orderItemMapper.selectByPrimaryKey(ticket.getOrderItemId());
        if (orderItem == null) {
            return Result.fail(Result.NOT_FOUND_ORDER, "订单不存在");
        }
        int ret = visitorDetailMapper.usedQuantity(ticket.getVisitorId(), ticket.getDay(), vo.getQuantity());
        if (ret <= 0) {
            throw Result.fail(Result.QUANTITY_ENOUGH, "余票不足").exception();
        }
        ret = orderItemDetailMapper.usedQuantity(ticket.getOrderItemId(), ticket.getDay(), vo.getQuantity());
        if (ret <= 0) {
            throw Result.fail(Result.QUANTITY_ENOUGH, "余票不足").exception();
        }
        ret = orderItemMapper.usedQuantity(ticket.getOrderItemId(), vo.getQuantity());
        if (ret <= 0) {
            throw Result.fail(Result.QUANTITY_ENOUGH, "余票不足").exception();
        }
        FxOrderItemConsume record = new FxOrderItemConsume()
                .setSerialNo(vo.getSerialNo())
                .setMaConfigId(maConfigId)
                .setOrderItemId(ticket.getOrderItemId())
                .setConsumeTime(Optional.ofNullable(vo.getTime()).orElse(new Date()));
        consumeMapper.insertSelective(record);
        consumeDetailMapper.insertSelective(new FxOrderItemConsumeDetail()
                .setConsumeId(record.getId())
                .setDay(ticket.getDay())
                .setVoucherNumber(vo.getVoucherNumber())
                .setQuantity(vo.getQuantity())
        );
        applicationContext.publishEvent(new TicketConsumeEvent(orderItem, Collections.singletonMap(orderItem.getStartTime(), vo.getQuantity()), vo, record));
        return Result.ok();
    }

    @Locks("#{#vo.number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<Boolean> hotelConsume(HotelConsumeVO vo, AuthUser user) {
        FxOrderItem orderItem = orderItemMapper.selectOne(new FxOrderItem().setNumber(vo.getNumber()));
        Assert.notNull(orderItem, "订单不存在");
        if (!user.getMerchant().getId().equals(orderItem.getSupplierMerchantId())) {
            return Result.fail("该订单不属于当前商户");
        }

        int count = consumeMapper.selectCount(new FxOrderItemConsume().setOrderItemId(orderItem.getId()));
        if (count > 0) {
            return Result.fail("该订单已核销");
        }

        count = refundOrderMapper.selectByJoinExampleTransform(JoinExample.builder(FxRefundOrder.class)
                .count()
                .where(JoinExample.Where.custom()
                        .andEqualTo(FxRefundOrder::getOrderItemId, orderItem.getId())
                        .andNotIn(FxRefundOrder::getStatus, Stream.of(
                                Constant.RefundOrderStatus.AUDIT_REFUSE,
                                Constant.RefundOrderStatus.REFUND_TICKET_FAIL
                                ).collect(Collectors.toSet())
                        )
                )
                .build(), int.class);
        if (count > 0) {
            return Result.fail("该订单已发起退订");
        }
        Map<Date, List<VisitorQuantityVO>> map = vo.getVisitorMap();
        if (CollUtil.isEmpty(map)) {
            map = orderManager.autoAssemblingVisitorQuantity(orderItem.getId(), vo.getQuantity(), vo.getDay());
        } else {
            boolean check = map.values().stream().anyMatch(list -> list.stream().anyMatch(v -> v.getId() == null));
            if (check) {
                orderManager.autoAssemblingVisitorQuantity(orderItem.getId(), map);
            }
        }
        return hotelConsume(orderItem, map);
    }

    @Locks("#{#orderItem.number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<Boolean> hotelConsume(FxOrderItem orderItem, Map<Date, List<VisitorQuantityVO>> map) {
        String serialNo = String.valueOf(snowflake.nextId());
        List<FxOrderItemTicket> tickets = ticketMapper.select(new FxOrderItemTicket().setOrderItemId(orderItem.getId()).setMaConfigId(orderItem.getMaConfigId()));
        Map<ThreeGroup<Integer, Integer, Date>, FxOrderItemTicket> ticketMap = tickets.stream().collect(Collectors.toMap(FxOrderItemTicket::visitorItemDay, Function.identity()));
        FxOrderItemConsume record = new FxOrderItemConsume()
                .setSerialNo(serialNo)
                .setMaConfigId(orderItem.getMaConfigId())
                .setOrderItemId(orderItem.getId())
                .setConsumeTime(new Date());
        consumeMapper.insertSelective(record);
        int totalQuantity = 0;
        Map<Date, Integer> dayInfo = new HashMap<>(map.size());
        for (Map.Entry<Date, List<VisitorQuantityVO>> entry : map.entrySet()) {
            int dayQuantity = 0;
            for (VisitorQuantityVO vq : entry.getValue()) {
                int ret = visitorDetailMapper.usedQuantity(vq.getId(), entry.getKey(), vq.getQuantity());
                if (ret <= 0) {
                    throw Result.fail(Result.QUANTITY_ENOUGH, "余票不足").exception();
                }
                dayQuantity += vq.getQuantity();
                FxOrderItemTicket ticket = ticketMap.get(new ThreeGroup<>(vq.getId(), orderItem.getId(), entry.getKey()));
                consumeDetailMapper.insertSelective(new FxOrderItemConsumeDetail()
                        .setConsumeId(record.getId())
                        .setDay(ticket.getDay())
                        .setVoucherNumber(ticket.getVoucherNumber())
                        .setQuantity(vq.getQuantity())
                );
            }
            int ret = orderItemDetailMapper.usedQuantity(orderItem.getId(), entry.getKey(), dayQuantity);
            if (ret <= 0) {
                throw Result.fail(Result.QUANTITY_ENOUGH, "余票不足").exception();
            }
            totalQuantity += dayQuantity;
            dayInfo.put(entry.getKey(), dayQuantity);
        }
        int ret = orderItemMapper.usedQuantity(orderItem.getId(), totalQuantity);
        if (ret <= 0) {
            throw Result.fail(Result.QUANTITY_ENOUGH, "余票不足").exception();
        }
        applicationContext.publishEvent(new TicketConsumeEvent(orderItem, dayInfo, new VoucherConsumeVO()
                .setSerialNo(serialNo)
                .setQuantity(totalQuantity)
                .setTime(record.getConsumeTime()),
                record
        ));
        return Result.ok(totalQuantity >= orderItem.getQuantity() * orderItem.getDays());
    }
}
