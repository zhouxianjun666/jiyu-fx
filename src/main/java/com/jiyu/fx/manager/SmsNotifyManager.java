package com.jiyu.fx.manager;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.base.manager.SmsManager;
import com.jiyu.base.mapper.MerchantMapper;
import com.jiyu.base.mapper.SmsTemplateMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.SmsBusinessType;
import com.jiyu.common.dto.Config;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Member;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.common.entity.base.SmsTemplate;
import com.jiyu.common.util.Util;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.mapper.FxOrderItemMapper;
import com.jiyu.fx.mapper.FxOrderItemTicketMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/11 15:56
 */
@Component
@Slf4j
public class SmsNotifyManager {
    public static final Pattern PATTERN = Pattern.compile("\\$\\{\\w*}");
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private SmsTemplateMapper smsTemplateMapper;
    @Resource
    private FxOrderItemTicketMapper orderItemTicketMapper;
    @Resource
    private MerchantMapper merchantMapper;
    @Autowired
    private SmsManager smsManager;
    @Autowired
    private Config config;

    public FxOrderItem getInfo(Integer itemId, Long itemNumber) {
        return orderItemMapper.selectByJoinExampleTransform(JoinExample.builder(FxOrderItem.class)
                .addCol("fx_order_item.*")
                .addCol(FxOrder.class, "order")
                .addCol(Member.class, "shipping")
                .addTable(new JoinExample.Table(FxOrder.class, FxOrder::getId, FxOrderItem::getOrderId))
                .addTable(new JoinExample.Table(Member.class, Member::getId, FxOrder::getShippingMemberId))
                .where(JoinExample.Where.custom()
                        .andEqualTo(FxOrderItem::getId, itemId)
                        .orEqualTo(FxOrderItem::getNumber, itemNumber)
                )
                .build(), FxOrderItem.class
        );
    }

    /**
     * 退订申请(景点)
     *
     * @param orderItem 子订单
     * @return
     */
    public Result<?> refundApplyScenic(FxOrderItem orderItem) {
        return send(orderItem.getSupplierPhone(), SmsBusinessType.ORDER_REFUND_APPLY_SCENIC, orderItem, null, null, orderItem.getQuantity());
    }

    /**
     * 退订申请(酒店)
     *
     * @param orderItem 子订单
     * @return
     */
    public Result<?> refundApplyHotel(FxOrderItem orderItem) {
        return send(orderItem.getSupplierPhone(), SmsBusinessType.ORDER_REFUND_APPLY_HOTEL, orderItem, null, null, orderItem.getQuantity());
    }

    /**
     * 退订申请(线路)
     *
     * @param orderItem 子订单
     * @return
     */
    public Result<?> refundApplyLine(FxOrderItem orderItem) {
        return send(orderItem.getSupplierPhone(), SmsBusinessType.ORDER_REFUND_APPLY_LINE, orderItem, null, null, orderItem.getQuantity());
    }

    /**
     * 退订失败
     *
     * @param orderItem 子订单
     * @param reason    原因
     * @return
     */
    public Result<?> refundFail(FxOrderItem orderItem, FxRefundOrder refundOrder, String reason) {
        return send(item -> item.getShipping().getPhone(), SmsBusinessType.ORDER_REFUND_FAIL, orderItem,
                null, refundOrder, orderItem.getQuantity(), params -> {
                    params.put("reason", reason);
                    return params;
                });
    }

    /**
     * 退订成功
     * @param orderItem 子订单
     * @param refundOrder 退订订单
     * @return
     */
    public Result<?> refundSuccess(FxOrderItem orderItem, FxRefundOrder refundOrder) {
        return send(item -> item.getShipping().getPhone(), SmsBusinessType.ORDER_REFUND_SUCCESS, orderItem,
                null, refundOrder, orderItem.getQuantity(), params -> params);
    }

    /**
     * 支付后失败
     *
     * @param orderItem 子订单
     * @param reason    原因
     * @return
     */
    public Result<?> orderFail(FxOrderItem orderItem, String reason) {
        return send(item -> item.getShipping().getPhone(), SmsBusinessType.ORDER_FAIL, orderItem,
                null, null, orderItem.getQuantity(), params -> {
                    params.put("reason", reason);
                    return params;
                });
    }

    /**
     * 预定审核(景点)
     * @param orderItem 子订单
     * @return
     */
    public Result<?> orderAuditScenic(FxOrderItem orderItem) {
        return send(orderItem.getSupplierPhone(), SmsBusinessType.ORDER_AUDIT_SCENIC, orderItem, null, null, orderItem.getQuantity());
    }

    /**
     * 预定审核(酒店)
     * @param orderItem 子订单
     * @return
     */
    public Result<?> orderAuditHotel(FxOrderItem orderItem) {
        return send(orderItem.getSupplierPhone(), SmsBusinessType.ORDER_AUDIT_HOTEL, orderItem, null, null, orderItem.getQuantity());
    }

    /**
     * 预定审核(线路)
     * @param orderItem 子订单
     * @return
     */
    public Result<?> orderAuditLine(FxOrderItem orderItem) {
        return send(orderItem.getSupplierPhone(), SmsBusinessType.ORDER_AUDIT_LINE, orderItem, null, null, orderItem.getQuantity());
    }

    /**
     * 凭证短信
     * @param orderItem 子订单
     * @param phone 手机号
     * @return
     */
    public Result<?> voucher(FxOrderItem orderItem, String phone) {
        FxOrderItem item = getInfo(orderItem.getId(), null);
        List<FxOrderItemTicket> tickets = orderItemTicketMapper.selectByJoinExampleTransform(JoinExample.builder(FxOrderItemTicket.class)
                .addCol(FxOrderItemTicket.class)
                .addCol(FxOrderItemVisitorDetail.class, FxOrderItemTicket::getVisitorDetail)
                .addTable(new JoinExample.Table(FxOrderItemVisitorDetail.class, FxOrderItemVisitorDetail::getId, FxOrderItemTicket::getVisitorDetailId))
                .where(JoinExample.Where.custom().andEqualTo(FxOrderItemTicket::getOrderItemId, orderItem.getId()))
                .build(), map -> new FxOrderItemTicket()
        );
        if (CollUtil.isNotEmpty(tickets)) {
            SmsTemplate template = smsTemplateMapper.selectByPrimaryKey(orderItem.getSmsTemplateId());
            if (template == null) {
                return Result.fail("短信模板不存在");
            }
            Merchant merchant = merchantMapper.selectByPrimaryKey(item.getOrder().getBuyMerchantId());
            for (FxOrderItemTicket ticket : tickets) {
                try {
                    Map<String, String> param = parseParams(template.getContent(), item, ticket, null, ticket.getVisitorDetail().getQuantity());
                    Result<?> result = smsManager.send(template, merchant.getSignature(), param, StrUtil.blankToDefault(phone, item.getShipping().getPhone()));
                    if (!result.isSuccess()) {
                        throw result.exception();
                    }
                } catch (Exception e) {
                    log.warn("发送凭证信息失败: 订单号:{}, 凭证码:{}", item.getOrderNumber(), ticket.getVoucherNumber(), e);
                }
            }
        }
        return Result.ok();
    }

    /**
     * 凭证
     * @param orderItem 子订单
     * @return
     */
    public Result<?> voucher(FxOrderItem orderItem) {
        return voucher(orderItem, null);
    }

    /**
     * 凭证(没有票的:酒店/线路)
     * @param orderItem 子订单
     * @return
     */
    public Result<?> voucherNoTicket(FxOrderItem orderItem) {
        FxOrderItem item = getInfo(orderItem.getId(), null);
        SmsTemplate template = smsTemplateMapper.selectByPrimaryKey(orderItem.getSmsTemplateId());
        Merchant merchant = merchantMapper.selectByPrimaryKey(item.getOrder().getBuyMerchantId());
        Map<String, String> param = parseParams(template.getContent(), item, null, null, item.getQuantity());
        return smsManager.send(template, merchant.getSignature(), param, item.getShipping().getPhone());
    }

    public Result<?> send(Function<FxOrderItem, String> phone, int businessType, FxOrderItem orderItem,
                          FxOrderItemTicket ticket, FxRefundOrder refundOrder, int quantity, Function<Map<String, String>, Object> params) {
        FxOrderItem item = getInfo(orderItem.getId(), null);
        String target = phone.apply(item);
        if (StrUtil.isBlank(target)) {
            return Result.fail("目标手机号为空");
        }
        Merchant merchant = merchantMapper.selectByPrimaryKey(item.getOrder().getBuyMerchantId());
        List<SmsTemplate> templates = smsTemplateMapper.select(new SmsTemplate().setBusinessType(businessType));
        SmsTemplate template = CollUtil.getFirst(templates);
        if (template == null) {
            return Result.fail("短信模板不存在");
        }
        Map<String, String> param = parseParams(template.getContent(), item, ticket, refundOrder, quantity);
        Object p = Optional.ofNullable(params).map(pp -> pp.apply(param)).orElse(param);
        return smsManager.send(templates, merchant.getSignature(), p, target);
    }

    public Result<?> send(String phone, int businessType, FxOrderItem orderItem, FxOrderItemTicket ticket, FxRefundOrder refundOrder, int quantity) {
        return send(item -> phone, businessType, orderItem, ticket, refundOrder, quantity, null);
    }

    public void sendRefundApplyNotify(FxOrderItem orderItem) {
        Util.runNoException(() -> {
            Result result = Result.fail("暂不支持该产品类型");
            if (orderItem.getProductType() == Constant.ProductType.SCENIC) {
                result = refundApplyScenic(orderItem);
            } else if (orderItem.getProductType() == Constant.ProductType.HOTEL) {
                result = refundApplyHotel(orderItem);
            } else if (orderItem.getProductType() == Constant.ProductType.LINE) {
                result = refundApplyLine(orderItem);
            }
            if (!result.isSuccess()) {
                log.warn("发送申请退订审核通知失败: 订单号: {}, 原因:{}", orderItem.getOrderNumber(), result.getMsg());
            }
        });
    }

    public Map<String, String> parseParams(String content, FxOrderItem orderItem, FxOrderItemTicket ticket, FxRefundOrder refundOrder, int quantity) {
        Map<String, String> map = new HashMap<>(1);
        Matcher matcher = PATTERN.matcher(content);
        while (matcher.find()) {
            String key = matcher.group();
            String val = key.substring(key.indexOf("${") + 2, key.length() - 1);
            try {
                switch (key) {
                    case "${product}":
                        map.put(val, orderItem.getProductName() + "-" + orderItem.getProductSubName());
                        break;
                    case "${quantity}":
                        map.put(val, String.valueOf(quantity));
                        break;
                    case "${days}":
                        map.put(val, String.valueOf(orderItem.getDays()));
                        break;
                    case "${amount}":
                        if (orderItem.getOrder() != null) {
                            map.put(val, String.valueOf(orderItem.getOrder().getPayAmount() / 100.0));
                        }
                        break;
                    case "${booking}":
                        map.put(val, DateUtil.formatDate(orderItem.getStartTime()));
                        break;
                    case "${number}":
                        if (orderItem.getOrder() != null) {
                            map.put(val, String.valueOf(orderItem.getOrder().getNumber()));
                        }
                        break;
                    case "${create}":
                        map.put(val, DateUtil.formatDateTime(Optional.ofNullable(orderItem.getOrder()).map(FxOrder::getCreateTime).orElse(orderItem.getCreateTime())));
                        break;
                    case "${shipping}":
                        if (orderItem.getShipping() != null) {
                            map.put(val, orderItem.getShipping().getNickname());
                        }
                        break;
                    case "${checkIn}":
                        map.put(val, DateUtil.formatDateTime(orderItem.getStartTime()));
                        break;
                    case "${checkOut}":
                        map.put(val, DateUtil.formatDateTime(orderItem.getEndTime()));
                        break;
                    case "${voucher}":
                        if (ticket != null) {
                            map.put(val, ticket.getVoucherNumber());
                        }
                        break;
                    case "${qr}":
                        if (ticket != null) {
                            map.put(val, ticket.getVoucherUrl().replace("http://q.jiyuwenhua.cn/", ""));
                        }
                        break;
                    case "${expiry}":
                        map.put(val, DateUtil.formatDateTime(orderItem.getMaxExpiryDate()));
                        break;
                    case "${buyEp}":
                        if (orderItem.getOrder() != null) {
                            map.put(val, orderItem.getOrder().getBuyMerchantName());
                        }
                        break;
                    case "${payTimeOut}":
                        Date start = Optional.ofNullable(orderItem.getOrder()).map(FxOrder::getCreateTime).orElse(orderItem.getCreateTime());
                        map.put(val, DateUtil.formatDateTime(DateUtil.offsetMinute(start, config.getOrderPayTimeout())));
                        break;
                    case "${payTimeoutMinute}":
                        map.put(val, String.valueOf(config.getOrderPayTimeout()));
                        break;
                    case "${auditTimeoutMinute}":
                        map.put(val, String.valueOf(config.getOrderAuditTimeout()));
                        break;
                    case "${supplyPhone}":
                        map.put(val, orderItem.getSupplierPhone());
                        break;
                    case "${supplyMerchant}":
                        map.put(val, orderItem.getSupplierMerchantName());
                        break;
                    case "${shippingPhone}":
                        if (orderItem.getShipping() != null) {
                            map.put(val, orderItem.getShipping().getPhone());
                        }
                        break;
                    case "${itemNumber}":
                        map.put(val, String.valueOf(orderItem.getNumber()));
                        break;
                    case "${refundNumber}":
                        if (refundOrder != null) {
                            map.put(val, String.valueOf(refundOrder.getNumber()));
                        }
                        break;
                    case "${refundQuantity}":
                        if (refundOrder != null) {
                            map.put(val, String.valueOf(refundOrder.getQuantity()));
                        }
                        break;
                    case "${refundMoney}":
                        if (refundOrder != null) {
                            map.put(val, String.valueOf(refundOrder.getMoney() / 100.0));
                        }
                        break;
                    case "${refundFee}":
                        if (refundOrder != null) {
                            map.put(val, String.valueOf(refundOrder.getFee() / 100.0));
                        }
                        break;
                    case "${refundTime}":
                        if (refundOrder != null) {
                            map.put(val, DateUtil.formatDateTime(refundOrder.getCreateTime()));
                        }
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                log.warn("发送消息解析参数:{}失败", key, e);
                throw e;
            }
        }
        return map;
    }
}
