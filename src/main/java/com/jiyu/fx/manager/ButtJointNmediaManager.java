package com.jiyu.fx.manager;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONUtil;
import com.jiyu.base.manager.JiYuRequestManager;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Util;
import com.jiyu.fx.config.NMediaApiConfig;
import com.jiyu.fx.entity.FxOrderCreditRecord;
import com.jiyu.fx.mapper.FxOrderCreditRecordMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zoujing
 * @date 2019/2/12 14:50
 */
@Component
@Slf4j
public class ButtJointNmediaManager {
    @Autowired
    private NMediaApiConfig apiConfig;

    @Autowired
    private JiYuRequestManager jiYuRequestManager;

    @Resource
    private FxOrderCreditRecordMapper fxOrderCreditRecordMapper;

    private String success = "success";

    /**
     * 通知新媒体平台消费积分
     * @param code 子产品code
     * @param merchantId 消费分发商的商户Id
     * @param orderItemId 订单号
     */
    @CheckResult
    public Result<?> creditConsume (Long code, Integer merchantId, Long orderItemId, Integer quantity) {
        Map<String, Object> params = new HashMap<>(5);
        params.put("outId", code);
        params.put("outSource", 1);
        params.put("merchantId", merchantId);
        params.put("orderId", orderItemId);
        params.put("quantity", quantity);
        FxOrderCreditRecord fxOrderCreditRecord = new FxOrderCreditRecord().setOrderItemNumber(orderItemId).setProductSubCode(code).setType(Constant.NewMediaType.CONSUME);
        return handleRequest(fxOrderCreditRecord, "/api/nmedia/credit/creditConsume", params, HttpMethod.POST);
    }

    /**
     * 通知新媒体平台积分消费取消
     * @param orderItemId 订单号
     */
    @CheckResult
    public Result<?> creditCancel (Long orderItemId) {
        FxOrderCreditRecord fxOrderCreditRecordRes = fxOrderCreditRecordMapper.selectOne(new FxOrderCreditRecord().setOrderItemNumber(orderItemId).setType(Constant.NewMediaType.CONSUME));
        if (fxOrderCreditRecordRes != null) {
            Map map = JSONUtil.parseObj(fxOrderCreditRecordRes.getResult());
            if (MapUtils.getBooleanValue(map, success)) {
                FxOrderCreditRecord fxOrderCreditRecord = new FxOrderCreditRecord().setOrderItemNumber(orderItemId).setType(Constant.NewMediaType.CANCEL);
                return handleRequest(fxOrderCreditRecord, "/api/nmedia/credit/cancel/" + orderItemId, null, HttpMethod.POST);
            }
        }
        return Result.ok();
    }

    /**
     * 通知新媒体平台积分生效
     * @param orderItemId 订单号
     */
    @CheckResult
    public Result<?> creditConfirm (Long orderItemId) {
        FxOrderCreditRecord fxOrderCreditRecordRes = fxOrderCreditRecordMapper.selectOne(new FxOrderCreditRecord().setOrderItemNumber(orderItemId).setType(Constant.NewMediaType.CONSUME));
        if (fxOrderCreditRecordRes != null) {
            Map map = JSONUtil.parseObj(fxOrderCreditRecordRes.getResult());
            if (MapUtils.getBooleanValue(map, success)) {
                FxOrderCreditRecord fxOrderCreditRecord = new FxOrderCreditRecord().setOrderItemNumber(orderItemId).setType(Constant.NewMediaType.CONFIRM);
                return handleRequest(fxOrderCreditRecord, "/api/nmedia/credit/confirm/" + orderItemId, null, HttpMethod.POST);
            }
        }
        return Result.ok();
    }

    private Result<?> handleRequest (FxOrderCreditRecord fxOrderCreditRecord, String url, Map<String, Object> params, HttpMethod method) {
        try {
            url = apiConfig.getDomain() + url;
            Result result =  jiYuRequestManager.request(apiConfig.getAccessKey(), apiConfig.getAccessSecret(), url, params, method, null);
            fxOrderCreditRecord.setResult(Util.cutStr(JSONUtil.toJsonStr(result), 1000));
            ((ButtJointNmediaManager) AopContext.currentProxy()).creditRecord(fxOrderCreditRecord);
            if (!result.isSuccess() && fxOrderCreditRecord.getType().equals(Constant.NewMediaType.CONSUME)) {
                log.warn("请求新媒体平台积分操作失败，url：{}, msg: {}", url, result.getMsg());
                return Result.fail(Result.NEW_MEDIA_FAIL,"请求新媒体平台积分操作失败:" + result.getMsg());
            } else {
                log.info("请求新媒体平台积分操作成功，url：{}", url);
                return Result.ok();
            }
        } catch (Exception e) {
            log.error("请求新媒体平台错误，url：{}", url , e);
            fxOrderCreditRecord.setException(Util.cutStr(ExceptionUtil.stacktraceToString(e, 3000), 3000));
            ((ButtJointNmediaManager) AopContext.currentProxy()).creditRecord(fxOrderCreditRecord);
            return fxOrderCreditRecord.getType().equals(Constant.NewMediaType.CONSUME) ? Result.fail(Result.NEW_MEDIA_FAIL,"请求新媒体平台积分操作失败"  + e.getMessage()) : Result.ok();
        }
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void creditRecord (FxOrderCreditRecord fxOrderCreditRecord) {
        fxOrderCreditRecordMapper.insertSelective(fxOrderCreditRecord);
    }
}
