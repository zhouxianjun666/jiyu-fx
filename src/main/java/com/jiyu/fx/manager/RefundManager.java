package com.jiyu.fx.manager;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.base.manager.MerchantPaymentConfigManager;
import com.jiyu.base.manager.WxManager;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.CheckResult;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.*;
import com.jiyu.fx.adapter.order.RefundOrder;
import com.jiyu.fx.dto.DayRefundTemp;
import com.jiyu.fx.dto.DetailDiscountDTO;
import com.jiyu.fx.dto.RefundTicketInfo;
import com.jiyu.fx.dto.RefundTicketVisitorInfo;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.event.bo.RefundAuditEvent;
import com.jiyu.fx.event.bo.RefundMoneyEvent;
import com.jiyu.fx.event.bo.RefundOrderEvent;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.vo.RefundVO;
import com.jiyu.fx.vo.VisitorQuantityVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/22 14:50
 */
@Component
@Slf4j
public class RefundManager {
    private Map<String, RefundOrder> refundOrderMap = Collections.emptyMap();
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private FxMerchantAccountManager epAccountManager;
    @Autowired
    private TicketManager ticketManager;
    @Autowired
    private MerchantPaymentConfigManager epPaymentConfigManager;
    @Autowired
    private WxManager wxManager;
    @Autowired
    private VoucherManager voucherManager;
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private FxOrderItemDetailMapper detailMapper;
    @Resource
    private FxOrderItemVisitorDetailMapper visitorDetailMapper;
    @Resource
    private FxRefundOrderMapper refundOrderMapper;
    @Resource
    private FxRefundVisitorMapper refundVisitorMapper;
    @Resource
    private FxRefundTicketSerialMapper refundTicketSerialMapper;
    @Resource
    private FxRefundMoneySerialMapper refundMoneySerialMapper;

    @PostConstruct
    public void init() {
        refundOrderMap = applicationContext.getBeansOfType(RefundOrder.class).values().stream()
                .collect(Collectors.toMap(RefundOrder::type, Function.identity()));
    }

    /**
     * 退订申请
     *
     * @param vo   退订信息
     * @param type 类型
     * @param user 当前用户
     * @return
     */
    @Locks("#{#vo.number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> apply(RefundVO vo, String type, AuthUser user) {
        if (!refundOrderMap.containsKey(type)) {
            return Result.fail("暂不支持该订单类型退订");
        }
        RefundOrder refundOrderMgr = refundOrderMap.get(type);
        Result<FxOrder> orderResult = refundOrderMgr.validate(vo.setRefundTime(new Date()), user);
        FxOrder order = orderResult.getValue();
        Result<FxOrderItem> orderItemResult = refundOrderMgr.checkOrderItem(vo.addResult(orderResult), order, user);
        FxOrderItem orderItem = orderItemResult.getValue();
        Result<Boolean> existResult = refundOrderMgr.isExist(vo.addResult(orderItemResult), orderItem, user);
        if (existResult.getValue()) {
            return existResult;
        }
        Result<?> beforeResult = existResult;
        // 解析每日游客退订数量
        Result<Map<Date, List<VisitorQuantityVO>>> dayRefundResult = refundOrderMgr.parseDayRefund(vo.addResult(existResult), orderItem, user, beforeResult);
        beforeResult = dayRefundResult;
        Map<Date, List<VisitorQuantityVO>> dayRefundMap = dayRefundResult.getValue();
        boolean checkVisitor = dayRefundMap.values().stream().flatMap(Collection::stream).anyMatch(v ->
                Objects.isNull(v.getId()) || v.getId() <= 0 ||
                        Objects.isNull(v.getQuantity()) || v.getQuantity() <= 0);
        if (checkVisitor) {
            return Result.fail("游客信息错误");
        }
        vo.setVisitorMap(dayRefundMap);
        List<DetailDiscountDTO> details = detailMapper.selectByJoinExampleTransform(JoinExample.builder(FxOrderItemDetail.class)
                .all(FxOrderItemDetail.class)
                .addCol(FxOrderItemDetailCredit.class, DetailDiscountDTO::getCredit)
                .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getId, FxOrderItemDetail::getOrderItemId))
                .addTable(new JoinExample.Table(FxOrderItemDetailCredit.class, JoinExample.JoinType.LEFT, FxOrderItemDetailCredit::getDay, FxOrderItemDetail::getDay).and(FxOrderItemDetailCredit::getItemNumber, FxOrderItem::getNumber))
                .where(JoinExample.Where.custom()
                        .andEqualTo(FxOrderItemDetail::getOrderItemId, orderItem.getId())
                )
                .build(), map -> new DetailDiscountDTO());
        Map<Date, DetailDiscountDTO> detailMap = details.stream().collect(Collectors.toMap(DetailDiscountDTO::getDay, Function.identity()));
        // 解析每日退货规则
        Result<Map<Date, RefundRule<AbstractRefundRuleDetail>>> dayRefundRuleResult = refundOrderMgr.parseDayRefundRule(vo.addResult(dayRefundResult), orderItem, detailMap, user, beforeResult);
        beforeResult = dayRefundRuleResult;
        Map<Date, RefundRule<AbstractRefundRuleDetail>> dayRefundRuleMap = dayRefundRuleResult.getValue();
        // 判断是否可退
        Result<Boolean> canRefundResult = refundOrderMgr.canRefund(vo.addResult(dayRefundRuleResult), orderItem, detailMap, dayRefundRuleMap, user, beforeResult);
        if (!canRefundResult.getValue()) {
            return Result.fail("该订单不允许退订", canRefundResult);
        }
        beforeResult = canRefundResult;
        List<FxOrderItemVisitorDetail> visitors = visitorDetailMapper.select(new FxOrderItemVisitorDetail().setOrderItemId(orderItem.getId()));
        // 每个游客总退票数量
        Map<Integer, Integer> visitorQuantityMap = dayRefundMap.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(VisitorQuantityVO::getId, Collectors.summingInt(VisitorQuantityVO::getQuantity)));
        // 验证游客退订信息
        Result<List<FxRefundVisitor>> visitorResult = refundOrderMgr.validateVisitor(vo.addResult(canRefundResult), orderItem, visitors, detailMap, visitorQuantityMap, user, beforeResult);
        List<FxRefundVisitor> refundVisitors = visitorResult.getValue();
        // 每天总退票数量
        Map<Date, Integer> dayQuantityMap = dayRefundMap.entrySet().stream().collect(
                Collectors.groupingBy(
                        Map.Entry::getKey,
                        Collectors.summingInt(m -> (Integer) m.getValue().stream().mapToInt(VisitorQuantityVO::getQuantity).sum())
                )
        );
        Result<Map<Date, DayRefundTemp>> tempResult = refundOrderMgr.getDayRefundTempMap(vo.addResult(visitorResult), orderItem, dayQuantityMap, detailMap, dayRefundRuleMap, order.getBuyMerchantId());
        beforeResult = tempResult;
        Map<Date, DayRefundTemp> tempMap = tempResult.getValue();
        Result<int[]> moneyAndFeeResult = refundOrderMgr.calcRefundMoneyAndFee(vo.addResult(tempResult), orderItem, order.getBuyMerchantId(), refundVisitors, tempMap, user, beforeResult);
        int money = moneyAndFeeResult.getValue()[0];
        int fee = moneyAndFeeResult.getValue()[1];
        int totalQuantity = dayQuantityMap.values().stream().mapToInt(m -> m).sum();
        Result<FxRefundOrder> refundOrderResult = refundOrderMgr.makeRefundOrder(vo.addResult(moneyAndFeeResult), orderItem, fee, money, totalQuantity);
        FxRefundOrder refundOrder = refundOrderResult.getValue();

        // 入库
        refundOrderMgr.save(vo.addResult(refundOrderResult), orderItem, visitorResult, refundOrder);
        // 校验手续费与退款金额
        refundOrderMgr.checkFeeAndMoney(vo, refundOrder);
        Result<?> res = refundOrderMgr.after(vo, refundOrder);
        applicationContext.publishEvent(new RefundOrderEvent(vo, type, refundOrder, order, orderItem, user));
        return res;
    }

    /**
     * 退订审核
     *
     * @param number  退订订单编号
     * @param success 审核结果
     * @param user    当前用户
     * @return
     */
    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> audit(long number, boolean success, AuthUser user) {
        FxRefundOrder refundOrder = refundOrderMapper.selectOne(new FxRefundOrder().setNumber(number));
        if (refundOrder == null) {
            return Result.fail("退订订单不存在");
        }
        if (refundOrder.getStatus() != Constant.RefundOrderStatus.AUDIT_WAIT) {
            return Result.fail("退订订单已审核");
        }
        FxOrderItem orderItem = orderItemMapper.selectByPrimaryKey(refundOrder.getOrderItemId());
        if (!Objects.equals(orderItem.getSupplierMerchantId(), user.getMerchant().getId())) {
            return Result.fail("当前企业不能审核退订");
        }
        if (refundOrder.getMoney() > orderItem.getSettlePrice()) {
            return Result.fail("退款金额不能大于订单支付金额");
        }
        if (refundOrder.getMoney() < 0) {
            return Result.fail("退款金额不能小于0");
        }
        refundOrderMapper.updateByPrimaryKeySelective(new FxRefundOrder()
                .setId(refundOrder.getId())
                .setAuditTime(new Date())
                .setAuditUserId(user.getId())
                .setAuditUserName(user.getFullName())
                .setStatus(success ? Constant.RefundOrderStatus.REFUNDING : Constant.RefundOrderStatus.AUDIT_REFUSE)
        );
        if (!success) {
            orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem().setId(refundOrder.getOrderItemId()).setCurrentPreRefundQuantity(0));
        }
        applicationContext.publishEvent(new RefundAuditEvent(refundOrder, orderItem, success));
        return Result.ok();
    }

    /**
     * 发起退票
     *
     * @param number 退订订单编号
     * @return
     */
    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> refundTicket(long number) {
        FxRefundOrder refundOrder = refundOrderMapper.selectOne(new FxRefundOrder().setNumber(number));
        if (refundOrder == null) {
            return Result.fail("退订订单不存在");
        }
        if (refundOrder.getStatus() != Constant.RefundOrderStatus.REFUNDING) {
            return Result.fail("退订订单状态不能发起退票");
        }
        FxRefundTicketSerial serial = refundTicketSerialMapper.selectOne(new FxRefundTicketSerial().setRefundOrderId(refundOrder.getId()));
        if (serial != null) {
            return Result.fail("该退订订单已发起退票");
        }
        FxOrderItem orderItem = orderItemMapper.selectByPrimaryKey(refundOrder.getOrderItemId());
        serial = new FxRefundTicketSerial()
                .setCreateTime(new Date())
                .setRefundOrderId(refundOrder.getId())
                .setQuantity(refundOrder.getQuantity())
                .setMaConfigId(orderItem.getMaConfigId());
        refundTicketSerialMapper.insertSelective(serial);
        // 如果未出票或者酒店、线路则直接退票成功
        if (orderItem.getStatus() != Constant.OrderItemStatus.SEND ||
                Stream.of(Constant.ProductType.HOTEL, Constant.ProductType.LINE)
                        .anyMatch(type -> Objects.equals(type, orderItem.getProductType()))) {
            return ticketManager.refundResult(number, String.valueOf(number), true, new Date());
        }
        List<FxRefundVisitor> refundVisitors = refundVisitorMapper.selectByJoinExampleTransform(JoinExample.builder(FxRefundVisitor.class)
                .all(FxRefundVisitor.class)
                .addCol(FxOrderItemTicket.class, FxRefundVisitor::getTicket)
                .addTable(new JoinExample.Table(FxOrderItemTicket.class, FxOrderItemTicket::getVisitorDetailId, FxRefundVisitor::getVisitorDetailId))
                .where(JoinExample.Where.custom()
                        .andEqualTo(FxRefundVisitor::getRefundOrderId, refundOrder.getId())
                        .andEqualTo(FxOrderItemTicket::getOrderItemId, orderItem.getId())
                )
                .build(), map -> new FxRefundVisitor()
        );
        Result<?> result = voucherManager.refund(orderItem.getMaConfigId(), new RefundTicketInfo()
                .setNumber(orderItem.getNumber())
                .setSerialNo(String.valueOf(refundOrder.getNumber()))
                .setApplyTime(refundOrder.getCreateTime())
                .setReason(refundOrder.getCause())
                .setVisitors(refundVisitors.stream()
                        .map(t -> new RefundTicketVisitorInfo()
                                .setId(t.getVisitorId())
                                .setTicketId(t.getTicket().getTicketId())
                                .setVoucherNumber(t.getTicket().getVoucherNumber())
                                .setQuantity(t.getQuantity())
                        )
                        .collect(Collectors.toList())
                )
        );
        if (!result.isSuccess()) {
            throw result.exception();
        }
        return Result.ok();
    }

    /**
     * 退票成功(该函数尽量不要抛异常)
     *
     * @param refundOrder 退订订单
     * @param serial      退订流水
     */
    @Locks("#{#refundOrder.number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public boolean refundTicketSuccess(FxRefundOrder refundOrder, FxRefundTicketSerial serial, List<FxRefundVisitor> visitors, Map<Date, Integer> dayMap) {
        int ret = orderItemMapper.changeRefundQuantity(refundOrder.getOrderItemId(), refundOrder.getQuantity());
        if (ret <= 0) {
            log.warn("退订订单:{},退票流水:{} 退票回调异常: 可退票不足", refundOrder.getNumber(), serial.getSerialNo());
            return false;
        }
        for (FxRefundVisitor visitor : visitors) {
            int vRet = visitorDetailMapper.refundQuantity(visitor.getVisitorId(), visitor.getDay(), visitor.getQuantity());
            if (vRet <= 0) {
                log.warn("退订订单:{},退票流水:{} 退票回调异常: 游客:{} 可退票不足", refundOrder.getNumber(), serial.getSerialNo(), visitor.getVisitorId());
                return false;
            }
        }
        for (Map.Entry<Date, Integer> entry : dayMap.entrySet()) {
            int quantity = entry.getValue();
            int dRet = detailMapper.refundQuantity(refundOrder.getOrderItemId(), entry.getKey(), quantity);
            if (dRet <= 0) {
                log.warn("退订订单:{},退票流水:{} 退票回调异常: 日期:{} 可退票不足", refundOrder.getNumber(), serial.getSerialNo(), DateUtil.formatDateTime(entry.getKey()));
                return false;
            }
        }
        refundOrderMapper.updateByPrimaryKeySelective(new FxRefundOrder().setId(refundOrder.getId()).setStatus(Constant.RefundOrderStatus.REFUND_MONEY));
        return true;
    }

    /**
     * 发起退款
     *
     * @param number 退订订单编号
     * @return
     */
    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @CheckResult
    public Result<?> refundMoney(long number) {
        FxRefundOrder refundOrder = refundOrderMapper.selectOne(new FxRefundOrder().setNumber(number));
        if (refundOrder == null) {
            return Result.fail("退订订单不存在");
        }
        if (refundOrder.getStatus() != Constant.RefundOrderStatus.REFUND_MONEY) {
            return Result.fail("退订订单状态不能发起退款");
        }
        FxRefundMoneySerial serial = refundMoneySerialMapper.selectOne(new FxRefundMoneySerial().setRefundOrderId(refundOrder.getId()));
        if (serial != null) {
            return Result.fail("该退订订单已发起退款");
        }
        FxOrder order = orderManager.getOrderByItem(refundOrder.getOrderItemId());
        if (order == null) {
            return Result.fail("订单不存在");
        }
        serial = new FxRefundMoneySerial()
                .setCreateTime(new Date())
                .setRefundOrderId(refundOrder.getId())
                .setMoney(refundOrder.getMoney())
                .setType(order.getPaymentType());
        refundMoneySerialMapper.insertSelective(serial);
        Result<?> result = refundMoney(order, refundOrder.getMoney(), String.valueOf(refundOrder.getNumber()), null, null);
        // 如果退款金额为0或者为余额方式则直接自动调用退款成功
        boolean isBalanceAndSuccess = result.isSuccess() && order.getPaymentType() == Constant.PaymentType.BALANCE;
        if (refundOrder.getMoney() == 0 || isBalanceAndSuccess || order.getPaymentType() == Constant.PaymentType.LOCAL_PAY) {
            return refundMoneySuccess(number, String.valueOf(refundOrder.getNumber()));
        }
        return result;
    }

    /**
     * 退款
     *
     * @param order           订单
     * @param money           退款金额
     * @param serialNo        流水号
     * @param accountBillType 账户流水类型
     * @param remark          备注
     * @return
     */
    public Result<?> refundMoney(FxOrder order, int money, String serialNo, Integer accountBillType, String remark) {
        if (money > order.getPayAmount()) {
            return Result.fail("退款金额不能大于订单支付金额");
        }
        if (money < 0) {
            return Result.fail("退款金额不能小于0");
        }
        // 0元第三方退款
        if (money == 0 && order.getPaymentType() != Constant.PaymentType.BALANCE) {
            return Result.ok();
        }
        // 现场到付
        if (order.getPaymentType() == Constant.PaymentType.LOCAL_PAY) {
            return Result.ok();
        }
        remark = StrUtil.blankToDefault(remark, "订单退订退款");
        accountBillType = Optional.ofNullable(accountBillType).orElse(Constant.AccountBillsType.ORDER_REFUND);
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderNumber(order.getNumber()));
        Integer supplierMerchantId = items.get(0).getSupplierMerchantId();
        if (order.getPaymentType() == Constant.PaymentType.BALANCE) {
            epAccountManager.change(Collections.singletonList(new AccountChangeInfo()
                    .setBalance(money)
                    .setCash(money)
                    .setType(accountBillType)
                    .setRemark(remark)
                    .setBossMerchantId(supplierMerchantId)
                    .setMerchantId(order.getBuyMerchantId())
                    .setNumber(serialNo)
                    .setOrderNumber(order.getNumber())
            ));
            return Result.ok();
        } else if (order.getPaymentType() == Constant.PaymentType.WX) {
            WxPaymentConfig wxPaymentConfig = epPaymentConfigManager.get(supplierMerchantId,  Constant.PaymentConfigType.WX);
            Assert.notNull(wxPaymentConfig, "微信收款方式未配置");
            return wxManager.refund(new WxRefundOrder()
                            .setOrderNumber(String.valueOf(order.getNumber()))
                            .setNumber(serialNo)
                            .setMoney(money)
                            .setPayAmount(order.getPayAmount())
                    , wxPaymentConfig.toCfg()
            );
        } else {
            return Result.fail("暂不支持");
        }
    }

    /**
     * 退款成功
     *
     * @param number   退订订单编号
     * @param serialNo 退订流水号
     * @return
     */
    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> refundMoneySuccess(long number, String serialNo) {
        FxRefundOrder refundOrder = refundOrderMapper.selectOne(new FxRefundOrder().setNumber(number));
        if (refundOrder == null) {
            return Result.fail("退订订单不存在");
        }
        FxRefundMoneySerial serial = refundMoneySerialMapper.selectOne(new FxRefundMoneySerial().setRefundOrderId(refundOrder.getId()));
        if (serial == null) {
            return Result.fail("该退订订单未发起退款");
        }
        refundMoneySerialMapper.updateByPrimaryKeySelective(serial.setSerialNo(serialNo).setRefundTime(new Date()));
        refundOrderMapper.updateByPrimaryKeySelective(new FxRefundOrder().setId(refundOrder.getId()).setStatus(Constant.RefundOrderStatus.COMPLETE));
        FxOrderItem orderItem = orderItemMapper.selectByPrimaryKey(refundOrder.getOrderItemId());
        applicationContext.publishEvent(new RefundMoneyEvent(refundOrder, serial, orderItem));
        return Result.ok();
    }

    /**
     * 查询退款状态
     *
     * @param refundOrder 退订订单
     * @return
     */
    public Result<?> queryRefundMoneyStatus(FxRefundOrder refundOrder) {
        FxOrder order = orderManager.getOrderByItem(refundOrder.getOrderItemId());
        int paymentType = Optional.ofNullable(order.getPaymentType()).orElse(Constant.PaymentType.BALANCE);
        if (paymentType == Constant.PaymentType.WX) {
            return queryRefundMoneyStatusByWx(refundOrder, order);
        }
        return Result.ok().put("status", 0);
    }

    /**
     * 处理退款状态
     *
     * @param refundOrder 退订订单
     * @return
     */
    public boolean processRefundMoneyStatus(FxRefundOrder refundOrder) {
        if (refundOrder.getStatus() == Constant.RefundOrderStatus.REFUND_MONEY) {
            log.info("退订订单: {} 退款中,主动查询退款状态", refundOrder.getNumber());
            Result<?> queryResult = queryRefundMoneyStatus(refundOrder);
            int status = queryResult.get("status");
            if (status == 0) {
                throw Result.fail(StrUtil.format("查询退订订单:{}退款状态异常", refundOrder.getNumber())).exception();
            }
            if (status == Constant.RefundOrderStatus.REFUND_MONEY) {
                throw Result.fail(StrUtil.format("该退订订单:{}正在退款中", refundOrder.getNumber())).exception();
            }
            if (status == Constant.RefundOrderStatus.COMPLETE) {
                refundMoneySuccess(refundOrder.getNumber(), queryResult.get("refund_id"));
                return true;
            }
        }
        return false;
    }

    public static double[] calcRefundMoneyAndFee(double percent, int price, int quantity) {
        // 退余额(当天价格 * 张数 * (1 - 手续费百分比)
        double m = NumberUtil.round(NumberUtil.mul(price * quantity, 1 - percent), 0).doubleValue();
        // 手续费(当天价格 * 张数 * 手续费百分比)
        double f = NumberUtil.round(NumberUtil.mul(price * quantity, percent), 0).doubleValue();
        return new double[]{ m, f };
    }

    private Result<?> queryRefundMoneyStatusByWx(FxRefundOrder refundOrder, FxOrder order) {
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderNumber(order.getNumber()));
        WxPaymentConfig paymentConfig = epPaymentConfigManager.get(items.get(0).getSupplierMerchantId(),  Constant.PaymentConfigType.WX);
        Assert.notNull(paymentConfig, "微信收款方式未配置");
        Result<TreeMap<String, Object>> result = wxManager.queryRefund(new WxRefundOrder()
                .setOrderNumber(String.valueOf(order.getNumber()))
                .setNumber(String.valueOf(refundOrder.getNumber())), paymentConfig.toCfg(), WxManager.SUCCESS);
        if (!result.isSuccess()) {
            return result;
        }
        TreeMap<String, Object> map = result.getValue();
        String tradeState = MapUtils.getString(map, "refund_status_0", "NOTPAY");
        switch (tradeState) {
            case "SUCCESS":
                return Result.ok(map).put("refund_id", map.get("refund_id_0")).put("status", Constant.RefundOrderStatus.COMPLETE);
            case "PROCESSING":
            case "CHANGE":
                return Result.ok().put("status", Constant.RefundOrderStatus.REFUND_MONEY);
            default:
                return Result.ok().put("status", 0);
        }
    }
}
