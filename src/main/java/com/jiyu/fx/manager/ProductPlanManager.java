package com.jiyu.fx.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.*;
import com.jiyu.fx.dto.ChangeStock;
import com.jiyu.fx.entity.FxProductChain;
import com.jiyu.fx.entity.FxProductPlan;
import com.jiyu.fx.entity.FxProductSub;
import com.jiyu.fx.entity.FxRefundRule;
import com.jiyu.fx.mapper.FxProductChainMapper;
import com.jiyu.fx.mapper.FxProductPlanMapper;
import com.jiyu.fx.mapper.FxProductSubMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/16 14:47
 */
@Component
public class ProductPlanManager {
    @Autowired
    private ProductSalesManager productSalesManager;
    @Resource
    private FxProductPlanMapper productPlanMapper;
    @Resource
    private FxProductSubMapper fxProductSubMapper;
    @Resource
    private FxProductChainMapper productChainMapper;

    /**
     * 变更库存(递增递减)
     * @param changeStocks 信息
     * @return 子产品编号->(开始时间->是否超卖)
     */
    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = {Exception.class, RuntimeException.class})
    public Map<Long, Map<Date, Boolean>> changeStock(List<ChangeStock> changeStocks) {
        List<FxProductSub> subs = fxProductSubMapper.selectByJoinExampleEntity(JoinExample.builder(FxProductSub.class)
                .where(JoinExample.Where.custom()
                        .andIn(FxProductSub::getCode, changeStocks.stream().map(ChangeStock::getCode).collect(Collectors.toSet()))
                ).build()
        );
        Assert.notEmpty(subs, "子产品不存在");
        Map<Long, Map<Date, Boolean>> result = new HashMap<>(changeStocks.size());
        Map<Long, FxProductSub> map = subs.stream().collect(Collectors.toMap(FxProductSub::getCode, Function.identity()));
        // 排序防止死锁
        changeStocks.sort(Comparator.comparingLong(ChangeStock::getCode));
        for (ChangeStock changeStock : changeStocks) {
            FxProductSub sub = map.get(changeStock.getCode());
            Assert.notNull(sub, StrUtil.format("产品:{}不存在", changeStock.getCode()));
            int productSubId = sub.getId();
            for (int i = 0; i < changeStock.getDays(); i++) {
                DateTime time = DateUtil.offsetDay(changeStock.getStart(), i);
                boolean oversell = false;
                int ret = productPlanMapper.updateIncrementWhereExample(
                        new FxProductPlan()
                                .setSaleQuantity(changeStock.getSold())
                                .setStock(changeStock.getAvailable()),
                        new JoinExample.JustWhere()
                                .where(JoinExample.Where.custom(false)
                                        .andEqualTo(FxProductPlan::getProductSubId, productSubId)
                                        .andEqualTo(FxProductPlan::getStartTime, time)
                                        .andIncrement(FxProductPlan::getStock, changeStock.getAvailable(), ">=", 0)
                                        .andIncrement(FxProductPlan::getSaleQuantity, changeStock.getSold(), ">=", 0)
                                )
                                .buildExampleWhere()
                );
                // 修改失败则判断是否允许超卖
                if (ret <= 0) {
                    if (changeStock.getAvailable() >= 0 && changeStock.getSold() < 0) {
                        throw Result.fail(StrUtil.format("产品:{}在{}回退已售失败", sub.getName(), DateUtil.formatDateTime(time))).exception();
                    }
                    FxProductPlan plan = productPlanMapper.selectOne(new FxProductPlan()
                            .setStartTime(time.toJdkDate())
                            .setProductSubId(productSubId)
                    );
                    Assert.notNull(plan, () -> StrUtil.format("产品:{}在{}日期没有销售计划", sub.getName(), DateUtil.formatDateTime(time)));
                    if (ArrayUtil.contains(new int[]{
                            Constant.SalesRuleType.ALLOW_OVERSELL,
                            Constant.SalesRuleType.OVERSELL_NEED_AUDIT,
                            Constant.SalesRuleType.ANYWAY_NEED_AUDIT,
                            Constant.SalesRuleType.ANYWAY_NEED_AUDIT_OVERSELL
                    }, plan.getSaleRuleType())) {
                        oversell = true;
                    } else {
                        throw Result.fail(StrUtil.format("产品:{}在{}库存不足", sub.getName(), DateUtil.formatDateTime(time))).exception();
                    }
                }
                result.computeIfAbsent(changeStock.getCode(), k -> new HashMap<>(changeStock.getDays()))
                        .putIfAbsent(time.toJdkDate(), oversell);
            }
        }
        return result;
    }

    /**
     * 根据时间获取销售计划
     * @param productSubId 子产品ID
     * @param time 时间
     * @param days 天数
     * @return
     */
    public List<FxProductPlan> getByTime(int productSubId, Date time, int days) {
        Set<Date> dates = new HashSet<>(days);
        for (int i = 0; i < days; i++) {
            dates.add(DateUtil.offsetDay(time, i));
        }
        return productPlanMapper.selectByJoinExampleTransform(JoinExample.builder(FxProductPlan.class)
                        .addCol(FxProductPlan.class)
                        .addCol(FxRefundRule::getFree, "refundRuleFree")
                        .addCol(FxRefundRule::getRefund, "refundRuleRefund")
                        .addCol(FxRefundRule::getRule, "refundRuleDetail")
                        .addTable(new JoinExample.Table(FxProductSub.class, FxProductSub::getId, FxProductPlan::getProductSubId))
                        .addTable(new JoinExample.Table(FxRefundRule.class, JoinExample.JoinType.LEFT, FxRefundRule::getId, FxProductPlan::getRefundId))
                        .where(JoinExample.Where.custom()
                                .andEqualTo(FxProductPlan::getProductSubId, productSubId)
                                .andEqualTo(FxProductPlan::getStatus, true)
                                .andIn(FxProductPlan::getStartTime, dates)
                                .andEqualTo(FxProductSub::getStatus, true)
                        )
                        .build(),
                map -> new FxProductPlan()
        );
    }

    /**
     * 根据时间获取销售计划
     * @param productSubId 子产品ID
     * @param start 开始时间
     * @param end 结束时间
     * @return
     */
    public List<FxProductPlan> getByTime(int productSubId, Date start, Date end) {
        return productPlanMapper.selectByJoinExampleTransform(JoinExample.builder(FxProductPlan.class)
                        .addCol(FxProductPlan.class)
                        .addCol(FxRefundRule::getFree, "refundRuleFree")
                        .addCol(FxRefundRule::getRefund, "refundRuleRefund")
                        .addCol(FxRefundRule::getRule, "refundRuleDetail")
                        .addTable(new JoinExample.Table(FxProductSub.class, FxProductSub::getId, FxProductPlan::getProductSubId))
                        .addTable(new JoinExample.Table(FxRefundRule.class, JoinExample.JoinType.LEFT, FxRefundRule::getId, FxProductPlan::getRefundId))
                        .where(JoinExample.Where.custom()
                                .andEqualTo(FxProductPlan::getProductSubId, productSubId)
                                .andEqualTo(FxProductPlan::getStatus, true)
                                .andGreaterThanOrEqualTo(FxProductPlan::getStartTime, start)
                                .andLessThanOrEqualTo(FxProductPlan::getStartTime, end)
                                .andEqualTo(FxProductSub::getStatus, true)
                        )
                        .build(),
                map -> new FxProductPlan()
        );
    }

    /**
     * 销售计划转换为每日信息
     * @param plans 销售计划
     * @return
     */
    public Map<BetweenDate, ProductPlanInfo> planToEveryDayInfo(List<FxProductPlan> plans) {
        return plans.stream().collect(Collectors.toMap(FxProductPlan::getBetweenDate, this::planToInfo, (v1, v2) -> {throw new RuntimeException(String.format("Duplicate key for values %s and %s", v1, v2));}, TreeMap::new));
    }

    /**
     * 获取价格日历
     * @param productSubId 子产品ID
     * @param merchantId 商户ID
     * @param start 开始时间
     * @param end 结束时间
     * @return
     */
    public Result<Map<String, List<PriceCalendar>>> priceCalendar(int productSubId, int merchantId, Date start, Date end) {
        MerchantSalesInfo salesInfo;
        FxProductChain chain = productChainMapper.selectOne(new FxProductChain().setProductSubId(productSubId).setMerchantId(merchantId));
        if (chain == null) {
            FxProductSub sub = fxProductSubMapper.selectByPrimaryKey(productSubId);
            if (sub == null || sub.getMerchantId() != merchantId) {
                return Result.fail("该企业未被分销");
            }
            salesInfo = new MerchantSalesInfo().setMerchantId(merchantId).setSupplyMerchantId(merchantId).setRebate(0).setRebateType(Constant.ProfitType.FIXED);
        } else {
            salesInfo = productSalesManager.chainToSalesInfo(chain);
        }
        List<FxProductPlan> plans = getByTime(productSubId, start, end);
        Map<Date, FxProductPlan> planMap = plans.stream().collect(Collectors.toMap(FxProductPlan::getStartTime, Function.identity()));
        Map<BetweenDate, ProductPlanInfo> everyDayInfo = planToEveryDayInfo(plans);
        Map<Date, MerchantSalesInfoPrice> salesInfoPriceMap = OrderManager.transformEpSales(salesInfo, everyDayInfo);
        Map<String, List<PriceCalendar>> map = salesInfoPriceMap.entrySet().stream().map(entry -> {
            ProductPlanInfo planInfo = OrderManager.getPlanByTime(everyDayInfo, entry.getKey());
            FxProductPlan plan = planMap.get(entry.getKey());
            PriceCalendar bean = BeanUtil.toBean(planInfo, PriceCalendar.class);
            bean.setSettlePrice(plan.getSettlePrice());
            return bean
                    .setStart(plan.getStartTime())
                    .setEnd(plan.getEndTime())
                    .setSold(plan.getSaleQuantity())
                    .setStock(plan.getStock());
        }).collect(Collectors.groupingBy(PriceCalendar::getDay, TreeMap::new, Collectors.toList()));
        return Result.ok(map);
    }

    private ProductPlanInfo planToInfo(FxProductPlan plan) {
        return BeanUtil.toBean(plan, ProductPlanInfo.class).setRefundRule(plan.getRefundRule());
    }
}
