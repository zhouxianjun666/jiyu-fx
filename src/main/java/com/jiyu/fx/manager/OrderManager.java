package com.jiyu.fx.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.*;
import cn.hutool.extra.servlet.ServletUtil;
import com.alone.tk.mybatis.JoinExample;
import com.github.pagehelper.PageInfo;
import com.jiyu.base.manager.MapperManager;
import com.jiyu.base.manager.MerchantPaymentConfigManager;
import com.jiyu.base.manager.WxManager;
import com.jiyu.base.mapper.MemberMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.*;
import com.jiyu.common.entity.base.Member;
import com.jiyu.common.util.Util;
import com.jiyu.fx.dto.*;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.event.bo.AuditOrderEvent;
import com.jiyu.fx.event.bo.CancelOrderEvent;
import com.jiyu.fx.event.bo.PaidOrderEvent;
import com.jiyu.fx.event.bo.WaitSendEvent;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.vo.BookingVisitorVO;
import com.jiyu.fx.vo.VisitorQuantityVO;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/15 11:11
 */
@Component
@Slf4j
public class OrderManager {
    @Resource
    private FxOrderItemVisitorMapper visitorMapper;
    @Resource
    private FxOrderMapper orderMapper;
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private FxOrderItemDetailMapper orderItemDetailMapper;
    @Resource
    private MemberMapper memberMapper;
    @Resource
    private FxOrderItemVisitorDetailMapper visitorDetailMapper;
    @Resource
    private FxGroupMapper groupMapper;
    @Resource
    private FxRefundOrderMapper refundOrderMapper;
    @Resource
    private FxGroupMemberMapper groupMemberMapper;
    @Resource
    private FxGroupProductMapper groupProductMapper;
    @Resource
    private FxMerchantAccountMapper fxMerchantAccountMapper;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private WxManager wxManager;
    @Autowired
    private MerchantPaymentConfigManager epPaymentConfigManager;
    @Autowired
    private FxMerchantAccountManager epAccountManager;
    @Autowired
    private MapperManager mapperManager;

    /**
     * 获取身份信息该天的已订票数
     *
     * @param productSubNumber 子产品编号
     * @param day              天
     * @param cardNo           身份信息
     * @return
     */
    public Map<String, Integer> getCardNoQuantityByDay(long productSubNumber, Date day, String... cardNo) {
        return getCardNoQuantityByDay(productSubNumber, day, Arrays.stream(cardNo).collect(Collectors.toSet()));
    }

    /**
     * 获取身份信息该天的已订票数
     *
     * @param productSubNumber 子产品编号
     * @param day              天
     * @param cardNos          身份信息
     * @return
     */
    public Map<String, Integer> getCardNoQuantityByDay(long productSubNumber, Date day, Set<String> cardNos) {
        List<VisitorCardNoQuantity> visitors = visitorMapper.selectByJoinExampleTransform(JoinExample.builder(FxOrderItemVisitor.class)
                .addCol(FxOrderItemVisitor::getCardNo, VisitorCardNoQuantity::getCardNo)
                .addCol("sum(fx_order_item_visitor_detail.quantity - fx_order_item_visitor_detail.refund_quantity)", VisitorCardNoQuantity::getQuantity)
                .addTable(new JoinExample.Table(FxOrderItemVisitorDetail.class, FxOrderItemVisitorDetail::getVisitorId, FxOrderItemVisitor::getId))
                .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getId, FxOrderItemVisitor::getOrderItemId))
                .where(JoinExample.Where.custom()
                        .andBetween(FxOrderItemVisitorDetail::getDay, DateUtil.beginOfDay(day), DateUtil.endOfDay(day))
                        .andIn(FxOrderItemVisitor::getCardNo, cardNos)
                        .andEqualTo(FxOrderItem::getProductSubNumber, productSubNumber)
                        .andNotEqualTo(FxOrderItem::getStatus, Constant.OrderItemStatus.CANCEL)
                )
                .groupBy(FxOrderItemVisitor::getCardNo)
                .build(), map -> new VisitorCardNoQuantity()
        );
        return Optional.ofNullable(visitors)
                .map(vs -> vs.stream().collect(Collectors.toMap(VisitorCardNoQuantity::getCardNo, VisitorCardNoQuantity::getQuantity)))
                .orElse(Collections.emptyMap());
    }

    /**
     * 根据子订单获取订单联系人
     *
     * @param itemId 子订单ID
     * @return
     */
    public Member getShippingByItem(Integer itemId, Long itemNumber) {
        if (itemId == null && itemNumber == null) {
            return null;
        }
        return memberMapper.selectByJoinExampleEntityOne(JoinExample.builder(Member.class)
                .all(Member.class)
                .addTable(new JoinExample.Table(FxOrder.class, FxOrder::getShippingMemberId, Member::getId))
                .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getOrderId, FxOrder::getId))
                .where(JoinExample.Where.custom()
                        .andEqualTo(FxOrderItem::getId, itemId)
                        .orEqualTo(FxOrderItem::getNumber, itemNumber)
                )
                .build());
    }

    /**
     * 根据子订单获取订单
     *
     * @param itemId     子订单ID
     * @param itemNumber 子订单编号
     * @return
     */
    public FxOrder getOrderByItem(Integer itemId, Long itemNumber) {
        if (itemId == null && itemNumber == null) {
            return null;
        }
        return orderMapper.selectByJoinExampleEntityOne(JoinExample.builder(FxOrder.class)
                .all(FxOrder.class)
                .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getOrderId, FxOrder::getId))
                .where(JoinExample.Where.custom()
                        .andEqualTo(FxOrderItem::getId, itemId)
                        .orEqualTo(FxOrderItem::getNumber, itemNumber)
                )
                .build());
    }

    /**
     * 根据子订单获取订单
     *
     * @param itemId 子订单ID
     * @return
     */
    public FxOrder getOrderByItem(int itemId) {
        return getOrderByItem(itemId, null);
    }

    /**
     * 根据子订单获取订单
     *
     * @param itemNumber 子订单编号
     * @return
     */
    public FxOrder getOrderByItem(long itemNumber) {
        return getOrderByItem(null, itemNumber);
    }

    /**
     * 获取销售计划
     *
     * @param dayPlanInfoMap 销售计划集合
     * @param time           时间
     * @return
     */
    public static ProductPlanInfo getPlanByTime(Map<BetweenDate, ProductPlanInfo> dayPlanInfoMap, Date time) {
        return dayPlanInfoMap.entrySet().stream()
                .filter(entry -> entry.getKey().isIn(time))
                .findFirst()
                .map(Map.Entry::getValue).orElse(null);
    }

    /**
     * 获取游客总票数
     *
     * @param list 游客集合
     * @return
     */
    public static int getTotalQuantity(List<BookingVisitorVO> list) {
        if (CollUtil.isEmpty(list)) {
            return 0;
        }
        return list.stream().map(BookingVisitorVO::getQuantity).reduce(0, Integer::sum);
    }

    /**
     * 转换商户每日销售链价格信息
     *
     * @param salesInfo      销售链
     * @param dayPlanInfoMap 销售计划
     * @return
     */
    public static Map<Date, MerchantSalesInfoPrice> transformEpSales(MerchantSalesInfo salesInfo,
                                                                     Map<BetweenDate, ProductPlanInfo> dayPlanInfoMap) {
        Map<Date, MerchantSalesInfoPrice> map = new HashMap<>(dayPlanInfoMap.size());
        dayPlanInfoMap.forEach((betweenDate, plan) -> map.put(betweenDate.getBegin(), BeanUtil.toBean(salesInfo, MerchantSalesInfoPrice.class)
                .setTime(betweenDate.getBegin())
                .setRebateValue(salesInfo.getRebateValue(plan.getLowPrice()))));
        return map;
    }

    /**
     * 票数为空&&天为空 = 每天所有游客的剩余票数
     * 票数为空&&天不为空 = 该天的所有游客的剩余票数
     * 票数不为空&&天为空 = 第一天的指定票数（只能有一个游客&&只能有一天）
     * 票数不为空&&天不为空 = 该天的指定票数（只能有一个游客）
     *
     * @param orderItemId 子订单ID
     * @param quantity    票数
     * @param day         天
     * @return
     */
    public Map<Date, List<VisitorQuantityVO>> autoAssemblingVisitorQuantity(int orderItemId, Integer quantity, Date day) {
        List<FxOrderItemVisitor> visitors = visitorMapper.select(new FxOrderItemVisitor().setOrderItemId(orderItemId));
        List<FxOrderItemDetail> details = orderItemDetailMapper.select(new FxOrderItemDetail().setOrderItemId(orderItemId));
        if (CollUtil.isEmpty(visitors)) {
            throw Result.fail("订单游客信息异常").exception();
        }
        if (CollUtil.isEmpty(details)) {
            throw Result.fail("订单详情异常").exception();
        }
        if (quantity != null && visitors.size() > 1) {
            throw Result.fail("订单游客不止一个,请确认游客信息").exception();
        }
        if (quantity != null && day == null && details.size() > 1) {
            throw Result.fail("订单有多天,请确认每天信息").exception();
        }
        if (quantity == null) {
            if (day == null) {
                List<FxOrderItemVisitorDetail> visitorDetails = visitorDetailMapper.select(new FxOrderItemVisitorDetail().setOrderItemId(orderItemId));
                if (CollUtil.isEmpty(visitorDetails)) {
                    throw Result.fail("订单游客详情信息异常").exception();
                }
                return visitorDetails.stream().filter(d -> d.getRemainQuantity() > 0).collect(Collectors.groupingBy(FxOrderItemVisitorDetail::getDay,
                        Collectors.mapping(d -> new VisitorQuantityVO().setId(d.getVisitorId()).setQuantity(d.getRemainQuantity()), Collectors.toList())
                ));
            }
            List<FxOrderItemVisitorDetail> visitorDetails = visitorDetailMapper.select(new FxOrderItemVisitorDetail().setOrderItemId(orderItemId).setDay(day));
            if (CollUtil.isEmpty(visitorDetails)) {
                throw Result.fail("订单游客详情信息异常").exception();
            }
            return Collections.singletonMap(day, visitorDetails.stream().filter(d -> d.getRemainQuantity() > 0).map(d -> new VisitorQuantityVO().setId(d.getVisitorId()).setQuantity(d.getRemainQuantity())).collect(Collectors.toList()));
        }
        int visitorId = visitors.get(0).getId();
        if (day == null) {
            day = details.get(0).getDay();
        }
        FxOrderItemVisitorDetail visitorDetail = visitorDetailMapper.selectOne(new FxOrderItemVisitorDetail().setDay(day).setVisitorId(visitorId));
        Assert.notNull(visitorDetail, "游客不存在");
        if (visitorDetail.getRemainQuantity() < quantity) {
            throw Result.fail("余票不足").exception();
        }
        return Collections.singletonMap(day, Collections.singletonList(new VisitorQuantityVO().setId(visitorId).setQuantity(quantity)));
    }

    /**
     * 自动装配游客（只能有一个游客）
     *
     * @param orderItemId 子订单ID
     * @param map         信息
     */
    public void autoAssemblingVisitorQuantity(int orderItemId, Map<Date, List<VisitorQuantityVO>> map) {
        List<FxOrderItemVisitor> visitors = visitorMapper.select(new FxOrderItemVisitor().setOrderItemId(orderItemId));
        if (CollUtil.isEmpty(visitors)) {
            throw Result.fail("订单游客信息异常").exception();
        }
        if (visitors.size() > 1) {
            throw Result.fail("订单游客不止一个,请确认游客信息").exception();
        }
        int visitorId = visitors.get(0).getId();
        for (List<VisitorQuantityVO> list : map.values()) {
            for (VisitorQuantityVO vo : list) {
                vo.setId(visitorId);
            }
        }
    }

    /**
     * 查询订单支付状态
     *
     * @param order 订单
     * @return
     */
    public Result<?> queryOrderPaymentStatus(FxOrder order) {
        boolean isNonPay = ArrayUtil.contains(new int[]{
                Constant.OrderStatus.PAYING,
                Constant.OrderStatus.PENDING_PAY
        }, order.getStatus());
        int paymentType = Optional.ofNullable(order.getPaymentType()).orElse(Constant.PaymentType.BALANCE);
        if (isNonPay && paymentType == Constant.PaymentType.BALANCE) {
            return Result.ok().put("status", Constant.OrderStatus.PENDING_PAY);
        }
        if (paymentType == Constant.PaymentType.WX) {
            return queryOrderPaymentStatusByWx(order);
        }
        return Result.ok().put("status", 0);
    }

    /**
     * 处理订单支付状态
     *
     * @param order 订单
     * @return
     */
    public boolean processOrderPaymentStatus(FxOrder order) {
        if (order.getStatus() == Constant.OrderStatus.PAYING) {
            log.info("订单: {} 支付中,主动查询支付状态", order.getNumber());
            Result<?> queryResult = queryOrderPaymentStatus(order);
            int status = queryResult.get("status");
            if (status == 0) {
                throw Result.fail(StrUtil.format("查询订单:{}支付状态异常", order.getNumber())).exception();
            }
            if (status == Constant.OrderStatus.PAYING) {
                throw Result.fail(StrUtil.format("该订单:{}正在支付中", order.getNumber())).exception();
            }
            if (status == Constant.OrderStatus.PAID) {
                paid(order.getNumber(), queryResult.get("transaction_id"));
                return true;
            }
        }
        return false;
    }

    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @Locks("#{#number}")
    public void paid(long number, String transactionId) {
        FxOrder order = orderMapper.selectOne(new FxOrder().setNumber(number));
        if (order.getStatus() == Constant.OrderStatus.PAID) {
            throw Result.fail("订单已支付").exception();
        }
        if (order.getStatus() != Constant.OrderStatus.PAYING) {
            throw Result.fail(StrUtil.format("订单状态异常: {}", order.getStatus())).exception();
        }
        order.setStatus(Constant.OrderStatus.PAID);
        order.setPaymentNumber(transactionId);
        order.setPaymentTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);
        applicationContext.publishEvent(new PaidOrderEvent(order));
    }

    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class}, propagation = Propagation.REQUIRES_NEW)
    public void newTransactionalCancel(long number, int type, String reason) {
        cancel(number, type, reason);
    }

    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void cancel(long number, int type, String reason) {
        FxOrder order = orderMapper.selectOne(new FxOrder().setNumber(number));
        if (order == null) {
            throw Result.fail(StrUtil.format("订单:{}不存在", number)).exception();
        }
        if (ArrayUtil.contains(new int[]{
                Constant.OrderStatus.COMPLETE,
                Constant.OrderStatus.CANCEL
        }, order.getStatus())) {
            throw Result.fail(StrUtil.format("订单:{}已完成不能取消", number)).exception();
        }
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderNumber(order.getNumber()));
        boolean isNotCancelItem = items.stream().anyMatch(item -> ArrayUtil.contains(new int[]{
                Constant.OrderItemStatus.TICKETING,
                Constant.OrderItemStatus.SEND,
                Constant.OrderItemStatus.COMPLETE,
                Constant.OrderItemStatus.CANCEL
        }, item.getStatus()));
        if (isNotCancelItem) {
            throw Result.fail(StrUtil.format("订单:{}当前不能取消", number)).exception();
        }
        orderMapper.updateByPrimaryKeySelective(new FxOrder().setId(order.getId())
                .setCancelType(type)
                .setCancelReason(reason)
                .setStatus(Constant.OrderStatus.CANCEL)
        );
        orderItemMapper.updateByExampleSelective(new FxOrderItem().setStatus(Constant.OrderItemStatus.CANCEL),
                Example.builder(FxOrderItem.class)
                        .where(WeekendSqls.<FxOrderItem>custom().andEqualTo(FxOrderItem::getOrderNumber, number))
                        .build()
        );
        applicationContext.publishEvent(new CancelOrderEvent(order, items, type, reason));
    }

    /**
     * 根据订单编号获取子订单详情列表
     *
     * @param number 订单编号
     * @return
     */
    public List<FxOrderItemDetail> getDetailsByNumber(long number) {
        return orderItemDetailMapper.selectByJoinExampleTransform(JoinExample.builder(FxOrderItemDetail.class)
                        .addCol(FxOrderItemDetail.class)
                        .addCol(FxOrderItem::getProductSubNumber)
                        .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getId, FxOrderItemDetail::getOrderItemId))
                        .where(JoinExample.Where.custom()
                                .andEqualTo(FxOrderItem::getOrderNumber, number)
                        )
                        .build(),
                map -> new FxOrderItemDetail()
        );
    }

    /**
     * 订单支付
     *
     * @param order   订单
     * @param type    支付类型
     * @param request
     * @return
     */
    @Locks("#{#order.number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> payment(FxOrder order, int type, HttpServletRequest request, String openid) {
        if (order.getStatus() == Constant.OrderStatus.CANCEL) {
            return Result.fail("订单已取消");
        }
        if (order.getStatus() == Constant.OrderStatus.COMPLETE) {
            return Result.fail("订单已完成");
        }
        if (order.getStatus() == Constant.OrderStatus.PAID) {
            return Result.fail("订单已支付");
        }
        boolean isPaid = processOrderPaymentStatus(order);
        if (isPaid) {
            return Result.ok();
        }
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderId(order.getId()));
        Set<Integer> mIds = items.stream().map(FxOrderItem::getSupplierMerchantId).collect(Collectors.toSet());
        if (mIds.size() > 1) {
            throw Result.fail("当前订单不支持").exception();
        }
        order.setItems(items);
        orderMapper.updateByPrimaryKeySelective(new FxOrder()
                .setId(order.getId()).setPaymentType(type)
                .setStatus(Constant.OrderStatus.PAYING)
        );
        Integer supplierMerchantId = items.get(0).getSupplierMerchantId();
        FxMerchantAccount account = fxMerchantAccountMapper.selectOne(new FxMerchantAccount().setMerchantId(order.getBuyMerchantId()).setBossMerchantId(supplierMerchantId));
        String c = StrUtil.nullToEmpty(account.getPayConfig());
        String[] types = c.split(",");
        if (type == Constant.PaymentType.BALANCE) {
            if (!ArrayUtil.contains(types, String.valueOf(Constant.PaymentType.BALANCE))) {
                Result.fail("当前商户不能使用余额支付").ifFailThrow();
            }
            if (account.getBalance() < order.getPayAmount() && !ArrayUtil.contains(types, String.valueOf(Constant.PaymentType.CREDIT_PAY))) {
                Result.fail("当前商户余额不足,并不能使用授信支付").ifFailThrow();
            }
            epAccountManager.change(Collections.singletonList(new AccountChangeInfo()
                    .setNumber(String.valueOf(order.getNumber()))
                    .setOrderNumber(order.getNumber())
                    .setMerchantId(order.getBuyMerchantId())
                    .setBossMerchantId(supplierMerchantId)
                    .setType(Constant.AccountBillsType.ORDER_BUY)
                    .setBalance(-order.getPayAmount())
                    .setCash(-order.getPayAmount())
                    .setRemark("订单购买扣款")
            ));
            paid(order.getNumber(), null);
            return Result.ok();
        } else {
            if (type == Constant.PaymentType.WX) {
                if (StrUtil.isBlank(openid) && !ArrayUtil.contains(types, String.valueOf(Constant.PaymentType.WX))) {
                    Result.fail("当前商户不能使用微信支付").ifFailThrow();
                }
                return paymentOfWx(order, request, openid).ifFailThrow();
            } else {
                throw Result.fail("暂不支持").exception();
            }
        }
    }

    private Result<?> paymentOfWx(FxOrder order, HttpServletRequest request, String openid) {
        WxPaymentConfig wxPaymentConfig = epPaymentConfigManager.get(order.getItems().get(0).getSupplierMerchantId(), Constant.PaymentConfigType.WX);
        Assert.notNull(wxPaymentConfig, "微信收款方式未配置");
        Result<TreeMap<String, Object>> result;
        String ip = ServletUtil.getClientIP(request);
        WxCfg wxCfg = wxPaymentConfig.toCfg();
        if (Util.isWx(request)) {
            if (StrUtil.isBlank(openid)) {
                throw Result.fail("缺少openid").exception();
            }
            result = wxManager.pay(order, wxCfg, openid, ip);
        } else {
            result = wxManager.pay(order, wxCfg, ip);
        }
        if (!result.isSuccess()) {
            return result;
        }
        Map<String, Object> map = result.getValue();
        String nonceStr = RandomUtil.randomString(15);
        long timestamp = System.currentTimeMillis() / 1000;
        String prepayId = MapUtils.getString(map, "prepay_id");
        String sign = WxManager.sign((TreeMap<String, Object>) MapUtil.builder(new TreeMap<String, Object>())
                .put("appId", wxCfg.getAppId())
                .put("nonceStr", nonceStr)
                .put("package", "prepay_id=" + prepayId)
                .put("signType", "MD5")
                .put("timeStamp", timestamp)
                .build(), wxCfg.getMchKey());
        return Result.ok(new WxPayResult()
                .setAppId(wxCfg.getAppId())
                .setPrepayId(prepayId)
                .setCodeUrl(MapUtils.getString(map, "code_url"))
                .setNonceStr(nonceStr)
                .setTimestamp(timestamp)
                .setSign(sign)
        );
    }

    /**
     * 预定审核
     *
     * @param item    子订单
     * @param success 审核是否通过
     * @return
     */
    @Locks({"#{#item.number}", "#{#item.orderNumber}"})
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> audit(FxOrderItem item, boolean success) {
        orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem()
                .setId(item.getId())
                .setAuditSuccess(success)
                .setAuditTime(new Date())
                .setAuditUserId(Util.getUser().getId())
                .setAuditUserName(Util.getUser().getFullName())
                .setStatus(Constant.OrderItemStatus.AUDITED)
        );
        applicationContext.publishEvent(new AuditOrderEvent(item, success));
        return Result.ok();
    }

    /**
     * 检查订单审核状态并出票
     *
     * @param number 订单号
     */
    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void checkAuditAndSend(List<FxOrderItem> items, long number) {
        boolean isAllAudit = items.stream().allMatch(n ->
                n.getStatus() != Constant.OrderItemStatus.WAIT_AUDIT &&
                        Optional.ofNullable(n.getAuditSuccess()).orElse(true));
        if (isAllAudit) {
            // 设置所有子订单状态为未出票
            orderItemMapper.updateByExampleSelective(
                    new FxOrderItem().setStatus(Constant.OrderItemStatus.NON_SEND),
                    Example.builder(FxOrderItem.class)
                            .where(WeekendSqls.<FxOrderItem>custom()
                                    .andEqualTo(FxOrderItem::getOrderNumber, number)
                            )
                            .build()
            );
            FxOrder order = orderMapper.selectOne(new FxOrder().setNumber(number));
            if (order.getPaymentType() == Constant.PaymentType.LOCAL_PAY) {
                log.info("当前订单: {} 为现场支付，不出票", number);
                return;
            }
            // 设置自己模拟出票的子订单状态为出票中
            Set<Integer> collect = Stream.of(Constant.ProductType.HOTEL, Constant.ProductType.LINE).collect(Collectors.toSet());
            orderItemMapper.updateByExampleSelective(
                    new FxOrderItem().setStatus(Constant.OrderItemStatus.TICKETING),
                    Example.builder(FxOrderItem.class)
                            .where(WeekendSqls.<FxOrderItem>custom()
                                    .andEqualTo(FxOrderItem::getOrderNumber, number)
                                    .andIn(FxOrderItem::getProductType, collect)
                            )
                            .build()
            );
            // 调用出票接口
            items.stream().filter(n -> !collect.contains(n.getProductType())).forEach(item -> applicationContext.publishEvent(new WaitSendEvent(item)));
        }
    }

    /**
     * 检查订单审核状态并出票
     *
     * @param number 订单号
     */
    @Locks("#{#number}")
    public void checkAuditAndSend(long number) {
        checkAuditAndSend(orderItemMapper.select(new FxOrderItem().setOrderNumber(number)), number);
    }

    /**
     * 获取退款手续费百分比
     * 固定扣费：单张扣费 / 最终售卖价格 保留4位小数；百分比扣费：单张扣费 / 100 保留4位小数;
     *
     * @param refundRule 退货规则
     * @param info       退订信息
     * @param finalPrice 最终售卖价格
     * @return
     */
    public static double getRefundFeePercent(RefundRule<AbstractRefundRuleDetail> refundRule, int finalPrice, RefundRuleCheckInfo info) {
        if (refundRule.isFree()) {
            return 0;
        }
        Optional<AbstractRefundRuleDetail> optional = refundRule.getDetails().stream().filter(detail -> detail.isIn(info)).findFirst();
        if (!optional.isPresent()) {
            return 0;
        }
        AbstractRefundRuleDetail detail = optional.get();
        return detail.isFixed() ? NumberUtil.div(detail.getValue(), finalPrice, 4) : NumberUtil.div(detail.getValue(), 100, 4);
    }

    /**
     * 检查订单是否已完成
     *
     * @param itemId
     */
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void checkOrderComplete(int itemId) {
        FxOrderItem item = orderItemMapper.selectByPrimaryKey(itemId);
        if (item.isComplete()) {
            orderItemMapper.updateByPrimaryKeySelective(new FxOrderItem().setId(item.getId()).setStatus(Constant.OrderItemStatus.COMPLETE));
            List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderId(item.getOrderId()));
            boolean isAllComplete = items.stream().allMatch(FxOrderItem::isComplete);
            if (isAllComplete) {
                orderItemMapper.updateByExampleSelective(
                        new FxOrderItem().setStatus(Constant.OrderItemStatus.COMPLETE),
                        Example.builder(FxOrderItem.class)
                                .where(WeekendSqls.<FxOrderItem>custom().andEqualTo(FxOrderItem::getOrderId, item.getOrderId()))
                                .build()
                );
                orderMapper.updateByPrimaryKeySelective(new FxOrder().setId(item.getOrderId()).setStatus(Constant.OrderStatus.COMPLETE));
            }
        }
    }

    /**
     * 获取订单列表
     *
     * @param search   搜索条件
     * @param user     当前用户
     * @param pageNum  页码
     * @param pageSize 每页条数
     * @return
     */
    public Object list(SearchOrder search, AuthUser user, Integer pageNum, Integer pageSize) {
        return list(search, user, b -> b, pageNum, pageSize);
    }

    /**
     * 获取订单列表
     *
     * @param search   搜索条件
     * @param builder
     * @param pageNum  页码
     * @param pageSize 每页条数
     * @return
     */
    @SuppressWarnings("unchecked")
    public Object list(SearchOrder search, JoinExample.Builder builder, Integer pageNum, Integer pageSize) {
        Object res = Util.pageOrOffsetOrNone(() -> orderMapper.selectByJoinExample(builder.build()), pageNum, pageSize);
        List<FxOrder> orderList;
        if (res instanceof PageInfo) {
            orderList = orderMapper.transformList(((PageInfo) res).getList(), map -> new FxOrder());
            ((PageInfo) res).setList(orderList);
        } else {
            orderList = orderMapper.transformList((List) res, map -> new FxOrder());
            res = orderList;
        }
        Set<Integer> ids = orderList.stream().map(FxOrder::getId).collect(Collectors.toSet());
        if (CollUtil.isNotEmpty(ids)) {
            List<OrderItemInfo> items = orderItemMapper.selectByJoinExampleTransform(JoinExample.builder(FxOrderItem.class)
                            .addCol(FxOrderItem.class)
                            .addCol(FxRefundOrder.class, "refundOrder")
                            .addTable(new JoinExample.Table(FxRefundOrder.class, JoinExample.JoinType.LEFT, FxRefundOrder::getId, FxOrderItem::getLastRefundId))
                            .where(JoinExample.Where.custom()
                                    .andIn(FxOrderItem::getOrderId, ids)
                                    .andIn(FxOrderItem::getStatus, search.getItemStatus())
                                    .andIn(FxRefundOrder::getStatus, search.getRefundStatus())
                            )
                            .build(),
                    map -> new OrderItemInfo()
            );
            fillItem(search, items, param -> {
                if (param.getType().isAssignableFrom(FxOrderItemVisitor.class)) {
                    return param.getBuilder().andWhere(JoinExample.Where.custom()
                            .optional(StrUtil.isNotBlank(search.getCardNo()), where -> where
                                    .andEqualTo(FxOrderItemVisitor::getCardNo, search.getCardNo())
                            )
                    );
                }
                if (param.getType().isAssignableFrom(FxOrderItemTicket.class)) {
                    return param.getBuilder().andWhere(JoinExample.Where.custom()
                            .optional(StrUtil.isNotBlank(search.getVoucherNumber()), where -> where
                                    .andEqualTo(FxOrderItemTicket::getVoucherNumber, search.getVoucherNumber())
                            )
                    );
                }
                return param.getBuilder();
            });

            Map<Integer, List<OrderItemInfo>> map = items.stream().collect(Collectors.groupingBy(OrderItemInfo::getOrderId));
            orderList.forEach(order -> order.setItems(map.get(order.getId())));
        }
        return res;
    }

    /**
     * 获取订单列表
     *
     * @param search   搜索条件
     * @param user     当前用户
     * @param fn       重置查询
     * @param pageNum  页码
     * @param pageSize 每页条数
     * @return
     */

    public Object list(SearchOrder search, AuthUser user, Function<JoinExample.Builder, JoinExample.Builder> fn, Integer pageNum, Integer pageSize) {
        JoinExample.Builder builder = fn.apply(getListBuilder(search, user));
        return list(search, builder, pageNum, pageSize);
    }

    /**
     * 获取团队订单列表
     *
     * @param search   搜索条件
     * @param user     当前用户
     * @param pageNum  页码
     * @param pageSize 每页条数
     * @return
     */
    @SuppressWarnings("unchecked")
    public Object groupOrderList(SearchGroupOrder search, AuthUser user, Integer pageNum, Integer pageSize) {
        int merchantId = user.getMerchant().getId();
        JoinExample.Builder builder = JoinExample.builder(FxGroup.class)
                .all(FxGroup.class)
                .optional(search.isSupplier(), b -> b
                        .addTable(new JoinExample.Table(FxOrder.class, FxOrder::getGroupId, FxGroup::getId))
                        .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getOrderId, FxOrder::getId))
                )
                .where(JoinExample.Where.custom()
                        .bean(search, JoinExample.tableAlias(FxGroup.class))
                        .optional(!search.isSupplier(), where -> where.andEqualTo(FxGroup::getMerchantId, merchantId))
                        .optional(search.isSupplier(), where -> where
                                .andEqualTo(FxOrderItem::getSupplierMerchantId, merchantId)
                        )
                )
                .groupBy(FxGroup::getId)
                .desc(FxGroup::getStartDate);
        Object res = Util.pageOrOffsetOrNone(() -> groupMapper.selectByJoinExample(builder.build()), pageNum, pageSize);
        List<GroupOrder> groupOrders;
        if (res instanceof PageInfo) {
            groupOrders = groupMapper.transformList(((PageInfo) res).getList(), map -> new GroupOrder());
            ((PageInfo) res).setList(groupOrders);
        } else {
            groupOrders = groupMapper.transformList((List) res, map -> new GroupOrder());
            res = groupOrders;
        }
        Set<Integer> ids = groupOrders.stream().map(GroupOrder::getId).collect(Collectors.toSet());
        if (CollUtil.isNotEmpty(ids)) {
            List<Map<String, Object>> list = groupProductMapper.selectByJoinExample(JoinExample.builder(FxGroupProduct.class)
                    .all(FxGroupProduct.class)
                    .addCol(FxOrder.class, "order")
                    .addCol(FxOrderItem.class, "item")
                    .addCol(FxRefundOrder.class, "refundOrder")
                    .addGroupCol(FxOrderItemDetail::getRefundRule, "refundRule")
                    .addTable(new JoinExample.Table(FxProductSub.class, FxProductSub::getCode, FxGroupProduct::getSubCode))
                    .addTable(new JoinExample.Table(FxOrder.class, JoinExample.JoinType.LEFT, FxOrder::getId, FxGroupProduct::getOrderId))
                    .addTable(new JoinExample.Table(FxOrderItem.class, JoinExample.JoinType.LEFT, FxOrderItem::getOrderId, FxOrder::getId))
                    .addTable(new JoinExample.Table(FxOrderItemDetail.class, JoinExample.JoinType.LEFT, FxOrderItemDetail::getOrderItemId, FxOrderItem::getId))
                    .addTable(new JoinExample.Table(FxRefundOrder.class, JoinExample.JoinType.LEFT, FxRefundOrder::getId, FxOrderItem::getLastRefundId))
                    .where(JoinExample.Where.custom()
                            .andIn(FxGroupProduct::getGroupId, ids)
                            .optional(search.isSupplier(), where -> where
                                    .andEqualTo(FxProductSub::getMerchantId, merchantId)
                            )
                    )
                    .groupBy(FxGroupProduct::getId)
                    .build()
            );

            Map<Integer, List<Map<String, Object>>> map = list.stream().collect(Collectors.groupingBy(m -> MapUtils.getIntValue(m, "groupId")));
            groupOrders.forEach(group -> group.setOrders(map.get(group.getId())));
        }
        return res;
    }

    /**
     * 获取订单详情
     *
     * @param request 参数
     * @param user    当前用户
     * @param fn      build FN
     * @return
     */
    public FxOrder detail(OrderDetailRequest request, AuthUser user, Function<JoinExample.Builder, JoinExample.Builder> fn) {
        JoinExample.Builder builder = getDetailBuilder(request, user);
        if (fn != null) {
            builder = fn.apply(builder);
        }
        FxOrder order = orderMapper.selectByJoinExampleTransform(builder.build(), FxOrder.class);
        if (order != null) {
            List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderId(order.getId()));
            List<OrderItemInfo> list = items.stream().map(item -> BeanUtil.toBean(item, OrderItemInfo.class)).collect(Collectors.toList());
            fillItem(request, list, FillItemParam::getBuilder);
            order.setItems(list);
        }
        return order;
    }

    /**
     * 获取
     *
     * @param number 订单号
     * @param user   当前用户
     * @param fn     build FN
     * @return
     */
    public Map<String, Object> groupOrderDetail(long number, AuthUser user, Function<JoinExample.Builder, JoinExample.Builder> fn) {
        int merchantId = user.getMerchant().getId();
        JoinExample.Builder builder = JoinExample.builder(FxOrder.class)
                .all(FxOrder.class)
                .addCol(FxGroup.class, "group")
                .addCol(FxGroupProduct.class, "product")
                .addCol(FxOrderItem.class, "item")
                .addTable(new JoinExample.Table(FxGroup.class, FxGroup::getId, FxOrder::getGroupId))
                .addTable(new JoinExample.Table(FxGroupProduct.class, FxGroupProduct::getOrderId, FxOrder::getId))
                .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getOrderId, FxOrder::getId))
                .where(JoinExample.Where.custom().andEqualTo(FxOrder::getNumber, number))
                .andWhere(JoinExample.Where.custom().andEqualTo(FxOrder::getBuyMerchantId, merchantId).orEqualTo(FxOrderItem::getSupplierMerchantId, merchantId))
                .groupBy(FxOrder::getId);
        if (fn != null) {
            builder = fn.apply(builder);
        }
        Map<String, Object> order = orderMapper.selectByJoinExampleOne(builder.build());
        if (order != null) {
            int itemId = MapUtils.getIntValue(MapUtils.getMap(order, "item"), "id");
            int budgetId = MapUtils.getIntValue(MapUtils.getMap(order, "product"), "id");
            order.put("details", orderItemDetailMapper.select(new FxOrderItemDetail().setOrderItemId(itemId)));
            order.put("refundOrders", refundOrderMapper.select(new FxRefundOrder().setOrderItemId(itemId)));
            order.put("children", groupMemberMapper.selectCount(new FxGroupMember().setBudgetId(budgetId).setAdult(false)));
        }
        return order;
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    private void fillItem(OrderItemFill fill, List<OrderItemInfo> items, Function<FillItemParam, JoinExample.Builder> fn) {
        if (CollUtil.isNotEmpty(items)) {
            Set<Integer> itemIds = items.stream().map(FxOrderItem::getId).collect(Collectors.toSet());
            Set<Long> numbers = items.stream().map(FxOrderItem::getNumber).collect(Collectors.toSet());
            Map<Class<?>, List> listMap = mapperManager.selectEntityListOfBean(fill, vm -> fn.apply(new FillItemParam()
                            .setBuilder(JoinExample.builder(vm.value())
                                    .where(JoinExample.Where.custom()
                                            .optional(vm.value().isAssignableFrom(FxOrderItemDetailCredit.class), w -> w.andIn("item_number", numbers))
                                            .optional(!vm.value().isAssignableFrom(FxOrderItemDetailCredit.class), w -> w.andIn("order_item_id", itemIds))
                                    )
                            )
                            .setFill(fill)
                            .setIds(itemIds)
                            .setNumbers(numbers)
                            .setType(vm.value())
                    )
            );
            Map<Class<?>, Map<Integer, List>> lMap = new HashMap<>(listMap.size());
            for (Map.Entry<Class<?>, List> entry : listMap.entrySet()) {
                lMap.put(entry.getKey(), (Map<Integer, List>) entry.getValue().stream()
                        .collect(Collectors.groupingBy(item -> (Integer) BeanUtil.getFieldValue(item, "orderItemId")))
                );
            }
            for (OrderItemInfo item : items) {
                for (Field field : ReflectUtil.getFields(item.getClass())) {
                    if (field.getType().isAssignableFrom(List.class)) {
                        Class<?> type = (Class<?>) TypeUtil.getTypeArgument(field.getGenericType());
                        if (lMap.containsKey(type)) {
                            ReflectUtil.setFieldValue(item, field, lMap.get(type).get(item.getId()));
                        }
                    }
                }
            }
        }
    }

    private JoinExample.Builder getListBuilder(SearchOrder search, AuthUser user) {
        int merchantId = user.getMerchant().getId();
        return JoinExample.builder(FxOrder.class)
                .all(FxOrder.class)
                .optional(search.isShipping(), b -> b.addCol(Member.class, FxOrder::getShipping))
                .optional(search.isJoinMember(), b -> b
                        .addTable(new JoinExample.Table(Member.class, Member::getId, FxOrder::getShippingMemberId))
                )
                .optional(search.isJoinItem(), b -> b
                        .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getOrderId, FxOrder::getId))
                )
                .optional(search.isJoinRefund(), b -> b
                        .addTable(new JoinExample.Table(FxRefundOrder.class, FxRefundOrder::getOrderItemId, FxOrderItem::getId))
                )
                .optional(search.isJoinVisitor(), b -> b
                        .addTable(new JoinExample.Table(FxOrderItemVisitor.class, FxOrderItemVisitor::getOrderItemId, FxOrderItem::getId))
                )
                .optional(search.isJoinTicket(), b -> {
                    JoinExample.JoinType joinType = StrUtil.isNotBlank(search.getVoucherNumber()) ? JoinExample.JoinType.JOIN : JoinExample.JoinType.LEFT;
                    b.addTable(new JoinExample.Table(FxOrderItemTicket.class, joinType, FxOrderItemTicket::getOrderItemId, FxOrderItem::getId));
                })
                .where(JoinExample.Where.custom()
                        .bean(search, JoinExample.tableAlias(FxOrder.class))
                        .andIsNull(FxOrder::getGroupId)
                )
                .optional(search.isSelf(), b -> b.andWhere(JoinExample.Where.custom().andEqualTo(FxOrder::getBuyMerchantId, merchantId)))
                .optional(!search.isSelf(), b -> b.andWhere(JoinExample.Where.custom()
                        .andEqualTo(FxOrder::getBuyMerchantId, merchantId)
                        .orEqualTo(FxOrderItem::getSupplierMerchantId, merchantId))
                )
                .groupBy(FxOrder::getId)
                .desc(FxOrder::getCreateTime);
    }

    private Result<?> queryOrderPaymentStatusByWx(FxOrder order) {
        List<FxOrderItem> items = orderItemMapper.select(new FxOrderItem().setOrderNumber(order.getNumber()));
        WxPaymentConfig paymentConfig = epPaymentConfigManager.get(items.get(0).getSupplierMerchantId(), Constant.PaymentConfigType.WX);
        Assert.notNull(paymentConfig, "微信收款方式未配置");
        Result<TreeMap<String, Object>> result = wxManager.queryOrder(order, paymentConfig.toCfg());
        if (!result.isSuccess()) {
            return result;
        }
        TreeMap<String, Object> map = result.getValue();
        String tradeState = MapUtils.getString(map, "trade_state", "NOTPAY");
        switch (tradeState) {
            case "SUCCESS":
                return Result.ok(map).put("transaction_id", map.get("transaction_id")).put("status", Constant.OrderStatus.PAID);
            case "NOTPAY":
            case "CLOSED":
            case "REVOKED":
            case "PAYERROR":
            case "REFUND":
                return Result.ok().put("status", Constant.OrderStatus.PENDING_PAY);
            case "USERPAYING":
                return Result.ok().put("status", Constant.OrderStatus.PAYING);
            default:
                return Result.ok().put("status", 0);
        }
    }

    private JoinExample.Builder getDetailBuilder(OrderDetailRequest search, AuthUser user) {
        int merchantId = user.getMerchant().getId();
        return JoinExample.builder(FxOrder.class)
                .all(FxOrder.class)
                .optional(search.isShipping(), b -> b.addCol(Member.class, FxOrder::getShipping))
                .optional(search.isJoinMember(), b -> b
                        .addTable(new JoinExample.Table(Member.class, Member::getId, FxOrder::getShippingMemberId))
                )
                .addTable(new JoinExample.Table(FxOrderItem.class, FxOrderItem::getOrderId, FxOrder::getId))
                .where(JoinExample.Where.custom().bean(search, JoinExample.tableAlias(FxOrder.class)))
                .andWhere(JoinExample.Where.custom().andEqualTo(FxOrder::getBuyMerchantId, merchantId).orEqualTo(FxOrderItem::getSupplierMerchantId, merchantId))
                .groupBy(FxOrder::getId)
                .desc(FxOrder::getCreateTime);
    }
}
