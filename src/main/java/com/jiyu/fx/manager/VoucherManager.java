package com.jiyu.fx.manager;

import cn.hutool.core.util.StrUtil;
import com.jiyu.base.manager.ShortUrlManager;
import com.jiyu.common.Constant;
import com.jiyu.common.annotation.Locks;
import com.jiyu.common.dto.Config;
import com.jiyu.common.dto.Result;
import com.jiyu.common.util.Binary62Util;
import com.jiyu.fx.adapter.voucher.VoucherProcess;
import com.jiyu.fx.annotation.VoucherLog;
import com.jiyu.fx.dto.*;
import com.jiyu.fx.entity.FxMaConfig;
import com.jiyu.fx.entity.FxVoucherLog;
import com.jiyu.fx.mapper.FxMaConfigMapper;
import com.jiyu.fx.mapper.FxVoucherLogMapper;
import com.jiyu.fx.vo.VoucherConsumeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/2/22 16:19
 */
@Component
@Slf4j
public class VoucherManager {
    private Map<String, VoucherProcess> voucherProcessMap = Collections.emptyMap();
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private TicketManager ticketManager;
    @Autowired
    private ShortUrlManager shortUrlManager;
    @Autowired
    private Config config;
    @Resource
    private FxMaConfigMapper maConfigMapper;
    @Resource
    private FxVoucherLogMapper voucherLogMapper;

    @PostConstruct
    public void init() {
        voucherProcessMap = applicationContext.getBeansOfType(VoucherProcess.class).values().stream()
                .collect(Collectors.toMap(VoucherProcess::type, Function.identity()));
    }

    /**
     * 出票
     * @param maConfigId 凭证&票务配置ID
     * @param vo 出票信息
     * @return
     */
    @VoucherLog(refId = "#{#vo.number}", type = Constant.VoucherProcessLog.SEND)
    @Locks("#{#vo.number}")
    public Result<?> ticketing(int maConfigId, SendTicketInfo vo) {
        FxMaConfig maConfig = checkMa(maConfigId);
        VoucherProcess process = checkProcess(maConfig.getService());
        Result<SendTicketResult> result = process.ticketing(maConfig, vo);
        SendTicketResult value = result.getValue();
        if (!result.isSuccess() && value == null) {
            return result;
        }
        if (!value.isAsync()) {
            ticketingResponse(maConfigId, value, result.isSuccess());
        }
        return Result.ok();
    }

    @VoucherLog(refId = "#{#value.number}", type = Constant.VoucherProcessLog.SEND, request = false)
    @Locks("#{#value.number}")
    public Result<?> ticketingResponse(int maConfigId, SendTicketResult value, boolean success) {
        for (TicketSendInfo info : value.getVouchers()) {
            String url = StrUtil.format("{}?c={}&v={}", config.getVoucherHtmlUrl(), Binary62Util.encode(maConfigId), info.getVoucherNumber());
            info.setVoucherUrl(shortUrlManager.transform(url));
        }
        Result<?> result = ticketManager.sendResult(
                value.getNumber(),
                value.getVouchers(),
                success,
                Optional.ofNullable(value.getProcTime()).orElse(new Date())
        );
        if (!result.isSuccess()) {
            log.error("出票通知结果异常: {}-{}", result.getCode(), result.getMsg());
        }
        return result;
    }

    /**
     * 退票
     * @param maConfigId 凭证&票务配置ID
     * @param vo 退票信息
     * @return
     */
    @VoucherLog(refId = "#{#vo.serialNo}", type = Constant.VoucherProcessLog.REFUND)
    @Locks("#{#vo.number}")
    public Result<?> refund(int maConfigId, RefundTicketInfo vo) {
        FxMaConfig maConfig = checkMa(maConfigId);
        VoucherProcess process = checkProcess(maConfig.getService());
        Result<RefundTicketResult> result = process.refund(maConfig, vo);
        RefundTicketResult value = result.getValue();
        if (!result.isSuccess() && value == null) {
            return result;
        }
        if (!value.isAsync()) {
            refundResponse(maConfigId, value, result.isSuccess());
        }
        return Result.ok();
    }

    @VoucherLog(refId = "#{#value.number}", type = Constant.VoucherProcessLog.REFUND, request = false)
    @Locks("#{#value.number}")
    public Result<?> refundResponse(int maConfigId, RefundTicketResult value, boolean success) {
        Result<?> result = ticketManager.refundResult(
                value.getNumber(),
                value.getSerialNo(),
                success,
                Optional.ofNullable(value.getProcTime()).orElse(new Date())
        );
        if (!result.isSuccess()) {
            log.error("退票通知结果异常: {}-{}", result.getCode(), result.getMsg());
        }
        return result;
    }

    /**
     * 拉取产品列表
     * @param maConfigId 凭证&票务配置ID
     * @param pageNum 页码
     * @param pageSize 当前页条数
     * @return
     */
    public Result<?> pullProduct(int maConfigId, Integer pageNum, Integer pageSize) {
        FxMaConfig maConfig = checkMa(maConfigId);
        VoucherProcess process = checkProcess(maConfig.getService());
        return process.pullProduct(maConfig, pageNum, pageSize);
    }

    /**
     * 核销
     * @param maConfigId 凭证&票务配置ID
     * @param vo 核销信息
     * @return
     */
    @VoucherLog(refId = "#{#vo.serialNo}", type = Constant.VoucherProcessLog.CONSUME, request = false)
    @Locks("#{#vo.serialNo}")
    public Result<?> consumeNotify(int maConfigId, VoucherConsumeVO vo) {
        return ticketManager.consumeNotify(maConfigId, vo);
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void saveLog(FxVoucherLog log) {
        voucherLogMapper.insertSelective(log);
    }

    private FxMaConfig checkMa(int id) {
        FxMaConfig maConfig = maConfigMapper.selectByPrimaryKey(id);
        Assert.notNull(maConfig, "凭证&票务配置不存在");
        return maConfig;
    }

    private VoucherProcess checkProcess(String type) {
        VoucherProcess process = voucherProcessMap.get(type);
        Assert.notNull(process, "当前票务系统暂未开放");
        return process;
    }
}
