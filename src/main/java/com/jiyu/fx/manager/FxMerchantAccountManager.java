package com.jiyu.fx.manager;

import cn.hutool.core.lang.Snowflake;
import com.alone.tk.mybatis.JoinExample;
import com.jiyu.base.dto.SearchBills;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AccountChangeInfo;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.dto.MerchantAccoutnBillsDto;
import com.jiyu.fx.entity.FxMerchantAccount;
import com.jiyu.fx.entity.FxMerchantAccountBills;
import com.jiyu.fx.mapper.FxMerchantAccountBillsMapper;
import com.jiyu.fx.mapper.FxMerchantAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/9 17:01
 */
@Component
public class FxMerchantAccountManager {
    @Autowired
    private Snowflake snowflake;
    @Autowired
    private UpdateVersionRetryManager updateVersionRetryManager;
    @Resource
    private FxMerchantAccountMapper fxMerchantAccountMapper;
    @Resource
    private FxMerchantAccountBillsMapper fxMerchantAccountBillsMapper;

    /**
     * 余额调整
     *
     * @param infos 需要调整的余额账户信息
     */
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void change(List<AccountChangeInfo> infos) {
        // 排序防止死锁
        infos.sort(Comparator.comparingLong(AccountChangeInfo::getMerchantId));
        List<FxMerchantAccountBills> bills = infos.stream().map(updateVersionRetryManager::merchantAccountChange).collect(Collectors.toList());
        fxMerchantAccountBillsMapper.insert(bills);
    }

    /**
     * 余额充值
     *
     * @param boss       老板
     * @param merchantId 商户
     * @param money      金额
     * @param remark     备注
     */
    public void recharge(int boss, int merchantId, int money, String remark) {
        money = Math.abs(money);
        getOrCreate(boss, merchantId);
        this.change(Collections.singletonList(new AccountChangeInfo()
                .setBossMerchantId(boss)
                .setMerchantId(merchantId)
                .setNumber(String.valueOf(snowflake.nextId()))
                .setRechargeType(0)
                .setRemark(remark)
                .setType(Constant.AccountBillsType.RECHARGE)
                .setBalance(money)
                .setCash(money)
        ));
    }

    /**
     * 余额扣除
     *
     * @param merchantId 商户
     * @param money      金额
     * @param remark     备注
     */
    public void deduct(int boss, int merchantId, int money, String remark) {
        money = -Math.abs(money);
        this.change(Collections.singletonList(new AccountChangeInfo()
                .setBossMerchantId(boss)
                .setMerchantId(merchantId)
                .setNumber(String.valueOf(snowflake.nextId()))
                .setRechargeType(0)
                .setRemark(remark)
                .setType(Constant.AccountBillsType.DEDUCT)
                .setBalance(money)
                .setCash(money)
        ));
    }

    /**
     * 调额
     *
     * @param merchantId 商户
     * @param balance    余额
     * @param cash       可提现
     * @param remark     备注
     */
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void adjustment(int boss, int merchantId, Long balance, Long cash, String remark) {
        if (balance == null && cash == null) {
            throw Result.fail("请输入调整值").exception();
        }
        boolean balanceCheck = balance != null && balance < 0;
        boolean cashCheck = cash != null && cash < 0;
        if (balanceCheck || cashCheck) {
            throw Result.fail("调整值不能小于0").exception();
        }
        FxMerchantAccountBills bills = updateVersionRetryManager.adjustment(boss, merchantId, balance, cash, remark);
        fxMerchantAccountBillsMapper.insertSelective(bills);
    }

    /**
     * 授信
     *
     * @param merchantId 商户
     * @param quota      额度
     */
    public void creditGranting(int boss, int merchantId, int quota) {
        FxMerchantAccount account = getOrCreate(boss, merchantId);
        fxMerchantAccountMapper.updateByVersion(new FxMerchantAccount().setId(account.getId()).setCredit(quota).setVersion(account.getVersion()));
    }

    /**
     * 获取交易流水
     *
     * @return
     */
    public List<MerchantAccoutnBillsDto> bills(SearchBills search, Function<JoinExample.Builder, JoinExample.Builder> fn) {
        JoinExample.Builder builder = JoinExample.builder(FxMerchantAccountBills.class)
                .addCol(FxMerchantAccountBills.class)
                .addCol(Merchant::getId, MerchantAccoutnBillsDto::getMerchantId)
                .addCol(Merchant::getName, MerchantAccoutnBillsDto::getMerchantName)
                .addTable(new JoinExample.Table(FxMerchantAccount.class, FxMerchantAccount::getId, FxMerchantAccountBills::getAccountId))
                .addTable(new JoinExample.Table(Merchant.class, Merchant::getId, FxMerchantAccount::getMerchantId))
                .where(JoinExample.Where.custom().bean(search, JoinExample.tableAlias(FxMerchantAccountBills.class)))
                .desc(FxMerchantAccountBills::getId);
        if (fn != null) {
            builder = fn.apply(builder);
        }
        return fxMerchantAccountBillsMapper.selectByJoinExampleTransform(builder.build(), map -> new MerchantAccoutnBillsDto());
    }

    /**
     * 获取交易流水
     *
     * @return
     */
    public List<MerchantAccoutnBillsDto> bills(SearchBills search) {
        return bills(search, null);
    }

    public FxMerchantAccount getOrCreate(int boss, int merchant) {
        FxMerchantAccount record = new FxMerchantAccount().setMerchantId(merchant).setBossMerchantId(boss);
        FxMerchantAccount account = fxMerchantAccountMapper.selectOne(record);
        return Optional.ofNullable(account).orElseGet(() -> {
            fxMerchantAccountMapper.insertSelective(record.setCreateTime(new Date()));
            return record;
        });
    }
}
