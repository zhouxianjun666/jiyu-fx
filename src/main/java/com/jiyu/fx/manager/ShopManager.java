package com.jiyu.fx.manager;

import com.jiyu.common.dto.AbstractUser;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.Result;
import com.jiyu.common.dto.WxUser;
import com.jiyu.fx.entity.shop.Shop;
import com.jiyu.fx.mapper.ShopMapper;
import org.apache.shiro.SecurityUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/3/28 11:44
 */
@Component
public class ShopManager {
    @Resource
    private ShopMapper shopMapper;

    /**
     * 获取当前店铺
     * @return
     */
    @NonNull
    public Shop currentShop() {
        return currentShop(false);
    }

    /**
     * 获取当前店铺
     * @param nullable 是否可为空
     * @return
     */
    public Shop currentShop(boolean nullable) {
        Object principal = SecurityUtils.getSubject().getPrincipal();
        if (!(principal instanceof AbstractUser)) {
            throw Result.fail("当前登录用户信息异常").exception();
        }
        Shop shop = ((AbstractUser) principal).get("shop", Shop.class);
        if (shop == null) {
            if (principal instanceof WxUser) {
                throw Result.fail("请重新登录").exception();
            } else if (principal instanceof AuthUser) {
                shop = shopMapper.selectOne(new Shop().setMerchantId(((AuthUser) principal).getMerchant().getId()));
            }
            if (!nullable) {
                Assert.notNull(shop, "当前商户未开通店铺");
            }
            ((AbstractUser) principal).put("shop", shop);
        }
        return shop;
    }
}
