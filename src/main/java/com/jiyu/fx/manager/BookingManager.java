package com.jiyu.fx.manager;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.StrUtil;
import com.jiyu.base.mapper.MemberMapper;
import com.jiyu.common.Constant;
import com.jiyu.common.dto.AuthUser;
import com.jiyu.common.dto.MerchantSalesInfoPrice;
import com.jiyu.common.dto.ProductSalesInfo;
import com.jiyu.common.dto.Result;
import com.jiyu.common.entity.base.Member;
import com.jiyu.fx.adapter.order.BookingOrder;
import com.jiyu.fx.dto.ItemPriceDTO;
import com.jiyu.fx.entity.*;
import com.jiyu.fx.event.bo.BookingOrderEvent;
import com.jiyu.fx.mapper.*;
import com.jiyu.fx.vo.BookingItemVO;
import com.jiyu.fx.vo.BookingVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/15 17:43
 */
@Component
@Slf4j
public class BookingManager {
    private Map<String, BookingOrder> bookingOrderMap = Collections.emptyMap();
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private Snowflake snowflake;
    @Resource
    private FxOrderMapper orderMapper;
    @Resource
    private FxOrderItemMapper orderItemMapper;
    @Resource
    private FxOrderItemDetailMapper orderItemDetailMapper;
    @Resource
    private FxOrderItemVisitorMapper visitorMapper;
    @Resource
    private FxOrderItemVisitorDetailMapper visitorDetailMapper;
    @Resource
    private MemberMapper memberMapper;

    @PostConstruct
    public void init() {
        bookingOrderMap = applicationContext.getBeansOfType(BookingOrder.class).values().stream()
                .collect(Collectors.toMap(BookingOrder::type, Function.identity()));
    }

    /**
     * 创建订单
     *
     * @param vo   预定信息
     * @param type 类型
     * @param user 当前用户
     * @return
     */
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> create(BookingVO vo, String type, AuthUser user) {
        if (!bookingOrderMap.containsKey(type)) {
            return Result.fail("暂不支持该订单类型预定");
        }
        BookingOrder bookingOrder = bookingOrderMap.get(type);
        bookingOrder.validate(vo, user);
        Result<Boolean> existResult = bookingOrder.isExist(vo, user);
        if (existResult.getValue()) {
            return existResult;
        }
        boolean checkQuantity = vo.getItems().stream().anyMatch(item -> item.getQuantity() <= 0);
        if (checkQuantity) {
            return Result.fail("游客票数不能小于1");
        }
        Result<FxOrder> orderResult = bookingOrder.makeOrder(vo, user);
        FxOrder order = orderResult.getValue();
        Result<Map<Long, ProductSalesInfo>> result = bookingOrder.validateAvailableSale(vo, order, user);
        Map<Long, ProductSalesInfo> salesInfoMap = result.getValue();
        Result<?> beforeResult = result;
        List<FxOrderItem> items = new ArrayList<>(vo.getItems().size());
        Map<Long, List<FxOrderItemDetail>> detailsMap = new HashMap<>(vo.getItems().size());
        Map<Long, List<FxOrderItemVisitor>> visitorsMap = new HashMap<>(vo.getItems().size());
        for (BookingItemVO item : vo.getItems()) {
            long itemNumber = snowflake.nextId();
            // 检查售卖信息
            ProductSalesInfo salesInfo = salesInfoMap.get(item.getProductSubNumber());
            Assert.notNull(salesInfo, StrUtil.format("产品:{},游玩日期:{}暂不可售", item.getProductSubNumber(), item.getBooking()));
            // 验证预定游玩日期
            beforeResult = bookingOrder.validateBookingDate(salesInfo, item, vo, user, beforeResult);
            if (salesInfo.getMaxBuyQuantity() > 0 && salesInfo.getMaxBuyQuantity() < item.getQuantity()) {
                throw Result.fail(StrUtil.format("超出该产品最大订购票数: {}", salesInfo.getMaxBuyQuantity())).exception();
            }
            // 验证游客信息
            beforeResult = bookingOrder.validateVisitor(salesInfo, item, vo, user, beforeResult);
            // 获取子产品总票数(不X天)
            int quantity = item.getQuantity();
            // 转换每日销售链价格信息
            Map<Date, MerchantSalesInfoPrice> salesInfoPriceMap = OrderManager.transformEpSales(salesInfo.getSalesInfos(), salesInfo.getDayPlanMap());
            // 计算抵扣信息
            Result<Map<Date, Long>> discountResult = bookingOrder.calcItemDiscount(salesInfo, item, quantity, salesInfoPriceMap, itemNumber, vo, user, beforeResult);
            beforeResult = discountResult;
            Map<Date, Long> discountMap = Optional.ofNullable(discountResult.getValue()).orElse(Collections.emptyMap());
            // 计算子产品价格
            Result<ItemPriceDTO> priceResult = bookingOrder.calcItemPrice(salesInfo, item, quantity, salesInfoPriceMap, discountMap, vo, user, beforeResult);
            beforeResult = priceResult;
            ItemPriceDTO price = priceResult.getValue();
            // 创建子订单
            Result<FxOrderItem> orderItemResult = bookingOrder.makeOrderItem(order, price, itemNumber, quantity, salesInfo, item, vo, user, beforeResult);
            beforeResult = orderItemResult;
            FxOrderItem orderItem = orderItemResult.getValue();
            items.add(orderItem);
            // 创建子订单详情
            Result<List<FxOrderItemDetail>> detailResult = bookingOrder.makeOrderItemDetail(order, orderItem, salesInfo, salesInfoPriceMap, item, vo, user, beforeResult);
            beforeResult = detailResult;
            detailsMap.put(orderItem.getNumber(), detailResult.getValue());
            // 创建游客信息
            Result<List<FxOrderItemVisitor>> visitorResult = bookingOrder.makeOrderItemVisitor(order, orderItem, item, vo, user, beforeResult);
            beforeResult = visitorResult;
            visitorsMap.put(orderItem.getNumber(), visitorResult.getValue());
        }
        // 保存会员&联系人信息
        Result<Member> memberResult = bookingOrder.saveOrderShipping(order, vo, user, beforeResult);
        Member member = memberResult.getValue();
        Result<Boolean> rollbackResult = bookingOrder.isRollback(order, vo, user, beforeResult);
        // 计算价格
        int lowPrice = items.stream().mapToInt(FxOrderItem::getLowPrice).sum();
        int settlePrice = items.stream().mapToInt(FxOrderItem::getSettlePrice).sum();
        long discountMoney = items.stream().mapToLong(FxOrderItem::getDiscountMoney).sum();
        if (settlePrice < 0 || lowPrice < 0) {
            throw Result.fail("下单失败").exception();
        }
        ItemPriceDTO priceDTO = new ItemPriceDTO().setLowPrice(lowPrice).setSettlePrice(settlePrice).setDiscountMoney(discountMoney);
        if (!rollbackResult.getValue()) {
            save(priceDTO, salesInfoMap, order, items, detailsMap, visitorsMap, member);
        }
        Result<?> res = bookingOrder.after(order, items, salesInfoMap, member, vo, user, rollbackResult);
        if (!rollbackResult.getValue()) {
            applicationContext.publishEvent(new BookingOrderEvent()
                    .setVo(vo)
                    .setType(type)
                    .setCurrentUser(user)
                    .setItems(items)
                    .setOrder(order)
            );
        }
        return res;
    }

    private void save(ItemPriceDTO price, Map<Long, ProductSalesInfo> salesInfoMap, FxOrder order,
                      List<FxOrderItem> items,
                      Map<Long, List<FxOrderItemDetail>> detailsMap,
                      Map<Long, List<FxOrderItemVisitor>> visitorsMap, Member member) {
        boolean isRetail = order.getPaymentChannel() == Constant.PaymentChannel.RETAIL;
        int amount = isRetail ? price.getLowPrice() : price.getSettlePrice();
        // 入库
        orderMapper.insertSelective(order
                .setShippingMemberId(member.getId())
                .setAmount(amount)
                .setPayAmount((int) Math.max(amount - price.getDiscountMoney(), 0))
        );
        items.forEach(item -> saveItem(order, item,
                detailsMap.get(item.getNumber()),
                visitorsMap.get(item.getNumber()),
                salesInfoMap.get(item.getProductSubNumber())
        ));
        visitorsMap.values().stream().flatMap(list -> list.stream()
                .filter(v -> (v.getCardType() != null && StrUtil.isNotBlank(v.getCardNo())) || StrUtil.isNotBlank(v.getPhone())))
                .forEach(v -> {
                    try {
                        memberMapper.insertOrUpdate(new Member().setPhone(v.getPhone()).setIdCard(v.getCardNo()).setNickname(v.getName()).setGender(v.getGender()));
                    } catch (Exception e) {
                        log.warn("保存会员失败", e);
                    }
                });
    }

    private void saveItem(FxOrder order, FxOrderItem orderItem,
                          List<FxOrderItemDetail> details, List<FxOrderItemVisitor> visitors,
                          ProductSalesInfo salesInfo) {
        // 获取最小生效时间与最大失效时间
        Date minEffectiveDate = details.stream().map(FxOrderItemDetail::getEffectiveDate).min(Date::compareTo).orElse(null);
        Assert.notNull(minEffectiveDate, StrUtil.format("产品:{},生效日期错误", salesInfo.getProductSubName()));
        Date maxExpiryDate = details.stream().map(FxOrderItemDetail::getExpiryDate).max(Date::compareTo).orElse(null);
        Assert.notNull(maxExpiryDate, StrUtil.format("产品:{},失效日期错误", salesInfo.getProductSubName()));
        orderItemMapper.insertSelective(orderItem
                .setMinEffectiveDate(minEffectiveDate)
                .setMaxExpiryDate(maxExpiryDate)
                .setOrderId(order.getId())
        );
        details.forEach(detail -> orderItemDetailMapper.insertSelective(detail.setOrderItemId(orderItem.getId()).setUsedQuantity(0).setRefundQuantity(0)));
        visitors.forEach(v -> visitorMapper.insertSelective(v.setOrderItemId(orderItem.getId())));
        List<FxOrderItemVisitorDetail> visitorDetails = visitors.stream()
                .flatMap(v -> details.stream().map(detail -> new FxOrderItemVisitorDetail()
                        .setOrderItemId(orderItem.getId())
                        .setDetailId(detail.getId())
                        .setVisitorId(v.getId())
                        .setDay(detail.getDay())
                        .setQuantity(v.getTempQuantity())
                        .setUsedQuantity(0)
                        .setRefundQuantity(0))
                )
                .collect(Collectors.toList());
        visitorDetailMapper.insert(visitorDetails);
    }
}
