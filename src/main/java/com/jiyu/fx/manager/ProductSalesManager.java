package com.jiyu.fx.manager;

import com.alone.tk.mybatis.JoinExample;
import com.jiyu.common.dto.MerchantSalesInfo;
import com.jiyu.common.dto.ProductSalesInfo;
import com.jiyu.common.entity.base.Merchant;
import com.jiyu.fx.entity.FxProduct;
import com.jiyu.fx.entity.FxProductChain;
import com.jiyu.fx.entity.FxProductSub;
import com.jiyu.fx.mapper.FxProductChainMapper;
import com.jiyu.fx.mapper.FxProductSubMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2019/1/17 8:37
 */
@Component
public class ProductSalesManager {
    @Resource
    private FxProductSubMapper productSubMapper;
    @Resource
    private FxProductChainMapper productChainMapper;

    /**
     * 获取子产品基本售卖信息
     *
     * @param codes 子产品编号集合
     * @return
     */
    public List<ProductSalesInfo> getBasicProductSalesInfos(Set<Long> codes) {
        return productSubMapper.selectByJoinExampleTransform(
                JoinExample.builder(FxProductSub.class)
                        .addCol(FxProductSub::getId, ProductSalesInfo::getProductSubId)
                        .addCol(FxProductSub::getCode, ProductSalesInfo::getProductSubNumber)
                        .addCol(FxProductSub::getName, ProductSalesInfo::getProductSubName)
                        .addCol(FxProduct::getCode, ProductSalesInfo::getProductNumber)
                        .addCol(FxProduct::getName, ProductSalesInfo::getProductName)
                        .addCol(FxProduct::getType, ProductSalesInfo::getProductType)
                        .addCol(FxProductSub::getTicketFlag, ProductSalesInfo::getTicketFlag)
                        .addCol(FxProductSub::getMaId, ProductSalesInfo::getMaProductId)
                        .addCol(FxProductSub::getVoucherId, ProductSalesInfo::getVoucherId)
                        .addCol(FxProductSub::getMerchantId, ProductSalesInfo::getSupplierMerchantId)
                        .addCol(Merchant::getName, ProductSalesInfo::getSupplierMerchantName)
                        .addCol(FxProduct::getPhone, ProductSalesInfo::getSupplierPhone)
                        .addCol(FxProductSub::getRequireSid, ProductSalesInfo::isRequireSid)
                        .addCol(FxProductSub::getSidDayQuantity, ProductSalesInfo::getSidDayQuantity)
                        .addCol(FxProductSub::getMinBuyQuantity, ProductSalesInfo::getMinBuyQuantity)
                        .addCol(FxProductSub::getMaxBuyQuantity, ProductSalesInfo::getMaxBuyQuantity)
                        .addCol(FxProductSub::getLowUseQuantity, ProductSalesInfo::getLowUseQuantity)
                        .addCol(FxProductSub::getRealName, ProductSalesInfo::getRealName)
                        .addCol(FxProductSub::getVoucherTemplate, ProductSalesInfo::getSmsTemplateId)
                        .addCol(FxProductSub::getTicketMsg, ProductSalesInfo::getTicketMsg)
                        .addCol(FxProductSub::getRefundAudit, ProductSalesInfo::isRefundAudit)
                        .addCol(FxProductSub::getPayType, ProductSalesInfo::getPayType)
                        .addTable(new JoinExample.Table(FxProduct.class, FxProduct::getId, FxProductSub::getProductId))
                        .addTable(new JoinExample.Table(Merchant.class, Merchant::getId, FxProductSub::getMerchantId))
                        .where(JoinExample.Where.custom()
                                .andIn(FxProductSub::getCode, codes)
                                .andEqualTo(FxProductSub::getStatus, true)
                        )
                        .build(),
                map -> new ProductSalesInfo()
        );
    }

    /**
     * 销售链转换企业销售信息
     * @param chain 销售链
     * @return
     */
    public MerchantSalesInfo chainToSalesInfo(FxProductChain chain) {
        return new MerchantSalesInfo()
                .setSupplyMerchantId(chain.getSaleMerchantId())
                .setMerchantId(chain.getMerchantId())
                .setRebateType(chain.getProfitType())
                .setRebate(chain.getProfit());
    }

    /**
     * 判断商户是否可以售卖子产品集合
     * @param codes 子产品编号集合
     * @param merchant 商户ID
     * @return
     */
    public boolean isSale(Set<Long> codes, int merchant) {
        int count = productChainMapper.selectByJoinExampleTransform(JoinExample.builder(FxProductChain.class)
                .count()
                .addTable(new JoinExample.Table(FxProductSub.class, FxProductSub::getId, FxProductChain::getProductSubId))
                .where(JoinExample.Where.custom()
                        .andIn(FxProductSub::getCode, codes)
                        .andEqualTo(FxProductChain::getMerchantId, merchant)
                )
                .build(),
                int.class
        );
        return count == codes.size();
    }
}
